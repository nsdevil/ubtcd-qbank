package com.nsdevil.keris.util;

import com.nsdevil.keris.common.bean.ResultMap;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Service
public class ServiceHandler {
    public HashMap<String, Object> sendGet(HttpServletRequest request, String url) throws Exception {
        HashMap<String, Object> resultMap = null;
        HttpGet httpGet = new HttpGet(url);
        String token = request.getSession().getAttribute("S_TOKEN").toString();

        httpGet.setHeader("Authorization", "Bearer " + token);
        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("Content-type", "application/json");

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpGet)) {

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent(), "UTF-8"), 8);
            String inputLine;
            StringBuffer res = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                res.append(inputLine);
            }
            in.close();
            //print result
            System.out.println(res.toString());
            resultMap = new ResultMap();
            resultMap.put("result", res.toString());
        }
        return resultMap;
    }

    // HTTP POST request
    public HashMap<String, Object> sendPost(Map<String, String> parameters, String url, Locale locale) throws Exception {
        HashMap<String, Object> resultMap = null;
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Authorization", "Bearer " + "");
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("Accept-Language", locale.getLanguage());

        JSONObject json = new JSONObject();
        json.put("loginId", parameters.get("loginId"));
        json.put("password", parameters.get("password"));
        httpPost.setEntity(new StringEntity(json.toString()));
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpPost)) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent(), "UTF-8"), 8);
            String inputLine;
            StringBuffer res = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                res.append(inputLine);
            }
            in.close();
            resultMap = new ResultMap();
            resultMap.put("result", res.toString());
        }
        return resultMap;
    }
}

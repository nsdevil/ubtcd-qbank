package com.nsdevil.keris.common.util;

import java.util.HashMap;
import java.util.List;

/**
 * íŽ˜ì�´ì§• í�´ëž˜ìŠ¤
 * @author Choi
 *
 */
public class PagingHelper {

	/** í•œ íŽ˜ì�´ì§€ ê¸€ ê°¯ìˆ˜ */
	private int pageSize;

	/** íŽ˜ì�´ì§€ ê·¸ë£¹ ê°¯ìˆ˜ */
	private int pageGroup;

	/** í˜„ìž¬ íŽ˜ì�´ì§€ ë²ˆí˜¸ */
	private int pageNumber;

	/** ì‹œìž‘ íŽ˜ì�´ì§€ */
	private int startPage;

	/** ì¢…ë£Œ íŽ˜ì�´ì§€ */
	private int endPage;

	/** ì´� íŽ˜ì�´ì§€ ìˆ˜ */
	private int totalPageCount;

	/**
	 * ê¸°ë³¸ íŽ˜ì�´ì§€/ê·¸ë£¹ ì‚¬ì�´ì¦ˆë¡œ ìƒ�ì„±
	 */
	public PagingHelper() {
		pageSize = 30;
		pageGroup = 10;
	}

	/**
	 * íŽ˜ì�´ì§€ íŽ˜ì�´ì§€/ê·¸ë£¹ ì‚¬ì�´ì¦ˆ ë³€ê²½í•˜ì—¬ ìƒ�ì„±
	 * @param pageSize
	 * @param pageGroup
	 */
	public PagingHelper(int pageSize, int pageGroup) {
		this.pageSize = pageSize;
		this.pageGroup = pageGroup;
	}

	/**
	 * íŒŒë�¼ë©”í„°ë¡œ ë°›ì�€ ê²€ìƒ‰ìš© ë§µì—� íŽ˜ì�´ì§•ìš© ë³€ìˆ˜ë¥¼ ì¶”ê°€í•˜ì—¬ ë¦¬í„´
	 * @param pagingParam
	 * @param totalCount
	 * @param aPageNumber
	 * @return
	 */
	public HashMap<String, Object> getPagingParam(HashMap<String, Object> pagingParam, int totalCount) {

		//íŽ˜ì�´ì§€ ë³€ìˆ˜ê°€ ë„�ì�´ê±°ë‚˜ ê³µë°±ì�´ë©´ 1ë¡œ ë³€ê²½
		if (pagingParam.get("page") == null || pagingParam.get("page").equals("")) {
			pagingParam.put("page", "1");
		}

		pageNumber = Integer.parseInt((String)pagingParam.get("page"));

		//íŽ˜ì�´ì§€ ë²ˆí˜¸ê°€ 0ë³´ë‹¤ ìž‘ìœ¼ë©´ 1ë¡œ ê³ ì •
		if (pageNumber <= 0) {
			pageNumber = 1;
		}

		int totalPageCount = totalCount / pageSize;

		//íŽ˜ì�´ì§€ê°€ 0ìœ¼ë¡œ ë‚˜ëˆ„ì–´ ë–¨ì–´ì§€ì§€ ì•Šìœ¼ë©´ ì´� íŽ˜ì�´ì§€ ìˆ˜ì—� 1ì�„ ë�”í•œë‹¤
		if (totalCount % pageSize != 0) {
			totalPageCount++;
		}

		this.totalPageCount = totalPageCount;

		//íŽ˜ì�´ì§€ ë²ˆí˜¸ê°€ ì´� íŽ˜ì�´ì§€ ìˆ˜ë³´ë‹¤ í�¬ë©´ ë§ˆì§€ë§‰ íŽ˜ì�´ì§€ ë²ˆí˜¸ë¡œ ë³€ê²½
		if (totalPageCount < pageNumber) {
			pageNumber = totalPageCount;
		}

		this.startPage = (pageNumber - 1) / pageGroup * pageGroup + 1;
		int endPage = startPage + (pageGroup - 1);

		//ë§ˆì§€ë§‰ íŽ˜ì�´ì§€ê°€ ì´� íŽ˜ì�´ì§€ ìˆ˜ë³´ë‹¤ í�¬ë©´ ë§ˆì§€ë§‰ íŽ˜ì�´ì§€ ë²ˆí˜¸ë¡œ ë³€ê²½
		if (endPage > totalPageCount) {
			endPage = totalPageCount;
		}

		this.endPage = endPage;

		int endRow = pageSize * pageNumber;
		int startRow = endRow - pageSize;

		if (startRow < 0) startRow = 0;

		pagingParam.put("startRow", startRow);
		pagingParam.put("endRow", pageSize);

		return pagingParam;
	}

	/**
	 * ë¦¬ìŠ¤íŠ¸ì—� ë„¤ë¹„ê²Œì�´í„° ë³€ìˆ˜ ì¶”ê°€
	 * @return
	 */
	public HashMap<String, Object> getPageList(List<HashMap<String, Object>> list, String functionName, int totalCnt) {

		int prevPage = pageNumber - 1;
		int nextPage = pageNumber + 1;

		//ì�´ì „ íŽ˜ì�´ì§€ê°€ 1ë³´ë‹¤ ìž‘ìœ¼ë©´ 1ë¡œ ì„¤ì •
		if (prevPage < 1) {
			prevPage = 1;
		}

		//ë‹¤ì�ŒíŽ˜ì�´ì§€ê°€ ë§ˆì§€ë§‰ íŽ˜ì�´ì§€ë³´ë‹¤ ë†’ìœ¼ë©´ ë§ˆì§€ë§‰ íŽ˜ì�´ì§€ ë²ˆí˜¸ë¡œ ì„¤ì •
		if (nextPage > totalPageCount) {
			nextPage = totalPageCount;
		}

		HashMap<String, Object> pagingList = new HashMap<String, Object>();

		String navString = "<ul><li><a href=\"javascript:" + functionName + "(1);\"title=\"ì²˜ì�Œ\" class=\"arrow bba\"></a></li>";

		navString += "<li><a href=\"javascript:" + functionName +"("+prevPage+");" + "\" title=\"ì�´ì „\" class=\"arrow ba\"></a></li>";

		for (int page = startPage; page <= endPage; page++) {
			if (page == pageNumber) {
				//í˜„ìž¬ íŽ˜ì�´ì§€ì�¼ë•Œ
				navString +="<li><a href=\"javascript:"+functionName+"("+page+");\" class=\"active\">"+page+"</a></li>";
				pagingList.put("page", page);
			} else {
				navString +="<li><a href=\"javascript:"+functionName+"("+page+");\">"+page+"</a></li>";
			}
		}

		navString += "<li><a href=\"javascript:" + functionName +"("+nextPage+");" + "\" title=\"ë‹¤ì�Œ\" class=\"arrow na\"></a></li>";

		navString += "<li><a href=\"javascript:"+functionName+"("+totalPageCount+");\" title=\"ë§¨ë��\" class=\"arrow nna\"></a></li>";

		pagingList.put("list", list);
		pagingList.put("pageNav", navString);
		pagingList.put("totalCnt", totalCnt);

		return pagingList;
	}
}

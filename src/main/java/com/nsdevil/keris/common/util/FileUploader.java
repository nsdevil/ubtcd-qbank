package com.nsdevil.keris.common.util;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUploader {

	public static String uploadFile(String uploadDir, MultipartFile file, String gname) {
		String fileName = null;
		String realName = null;
		//íŒŒì�¼ì�´ ìžˆìœ¼ë©´ ì—…ë¡œë“œ
		if (file != null) {
			//ì—…ë¡œë“œ í�´ë�” ìƒ�ì„±
			String[] dirList = uploadDir.split("/");
			String filePath = "";
			for (String dir : dirList) {
				filePath += dir + "/";
				File fileDir = new File(filePath);
				if (!fileDir.exists()) {
					fileDir.mkdirs();
				}
			}
			fileName = file.getOriginalFilename();
			String fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
    		realName = gname;

			//upload ê°€ëŠ¥í•œ íŒŒì�¼ íƒ€ìž… ì§€ì •
	        if(fileExt.equalsIgnoreCase("jpg") ||
	        		fileExt.equalsIgnoreCase("jpeg") ||
	        		fileExt.equalsIgnoreCase("gif") ||
	        		fileExt.equalsIgnoreCase("pdf") ||
	        		fileExt.equalsIgnoreCase("png") ||
	        		fileExt.equalsIgnoreCase("bmp") ||
	        		fileExt.equalsIgnoreCase("mp3") ||
	        		fileExt.equalsIgnoreCase("mp4") ||
	        		fileExt.equalsIgnoreCase("wmv") ||
	        		fileExt.equalsIgnoreCase("xml") ||
	        		fileExt.equalsIgnoreCase("xls") ||
	        		fileExt.equalsIgnoreCase("xlsx") ||
	        		fileExt.equalsIgnoreCase("txt") ||
	        		fileExt.equalsIgnoreCase("hwp") ||
	        		fileExt.equalsIgnoreCase("doc") ||
	        		fileExt.equalsIgnoreCase("docx") ||
	        		fileExt.equalsIgnoreCase("ppt") ||
	        		fileExt.equalsIgnoreCase("pptx") ||
	        		fileExt.equalsIgnoreCase("zip")) {

	            try {
	            	byte[] bytes = file.getBytes();
					File outFile = new File(uploadDir + "/" + realName);
					FileOutputStream fileOutputStream = new FileOutputStream(outFile);
					fileOutputStream.write(bytes);
					fileOutputStream.close();
	            } catch(IOException e) {
	            	e.printStackTrace();
	            }
	        } else {
	        	realName = "not";
	        }
		}
		return realName;
	}


	public static boolean evalutionFileCreateMXM(String dirPath, String fileName, XSSFWorkbook workbook) {
		boolean isSuccess = true;
		try{

			String[] dirList = dirPath.split("/");
			String filePath = "";
			for (String dir : dirList) {
				filePath += dir + "/";
				File fileDir = new File(filePath);
				if (!fileDir.exists()) {
					fileDir.mkdirs();
				}
			}

			File outFile = new File(dirPath + fileName);
//			FileOutputStream fileOutputStream = new FileOutputStream(outFile);
//			workbook.write(fileOutputStream);
//			byte[] bytes = fileName.getBytes();
//			fileOutputStream.write(bytes);
//			fileOutputStream.close();
			FileOutputStream fileOut = new FileOutputStream(outFile);
			workbook.write(fileOut);
			fileOut.flush();
			fileOut.close();
			isSuccess = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isSuccess;
	}
	public static boolean evalutionFileCreate(String dirPath, String fileName, HSSFWorkbook workbook) {
		boolean isSuccess = false;
		try{

			String[] dirList = dirPath.split("/");
			String filePath = "";
			for (String dir : dirList) {
				filePath += dir + "/";
				File fileDir = new File(filePath);
				if (!fileDir.exists()) {
					fileDir.mkdirs();
				}
			}

			File outFile = new File(dirPath + fileName);
			FileOutputStream fileOutputStream = new FileOutputStream(outFile);
			workbook.write(fileOutputStream);
			byte[] bytes = fileName.getBytes();
			fileOutputStream.write(bytes);
			fileOutputStream.close();
			isSuccess = true;
		} catch (Exception e) {
		       e.printStackTrace();
	    }

		return isSuccess;
	}
}

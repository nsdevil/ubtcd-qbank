package com.nsdevil.keris.common;

import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


        HttpSession session = request.getSession();
        String contextPath = request.getContextPath();
        if (session.getAttribute("S_TOKEN") == null) {
            session.invalidate();
            response.sendRedirect(contextPath);
            return false;
        } else {
            String role = session.getAttribute("S_ROLE").toString();

            if (role.contains("ROLE_PROFESSOR") || role.contains("ROLE_ADMIN") || role.contains("ROLE_MANAGER")) {
                return true;
            }

            String redirectURL = request.getContextPath() + "/error";

            response.sendRedirect(redirectURL);
            return false;
        }
    }

}

package com.nsdevil.keris.app;

import com.nsdevil.keris.common.bean.ResultMap;
import com.nsdevil.keris.common.exception.LogicException;
import com.nsdevil.keris.common.exception.ResourceNotFoundException;
import com.nsdevil.keris.common.exception.RuntimeLogicException;
import com.nsdevil.keris.util.Util;
import org.mybatis.spring.MyBatisSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;

public class CommonController {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected String JSON_VIEW = "jacksonView";

    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ModelAndView exceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception ex) {
        logger.debug("start URI : " + request.getRequestURI());

        String errName 	= ex.getClass().getName();
        String err 	= "000";
        String errMsg = null;
        String errLogMsg = "";

        if(ex instanceof LogicException) {
            err = ((LogicException)ex).getCode();
            errMsg = ((LogicException)ex).getMessage();
        } else if(ex instanceof RuntimeLogicException) {
            err = ((RuntimeLogicException)ex).getCode();
            errMsg = ((RuntimeLogicException)ex).getMessage();
        } else if(ex instanceof ResourceNotFoundException) {
            err = ((ResourceNotFoundException)ex).getCode();
            errMsg = ((ResourceNotFoundException)ex).getMessage();
        } else if (ex instanceof MyBatisSystemException) {
            err = "901";
            errMsg = "DB_CONNECTION_FAIL";
            errLogMsg = getStackTrace(ex);
        } else {
            err = "999";
            errMsg = "UNKNOWN_ERROR";
            errLogMsg = ex.getMessage();
            errLogMsg = getStackTrace(ex);
        }

        ResultMap resultMap = new ResultMap();
        resultMap.setStatus(err);
        resultMap.setMsg(errMsg);

        String viewName;
        if("XMLHttpRequest".equals(Util.nvl(request.getHeader("X-Requested-With")))) {
            viewName = JSON_VIEW;
        } else {
            viewName = "redirect:/error/" + err;
        }

        ModelAndView  model = new ModelAndView(viewName);
        model.addAllObjects(resultMap);
        logger.error("["+errName+"] ["+err+"] ["+errMsg+"] [" +errLogMsg+"]");
        logger.debug("end");
        return model;
    }

    /**
     * Exception의 StackTrace를 문자열로 취득
     * @param ex
     * @return
     */
    private String getStackTrace(Exception ex) {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }
}

package com.nsdevil.keris.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nsdevil.keris.common.bean.ResultMap;
import com.nsdevil.keris.util.AppConstants;
import com.nsdevil.keris.util.ServiceHandler;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
public class MainController extends CommonController{

    private static final String JSON_VIEW = "jacksonView";
    @Autowired
    ServiceHandler serviceHandler;

    @RequestMapping(value = {"", "/", "/qbank", "/login"}, method = RequestMethod.GET)
    public String loginView(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("S_TOKEN") != null) {
            if (session.getAttribute("S_ROLE") != null) {
                if (session.getAttribute("S_ROLE").toString().equalsIgnoreCase("ROLE_PROFESSOR") ||
                        session.getAttribute("S_ROLE").toString().equalsIgnoreCase("ROLE_ADMIN") ||
                            session.getAttribute("S_ROLE").toString().equalsIgnoreCase("ROLE_MANAGER")) {
                    return "redirect:/main";
                }else {
                }
            }
        }
        return "login";
    }
    @RequestMapping(value = "/app/auth/signin", method = RequestMethod.POST)
    public String loginApi(Locale locale, HttpServletRequest request, @RequestBody HashMap<String, Object> param, Model model) throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put("loginId", param.get("loginId").toString());
        params.put("password", param.get("password").toString());
        HashMap<String, Object> result = serviceHandler.sendPost(params,
                (AppConstants.DEBUG ? AppConstants.BASE_URL1 : AppConstants.BASE_URL3) + "/api/auth/signin", locale);
        JSONObject jsonObject = new JSONObject(result.get("result").toString());
        ResultMap resultMap = new ResultMap();
        if (jsonObject.has("success")) {
            if (jsonObject.getBoolean("success")) {
                resultMap.setMsg("success");
            } else {
                resultMap.setMsg(jsonObject.get("message").toString());
            }
        }
        if (jsonObject.has("status")) {
            resultMap.setStatus(jsonObject.get("status").toString());
        }
        if (jsonObject.has("accessToken")) {
            resultMap.setMsg("success");
            HttpSession session = request.getSession();
            session.setAttribute("S_TOKEN", jsonObject.get("accessToken").toString());
            HashMap<String, Object> userInfo = serviceHandler.sendGet(request,
                    (AppConstants.DEBUG ? AppConstants.BASE_URL1 : AppConstants.BASE_URL3) + "/api/user/me");
            JSONObject userObject = new JSONObject(userInfo.get("result").toString());
            if (userObject.has("loginId")) {
                session.setAttribute("S_LOGINID", userObject.get("loginId").toString());
            }
            if (userObject.has("name")) {
                session.setAttribute("S_NAME", userObject.get("name").toString());
            }
            if (userObject.has("schCode")) {
                session.setAttribute("S_SCHCODE", userObject.get("schCode").toString());
            }
            if (userObject.has("authorities")) {
                JSONArray authorities = new JSONArray(userObject.get("authorities").toString());
                for (int i = 0; i < authorities.length(); i++) {
                    JSONObject auth = (JSONObject) authorities.get(i);
                    String role = auth.get("authority").toString();
                    session.setAttribute("S_ROLE", role);
//                    if (role.equals("ROLE_ADMIN"))
//                    {
//
//                    }
//                    else{
//                        if (userObject.has("payment"))
//                        {
//                            if (userObject.get("payment").toString().equals("0"))
//                            {
//                                request.getSession().invalidate();
//                                resultMap.setStatus("888");
//                            }
//                        }
//                    }
                }
            }
        }

        model.addAllAttributes(resultMap);
        return JSON_VIEW;
    }

    @RequestMapping(value = "/admin/login", method = RequestMethod.POST)
    public String loginRequest(HttpServletRequest request, HashMap<String, Object> param) {
        System.err.println(param.toString());
        return "redirect:/login";
    }
    @RequestMapping(value = "/index.html", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String index(HttpServletRequest request) {
        return "index";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request) {
        request.getSession().invalidate();
        return "redirect:/";
    }

    @RequestMapping(value = {"/main"}, method = RequestMethod.GET)
    public String mainView(HttpServletRequest request, HttpServletResponse response) {
        return "main";
    }

    @RequestMapping(value = {"multimedia/essential"}, method = RequestMethod.GET)
    public String multimedia_essential(HttpServletRequest request) {
        return "multimedia/essential";
    }

    @RequestMapping(value = {"multimedia/multimedia_card_search"}, method = RequestMethod.GET)
    public String multimedia_card_search(HttpServletRequest request) {
        return "multimedia/multimedia_card_search";
    }

    @RequestMapping(value = {"multimedia/multimedia_search"}, method = RequestMethod.GET)
    public String multimedia_search(HttpServletRequest request) {
        return "multimedia/multimedia_search";
    }

    @RequestMapping(value = {"multimedia/test_subject"}, method = RequestMethod.GET)
    public String test_subject(HttpServletRequest request) {
        return "multimedia/test_subject";
    }

    @RequestMapping(value = {"my/show"}, method = RequestMethod.GET)
    public String my_show(HttpServletRequest request) {
        return "my/show";
    }

    @RequestMapping(value = {"my/update"}, method = RequestMethod.GET)
    public String my_update(HttpServletRequest request) {
        return "my/update";
    }

    @RequestMapping(value = {"my/pc_certification"}, method = RequestMethod.GET)
    public String pc_ertification(HttpServletRequest request) {
        return "my/pc_certification";
    }

    @RequestMapping(value = {"my/form/pc_add"}, method = RequestMethod.GET)
    public String pc_add(HttpServletRequest request) {
        return "my/form/pc_add";
    }

    @RequestMapping(value = {"my/form/pc_update"}, method = RequestMethod.GET)
    public String pc_update(HttpServletRequest request) {
        return "my/form/pc_update";
    }

    @RequestMapping(value = {"my/student"}, method = RequestMethod.GET)
    public String my_student(HttpServletRequest request) {
        return "my/student";
    }

    @RequestMapping(value = {"my/teacher"}, method = RequestMethod.GET)
    public String my_teacher(HttpServletRequest request) {
        return "my/teacher";
    }

    @RequestMapping(value = {"notice/survey_list"}, method = RequestMethod.GET)
    public String notice_survey_list(HttpServletRequest request) {
        return "notice/survey_list";
    }

    @RequestMapping(value = {"notice/survey_result"}, method = RequestMethod.GET)
    public String notice_survey_result(HttpServletRequest request) {
        return "notice/survey_result";
    }

    @RequestMapping(value = {"notice/no-search"}, method = RequestMethod.GET)
    public String notice_no_search(HttpServletRequest request) {
        return "notice/no-search";
    }

    @RequestMapping(value = {"notice/search"}, method = RequestMethod.GET)
    public String notice_search(HttpServletRequest request) {
        return "notice/search";
    }

    @RequestMapping(value = {"notice/show"}, method = RequestMethod.GET)
    public String notice_show(HttpServletRequest request) {
        return "notice/show";
    }

    @RequestMapping(value = {"notice/write"}, method = RequestMethod.GET)
    public String notice_write(HttpServletRequest request) {
        return "notice/write";
    }

    @RequestMapping(value = {"qna/question"}, method = RequestMethod.GET)
    public String qna_question(HttpServletRequest request) {
        return "qna/addquestion";
    }

    @RequestMapping(value = {"qna/question_reply"}, method = RequestMethod.GET)
    public String qna_question_reply(HttpServletRequest request) {
        return "qna/addquestion_reply";
    }

    @RequestMapping(value = {"qna/list"}, method = RequestMethod.GET)
    public String qna_list(HttpServletRequest request) {
        return "qna/list";
    }

    @RequestMapping(value = {"qna/reply"}, method = RequestMethod.GET)
    public String qna_reply(HttpServletRequest request) {
        return "qna/reply";
    }

    @RequestMapping(value = {"qna/reply/add"}, method = RequestMethod.GET)
    public String qna_reply_add(HttpServletRequest request) {
        return "qna/reply_add";
    }

    @RequestMapping(value = {"qna/show"}, method = RequestMethod.GET)
    public String qna_show(HttpServletRequest request) {
        return "qna/show";
    }

    @RequestMapping(value = {"qna/write"}, method = RequestMethod.GET)
    public String qna_write(HttpServletRequest request) {
        return "qna/write";
    }

    @RequestMapping(value = {"/question_card/institutional/default_card_list"}, method = RequestMethod.GET)
    public String question_card_default_card_list(HttpServletRequest request) {
        return "question_card/institutional/default_card_list";
    }

    @RequestMapping(value = {"question_card/institutional/default_list1"}, method = RequestMethod.GET)
    public String question_card_default_list1(HttpServletRequest request) {
        return "question_card/institutional/default_list1";
    }

    @RequestMapping(value = {"question_card/institutional/default_list2"}, method = RequestMethod.GET)
    public String question_card_default_list2(HttpServletRequest request) {
        return "question_card/institutional/default_list2";
    }

    @RequestMapping(value = {"question_card/institutional/default_list3"}, method = RequestMethod.GET)
    public String question_card_default_list3(HttpServletRequest request) {
        return "question_card/institutional/default_list3";
    }

    @RequestMapping(value = {"question_card/institutional/default_list3_2"}, method = RequestMethod.GET)
    public String question_card_default_list3_2(HttpServletRequest request) {
        return "question_card/institutional/default_list3_2";
    }

    @RequestMapping(value = {"question_card/institutional/default_list3_3"}, method = RequestMethod.GET)
    public String question_card_default_list3_3(HttpServletRequest request) {
        return "question_card/institutional/default_list3_3";
    }

    @RequestMapping(value = {"question_card/institutional/default_list4"}, method = RequestMethod.GET)
    public String question_card_default_list4(HttpServletRequest request) {
        return "question_card/institutional/default_list4";
    }


    @RequestMapping(value = {"question_card/institutional/default_list5"}, method = RequestMethod.GET)
    public String question_card_default_list5(HttpServletRequest request) {
        return "question_card/institutional/default_list5";
    }

    @RequestMapping(value = {"question_card/institutional/default_list6"}, method = RequestMethod.GET)
    public String question_card_default_list6(HttpServletRequest request) {
        return "question_card/institutional/default_list6";
    }

    @RequestMapping(value = {"question_card/institutional/default_list7"}, method = RequestMethod.GET)
    public String question_card_default_list7(HttpServletRequest request) {
        return "question_card/institutional/default_list7";
    }

    @RequestMapping(value = {"question_card/institutional/default_list8"}, method = RequestMethod.GET)
    public String question_card_default_list8(HttpServletRequest request) {
        return "question_card/institutional/default_list8";
    }

    @RequestMapping(value = {"question_card/institutional/default_list9"}, method = RequestMethod.GET)
    public String question_card_default_list9(HttpServletRequest request) {
        return "question_card/institutional/default_list9";
    }

    @RequestMapping(value = {"question_card/institutional/default_list10"}, method = RequestMethod.GET)
    public String question_card_default_list10(HttpServletRequest request) {
        return "question_card/institutional/default_list10";
    }

    @RequestMapping(value = {"question_card/institutional/default_list11"}, method = RequestMethod.GET)
    public String question_card_default_list11(HttpServletRequest request) {
        return "question_card/institutional/default_list11";
    }

    @RequestMapping(value = {"multimedia/form/clinical_expression_search"}, method = RequestMethod.GET)
    public String multimedia_form_clinical_expression_search(HttpServletRequest request) {
        return "multimedia/form/clinical_expression_search";
    }

    @RequestMapping(value = {"question_card/institutional/default_list12"}, method = RequestMethod.GET)
    public String question_card_default_list12(HttpServletRequest request) {
        return "question_card/institutional/default_list12";
    }

    @RequestMapping(value = {"question_card/institutional/default_list13"}, method = RequestMethod.GET)
    public String question_card_default_list13(HttpServletRequest request) {
        return "question_card/institutional/default_list13";
    }

    @RequestMapping(value = {"question_card/institutional/excel_upload"}, method = RequestMethod.GET)
    public String question_card_excel_upload(HttpServletRequest request) {
        return "question_card/institutional/excel_upload";
    }

    @RequestMapping(value = {"question_registration/each/preview/question_preview"}, method = RequestMethod.GET)
    public String question_register_each_question_preview(HttpServletRequest request) {
        return "question_registration/each/preview/question_preview";
    }

    @RequestMapping(value = {"question_card/institutional/institution_add"}, method = RequestMethod.GET)
    public String question_card_institution_add(HttpServletRequest request) {
        return "question_card/institutional/institution_add";
    }

    @RequestMapping(value = {"question_card/personal/personal_list"}, method = RequestMethod.GET)
    public String question_card_personal_personal_list(HttpServletRequest request) {
        return "question_card/personal/personal_list";
    }

    @RequestMapping(value = {"question_card/personal/main_card_list"}, method = RequestMethod.GET)
    public String question_card_personal_main_card_list(HttpServletRequest request) {
        return "question_card/personal/main_card_list";
    }

    @RequestMapping(value = {"question_card/personal/submenu_card_lilst"}, method = RequestMethod.GET)
    public String question_card_personal_submenu_card_lilst(HttpServletRequest request) {
        return "question_card/personal/submenu_card_lilst";
    }

    @RequestMapping(value = {"question_card/personal/form/update"}, method = RequestMethod.GET)
    public String question_card_personal_form_update(HttpServletRequest request) {
        return "question_card/personal/form/update";
    }

    @RequestMapping(value = {"question_card/institutional/form/excel_upload"}, method = RequestMethod.GET)
    public String question_card_institutional_form_excel_upload(HttpServletRequest request) {
        return "question_card/institutional/form/excel_upload";
    }

    @RequestMapping(value = {"question_registration/each/essential"}, method = RequestMethod.GET)
    public String question_registration_each_essential(HttpServletRequest request) {
        return "question_registration/each/essential";
    }

    @RequestMapping(value = {"question_registration/each/form_regist"}, method = RequestMethod.GET)
    public String question_registration_each_form_regist(HttpServletRequest request) {
        return "question_registration/each/form_regist";
    }

    @RequestMapping(value = {"question_registration/each/making"}, method = RequestMethod.GET)
    public String question_registration_each_making(HttpServletRequest request) {
        return "question_registration/each/making";
    }

    @RequestMapping(value = {"question_registration/each/form_regist_preview"}, method = RequestMethod.GET)
    public String question_registration_each_form_regist_preview(HttpServletRequest request) {
        return "question_registration/each/form_regist_preview";
    }

    @RequestMapping(value = {"question_registration/each/temporary"}, method = RequestMethod.GET)
    public String question_registration_each_temporary(HttpServletRequest request) {
        return "question_registration/each/temporary";
    }

    @RequestMapping(value = {"question_registration/each/making/multiple"}, method = RequestMethod.GET)
    public String question_registration_each_making_multiple(HttpServletRequest request) {
        return "question_registration/each/making/multiple";
    }

    @RequestMapping(value = {"question_registration/each/making/short"}, method = RequestMethod.GET)
    public String question_registration_each_making_short(HttpServletRequest request) {
        return "question_registration/each/making/short";
    }

    @RequestMapping(value = {"question_registration/each/making/type_r"}, method = RequestMethod.GET)
    public String question_registration_each_making_type_r(HttpServletRequest request) {
        return "question_registration/each/making/type_r";
    }

    @RequestMapping(value = {"question_registration/each/making/type_r2"}, method = RequestMethod.GET)
    public String question_registration_each_making_type_r2(HttpServletRequest request) {
        return "question_registration/each/making/type_r2";
    }

    @RequestMapping(value = {"question_registration/each/popup/change_card"}, method = RequestMethod.GET)
    public String question_registration_each_popup_change_card(HttpServletRequest request) {
        return "question_registration/each/popup/change_card";
    }

    @RequestMapping(value = {"question_registration/each/popup/edit"}, method = RequestMethod.GET)
    public String question_registration_each_popup_edit(HttpServletRequest request) {
        return "question_registration/each/popup/edit";
    }

    @RequestMapping(value = {"question_registration/each/popup/preview"}, method = RequestMethod.GET)
    public String question_registration_each_popup_preview(HttpServletRequest request) {
        return "question_registration/each/popup/preview";
    }

    @RequestMapping(value = {"question_registration/each/preview/preview"}, method = RequestMethod.GET)
    public String question_registration_each_preview_preview(HttpServletRequest request) {
        return "question_registration/each/preview/preview";
    }

    //    @RequestMapping(value = {"question_registration/each/preview/preview/id"}, method = RequestMethod.GET)
//    public String  getQuestionId(@Req("id") String questionId) {
//        return "question_registration/each/preview/preview";
//    }
    @RequestMapping(value = {"question_registration/each/preview/change_history"}, method = RequestMethod.GET)
    public String question_registration_each_preview_change_history(HttpServletRequest request) {
        return "question_registration/each/preview/change_history";
    }

    @RequestMapping(value = {"question_registration/each/preview/exam_history"}, method = RequestMethod.GET)
    public String question_registration_each_preview_exam_history(HttpServletRequest request) {
        return "question_registration/each/preview/exam_history";
    }

    @RequestMapping(value = {"question_registration/each/preview/share_history"}, method = RequestMethod.GET)
    public String question_registration_each_preview_share_history(HttpServletRequest request) {
        return "question_registration/each/preview/share_history";
    }

    @RequestMapping(value = {"question_registration/each/question_card/clinical_expression"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_clinical_expression(HttpServletRequest request) {
        return "question_registration/each/question_card/clinical_expression";
    }

    @RequestMapping(value = {"question_registration/each/question_card/clinical_technique"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_clinical_technique(HttpServletRequest request) {
        return "question_registration/each/question_card/clinical_technique";
    }

    @RequestMapping(value = {"question_registration/each/question_card/curriculum"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_curriculum(HttpServletRequest request) {
        return "question_registration/each/question_card/curriculum";
    }

    @RequestMapping(value = {"question_card/questionbank"}, method = RequestMethod.GET)
    public String question_card_questionbank(HttpServletRequest request) {
        return "question_card/questionbank";
    }

    @RequestMapping(value = {"question_registration/each/question_card/diagnosis_name"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_diagnosis_name(HttpServletRequest request) {
        return "question_registration/each/question_card/diagnosis_name";
    }

    @RequestMapping(value = {"question_registration/each/question_card/etc"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_etc(HttpServletRequest request) {
        return "question_registration/each/question_card/etc";
    }

    @RequestMapping(value = {"question_registration/each/question_card/expected_difficulty"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_expected_difficulty(HttpServletRequest request) {
        return "question_registration/each/question_card/expected_difficulty";
    }

    @RequestMapping(value = {"question_registration/each/question_card/health_care_term"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_health_care_term(HttpServletRequest request) {
        return "question_registration/each/question_card/health_care_term";
    }

    @RequestMapping(value = {"question_registration/each/question_card/individual_question_card"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_individual_question_card(HttpServletRequest request) {
        return "question_registration/each/question_card/individual_question_card";
    }

    @RequestMapping(value = {"question_registration/each/question_card/knowledge_level"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_knowledge_level(HttpServletRequest request) {
        return "question_registration/each/question_card/knowledge_level";
    }

    @RequestMapping(value = {"question_registration/each/question_card/learning_result"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_learning_result(HttpServletRequest request) {
        return "question_registration/each/question_card/learning_result";
    }

    @RequestMapping(value = {"question_registration/each/question_card/question_relevance"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_question_relevance(HttpServletRequest request) {
        return "question_registration/each/question_card/question_relevance";
    }

    @RequestMapping(value = {"question_registration/each/question_card/reference"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_reference(HttpServletRequest request) {
        return "question_registration/each/question_card/reference";
    }

    @RequestMapping(value = {"question_registration/each/question_card/subject"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_subject(HttpServletRequest request) {
        return "question_registration/each/question_card/subject";
    }

    @RequestMapping(value = {"question_registration/each/question_card/test_subject1"}, method = RequestMethod.GET)
    public String question_registration_each_question_card_test_subject1(HttpServletRequest request) {
        return "question_registration/each/question_card/test_subject1";
    }

    @RequestMapping(value = {"kexam/examlist"}, method = RequestMethod.GET)
    public String exam_examlist(HttpServletRequest request) {
        return "kexam/examlist";
    }

    @RequestMapping(value = {" kexam/teacherClassSee"}, method = RequestMethod.GET)
    public String teacherclassSee(HttpServletRequest request) {
        return " kexam/teacherClassSee";
    }

    @RequestMapping(value = {"kexam/examregister"}, method = RequestMethod.GET)
    public String exam_register(HttpServletRequest request) {
        return "kexam/examregister";
    }

    @RequestMapping(value = {"kexam/classlist"}, method = RequestMethod.GET)
    public String classlist(HttpServletRequest request) {
        return "kexam/classlist";
    }

    @RequestMapping(value = {"kexam/schoollist"}, method = RequestMethod.GET)
    public String schoollist(HttpServletRequest request) {
        return "kexam/schoollist";
    }

    @RequestMapping(value = {"list/classlist"}, method = RequestMethod.GET)
    public String list_classlist(HttpServletRequest request) {
        return "list/classlist";
    }

    @RequestMapping(value = {"list/teacherClasses"}, method = RequestMethod.GET)
    public String list_teacherClasses(HttpServletRequest request) {
        return "list/teacherClasses";
    }

    @RequestMapping(value = {"list/studentlist"}, method = RequestMethod.GET)
    public String list_studentlist(HttpServletRequest request) {
        return "list/studentlist";
    }

    @RequestMapping(value = {"list/teacherlist"}, method = RequestMethod.GET)
    public String list_teacherlist(HttpServletRequest request) {
        return "list/teacherlist";
    }

    @RequestMapping(value = {"list/classteachers"}, method = RequestMethod.GET)
    public String list_classteachers(HttpServletRequest request) {
        return "list/classteachers";
    }

    @RequestMapping(value = {"question_card/questionset"}, method = RequestMethod.GET)
    public String question_card_questionset(HttpServletRequest request) {
        return "question_card/questionset";
    }

    @RequestMapping(value = {"error"}, method = RequestMethod.GET)
    public String error(HttpServletRequest request) {
        return "layout/error";
    }

}

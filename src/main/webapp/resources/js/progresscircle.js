function makesvg($this, total, num, inner_text) {
  inner_text = inner_text || "";
  var abs_percentage = (num * 100 - 22) / total;
  var num_str = num;
  var total_str = "/" + total;
  var svg;

  if ($this.hasClass('hide-total')) {
    svg = '\
    <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">\
    <circle class="circle-chart-background" cx = "16.9" cy = "16.9" r = "15.4" />\
    <circle class="circle-chart-circle" stroke-dasharray="' + abs_percentage + ',100" cx="16.9" cy="16.9" r="15.4" transform="rotate(-86 16.9 16.9)"/>\
    <g class="circle-chart-info">\
    <text class="circle-chart-num" x="50%" y="16">'+ num_str + '</text>\
    <text class="circle-chart-total" x="50%" y="16">'+ total_str + '</text>\
    '
  }else{
    svg = '\
    <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">\
    <circle class="circle-chart-background" cx = "16.9" cy = "16.9" r = "15.4" />\
    <circle class="circle-chart-circle" stroke-dasharray="' + abs_percentage + ',100" cx="16.9" cy="16.9" r="15.4" transform="rotate(-86 16.9 16.9)"/>\
    <g class="circle-chart-info">\
    <text class="circle-chart-num" x="15.5" y="18">'+ num_str + '</text>\
    <text class="circle-chart-total" x="16" y="18">'+ total_str + '</text>\
    '
  }

  if (inner_text) {
    svg += '<text class="circle-chart-subline" x="16.91549431" y="21.5">' + inner_text + '</text>'
  }

  if (abs_percentage < 0) {
    $(".circle-graph").addClass("disabled");
  }else{
    $(".circle-graph").removeClass("disabled");
  }

  svg += '</g></svg > ';

  return svg
}

(function ($) {

  $.fn.circlechart = function () {
    this.each(function () {
      var total = $(this).data("total");
      var num = $(this).data("num");
      var inner_text = $(this).text();
      var $this = $(this);

      $(this).html(makesvg($this, total, num, inner_text));
    });
    return this;
  };

}(jQuery));
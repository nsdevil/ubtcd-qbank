$(document).ready(function(){
    $("article.share li").on('click',function(){
        var class_name = $(this).parent().parent().attr("class");
        $(this).siblings().removeClass("on");
        $(this).addClass("on");

        var value = $(this).text();

        $("input[name='"+class_name+"']").val(value);
        $("aside ."+class_name+" span").text(value);
    });
})

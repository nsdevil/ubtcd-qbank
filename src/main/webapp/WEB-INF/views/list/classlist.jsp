<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <title>Class list</title>
    <link rel="stylesheet" href="${CSS}/common/header.css">
    <link rel="stylesheet" href="${CSS}/common/footer.css">
    <link rel="stylesheet" href="${CSS}/my/show.css">
    <link rel="stylesheet" href="${CSS}/question_registration/register_information.css">

    <script src="${JS}/vue/vue-router.js"></script>
    <style>
        .displaydiv {
            display: block !important;
        }
        table {
            width: 100%;
        }

        .btn-red {
            width: 120px;
        }

        .btn-red-line {
            height: 34px;
            width: 120px;
            margin-right: 15px;
        }

        .search-wrap {
            float: right;
            margin-top: 15px;
            margin-bottom: 15px;
        }
        .btn-line-red{
            padding: 2px;
            color: #fb5f33;
            font-size: 17px;
            border-bottom: 1px solid;
        }
        .modifyinput{
            width: 100%;
            border: 1px solid #eee;
            padding: 5px;
            border-radius: 5px;
            outline: none;
            height: 40px;
            margin-top: 20px !important;
        }
        .modalDialog{
            overflow: auto
        }
        .btn-red-small{
            color: #fb5f33;
            border: 1px solid #fb5f33;
            padding: 5px;
        }
        #se-pre-con {
            position: fixed;
            -webkit-transition: opacity 5s ease-in-out;
            -moz-transition: opacity 5s ease-in-out;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
        }
    </style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="classlistvue">
    <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
        <div class="breadcrumb">
            <span><a href="../main.jsp"><spring:message code="all.home"/></a></span>
            <span><a href=""><spring:message code="all.user.register"/></a></span>
            <span><a href="../my/pc_certification.jsp"><spring:message code="all.student"/></a></span>
        </div>
        <h2 style="margin-bottom: 10px">{{schoolName}} <spring:message code="sschool"/> <spring:message code="class.list"/></h2>
        <div class="table-wrap certi-table">
            <table>
                <colgroup>
                    <col style="width : 100px;">
                    <col style="width : 150px;">
<%--                    <col style="width : 100px;">--%>
<%--                    <col style="width : 150px;">--%>
                </colgroup>
                <tbody class="border-red-top-2 border-gray-bottom-2">
                <th><spring:message code="class"/></th>
                <th>
                    <input type="text" class="" v-model="groupName" name="groupNameClasslist" autocorrect="off" spellcheck="false"
                           id="groupNameClasslist"  autocomplete="off"/>
                </th>
<%--                <th><spring:message code="name"/></th>--%>
<%--                <th>--%>
<%--                    <input type="text" class="" v-model="name"/>--%>
<%--                </th>--%>
                </tbody>
            </table>
            <div class="flex search-wrap">
                <button class="btn-red-line" @click="defaultSearch()">
                    <spring:message code="all.defaultSearch"/>
                </button>
                <button class="btn-red" @click="searchFilter()">
                    <spring:message code="all.search"/>
                </button>
            </div>
            <div class="table-wrap search-div" style="margin-top: 20px; margin-bottom: 20px;" v-if="'${sessionScope.S_ROLE}' == 'ROLE_MANAGER'">
                <button class="btn-red" @click="classAddModal()" style="width: 180px; height: 36px;">
                    <i class="plus"></i>
                    <spring:message code="classlist.schoolAdd"/>
                </button>
            </div>
        </div>
        <div class="table-wrap certi-table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
            style="table-layout: auto;">
            <colgroup>
                <col width="4%">
                <col width="13%">
                <col width="13%">
                <col width="13%">
                <col width="8%">
                <col width="10%">
                <col width="8%" v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN'">
            </colgroup>
            <thead>
                <tr class="title_table border-red-top-2">
                    <th>
                        <span><spring:message code="all.number"/></span>
                    </th>
                    <th>
                        <span><spring:message code="classlist.schoolID"/></span>
                    </th>
                    <th>
                        <span><spring:message code="classlist.schoolName"/></span>
                    </th>
                    <th>
                        <span><spring:message code="classlist.groupNo"/></span>
                    </th>
                    <th>
                        <span><spring:message code="classlist.teacherCount"/></span>
                    </th>
                    <th>
                        <span><spring:message code="classlist.studentCount"/></span>
                    </th>
                    <th v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN'">
                        <span><spring:message code="student.register"/></span>
                    </th>
                </tr>
            </thead>
            <tbody v-if="classlist.length > 0">
                <tr v-for="(item,index) in classlist" :key="index">
                    <td>
                        {{index+1}}
                    </td>
                    <td>
                        {{item.sch_code}}
                    </td>
                    <td>
                        {{item.group_name}}
                    </td>
                    <td>
                        <span>{{item.class_id}}</span>
    <%--                            {{item.class_id}}--%>
                    </td>
                    <td>
                        <a href="#" class="btn-line-red" v-if="item.teachcount != '0'"
                           @click="newPages('classteachers', item.sch_code, item.class_id)"> {{item.teachcount}}</a>
                        <span v-else>0</span>
                    </td>
                    <td>
                        <a href="#" class="btn-line-red"
                           @click="newPages('studentlist', item.sch_code, item.class_id)"> {{item.stcount}}</a>
                    </td>
    <%--                <td>--%>
    <%--                    <button class="btn-red"  style="height: 21px; margin: 0px; display: inline"--%>
    <%--                            @click="popupClose(item.sch_code, item.class_id)"><spring:message code="default.add"/></button>--%>
    <%--                </td>--%>
                    <td v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN'">
                        <button class="btn-red"  style="height: 21px; margin: 0px; display: inline"
                                @click="excelClose(item.sch_code, item.class_id)"><spring:message code="default.excelUpload"/></button>
                    </td>
                </tr>
            </tbody>
            <tbody v-else>
                <tr>
                    <td colspan="7">
                        <spring:message code="all.empty" />
                    </td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>

<%--    excel register--%>
    <div v-show="excelModal" class="modalDialog" :class="{'displaydiv': excelModal}"
         style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <a href="javascript:void(0)" @click="excelClose()" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p style="font-weight: 800"><spring:message code="all.user.register"/></p>
                    </div>
                    <div class="modal-body">
                        <article class="flex">
                            <div class="detail">
                                <div class="default">
                                    <h3>** <spring:message code="user.require" />
                                    </h3>
                                    <ul class="flex">
                                        <li>
                                            <div class="title"><spring:message code="user.schCode"/>**</div>
                                            <spring:message code="user.schCode" var="schCode"/>
                                            <input type="text" class="desc" placeholder="${schCode}" v-model="excel.schCode" disabled name="descks"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.classID"/>**</div>
                                            <spring:message code="user.classID" var="classID"/>
                                            <input type="text" class="desc" placeholder="${classID}" v-model="excel.groupNo" disabled name="grouNO"/>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form-box" style="text-align: center">
                                    <h3><spring:message code="default.excelUpload" /></h3>
                                    <div class="file" style="position: relative;">
                                        <p><spring:message code="question.register.fileUpload" /> (XLS, 10MB)</p>
                                        <%--<button class="btn-white shadow" onclick=";"><i class="download2"></i>양식 다운로드</button>--%>
                                        <button class="btn-white shadow" onclick="file_load(event);"
                                                style="margin-left: 40%; margin-top: 20px;  margin-bottom: 20px; width: 160px;">
                                            <i class="upload" style="margin-right: 15px;"></i>
                                            <spring:message code="question.register.fileUpload" />
                                        </button>
                                        <input style="display: none;" @change="previewFileExcel($event)" name="file" type="file">
                                        <p style="position: absolute;
    top: 0;
    left: 0;
    margin-top: 65px;
    margin-left: 30px;
    font-weight: 500;
    color: #000;">{{excel.fileName}}</p>
                                    </div>
                                </div>
                                <div class="password clear">
                                    <button type="button" class="btn-red shadow" @click="userExcel()"
                                            style="height: 39px;">
                                        <spring:message code="examlist.register" /></button>
                                    <button type="button" @click="excelModal = !excelModal" class="btn-gray shadow"
                                            style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                                        <spring:message code="all.close" /></button>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
<%--    end excel register--%>

    <div id="classmodal" class="modalDialog">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container" style="width: 600px;">
                    <a href="#close" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p> <spring:message code="classlist.schoolAdd" /></p>
                    </div>
                    <div class="modal-body" style="padding: 38px;">
                        <span>Жишээ :  1a, 2б, 3в, 4г (гэж Монголоор бичнэ үү.)</span>
                        <spring:message code="classlist.schoolName" var="classlist"/>
                        <input class="modifyinput" placeholder="${classlist}" type="text"
                               v-model="create.groupName" style="margin-bottom: 20px;"/>
                        <br>
                        <span>Жишээ: 01A , 01B, 01V, 01G, 01D (гэх мэт заавал 3 оронтой байна.)</span>
                        <spring:message code="classlist.groupNo" var="groupNo"/>
                        <div class="flex">
                            <input class="modifyinput" v-model="schCode" disabled style="width: 30%; margin-right: 16px;">
                            <input class="modifyinput" placeholder="${groupNo}" type="text" style="width: 50%"
                                   v-model="clasgroupNo"/>
                        </div>
                        <br>
                        <spring:message code="schoollist.schoolName" var="schoolName"/>
                        <input class="modifyinput" placeholder="${schoolName}" type="text"
                               disabled v-model="schoolName"/>
                    </div>
                    <div class="modal-footer flex">
                        <a href="#close" class="modal-default-button" type="button">
                            <spring:message code="schoollist.cancel"/>
                        </a>
                        <button class="modal-default-button margin-left" type="button" @click="saveClass()">
                            <spring:message code="schoollist.save"/>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    let router = new VueRouter({
        mode: 'history',
        routes: []
    });
    let app = new Vue({
        router,
        data: {
            studentlist: [],
            studentPage: 1,
            pages: 0,
            itemsPerPage: 1,
            totalElement: 0,
            userRegisterModal: false,
            userListModal: false,
            excelModal: false,
            classlist: [],
            schoolName: '',
            groupName: '',
            name: '',
            schCode: '',
            excel:{
                fileName: '',
                excelFile: null,
                schCode: '',
                groupNo: ''
            },
            create: {
                schId: '',
                schoolName: '',
                schoolId: '',
                image: 'https://mongolia-today.mn/wp-content/uploads/2017/08/%D1%85%D1%83%D1%83%D0%BB%D1%8C-%D1%81%D0%B0%D1%85%D0%B8%D1%83%D0%BB%D0%B0%D1%85%D0%B8%D0%B9%D0%BD-%D1%81%D1%83%D1%80%D0%B3%D1%83%D1%83%D0%BB%D1%8C.jpg',
                groupNo: '',
                groupName: ''
            },
            student:{
                loginId: '',
                email: '',
                password: '',
                passComp: '',
                phone: '',
                lastname: '',
                name: '',
                image: '',
                schCode: '',
                groupNo: '',
                gender: '',
                addLogin: ''
            },
            clasgroupNo:'',
            songonoClassNo: '',
            chooseSongonoName: '',
            chooseSongonoId: ''
        },
        computed: {
            paginate: function () {
                if (!this.studentlist || this.studentlist.length != this.studentlist.length) {
                    return
                }
                this.resultCount = this.studentlist.length
                if (this.studentPage >= this.pages) {
                    this.studentPage = this.pages
                }
                var index = this.studentPage * this.itemsPerPage - this.itemsPerPage
                return this.studentlist.slice(index, index + this.itemsPerPage)
            }
        },

        created() {
            if (typeof this.$route.query.schCode != 'undefined') {
                let schCode = this.$route.query.schCode;
                this.schCode = schCode
                this.loaddata()
                this.schoolNameTake()
            } else {
                alert("School id undefined")
            }
        },
        methods: {
            classAddModal(){
                this.create.schName = ''
                this.create.schCode = ''
                this.create.groupName = ''
                this.clasgroupNo = ''
                window.location.href = "#classmodal"
            },
            saveClass(){
                try {
                    if (this.create.groupName != '' && this.clasgroupNo != '')
                    {
                        var language =  "<%=pageContext.getResponse().getLocale()%>"
                        var locale = ''
                        if (language == 'kr')
                        {
                            locale = 'kr-KR'
                        }
                        if(language == 'mn')
                        {
                            locale = 'mn-MN'
                        }
                        if(language == 'en'){
                            locale = 'en-EN'
                        }
                        const headers = {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                            'Accept-Language' : locale
                        }
                        if(this.clasgroupNo.length != 3){
                            alert("Class code must be 3 character!")
                            return;
                        }

                        var _this = this;

                        let item = {
                            groupNo: this.clasgroupNo,
                            groupName: this.create.groupName,
                            schCode: '${sessionScope.S_SCHCODE}'
                        };
                        axios.post('${BASEURL}/kexam/addnewclass', item, {
                            headers: headers
                        }).then(function (response) {
                            if (response.data.status == 200) {
                                alert(response.data.message);
                                window.location.href = "#close"
                                _this.loaddata(_this.studentPage);
                            } else {
                                alert(response.data.message);
                            }
                        }, (error) => {
                            console.log(error);
                            alert('Error')
                        });
                    }
                    else{
                        if (this.create.groupName == '')
                        {
                            alert("<spring:message code="putClassName"/>")
                        }
                        else{
                            alert("<spring:message code="putClassId"/>")
                        }
                    }

                } catch (error) {
                    console.log(error)
                }
            },
            newPages(link, schCode, groupNo){
                window.location.href = "${HOME}/list/" + link + '?schCode=' + schCode + '&groupNo=' + groupNo
            },
            previewFileExcel(event) {
                this.excel.excelFile = event.target.files[0];
                this.excel.fileName = event.target.files[0].name;
            },
            loading(cmd) {
                var l = document.getElementById("se-pre-con");
                l.style.display = cmd;
            },
            userExcel() {
                try {
                    let formData = new FormData()

                    if (this.excel.fileName == '') {
                        alert("choose excel file")
                        return;
                    }

                    if (this.excel.schCode == '') {
                        alert("chooose School Code")
                        return;
                    }

                    if (this.excel.groupNo == '') {
                        alert("chooose class id")
                        return;
                    }

                    formData.append('efile', this.excel.excelFile);
                    formData.append('schCode', this.excel.schCode);
                    formData.append('groupNo', this.excel.groupNo);

                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    var _this = this
                    _this.loading('block')
                    _this.excelModal = false
                    axios.post('${BASEURL}/create/excel/users', formData,
                        {headers: headers}).then((response) => {
                        if (response.data.status === 200) {
                            _this.loading('none')
                            alert(response.data.message)
                            _this.excel.excelFile = null;
                            _this.excel.fileName = "";
                            window.location.reload()
                            _this.clearExcelData

                        } else {
                            _this.loading('none')
                            alert(response.data.message)
                        }
                    }, (error) => {
                        _this.loading('none')
                        alert(error)
                    });


                } catch (error) {
                    console.log(error)
                    alert(error)
                    this.loading('none')
                }
            },
            clearExcelData(){
                this.excel.fileName = ""
                this.excel.fileName = ""
                this.excel.groupNo = ""
                this.excel.schCode = ""
            },
            excelClose(schCode, groupNo) {
                this.excel.schCode = schCode
                this.excel.groupNo = groupNo
                if (!this.excelModal) {
                    document.body.style.position = 'fixed';
                } else {
                    document.body.style.position = 'relative';
                }
                this.excelModal = !this.excelModal;
            },
            popupClose(schCode, groupNo) {
                this.student.schCode = schCode
                this.student.groupNo = groupNo
                this.student.password = ''
                this.student.loginId = groupNo
                if (!this.userRegisterModal) {
                    document.body.style.position = 'fixed';
                } else {
                    document.body.style.position = 'relative';
                }
                this.userRegisterModal = !this.userRegisterModal;
            },
            schoolNameTake() {
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this;
                    _this.loading('block')
                    axios.get('${BASEURL}/school/detail/'+this.schCode,
                        {
                            params:{},
                            headers: headers
                        }).then((response) => {
                        if (response.status == 200) {
                            _this.loading('none')
                            _this.schoolName = response.data.name
                        } else {
                            _this.loading('none')
                            alert("ERROR: ")
                        }
                    }, (error) => {
                        _this.loading('none')
                        console.log(error);
                    });
                } catch (error) {
                    console.log(error)
                }
            },

            loaddata() {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }
                    let item = {
                        groupName: this.groupName,
                        schCode: this.schCode,
                    }
                    var _this = this

                    axios.get('${BASEURL}/kexam/school/classes', {
                        params: item,
                        headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.classlist = response.data
                        } else {
                            alert(response.data.message);
                        }
                    }, (error) => {
                        console.log(error);
                        alert('Error')
                    });

                } catch (error) {
                    console.log(error)
                    alert(error)
                }
            },
            searchFilter() {
                this.loaddata();
            },
            defaultSearch() {
                window.location.reload()
            },
        }
    });
    app.$mount('#classlistvue')
</script>
</body>
</html>

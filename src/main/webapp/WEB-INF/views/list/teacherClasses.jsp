<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <title>Class list</title>
    <link rel="stylesheet" href="${CSS}/common/header.css">
    <link rel="stylesheet" href="${CSS}/common/footer.css">
    <link rel="stylesheet" href="${CSS}/my/show.css">
    <link rel="stylesheet" href="${CSS}/question_registration/register_information.css">

    <script src="${JS}/vue/vue-router.js"></script>
    <style>
        .displaydiv {
            display: block !important;
        }

        table {
            width: 100%;
        }

        .btn-red {
            width: 120px;
        }

        .btn-red-line {
            height: 34px;
            width: 120px;
            margin-right: 15px;
        }

        .search-wrap {
            float: right;
            margin-top: 15px;
            margin-bottom: 15px;
        }
        .btn-line-red{
            padding: 2px;
            color: #fb5f33;
            font-size: 17px;
            border-bottom: 1px solid;
        }
        .modifyinput{
            width: 100%;
            border: 1px solid #eee;
            padding: 5px;
            border-radius: 5px;
            outline: none;
            height: 40px;
            margin-top: 20px !important;
        }
        .modalDialog{
            overflow: auto
        }
        #se-pre-con {
            position: fixed;
            -webkit-transition: opacity 5s ease-in-out;
            -moz-transition: opacity 5s ease-in-out;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
        }
    </style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="teacherClassList">
    <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
        <div class="breadcrumb">
            <span><a href="../main.jsp"><spring:message code="all.home"/></a></span>
            <span><a href=""><spring:message code="all.user.register"/></a></span>
            <span><a href="../my/pc_certification.jsp"><spring:message code="all.student"/></a></span>
        </div>
        <h2 style="margin-bottom: 10px"><spring:message code="class.list"/></h2>
        <div class="table-wrap certi-table">
            <table>
                <colgroup>
                    <col style="width : 100px;">
                    <col style="width : 150px;">
                    <%--                    <col style="width : 100px;">--%>
                    <%--                    <col style="width : 150px;">--%>
                </colgroup>
                <tbody class="border-red-top-2 border-gray-bottom-2">
                    <tr>
                        <th><spring:message code="class"/></th>
                        <th>
                            <input type="text" class="" v-model="clasgroupNo" name="groupNameClasslist" autocorrect="off" spellcheck="false"
                                   id="groupNameClasslist"  autocomplete="off"/>
                        </th>
                    </tr>
                </tbody>
            </table>
            <div class="flex search-wrap">
                <button class="btn-red-line" @click="defaultSearch()">
                    <spring:message code="all.defaultSearch"/>
                </button>
                <button class="btn-red" @click="searchFilter()">
                    <spring:message code="all.search"/>
                </button>
            </div>
            <div class="table-wrap search-div" style="margin-top: 20px; margin-bottom: 20px;" v-if="'${sessionScope.S_ROLE}' == 'ROLE_MANAGER'">
                <button class="btn-red" @click="classAddModal()" style="width: 180px; height: 36px;">
                    <i class="plus"></i>
                    <spring:message code="classlist.schoolAdd"/>
                </button>
            </div>
        </div>
        <div class="table-wrap certi-table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   style="table-layout: auto;">
                <colgroup>
                    <col width="4%">
                    <col width="13%">
                    <col width="13%">
                    <col width="13%">
                    <col width="8%">
                    <col width="10%">
                    <col width="8%">
                </colgroup>
                <thead>
                <tr class="title_table border-red-top-2">
                    <th>
                        <span><spring:message code="all.number"/></span>
                    </th>
                    <th>
                        <span><spring:message code="classlist.schoolName"/></span>
                    </th>
                    <th>
                        <span><spring:message code="classlist.groupNo"/></span>
                    </th>
                    <th>
                        <span><spring:message code="classlist.studentCount"/></span>
                    </th>
                </tr>
                </thead>
                <tbody v-if="classlist.length > 0">
                <tr v-for="(item,index) in classlist" :key="index">
                    <td>
                        {{index+1}}
                    </td>
                    <td>
                        {{item.group_name}}
                    </td>
                    <td>
                        {{item.class_id}}
                    </td>
                    <td>
                        <a href="#" class="btn-line-red" v-if="item.scount != '0'"
                           @click="newPages('studentlist', item.class_id)">{{item.scount}}</a>
                        <span v-else>0</span>
                    </td>
                </tr>
                </tbody>
                <tbody v-else>
                <tr>
                    <td colspan="4">
                        <spring:message code="all.empty" />
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <%--    user register--%>
    <div v-show="studentsList" class="modalDialog" :class="{'displaydiv': studentsList}"
         style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <a href="javascript:void(0)" @click="popupClose()" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p style="font-weight: 800"><spring:message code="student.register"/></p>
                    </div>
                    <div class="modal-body">
                        <article class="flex">

                            <div class="detail">
                                <div class="password clear">
                                    <button type="button" @click="popupClose()" class="btn-gray shadow"
                                            style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                                        <spring:message code="all.close" /></button>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--    end user register--%>
</section>
<script type="text/javascript">
    let router = new VueRouter({
        mode: 'history',
        routes: []
    });
    let app = new Vue({
        router,
        data: {
            studentsList: false,
            classlist: [],
            clasgroupNo:''
        },
        computed: {
        },

        created() {
           this.loaddata()
        },
        methods: {
            newPages(link, groupNo){
                window.location.href = "${HOME}/list/" + link + '?schCode=' + '${sessionScope.S_SCHCODE}' + '&groupNo=' + groupNo
            },
            loading(cmd) {
                var l = document.getElementById("se-pre-con");
                l.style.display = cmd;
            },
            loaddata() {
                try {

                    var _this = this;


                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    _this.loading('block')
                    axios.get('${BASEURL}/kexam/useroradmin/classes', {
                            params: {
                                groupName: this.clasgroupNo,
                            },
                            headers: headers
                        }
                    ).then(function (response) {
                        _this.loading('none')
                        _this.classlist = response.data;
                    }, function (error) {
                        console.log(error);
                        _this.loading('none')
                    });

                } catch (error) {
                    console.log(error)
                    alert(error)
                    this.loading('none')
                }
            },
            searchFilter() {
                this.loaddata();
            },
            defaultSearch() {
                window.location.reload()
            },
        }
    });
    app.$mount('#teacherClassList')
</script>
</body>
</html>

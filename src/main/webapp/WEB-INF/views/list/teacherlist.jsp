<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <link rel="stylesheet" href="${CSS}/common/header.css">
    <link rel="stylesheet" href="${CSS}/common/footer.css">
    <link rel="stylesheet" href="${CSS}/notice/list.css">
    <link rel="stylesheet" href="${CSS}/my/show.css">
    <link rel="stylesheet" href="${CSS}/question_registration/register_information.css">

    <script src="${JS}/vue/vue-router.js"></script>
    <style>
        .displaydiv {
            display: block !important;
        }
        .examListDiv{
            display: block !important;
        }
        .btn-red{
            width: 120px;
        }
        .btn-red-line{
            height: 34px;
            width: 120px;
            margin-right: 15px;
        }
        .btn-line-red{
            padding: 2px;
            color: #fb5f33;
            font-size: 17px;
            border-bottom: 1px solid;
        }
        .positionFix{
            display: block;
        }
        .modalDialog{
            overflow: auto
        }
        #se-pre-con {
            position: fixed;
            -webkit-transition: opacity 5s ease-in-out;
            -moz-transition: opacity 5s ease-in-out;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
        }
    </style>
</head>
<body>

<div id="se-pre-con" style="display: none"></div>
<section id="teacherlist">
    <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
        <div class="breadcrumb">
            <span><a href="../main.jsp"><spring:message code="all.home"/></a></span>
            <span><a href=""><spring:message code="all.user.register"/></a></span>
            <span><a href="../my/pc_certification.jsp"><spring:message code="all.teacher"/></a></span>
        </div>
        <h2>{{schoolName}} <spring:message code="sschool"/> <spring:message code="teacher.list" /></h2>
        <div class="table-wrap">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <colgroup>
                    <col width="8%">
                    <col width="24%">
                    <col width="8%">
                    <col width="24%">
                </colgroup>
                <tbody>
                    <tr class="border-red-top-2 border-gray-bottom-2">
                        <td class="tit">
                            <label for="classinoutname"> <spring:message code="name"/></label>
                        </td>
                        <td>
                            <input v-model="name" class="classInput" autocomplete="off" id="classinoutname" value="">
                        </td>
                        <td class="tit">
                            <label for="teacherlistloginId">Нэвтрэх код</label>
                        </td>
                        <td>
                            <input v-model="loginId" class="classInput" id="teacherlistloginId" autocomplete="off" value="">
                        </td>

                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-wrap search-div" style="display: flex; float: right; margin-top: 20px; margin-bottom: 20px;">
            <button class="btn-red-line" @click="defaultSearch()">
                <spring:message code="schoollist.defaultSearch"/>
            </button>
            <button class="btn-red" @click="searchFilter()" style="height: 35px;">
                <spring:message code="schoollist.search"/>
            </button>
        </div>
        <div class="table-wrap search-div" style="margin-top: 20px; margin-bottom: 20px; display: flex;">

<%--            <input style="position: absolute; height: 34px; width: 212px; opacity: 0;" name="file" type="file"  v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN'">--%>
            <button type="button" class="btn-red-line shadow" style="width: 200px; height: 31px" @click="excelClose()"  v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN'">
                <spring:message code="default.excelUpload"/>
            </button>

            <button class="btn-red" @click="popupClose()" style="height: 35px; width: 160px; margin-right: 20px;">
                <i class="plus"></i><spring:message code="teacher.register"/>
            </button>
        </div>

<%--        <div class="my-certi flex flex-space-between">--%>


<%--            <button type="button" class="btn-red shadow" style="height: 37px"--%>
<%--                    @click="popupClose()">--%>
<%--                <i class="plus"></i>--%>
<%--                <spring:message code="teacher.register"/>--%>
<%--            </button>--%>
<%--        </div>--%>
        <div class="table-wrap certi-table">
            <table>
                <colgroup>
                    <col style="width : 4%">
                    <col style="width : 10%">
                    <col style="width : 6%">
                    <col style="width : 7%">
                    <col style="width : 7%">
                    <col style="width : 10%">
                    <col style="width : 10%">
                    <col style="width : 6%">
                    <col style="width : 6%">
                </colgroup>
                <thead class="border-red-top-2 border-gray-bottom-2">
                    <tr>
                        <th><spring:message code="all.number"/></th>
                        <th><spring:message code="name"/></th>
                        <th>А.тушаал</th>
                        <th><spring:message code="main.examCount"/></th>
                        <th><spring:message code="main.questionCount"/></th>
                        <th><spring:message code="user.loginID"/></th>
                        <th><spring:message code="login.pass"/></th>
                        <th><spring:message code="class"/></th>
                        <th>...</th>
                    </tr>
                </thead>
                <tbody v-if="teacherlist.length > 0">
                    <tr v-for="(item,index) in teacherlist">
                        <td>{{index+1}}</td>
                        <td>{{item.name}}</td>
                        <td>
                            <span v-if="item.role_id == 3">
                                <spring:message code="all.teacher" />
                            </span>
                            <span v-else>
                                <spring:message code="all.meneger" />
                            </span>
                        </td>
                        <td>
                            <span class="btn-line-red" @click="examListClose(item.login_id)">{{item.ecount}}</span>
<%--                            <span>{{item.ecount}}</span>--%>
                        </td>
                        <td>{{item.qcount}}</td>
                        <td>{{item.login_id}}</td>
                        <td>{{item.job_code}}</td>
                        <td>
                            <a href="#" class="btn-line-red" @click="classListSee(item.sch_code, item.id)">
                                {{item.tclasses.length}}</a>
                        </td>
                        <td>
                            <button type="button" class="btn-red-line" @click="updateUserModal(item.id)">Edit</button>
                        </td>
                    </tr>
                </tbody>
                <tbody v-else>
                <tr>
                    <td colspan="8">
                        <spring:message code="all.empty" />
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="pagination">
            <button class="start" @click="firstQuestion()"><img src="${IMG}/icons/start.png" class="auto"></button>
            <button class="prev" @click="prevQuestion()"><img src="${IMG}/icons/prev.png" class="auto"></button>
            <ul class="paginate-list">
                <li class="page-num " v-for="(item,index) in pages " :key="index"
                    :class="{'active': currentPage === item}"
                    v-if="Math.abs(item - currentPage) < 3 || item == pages || item == 1">

                    <a href="javascript:void(0);" @click="questionCurrent(item)" :class="{
					last: (item == pages && Math.abs(item - currentPage) > 3),
					first: (item == 1 && Math.abs(item - currentPage) > 3)}">{{item}}</a>
                </li>
            </ul>
            <button class="next" @click="nextQuestion()"><img src="${IMG}/icons/next.png" class="auto"></button>
            <button class="end" @click="lastQuestion()"><img src="${IMG}/icons/end.png" class="auto"></button>
        </div>
    </div>

<%-- exam count teacher model --%>
    <div v-show="examListModal" class="modalDialog" :class="{'examListDiv': examListModal}"
         style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <a href="javascript:void(0)" @click="examListClose()" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p style="font-weight: 800"><spring:message code="teacher.register"/></p>
                    </div>
                    <div class="modal-body">
                        <article class="flex">
                            <div class="detail">
<%--                                    <h3>** <spring:message code="user.require" />--%>
<%--                                    </h3>--%>
                                <div class="modal-table" style="max-height: 580px; padding: 0px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                           style="table-layout: auto;">
                                        <colgroup>
                                            <col width="4%">
                                            <col width="13%">
                                            <col width="13%">
                                            <col width="8%">
                                            <col width="8%">
                                            <col width="10%">
                                            <col width="9%">
                                        </colgroup>
                                        <thead>
                                        <tr class="title_table border-red-top-2">
                                            <th>№</th>
                                            <th>
                                                <span>Exam name</span>
                                            </th>
                                            <th>
                                                <span>Subject</span>
                                            </th>
                                            <th>
                                                <span>Questions</span>
                                            </th>
                                            <th>
                                                <span>Classes</span>
                                            </th>
                                            <th>
                                                <span>Exam gived</span>
                                            </th>
                                            <th>
                                                <span>Retaked</span>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody v-if="teacherExams.length > 0">
                                        <tr v-for="(item,index) in teacherExams" :key="index">
                                            <td>
                                                {{index+1}}
                                            </td>
                                            <td>
                                                {{item.exam_name}}
                                            </td>
                                            <td style="padding: 0px">
                                                {{item.exam_subject}}
                                            </td>
                                            <td>
                                                {{item.qcount}}
                                            </td>
                                            <td>
                                                {{item.ccount}}
                                            </td>
                                            <td>
                                                {{item.firstsubmit}}
                                            </td>
                                            <td>
                                                {{item.lastsubmit}}
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody  v-else>
                                        <tr>
                                            <td colspan="7">
                                                <spring:message code="all.empty" />
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="password clear">
                                    <button type="button" @click="examListClose()" class="btn-gray shadow"
                                            style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                                        <spring:message code="all.close" /></button>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
<%--    end exam count teacher model--%>
    <div v-show="userRegisterModal" class="modalDialog" :class="{'positionFix': userRegisterModal}"
         style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <a href="javascript:void(0)" @click="popupClose()" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p style="font-weight: 800"><spring:message code="teacher.register"/></p>
                    </div>
                    <div class="modal-body">
                        <article class="flex">
                            <div class="profile" style="margin-top: 20px; padding-left: 50%;">
                                <div class="image"><img src="${IMG}/icons/profile.png" class="auto"></div>
                                <%--                                <div class="update-btn">--%>
                                <%--                                    <button onclick="location.href='./update.jsp';">--%>
                                <%--                                        <spring:message code="save" />--%>
                                <%--                                    </button>--%>
                                <%--                                </div>--%>
                            </div>
                            <div class="detail">
                                <div class="default">
                                    <h3>** <spring:message code="user.require" />
                                    </h3>
                                    <ul class="flex">
                                        <li>
                                            <div class="title"><spring:message code="LastName"/> **
                                                <spring:message code="LastName" var="lasts"/></div>
                                            <input type="text" class="desc" placeholder="${lasts}" v-model="teacher.lastName" name="lastlast"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="FirstName"/> **
                                                <spring:message code="FirstName" var="firsts"/></div>
                                            <input type="text" class="desc" placeholder="${firsts}" v-model="teacher.name" name="stuname"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="email"/></div>
                                            <spring:message code="email" var="emails"/>
                                            <input type="text" class="desc" placeholder="${emails}" v-model="teacher.email" name="stuemail"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.loginID"/> **</div>
                                            <spring:message code="user.loginID" var="loginID"/>
                                            <div class="flex">
                                                <input type="text" class="desc" placeholder="loginID" v-model="teacher.loginId" name="stulogin" disabled style="width: 70%"/>
                                                <input type="number" placeholder="XXX" v-model="teacher.addLogin" style="width: 25%">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.password"/>**</div>
                                            <spring:message code="user.password" var="password"/>
                                            <input type="password" class="desc" placeholder="${password}" v-model="teacher.password" name="stupass"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.passComp"/>**</div>
                                            <spring:message code="user.passComp" var="passComp"/>
                                            <input type="password" class="desc" placeholder="${passComp}" v-model="teacher.passComp" name="stucomp"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="phonenumber"/></div>
                                            <spring:message code="phonenumber" var="phonenumber"/>
                                            <input type="number" class="desc" placeholder="${phonenumber}" v-model="teacher.phone" name="stuphone"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.schCode"/></div>
                                            <spring:message code="user.schCode" var="schCode"/>
                                            <input type="text" class="desc" placeholder="${schCode}" v-model="schCode" disabled/>
                                        </li>
<%--                                        <li>--%>
<%--                                            <div class="title"><spring:message code="user.classID"/></div>--%>
<%--                                            <spring:message code="user.classID" var="classID"/>--%>
<%--                                            <input type="text" class="desc" placeholder="${classID}" v-model="teacher.groupNo" disabled/>--%>
<%--                                        </li>--%>
                                        <li>
                                            <div class="title"><spring:message code="user.gender"/></div>
                                            <select class="wd-100" name="subclass" v-model="teacher.gender">
                                                <option value="" disabled="" selected=""><spring:message code="user.select"/></option>
                                                <option value="woman"><spring:message code="user.woman"/></option>
                                                <option value="man"><spring:message code="user.man"/></option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="password clear">
                                    <button type="button" class="btn-red shadow" @click="userRegister()" style="height: 39px;">
                                        <spring:message code="examlist.register" /></button>
                                    <button type="button" @click="popupClose()" class="btn-gray shadow"
                                            style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                                        <spring:message code="all.close" /></button>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--    excel register--%>
    <div v-show="excelModal" class="modalDialog" :class="{'displaydiv': excelModal}"
         style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <a href="javascript:void(0)" @click="excelClose()" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p style="font-weight: 800"><spring:message code="all.user.register"/></p>
                    </div>
                    <div class="modal-body">
                        <article class="flex">
                            <div class="detail">
                                <div class="default">
                                    <h3>** <spring:message code="user.require" />
                                    </h3>
                                    <ul class="flex">
                                        <li>
                                            <div class="title"><spring:message code="user.schCode"/>**</div>
                                            <spring:message code="user.schCode" var="schCode"/>
                                            <input type="text" class="desc" placeholder="${schCode}" v-model="schCode" disabled name="descks"/>
                                        </li>
<%--                                        <li>--%>
<%--                                            <div class="title"><spring:message code="user.classID"/>**</div>--%>
<%--                                            <spring:message code="user.classID" var="classID"/>--%>
<%--                                            <input type="text" class="desc" placeholder="${classID}" v-model="excel.groupNo" disabled name="grouNO"/>--%>
<%--                                        </li>--%>
                                    </ul>
                                </div>
                                <div class="form-box" style="text-align: center">
                                    <h3><spring:message code="default.excelUpload" /></h3>
                                    <div class="file" style="position: relative;">
                                        <p><spring:message code="question.register.fileUpload" /> (XLS, 10MB)</p>
                                        <%--<button class="btn-white shadow" onclick=";"><i class="download2"></i>양식 다운로드</button>--%>
                                        <button class="btn-white shadow" onclick="file_load(event);"
                                                style="margin-left: 40%; margin-top: 20px;  margin-bottom: 20px; width: 160px;">
                                            <i class="upload" style="margin-right: 15px;"></i>
                                            <spring:message code="default.excelUpload" />
                                        </button>
                                        <input style="display: none;" @change="previewFileExcel($event)" name="file" type="file">
                                        <p style="position: absolute;
    top: 0;
    left: 0;
    margin-top: 65px;
    margin-left: 30px;
    font-weight: 500;
    color: #000;">{{excel.fileName}}</p>
                                    </div>
                                </div>
                                <div class="password clear">
                                    <button type="button" class="btn-red shadow" @click="userExcel()"
                                            style="height: 39px;">
                                        <spring:message code="examlist.register" /></button>
                                    <button type="button" @click="excelModal = !excelModal" class="btn-gray shadow"
                                            style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                                        <spring:message code="all.close" /></button>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--    end excel register--%>
<%--class see model--%>
    <div id="classSeeModal" class="modalDialog">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container" style="width: 800px">
                    <a href="#close" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p> <spring:message code="classlist.schoolAdd" /></p>
                    </div>
                    <div class="modal-body" style="height: 580px; overflow: auto; ">
                        <div class="modal-table" style="max-height: 580px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                   style="table-layout: auto;">
                                <colgroup>
                                    <col width="4%">
                                    <col width="4%">
                                    <col width="15%">
                                    <col width="15%">
                                    <col width="15%">
                                </colgroup>
                                <thead>
                                <tr class="title_table border-red-top-2">
                                    <th></th>
                                    <th>
                                        <span><spring:message code="all.number"/></span>
                                    </th>
                                    <th>
                                        <span><spring:message code="classlist.schoolName"/></span>
                                    </th>
                                    <th>
                                        <span><spring:message code="classlist.schoolID"/></span>
                                    </th>
                                    <th>
                                        <span><spring:message code="classlist.teacherCount"/></span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody v-if="classList.length > 0">
                                    <tr v-for="(item,index) in classList" :key="index">
                                        <td>
                                            <input v-if="item.checked" style="width: 30px; height: 30px;
                                                -webkit-appearance: checkbox;" type="checkbox"
                                                   v-model="item.checked" value="true"/>
                                            <input v-else="item.checked" style="width: 30px; height: 30px;
                                                -webkit-appearance: checkbox;" type="checkbox"
                                                   v-model="item.checked" value="false"/>
                                        </td>
                                        <td>
                                            {{index+1}}
                                        </td>
                                        <td>
                                            {{item.group_name}}
                                        </td>
                                        <td>
                                            {{item.class_id}}
                                        </td>
                                        <td>
                                            {{item.stcount}}
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody  v-else>
                                    <tr>
                                        <td colspan="3">
                                            <spring:message code="all.empty" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer flex">
                        <a href="#close" class="modal-default-button" type="button">
                            <spring:message code="all.close"/>
                        </a>
                        <button class="modal-default-button margin-left" type="button" @click="teacherToClass()">
                            <spring:message code="schoollist.save"/>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<%--    end class model --%>
</section>
<script type="text/javascript">
    let router = new VueRouter({
        mode: 'history',
        routes: []
    });
    let app = new Vue({
        router,
        data: {
            userRegisterModal: false,
            excelModal: false,
            examListModal: false,
            teacherlist: [],
            classList: [],
            teacherExams: [],
            currentPage: 1,
            pages: 0,
            itemsPerPage: 1,
            totalElement: 0,
            schoolName: '',
            loginId: '',
            name: '',
            groupName: '',
            teacherId: '',
            excel:{
                fileName: '',
                excelFile: null,
            },
            teacher:{
                loginId: '',
                email: '',
                password: '',
                passComp: '',
                phone: '',
                name: '',
                image: '',
                lastName:'',
                schCode: '',
                groupNo: '',
                addLogin: ''
            },
            searchIdfalse: false
        },
        computed: {
            paginate: function () {
                if (!this.teacherlist || this.teacherlist.length != this.teacherlist.length) {
                    return
                }
                this.resultCount = this.teacherlist.length
                if (this.currentPage >= this.pages) {
                    this.currentPage = this.pages
                }
                var index = this.currentPage * this.itemsPerPage - this.itemsPerPage
                return this.teacherlist.slice(index, index + this.itemsPerPage)
            }
        },

        created() {
            if (typeof this.$route.query.schCode != 'undefined') {
                let schCode = this.$route.query.schCode;
                this.schCode = schCode

                this.loaddata(this.currentPage)
                this.schoolNameTake()
            } else {
                alert("School id undefined")
            }
        },
        methods: {
            loading(cmd) {
                var l = document.getElementById("se-pre-con");
                l.style.display = cmd;
            },
            previewFileExcel(event) {
                this.excel.excelFile = event.target.files[0];
                this.excel.fileName = event.target.files[0].name;
            },
            clearUserData(){
                this.teacher.email = ""
                this.teacher.lastName = ""
                this.teacher.loginId = ""
                this.teacher.name = ""
                this.teacher.password = ""
                this.teacher.phone = ""
                this.teacher.role = ""
                // this.teacher.schCode = ""
                this.teacher.gender = ''
            },
            realNew(ligon){
                  try{
                      var language =  "<%=pageContext.getResponse().getLocale()%>"
                      var locale = ''

                      if (language == 'kr')
                      {
                          locale = 'kr-KR'
                      }
                      if(language == 'mn')
                      {
                          locale = 'mn-MN'
                      }
                      if(language == 'en'){
                          locale = 'en-EN'
                      }
                      const headers = {
                          'Content-Type': 'application/json',
                          'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                          'Accept-Language' : locale
                      }
                      let item = {
                          uId: ligon,
                      }
                      var _this = this

                      axios.get('${BASEURL}/kexam/teacher/exams',{
                          params: item,
                          headers: headers
                      }).then(function (response) {
                          if (response.status == 200) {
                              _this.teacherExams = response.data.result.exams
                              console.log("teacherExamsteacherExams")
                              console.log(_this.teacherExams)
                          } else {
                              console.log(response.data.message);
                          }
                      },(error) => {
                          console.log(error);
                          alert('Error')
                      });
                  }
                  catch (error) {
                      console.log(error)
                  }
            },
            popupClose() {
                this.teacher.password = ''
                this.teacher.loginId = this.schCode + 'T'

                if (!this.userRegisterModal) {
                    document.body.style.position = 'fixed';
                } else {
                    document.body.style.position = 'relative';
                }
                this.userRegisterModal = !this.userRegisterModal;
            },
            examListClose(item){
                if (!this.examListModal) {
                    this.realNew(item)
                    document.body.style.position = 'fixed';
                } else {
                    document.body.style.position = 'relative';
                }
                this.examListModal = !this.examListModal;
            },
            excelClose() {
                if (!this.excelModal) {
                    document.body.style.position = 'fixed';
                } else {
                    document.body.style.position = 'relative';
                }
                this.excelModal = !this.excelModal;
            },
            updateUserModal(id) {
                window.location.href = "${HOME}/my/update?id=" + id
            },
            userExcel() {
                try {
                    let formData = new FormData()

                    if (this.excel.fileName == '') {
                        alert("choose excel file")
                        return;
                    }

                    if (this.excel.schCode == '') {
                        alert("chooose School Code")
                        return;
                    }

                    formData.append('efile', this.excel.excelFile);
                    formData.append('schCode', this.schCode);
                    formData.append('groupNo', '');

                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this
                    _this.loading('block')
                    _this.excelModal = false
                    axios.post('${BASEURL}/create/excel/users', formData,
                        {headers: headers}).then((response) => {
                        if (response.data.status === 200) {
                            alert(response.data.message)
                            _this.excel.excelFile = null;
                            _this.excel.fileName = "";
                            document.body.style.position = 'relative'
                            _this.loaddata()
                            // window.location.reload()
                            _this.clearExcelData
                            _this.loading('none')
                        } else {
                            _this.loading('none')
                            alert(response.data.message)
                        }
                    }, (error) => {
                        _this.loading('none')
                        alert(error)
                    });
                } catch (error) {
                    console.log(error)
                    alert(error)
                }
            },
            teacherToClass(){
                try {
                    let arrays = []

                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }

                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    for(let i=0; i < this.classList.length; i++){
                        if (this.classList[i].checked)
                        {
                            arrays.push(this.classList[i].class_id)
                        }
                    }
                    var _this = this

                    axios.post('${BASEURL}/kexam/put/user/classes',{
                        uId: this.teacherId,
                        classes: arrays
                    },{headers:headers}).then(function (response) {
                        if (response.data.status == 200) {
                            alert(response.data.message)
                            window.location.href = '#close'
                            _this.loaddata(_this.currentPage)
                        } else {
                            console.log(response.data.message);
                        }
                    },(error) => {
                        console.log(error);
                        alert('Error')
                    });

                } catch (error) {
                    console.log(error)
                    alert(error)
                }
            },
            classListSee(schoolCode, teacherId){
                try {
                    this.teacherId = teacherId
                    var localTeacher = []
                    for (let j=0; j < this.teacherlist.length; j++) {
                        if (this.teacherlist[j].id == teacherId) {
                            localTeacher = this.teacherlist[j].tclasses
                        }
                    }
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    let item = {
                        groupName: this.groupName,
                        schCode: schoolCode,
                    }
                    var _this = this

                    axios.get('${BASEURL}/kexam/school/classes',{
                        params: item,
                        headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.classList = response.data
                            for (let i=0; i < _this.classList.length; i++)
                            {
                                _this.classList[i].checked = false
                                if (localTeacher.length > 0)
                                {
                                    for (let q=0; q<localTeacher.length; q++)
                                    {
                                        if (localTeacher[q].class_id == _this.classList[i].class_id){
                                            _this.classList[i].checked = true
                                        }
                                    }
                                }

                            }

                            window.location.href = "#classSeeModal"
                        } else {
                            console.log(response.data.message);
                        }
                    },(error) => {
                        console.log(error);
                        alert('Error')
                    });

                } catch (error) {
                    console.log(error)
                    alert(error)
                }
            },
            schoolNameTake() {
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this;
                    _this.loading('block')
                    axios.get('${BASEURL}/school/detail/'+this.schCode,
                        {
                            params:{},
                            headers: headers
                        }).then((response) => {
                        if (response.status == 200) {
                            _this.loading('none')
                            _this.schoolName = response.data.name
                        } else {
                            _this.loading('none')
                            alert("ERROR: ")
                        }
                    }, (error) => {
                        _this.loading('none')
                        console.log(error);
                    });
                } catch (error) {
                    console.log(error)
                        this.loading('none')
                }
            },
            loaddata(page) {
                try {
                    if (page > 0) {
                        page = page - 1
                    }
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    if (!this.searchIdfalse)
                    {
                        this.loginId = ''
                    }

                    var _this = this;
                    _this.loading('block')
                    axios.post('${BASEURL}/teachers',
                        {
                            page : page,
                            schCode: this.schCode,
                            groupNo: this.groupNo,
                            sort: '',
                            name: this.name,
                            loginId: this.loginId,
                            sortfield: '',
                            role: ''
                        },
                        {headers: headers}).then((response) => {
                        if (response.status == 200) {
                            _this.teacherlist = response.data.content;
                            _this.pages = response.data.totalPages
                            _this.totalElement = response.data.totalElements
                            _this.loading('none')
                        } else {
                            _this.loading('none')
                            alert("ERROR: ")
                        }
                    }, (error) => {
                        _this.loading('none')
                        console.log(error);
                    });
                } catch (error) {
                    console.log(error)
                        this.loading('none')
                }
            },
            userRegister(){
                try {
                    if ( this.teacher.password != '')
                    {
                        if (this.teacher.password.length > 3)
                        {
                            if (this.teacher.password == this.teacher.passComp)
                            {
                                if (this.teacher.email == '' && this.teacher.loginId == ''
                                    && this.teacher.password == ''&& this.teacher.passComp == '')
                                {
                                    alert("<spring:message code="user.alertRequire"/>")
                                }
                                else{
                                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                                    var locale = ''
                                    if (language == 'kr')
                                    {
                                        locale = 'kr-KR'
                                    }
                                    if(language == 'mn')
                                    {
                                        locale = 'mn-MN'
                                    }
                                    if(language == 'en'){
                                        locale = 'en-EN'
                                    }

                                    const headers = {
                                        'Content-Type': 'application/json',
                                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                                        'Accept-Language' : locale
                                    }
                                    var _this = this;
                                    if (this.teacher.addLogin.length > 3)
                                    {
                                        alert("Нэвтрэх код хамгийн ихдээ 3 тоо оруулна уу.")
                                        return;
                                    }
                                    if (this.teacher.addLogin.length < 1)
                                    {
                                        alert("Нэвтрэх кодыг бичнэ үү.")
                                        return;
                                    }

                                    var newLog = this.teacher.loginId + this.teacher.addLogin

                                    let item = {
                                        // image : '',
                                        loginId : newLog,
                                        email : this.teacher.email,
                                        password : this.teacher.password,
                                        phone : this.teacher.phone,
                                        name : this.teacher.name,
                                        lastName : this.teacher.lastName,
                                        schCode : this.schCode,
                                        groupNo : '',
                                        role : 'teacher',
                                        gender: this.teacher.gender
                                    };
                                    axios.post('${BASEURL}/create/user', item, {
                                        headers: headers
                                    }).then(function (response) {
                                        if (response.data.status == 200) {
                                            if (response.data.success== true)
                                            {
                                                alert(response.data.message)
                                                _this.popupClose()
                                                _this.clearUserData()
                                            }
                                            else{
                                                alert(response.data.message)

                                            }
                                        } else {
                                            alert(response.data.message);
                                        }
                                    }, (error) => {
                                        console.log(error);
                                        alert(response.data.message);

                                    });
                                }
                            }
                            else{
                                alert("Нууц үгээ зөв оруулна уу.");
                                return
                            }
                        }
                        else
                        {
                            alert("Нууц үг багадаа 3 дээш тэмдэгт байна.")
                            return
                        }
                    }
                    else{
                        alert("Та нууц үгээ оруулна уу.")
                        return
                    }

                } catch (error) {
                    console.log(error)
                }
            },
            nextQuestion() {
                if (this.pages > this.currentPage) {
                    this.currentPage = this.currentPage + 1
                    this.loaddata(this.currentPage)
                }
            },
            questionCurrent(item) {
                this.currentPage = item
                this.loaddata(this.currentPage)
            },
            prevQuestion() {
                if (this.currentPage > 1) {
                    this.currentPage = this.currentPage - 1
                    this.loaddata(this.currentPage)
                }
            },
            lastQuestion() {
                this.currentPage = this.pages
                this.loaddata(this.currentPage)
            },
            firstQuestion() {
                this.currentPage = 1
                this.loaddata(this.currentPage)
            },
            searchFilter() {
                this.searchIdfalse = true
                this.loaddata(this.currentPage);
            },
            defaultSearch() {
                window.location.reload()
            },
        }
    });
    app.$mount('#teacherlist')
</script>
</body>
</html>

<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
  <title>Student list</title>
  <link rel="stylesheet" href="${CSS}/common/header.css">
  <link rel="stylesheet" href="${CSS}/common/footer.css">
  <link rel="stylesheet" href="${CSS}/my/show.css">
  <link rel="stylesheet" href="${CSS}/question_registration/register_information.css">

  <script src="${JS}/vue/vue-router.js"></script>
  <style>
    .displaydiv {
      display: block !important;
    }
    table{
      width: 100%;
    }
    .btn-red{
      width: 120px;
    }
    .btn-red-line{
      height: 34px;
      width: 120px;
      margin-right: 15px;
    }
    .search-wrap{
      float: right;
      margin-top: 15px;
      margin-bottom: 15px;
    }
    .btn-red-delete{
      height: 31px;
      font-size: 12px;
      width: 60px;
      color: #fff;
      background-color: #fb5f33;
      border: 0;
      border-radius: 2px;
    }
    p.warning{
      color: #000;
      margin: 10px;
      font-size: 14px;
      font-weight: 900;
    }
    span.spanwar{
      font-size: 15px;
      margin-left: 10px;
      font-weight: 900;
    }
    .spanwar span{
      color: #fb5f33;
    }
    #se-pre-con {
      position: fixed;
      -webkit-transition: opacity 5s ease-in-out;
      -moz-transition: opacity 5s ease-in-out;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
    }
  </style>
</head>
<body>

<div id="se-pre-con" style="display: none"></div>
<section id="studentlist">
  <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
    <div class="breadcrumb">
      <span><a href=""><spring:message code="all.home"/></a></span>
      <span><a href=""><spring:message code="all.user.register"/></a></span>
      <span><a href=""><spring:message code="all.student"/></a></span>
    </div>
    <h2 style="margin-bottom: 10px">{{schoolName}} <spring:message code="sschool" />
      <span v-if="groupNoFalse == true">{{groupNo}}  <spring:message code="class" /></span>
      <spring:message code="student.list" /></h2>
    <div class="table-wrap">
      <table>
        <colgroup>
          <col style="width : 100px;" v-if="groupNoFalse == false">
          <col style="width : 150px;" v-if="groupNoFalse == false">
          <col style="width : 100px;">
          <col style="width : 150px;">
          <col style="width : 100px;">
          <col style="width : 150px;">
        </colgroup>
        <tbody class="border-red-top-2 border-gray-bottom-2">
        <th v-if="groupNoFalse == false"><spring:message code="classlist.groupNo"/></th>
        <th v-if="groupNoFalse == false">
          <input type="text" class="" v-model="groupNo" autocomplete="off" id="studentgroupNo"/>
        </th>
        <th>
          <label for="namenameSearch"><spring:message code="name"/></label>
        </th>
        <th>
          <input type="text" class="" v-model="name" id="namenameSearch" autocomplete="off" value="" class="classInput"
                 autocorrect="false"/>
        </th>
        <th>
          <label for="loginCodeSearch">Нэвтрэх код</label>
        </th>
        <th>
          <input type="text" v-model="searchlogin" value="" class="classInput" id="loginCodeSearch" autocomplete="off"/>
        </th>
        </tbody>
      </table>
      <div class="flex search-wrap" style="float: left;" v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_ROLE}'=='ROLE_MANAGER'">
        <button class="btn-red-line" @click="songonoClose()">
          <spring:message code="default.studentadd"/></button>

        <button class="btn-red" @click="popupClose()" v-if="groupNoFalse == true">
          <spring:message code="default.studentregister"/></button>

      </div>
      <div class="flex search-wrap">
        <button class="btn-red-line" @click="defaultSearch()">
          <spring:message code="all.defaultSearch"/>
        </button>
        <button class="btn-red" @click="searchFilter()">
          <spring:message code="all.search" />
        </button>
      </div>
    </div>
    <div class="table-wrap certi-table">
      <table>
        <colgroup>
          <col style="width : 70px;">
          <col style="width : 100px;">
          <col style="width : 150px;">
          <col style="width : 150px;">
          <col style="width : 150px;">
          <col style="width : 160px;" v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_ROLE}'=='ROLE_MANAGER'">
        </colgroup>
        <thead class="border-red-top-2 border-gray-bottom-2">
        <tr>
          <th><spring:message code="all.number"/></th>
          <th><spring:message code="class"/></th>
          <th><spring:message code="name"/></th>
          <th><spring:message code="user.loginID"/></th>
          <th><spring:message code="login.pass"/></th>
          <th v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_ROLE}'=='ROLE_MANAGER'">...</th>
        </tr>
        </thead>
        <tbody v-if="studentlist.length > 0">
        <tr v-for="(item,index) in studentlist">
          <td>{{index+1}}</td>
          <td>{{item.group_name}}</td>
          <td>{{item.name}}</td>
          <td>{{item.login_id}}</td>
          <td>{{item.job_code}}</td>
          <td v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_ROLE}'=='ROLE_MANAGER'">
            <div class="flex" style="padding-left: 20px;">
              <button type="button" class="btn-red-line" @click="updateUserModal(item.id)">Details</button>
              <button type="button" class="btn-red-delete" @click="deleteClose(item.id, item.name, item.login_id)"><spring:message code="removeUser" /></button>
              <button type="button" class="btn-red-line" @click="kickoutModal(item.id)" style="margin-left: 10px;">Гаргах</button>
            </div>
          </td>
        </tr>
        </tbody>
        <tbody v-else>
        <tr>
          <td colspan="7">
            <spring:message code="all.empty" />
          </td>
        </tr>
        </tbody>
      </table>
    </div>

    <div class="pagination">
      <button class="start" @click="firstQuestion()"><img src="${IMG}/icons/start.png" class="auto"></button>
      <button class="prev" @click="prevQuestion()"><img src="${IMG}/icons/prev.png" class="auto"></button>
      <ul class="paginate-list">
        <li class="page-num " v-for="(item,index) in pages " :key="index"
            :class="{'active': currentPage === item}"
            v-if="Math.abs(item - currentPage) < 3 || item == pages || item == 1">

          <a href="javascript:void(0);" @click="questionCurrent(item)" :class="{
					last: (item == pages && Math.abs(item - currentPage) > 3),
					first: (item == 1 && Math.abs(item - currentPage) > 3)}">{{item}}</a>

        </li>
      </ul>
      <button class="next" @click="nextQuestion()"><img src="${IMG}/icons/next.png" class="auto"></button>
      <button class="end" @click="lastQuestion()"><img src="${IMG}/icons/end.png" class="auto"></button>
    </div>
  </div>

  <%--    user register--%>
  <div v-show="userRegisterModal" class="modalDialog" :class="{'displaydiv': userRegisterModal}"
       style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">
          <a href="javascript:void(0)" @click="popupClose()" title="modelclose" class="modelclose">X</a>
          <div class="modal-header">
            <p style="font-weight: 800"><spring:message code="student.register"/></p>
          </div>
          <div class="modal-body">
            <article class="flex">
              <div class="profile" style="margin-top: 20px; padding-left: 50%;">
                <div class="image"><img src="${IMG}/icons/profile.png" class="auto"></div>
              </div>
              <div class="detail">
                <div class="default">
                  <h3>** <spring:message code="user.require" />
                  </h3>
                  <ul class="flex">
                    <li>
                      <div class="title"><spring:message code="LastName"/> **
                        <spring:message code="LastName" var="LastNames"/></div>
                      <input type="text" class="desc" placeholder="${LastNames}" v-model="student.lastname" name="student.lastname"/>
                    </li>
                    <li>
                      <div class="title"><spring:message code="FirstName"/> **
                        <spring:message code="FirstName" var="firsts"/></div>
                      <input type="text" class="desc" placeholder="${firsts}" v-model="student.name" name="student.name"/>
                    </li>

                    <li>
                      <div class="title"><spring:message code="email"/></div>
                      <spring:message code="email" var="emails"/>
                      <input type="text" class="desc" placeholder="${emails}" v-model="student.email" name="student.email"/>
                    </li>
                    <li>
                      <div class="title"><spring:message code="user.loginID"/> **</div>
                      <spring:message code="user.loginID" var="loginID"/>
                      <div class="flex">
                        <input type="text" class="desc" placeholder="loginID" v-model="student.loginId" name="student.loginId" style="width: 70%" disabled/>
                        <input type="number" class="desc" placeholder="XXX" v-model="student.addLogin" name="student.addLogin" style="width: 25%"/>
                      </div>
                    </li>
                    <li>
                      <div class="title"><spring:message code="user.password"/>**</div>
                      <spring:message code="user.password" var="password"/>
                      <input type="password" class="desc" placeholder="${password}" v-model="student.password" name="student.password"
                             @keydown="nameKeydown($event)"/>
                    </li>
                    <li>
                      <div class="title"><spring:message code="user.passComp"/>**</div>
                      <spring:message code="user.passComp" var="passComp"/>
                      <input type="password" class="desc" placeholder="${passComp}" v-model="student.passComp" name="student.passComp"/>
                    </li>
                    <li>
                      <div class="title"><spring:message code="phonenumber"/></div>
                      <spring:message code="phonenumber" var="phonenumber"/>
                      <input type="text" class="desc" placeholder="${phonenumber}" v-model="student.phone" name="student.phone"/>
                    </li>
                    <li>
                      <div class="title"><spring:message code="user.schCode"/></div>
                      <spring:message code="user.schCode" var="schCode"/>
                      <input type="text" class="desc" placeholder="${schCode}" v-model="student.schCode" disabled/>
                    </li>
                    <li>
                      <div class="title"><spring:message code="user.classID"/></div>
                      <spring:message code="user.classID" var="classID"/>
                      <input type="text" class="desc" placeholder="${classID}" v-model="student.groupNo" disabled/>
                    </li>
                    <li>
                      <div class="title"><spring:message code="user.gender"/></div>
                      <select class="wd-100" name="subclass" v-model="student.gender">
                        <option value="" disabled="" selected=""><spring:message code="user.select"/></option>
                        <option value="0"><spring:message code="user.woman"/></option>
                        <option value="1"><spring:message code="user.man"/></option>
                      </select>
                    </li>
                  </ul>
                </div>
                <div class="password clear">
                  <button type="button" class="btn-red shadow" @click="userRegister()" style="height: 39px;">
                    <spring:message code="examlist.register" /></button>
                  <button type="button" @click="popupClose()" class="btn-gray shadow"
                          style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                    <spring:message code="all.close" /></button>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
  <%--    end user register--%>


  <%--    user delete modal --%>
  <div v-show="userDeleteModal" class="modalDialog" :class="{'displaydiv': userDeleteModal}"
       style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container" style="width: 500px">
          <a href="javascript:void(0)" @click="deleteClose()" title="modelclose" class="modelclose">X</a>
          <div class="modal-header">
            <p style="font-weight: 800"><spring:message code="removeUserConfirm"/></p>
          </div>
          <div class="modal-body">
            <article class="flex">
              <div class="detail" style=" padding-top: 20px;">
                <div style="margin-bottom: 20px;">
                  <div style="text-align: center">
                    <img src="${IMG}/etc/warning.png" style="width: 200px; margin: 0px 26px 26px 26px;">
                  </div>
                  <div style="text-align: center">
                    <span class="spanwar"><span><spring:message code="name"/></span> : {{deleteUserName}}</span>
                    <span class="spanwar"><span><spring:message code="user.loginID"/> </span>: {{deleteLoginId}} </span>
                  </div>
                  <div style="margin: 20px 0px 20px 0px;">
                    <p class="warning">1. Please check the name of the student you want to delete!</p>
                    <p class="warning">2. All test results given by the student together with the student <br>устахыг сануулж байна.</p>
                    <p class="warning">3. Please note that deleted results cannot be restored from the system.</p>
                  </div>
                </div>
                <div style="display: flex; text-align: center; margin-left: 10%;">
                  <button type="button" class="btn-red shadow" @click="userDelete()"
                          style="height: 39px; width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                    <spring:message code="removeUser" /></button>
                  <button type="button" @click="deleteClose()" class="btn-gray shadow"
                          style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                    <spring:message code="all.close" /></button>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
  <%--    end user delete modal--%>

  <%--    kickout modal--%>
  <div v-show="kickoutClose" class="modalDialog" :class="{'displaydiv': kickoutClose}"
       style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container" style="width: 500px">
          <a href="javascript:void(0)" @click="kickoutModal()" title="modelclose" class="modelclose">X</a>
          <div class="modal-header">
            <p style="font-weight: 800">
             Student get out of class
            </p>
          </div>
          <div class="modal-body">
            <article class="flex">
              <div class="detail" style=" padding-top: 20px;">
                <div style="margin-bottom: 20px;">
                  <div style="text-align: center">
                    <img src="${IMG}/etc/warning.png" style="width: 200px; margin: 0px 26px 26px 26px;">
                  </div>
                  <div style="margin: 20px 0px 20px 0px;">
                    <p class="warning">1. Please check your child's name and ID before you getout!</p>
                    <p class="warning">2. Are you sure you want to get these kids out of this class?</p>
                  </div>
                </div>
                <div style="display: flex; text-align: center; margin-left: 10%;">
                  <button type="button" class="btn-red shadow" @click="kickout()"
                          style="height: 39px; width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">Гаргах</button>
                  <button type="button" @click="kickoutModal()" class="btn-gray shadow"
                          style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                    <spring:message code="all.close" /></button>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
  <%--    kick out modal end--%>
  <%--    Сонгоний анги нэмэгдэх хүүхдүүд --%>
  <div v-show="userListModal" id="popup-modal" class="modalDialog" :class="{'displaydiv': userListModal}"
       style="opacity: 1;pointer-events: painted; overflow: scroll;background: rgba(0, 0, 0, 0.8);display: none">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">
          <a href="javascript:void(0)" @click="songonoClose()" title="modelclose" class="modelclose">X</a>
          <div class="modal-header">
            <p><spring:message code="additional.classSet" /></p>
          </div>
          <div class="modal-body">
            <%--                        <div class="modal-table" style="min-height: auto;">--%>
            <%--&lt;%&ndash;                            <p><span class="red">{{totalElement}}</span>&ndash;%&gt;--%>
            <%--&lt;%&ndash;                                <spring:message code="additional.examCount" /></p><br>&ndash;%&gt;--%>
            <%--                            <table width="100%" border="0" cellspacing="0" cellpadding="0">--%>
            <%--                                <colgroup>--%>
            <%--&lt;%&ndash;                                    <col width="20%">&ndash;%&gt;--%>
            <%--                                    <col width="50%">--%>
            <%--                                    <col width="50%">--%>
            <%--                                </colgroup>--%>
            <%--                                <thead>--%>
            <%--                                <tr class="title_table border-red-top-2">--%>
            <%--                                    <th>--%>
            <%--                                        <span><spring:message code="classlist.schoolName" /></span>--%>
            <%--                                    </th>--%>
            <%--                                    <th>--%>
            <%--                                        <span><spring:message code="classlist.schoolID" /></span>--%>
            <%--                                    </th>--%>
            <%--                                </tr>--%>
            <%--                                </thead>--%>
            <%--                                <tbody>--%>
            <%--                                    <tr>--%>
            <%--                                        <td>{{chooseSongonoName}}</td>--%>
            <%--                                        <td>{{chooseSongonoId}}</td>--%>
            <%--                                    </tr>--%>
            <%--                                </tbody>--%>
            <%--                            </table>--%>
            <%--                        </div>--%>
            <div class="modal-table" style="min-height: auto;">
              <div class="table-wrap">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <colgroup>
                    <col width="15%">
                    <col width="24%">
                    <col width="15%">
                    <col width="24%">
                  </colgroup>
                  <tbody>
                  <tr class="title_table border-red-top-2">
                    <td colspan="4" style="text-align: center">
                      <span><spring:message code="schoollist.search" /></span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <spring:message code="name" />
                    </td>
                    <td>
                      <input name="classsearch" v-model="searchclassname" v-on:keyup.enter="searchSongo()" style="width: 220px">
                    </td>
                    <td>
                      <spring:message code="user.loginID" />
                    </td>
                    <td>
                      <input name="classsearch" v-model="searchclassid" v-on:keyup.enter="searchSongo()" style="width: 220px">
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <spring:message code="classlist.search" />
                    </td>
                    <td colspan="3">
                      <select class="select2 select2-container select2-container--default"
                              style="width: 300px;" @change="searchSongo()"
                              v-model="songonoClassNo">
                        <template v-for="(item, index) in classlist">
                          <option :value="item.class_id" :key="index">{{item.group_name}}</option>
                        </template>
                      </select>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="modal-table" style="min-height: auto;padding: 0 20px;">
              <p style="float: left;padding: 10px 5px;color: rgb(251, 95, 51);border: 1px solid rgb(251, 95, 51);">
                <spring:message code="schoollist.studentCount" /> : (<span>{{totalElement2}}</span>)
              </p>
            </div>
            <div class="modal-table">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <colgroup>
                  <col width="5%">
                  <col width="30%">
                  <col width="30%">
                  <col width="30%">
                  <col width="20%">
                </colgroup>
                <thead>
                <tr class="title_table border-red-top-2">
                  <th>
                    <%--                                        <input type="checkbox" v-model="selectall" @change="checkallclass"--%>
                    <%--                                               id="checkall"--%>
                    <%--                                               style="width: 25px;height: 25px;--%>
                    <%--													-webkit-appearance:checkbox;"/>--%>
                  </th>
                  <th>
                    <span><spring:message code="lastname" /></span>
                  </th>

                  <th>
                    <span><spring:message code="name" /></span>
                  </th>
                  <th>
                    <span><spring:message code="user.classID" /></span>
                  </th>
                  <th>
                    <span><spring:message code="default.add"/></span>
                  </th>
                </tr>
                </thead>
                <tbody>
                <template v-if="studentlistadd.length > 0">
                  <tr v-for="(item,index) in studentlistadd" :key="index">
                    <td>
                      <%--                                            <input @change="changeItem"--%>
                      <%--                                                   style="width: 25px;height: 25px;-webkit-appearance:checkbox;"--%>
                      <%--                                                   type="checkbox" v-model="item.checked"/>--%>
                      <span>{{index+1}}</span>
                    </td>
                    <td class="t-left">{{item.last_name}}</td>
                    <%--<td><img width="35" height="35" :src="item.photoThumbnailUrl"/></td>--%>
                    <td class="t-left">{{item.name}}</td>
                    <td class="t-left">{{item.group_nums}}</td>
                    <td>
                      <button class="btn-red-line" @click="classUserAdd(item.id)"
                              style="display: initial; height: 17px; width: 63px;
                                                margin-right: 0px;" v-if="!item.checkedClass">
                        <spring:message code="default.add"/></button>
                    </td>
                  </tr>
                </template>
                <template v-else>
                  <tr>
                    <td colspan="5" style="height: 100px;text-align: center;">
                      <spring:message code="all.empty" />
                    </td>
                  </tr>
                </template>
                </tbody>
              </table>
            </div>
            <div class="pagination">
              <button class="start" @click="firstStudent()"><img src="${IMG}/icons/start.png" class="auto"></button>
              <button class="prev" @click="prevStudent()"><img src="${IMG}/icons/prev.png" class="auto"></button>
              <ul class="paginate-list">
                <li class="page-num " v-for="(items,indexs) in pagestudent " :key="indexs"
                    :class="{'active': studentPage === items}"
                    v-if="Math.abs(items - studentPage) < 3 || items == pagestudent || items == 1">

                  <a href="javascript:void(0);" @click="studentCurrent(items)" :class="{
                                        last: (items == pagestudent && Math.abs(items - studentPage) > 3),
                                        first: (items == 1 && Math.abs(items - studentPage) > 3)}">{{items}}</a>

                </li>
              </ul>
              <button class="next" @click="nextStudent()"><img src="${IMG}/icons/next.png" class="auto"></button>
              <button class="end" @click="lastStudent()"><img src="${IMG}/icons/end.png" class="auto"></button>
            </div>
          </div>
          <div class="modal-footer flex btn-wrap">
            <button type="button" @click="songonoClose()" class="btn-gray shadow"><spring:message code="all.close" /></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  let router = new VueRouter({
    mode: 'history',
    routes: []
  });
  let app = new Vue({
    router,
    data: {
      userRegisterModal: false,
      userListModal: false,
      userDeleteModal:false,
      studentlistadd: [],
      currentPage: 1,
      pages: 0,
      itemsPerPage: 1,
      totalElement: 0,
      studentlist: [],
      studentPage: 1,
      pagestudent: 0,
      itemsPerPage2: 1,
      totalElement2: 0,
      classlist: [],
      schoolName: '',
      groupNo: '',
      name: '',
      searchlogin: '',
      schgroupNo: '',
      student:{
        loginId: '',
        email: '',
        password: '',
        passComp: '',
        phone: '',
        lastname: '',
        name: '',
        image: '',
        schCode: '',
        groupNo: '',
        gender: '',
        addLogin: ''
      },
      clasgroupNo:'',
      songonoClassNo: '',
      groupName: '',
      userDeleteId:'',
      deleteLoginId: '',
      deleteUserName: '',
      searchclassname: '',
      searchclassid: '',
      groupNoFalse: false,
      searchIdfalse: false,
      kickoutClose: false,
      kickoutId: '',
      regUser: ''

    },
    computed: {
      paginate: function () {
        if (!this.studentlist || this.studentlist.length != this.studentlist.length) {
          return
        }
        if (this.currentPage >= this.pages) {
          this.currentPage = this.pages
        }
        var index = this.currentPage * this.itemsPerPage - this.itemsPerPage
        return this.studentlist.slice(index, index + this.itemsPerPage)

        if (!this.studentlistadd || this.studentlistadd.length != this.studentlistadd.length) {
          return
        }
        if (this.studentPage >= this.pagestudent) {
          this.studentPage = this.pagestudent
        }
        var indexs = this.studentPage * this.itemsPerPage2 - this.itemsPerPage2
        return this.studentlistadd.slice(indexs, indexs + this.itemsPerPage2)
      },
    },
    watch: {
      name(val) {
        this.student.password = val.replace(/\W/g,"");
      },
    },
    mounted(){
      if (this.$route.query.groupNo != undefined)
      {
        this.groupNoFalse = true
      }
    },
    created() {
      if (typeof this.$route.query.schCode != 'undefined') {
        let groupNo = this.$route.query.groupNo;
        this.groupNo = groupNo

        let schCode = this.$route.query.schCode;
        this.schCode = schCode

        this.loaddata(this.currentPage)
        this.schoolNameTake()
      } else {
        alert("School id undefined")
      }
    },
    methods: {
      loading(cmd) {
        var l = document.getElementById("se-pre-con");
        l.style.display = cmd;
      },
      nameKeydown(e) {
        if (/^\W$/.test(e.key)) {
          e.preventDefault();
        }
      },
      kickout(){
        try {
          var language =  "<%=pageContext.getResponse().getLocale()%>"
          var locale = ''
          if (language == 'kr')
          {
            locale = 'kr-KR'
          }
          if(language == 'mn')
          {
            locale = 'mn-MN'
          }
          if(language == 'en'){
            locale = 'en-EN'
          }
          const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
            'Accept-Language' : locale
          }

          var _this = this;
          _this.currentPage = 1
          axios.post('${BASEURL}/kexam/remove/classuser',{
            userId : this.kickoutId,
            classId : this.groupNo
          },{
            headers: headers
          }).then(function (response) {

            if (response.data.status == 200) {
              _this.kickoutClose = false
              alert(response.data.message)
              window.location.reload()
            } else {
              alert(response.data.message);
            }
          }, (error) => {
            console.log(error);
            alert('Error')
          });
        } catch (error) {
          console.log(error)
        }
      },
      updateUserModal(id) {
        window.location.href = "${HOME}/my/update?id=" + id
      },
      popupClose() {
        this.student.schCode = this.schCode
        this.student.groupNo = this.groupNo
        this.student.password = ''
        this.student.loginId = this.groupNo
        if (!this.userRegisterModal) {
          document.body.style.position = 'fixed';
        } else {
          document.body.style.position = 'relative';
        }
        this.userRegisterModal = !this.userRegisterModal;
      },
      deleteClose(item, name , loginId){

        if (!this.userDeleteModal) {
          this.userDeleteId = item
          this.deleteUserName = name
          this.deleteLoginId = loginId
          document.body.style.position = 'fixed';
        } else {
          document.body.style.position = 'relative';
        }
        this.userDeleteModal = !this.userDeleteModal;
      },
      kickoutModal(item){
        if (!this.kickoutClose) {
          this.kickoutId = item
          document.body.style.position = 'fixed';
        } else {
          document.body.style.position = 'relative';
        }
        this.kickoutClose = !this.kickoutClose;
      },
      songonoClose(){
        if (!this.userListModal) {
          this.songonoAngi(this.currentPage);
          this.classlistcall()
          document.body.style.position = 'fixed';
        } else {
          document.body.style.position = 'relative';
        }
        this.userListModal = !this.userListModal;
      },
      userDelete(){
        try {
          var language =  "<%=pageContext.getResponse().getLocale()%>"
          var locale = ''
          if (language == 'kr')
          {
            locale = 'kr-KR'
          }
          if(language == 'mn')
          {
            locale = 'mn-MN'
          }
          if(language == 'en'){
            locale = 'en-EN'
          }
          const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
            'Accept-Language' : locale
          }

          var _this = this;
          axios.post('${BASEURL}/user/delete',{
            userId : this.userDeleteId,
          },{headers: headers }).then(function (response) {
            if (response.data.status == 200) {
              alert(response.data.message);
              _this.loaddata(_this.currentPage)
            } else {
              alert(response.data.message);
            }
          }, (error) => {
            console.log(error);
            alert('Error')
          });
        } catch (error) {
          console.log(error)
        }
      },
      classUserAdd(userIdSend){
        try {
          var language =  "<%=pageContext.getResponse().getLocale()%>"
          var locale = ''
          if (language == 'kr')
          {
            locale = 'kr-KR'
          }
          if(language == 'mn')
          {
            locale = 'mn-MN'
          }
          if(language == 'en'){
            locale = 'en-EN'
          }
          const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
            'Accept-Language' : locale
          }

          var _this = this;

          axios.post('${BASEURL}/kexam/add/classuser',{
            userId : userIdSend,
            classId : this.groupNo
          },{
            headers: headers
          }).then(function (response) {

            console.log("response.classuser")
            console.log(response)

            if (response.data.status == 200) {
              alert(response.data.message);

              this.userDeleteId = ''
              this.deleteUserName = ''
              this.deleteLoginId = ''

              _this.loaddata(_this.currentPage)
              _this.songonoAngi(_this.currentPage)
            } else {
              alert(response.data.message);
            }
          }, (error) => {
            console.log(error);
            alert('Error')
          });
        } catch (error) {
          console.log(error)
        }
      },
      searchSongo(){
        this.currentPage = 1
        this.songonoAngi(this.currentPage)
      },
      songonoAngi(page) {
        try {
          if (page > 0) {
            page = page - 1
          }
          var language =  "<%=pageContext.getResponse().getLocale()%>"
          var locale = ''
          if (language == 'kr')
          {
            locale = 'kr-KR'
          }
          if(language == 'mn')
          {
            locale = 'mn-MN'
          }
          if(language == 'en'){
            locale = 'en-EN'
          }
          const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
            'Accept-Language' : locale
          }
          var _this = this;
          _this.loading('block')
          axios.post('${BASEURL}/users/all',
                  {
                    page: page,
                    schCode: this.schCode,
                    groupNo: this.songonoClassNo,
                    name: this.searchclassname,
                    loginId: this.searchclassid,
                    sort: '',
                    sortfield: '',
                    role: '4'

                  },{headers:headers}
          ).then(function (response) {
            _this.loading('none')
            if (response.status == 200) {
              for (let i = 0; i < response.data.content.length; i++)
              {
                var songoDetail = response.data.content[i]
                songoDetail.checkedClass = false
                var songoName = []
                if (songoDetail.group_nums.length > 0) {
                  if (songoDetail.group_nums.includes(","))
                  {
                    songoName = songoDetail.group_nums.split(',');
                    if (songoName.includes(_this.groupNo))
                    {
                      songoDetail.checkedClass = true
                    }
                  }
                  else
                  {
                    songoName = songoDetail.group_nums
                    if (songoName.includes(_this.groupNo))
                    {
                      songoDetail.checkedClass = true
                    }
                  }


                }
              }

              _this.studentlistadd = response.data.content;
              _this.pagestudent = response.data.totalPages
              _this.totalElement2 = response.data.totalElements
            } else {
              alert("response status error")
            }
          }, function (error) {
            _this.loading('none')
            console.log("axios error found")
            console.log(error)
          });
        } catch (error) {
          console.log(error)
          this.loading('none')
        }
      },
      userRegister(){
        try {
          if (this.student.password != '')
          {
            if (this.student.password.length > 3)
            {
              if (this.student.password == this.student.passComp)
              {
                if (this.student.email == '' && this.student.loginId == ''
                        && this.student.password == ''&& this.student.passComp == '')
                {
                  alert("<spring:message code="user.alertRequire"/>")
                }
                else{
                  var language =  "<%=pageContext.getResponse().getLocale()%>"
                  var locale = ''
                  if (language == 'kr')
                  {
                    locale = 'kr-KR'
                  }
                  if(language == 'mn')
                  {
                    locale = 'mn-MN'
                  }
                  if(language == 'en'){
                    locale = 'en-EN'
                  }

                  const headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                    'Accept-Language' : locale
                  }
                  var _this = this;
                  if (this.student.addLogin.length > 3)
                  {
                    alert("Нэвтрэх код сүүлийн 3 орон байх хэрэгтэй.")
                    return;
                  }
                  if (this.student.addLogin.length < 1){
                    alert("Нэвтрэх код нэгээс их байх хэрэгтэй")
                    return;
                  }

                  var varLogin = this.student.loginId + this.student.addLogin

                  let item = {
                    // image : '',
                    loginId : varLogin,
                    email : this.student.email,
                    password : this.student.password,
                    phone : this.student.phone,
                    name : this.student.name,
                    lastName: this.student.lastname,
                    schCode : this.student.schCode,
                    groupNo : this.student.groupNo,
                    role : '4',
                    gender: this.student.gender
                  };
                  axios.post('${BASEURL}/create/user', item, {
                    headers: headers
                  }).then(function (response) {
                    if (response.data.status == 200) {
                      if (response.data.success== true)
                      {
                        alert(response.data.message)
                        _this.popupClose()
                        _this.clearUserData()
                        _this.loaddata(_this.currentPage)
                      }
                      else{
                        alert(response.data.message)
                      }
                    } else {
                      alert(response.data.message);
                    }
                  }, (error) => {
                    console.log(error);
                    alert(error);
                    console.log(response)
                  });
                }
              }
              else{
                alert("Нууц үгээ зөв оруулна уу.");
                return
              }
            }
            else
            {
              alert("Нууц үг багадаа 3 дээш тэмдэгт байна.")
              return
            }
          }
          else{
            alert("Нууц үгээ оруулна уу.")
            return
          }
        } catch (error) {
          alert(error)
        }
      },
      clearUserData(){
        this.student.email = ""
        this.student.groupNo = ""
        this.student.lastName = ""
        this.student.loginId = ""
        this.student.name = ""
        this.student.lastname = ""
        this.student.password = ""
        this.student.phone = ""
        this.student.schCode = ""
        this.student.gender = ""
        this.student.addLogin = ''
      },
      studentCurrent(item) {
        this.studentPage = item
        this.songonoAngi(this.studentPage)
      },
      nextStudent() {
        if (this.pagestudent > this.studentPage) {
          this.studentPage = this.studentPage + 1
          this.songonoAngi(this.studentPage)
        }
      }
      ,
      prevStudent() {
        if (this.studentPage > 1) {
          this.studentPage = this.studentPage - 1
          this.songonoAngi(this.studentPage)
        }
      }
      ,
      lastStudent() {
        this.studentPage = this.pagestudent
        this.songonoAngi(this.currentPage)
      }
      ,
      firstStudent() {
        this.studentPage = 1
        this.songonoAngi(this.studentPage)
      },
      classlistcall() {
        try {
          var language = "<%=pageContext.getResponse().getLocale()%>"
          var locale = ''
          if (language == 'kr') {
            locale = 'kr-KR'
          }
          if (language == 'mn') {
            locale = 'mn-MN'
          }
          if (language == 'en') {
            locale = 'en-EN'
          }
          const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
            'Accept-Language': locale
          }
          let item = {
            groupName: this.groupName,
            schCode: this.schCode,
          }
          var _this = this

          axios.get('${BASEURL}/kexam/school/classes', {
            params: item,
            headers: headers
          }).then(function (response) {
            if (response.status == 200) {
              _this.classlist = response.data
            } else {
              alert(response.data.message);
            }
          }, (error) => {
            console.log(error);
            alert('Error')
          });

        } catch (error) {
          console.log(error)
          alert(error)
        }
      },
      schoolNameTake() {
        try {
          var language =  "<%=pageContext.getResponse().getLocale()%>"
          var locale = ''
          if (language == 'kr')
          {
            locale = 'kr-KR'
          }
          if(language == 'mn')
          {
            locale = 'mn-MN'
          }
          if(language == 'en'){
            locale = 'en-EN'
          }
          const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
            'Accept-Language' : locale
          }
          var _this = this;
          _this.loading('block')
          axios.get('${BASEURL}/school/detail/'+this.schCode,
                  {
                    params:{},
                    headers: headers
                  }).then((response) => {
            if (response.status == 200) {
              _this.loading('none')
              _this.schoolName = response.data.name
              _this.regUser = response.data.regUser
            } else {
              _this.loading('none')
              alert("ERROR: ")
            }
          }, (error) => {
            _this.loading('none')
            console.log(error);
          });
        } catch (error) {
          console.log(error)
        }
      },
      loaddata(page) {
        try {
          if (page > 0) {
            page = page - 1
          }
          var language =  "<%=pageContext.getResponse().getLocale()%>"
          var locale = ''
          if (language == 'kr')
          {
            locale = 'kr-KR'
          }
          if(language == 'mn')
          {
            locale = 'mn-MN'
          }
          if(language == 'en'){
            locale = 'en-EN'
          }
          const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
            'Accept-Language' : locale
          }

          if (!this.searchIdfalse)
          {
            this.searchlogin = ''
          }
          var _this = this;
          _this.loading('block')
          axios.post('${BASEURL}/users',
                  {
                    page : page,
                    schCode: this.schCode,
                    groupNo: this.groupNo,
                    name: this.name,
                    loginId: this.searchlogin,
                    sort: '',
                    sortfield: '',
                    role: '4'
                  },
                  {headers: headers}).then((response) => {

            if (response.status == 200) {
              _this.studentlist = response.data.content
              _this.pages = response.data.totalPages
              _this.totalElement = response.data.totalElements
              _this.loading('none')
            } else {
              _this.loading('none')
              alert("Алдаа : 235")
            }
          }, (error) => {
            _this.loading('none')
            console.log(error);
          });
        } catch (error) {
          console.log(error)
          this.loading('none')
        }
      },
      nextQuestion() {
        if (this.pages > this.currentPage) {
          this.currentPage = this.currentPage + 1
          this.loaddata(this.currentPage)
        }
      },
      questionCurrent(item) {
        this.currentPage = item
        this.loaddata(this.currentPage)
      },
      prevQuestion() {
        if (this.currentPage > 1) {
          this.currentPage = this.currentPage - 1
          this.loaddata(this.currentPage)
        }
      }
      ,
      lastQuestion() {
        this.currentPage = this.pages
        this.loaddata(this.currentPage)
      }
      ,
      firstQuestion() {
        this.currentPage = 1
        this.loaddata(this.currentPage)
      }
      ,
      searchFilter() {
        this.searchIdfalse = true
        this.loaddata(this.currentPage);
      },
      defaultSearch() {
        window.location.reload()
      },

    }
  });
  app.$mount('#studentlist')
</script>
</body>
</html>

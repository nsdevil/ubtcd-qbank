<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
    <head>
        <link href="${CSS}/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="${CSS}/lib/froala_editor/froala_editor.pkgd.min.css">
        <link rel="stylesheet" href="${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href="${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href="${CSS}/question_registration/each/making.css">
        <link rel="stylesheet" href="${CSS}/multimedia/multimedia.css">
        <script src="${JS}/lib/froala_editor/froala_editor.pkgd.min.js"></script>
        <script src="${JS}/js/making.js"></script>
    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                 <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href=""></a>멀티미디어자료</span>
                    <span><a href="">자료등록</a></span>
                </div>

                <nav class="mgt20">
                    <ul>
                        <li class="on">필수정보</li>
                        <li>문항카드</li>
                    </ul>
                </nav>
                <div class="content-wrap type2 flex">
                    <article>
                        <h3>자료 이름</h3>
                        <div class="content">
							<input type="text" class="input" placeholder="자료 이름을 입력하세요.">
						</div>
                        <h3>자료 유형</h3>
                        <div class="content">
							<div class="flex align-items-center">
								<div class="radio-wrap">
									<input type="radio" class="radio" name="mutimedia_type" id="image" value="이미지">
									<label for="image" onclick="mutimedia_type(this);">이미지</label>
								</div>
								<div class="radio-wrap">
									<input type="radio" class="radio" name="mutimedia_type" id="audio" value="오디오">
									<label for="audio" onclick="mutimedia_type(this);">오디오</label>
								</div>
								<div class="radio-wrap">
									<input type="radio" class="radio" name="mutimedia_type" id="video" value="동영상">
									<label for="video" onclick="mutimedia_type(this);">동영상</label>
								</div>
<!-- 								<div class="radio-wrap"> -->
<!-- 									<input type="radio" class="radio" name="mutimedia_type" id="VR" value="VR"> -->
<!-- 									<label for="VR" onclick="mutimedia_type(this);">VR</label> -->
<!-- 								</div> -->
<!-- 								<div class="radio-wrap"> -->
<!-- 									<input type="radio" class="radio" name="mutimedia_type" id="stack" value="STACK"> -->
<!-- 									<label for="stack" onclick="mutimedia_type(this);">STACK</label> -->
<!-- 								</div> -->
<!-- 								<div class="radio-wrap"> -->
<!-- 									<input type="radio" class="radio" name="mutimedia_type" id="example" value="환자사례"> -->
<!-- 									<label for="example" onclick="mutimedia_type(this);">환자사례</label> -->
<!-- 								</div> -->
							</div>
                            <ul class="content-list">
                                <!-- 컨텐츠 추가영역 -->
                            </ul>
                        </div>


                        <div class="btn-wrap flex">
                            <div class="mgl-auto">
                                <button class="btn-red next shadow" onclick="location.href='./test_subject.jsp';">다음단계로<i class="next-white"></i></button>
                            </div>
                        </div>
                    </article>

                    <aside>
                    </aside>
                </div>
            </div>
            <div id="preview"></div>
        </section>
    </body>
</html>

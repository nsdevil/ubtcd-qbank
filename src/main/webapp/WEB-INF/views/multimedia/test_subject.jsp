<%@ page contentType = "text/html;charset=utf-8" %>
<% String page_name = "multimedia_subject"; %>
<!DOCTYPE html>
<html lang="ko">
    <head> 
        <link rel="stylesheet" href="${CSS}/common/header.css">
        <link rel="stylesheet" href="${CSS}/common/footer.css">
        <link rel="stylesheet" href="${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href="${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href="${CSS}/question_registration/each/question_card/test_subject1.css">
        <link rel="stylesheet" href="${CSS}/multimedia/multimedia.css">
    </head>
    <body> 
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
					<span><a href="${HOME}/main.jsp">Home</a></span>
                    <span><a href=""></a>멀티미디어자료</span>
                    <span><a href="">자료등록</a></span>
                </div>

                <nav class="mgt20">
                    <ul>
                        <li>필수정보</li>
                        <li class="on">문항카드</li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                <div class="content-wrap type3 flex">

                    <article>
						<div class="flex">
							<h3>교육과정</h3>
							<div class="cont-type3">
								<div class="search">
									<input type="text" name="search" class="search-input" placeholder="1click search" onkeyup="search_keyword(this);">
								   
									<div class="search-sub">
										<ul>
											<li onclick="curriculum_select(this);">
												<span class="course">수와 연산</span>
												<span class="unit">수의 연산</span>
												<span class="interruption">곱셈</span>
												<span class="subsection">세 자리 수의 덧셈과 뺄셈</span>
											</li>
											<li onclick="curriculum_select(this);">
												<span class="course">수와 연산</span>
												<span class="unit">수의 연산</span>
												<span class="interruption">곱셈</span>
												<span class="subsection">자연수의 곱셈과 나눗셈</span>
											</li>
											<li onclick="curriculum_select(this);">
												<span class="course">도형</span>
												<span class="unit">평면 도형</span>
												<span class="interruption">평면도형과 그 구성 요소</span>
												<span class="subsection">원의 구성 요소</span>
											</li>
										</ul>
									</div>
								</div>

								<div class="search-list">
									<ul>
										<li>
											<select class="wd-100" name="course">
												<option value="" disabled selected>교육과정명</option>
												<option value="수와 연산">수와 연산</option>
												<option value="도형">도형</option>
												<option value="측정">측정</option>
												<option value="규칙성">규칙성</option>
											</select>
										</li>
										<li>
											<select class="wd-100" name="unit">
												<option value="" disabled selected>단원명</option>
												<option value="수의 체계">수의 체계</option>
												<option value="수의 연산">수의 연산</option>
												<option value="평면 도형">평면 도형</option>
												<option value="입체 도형">입체 도형</option>
											</select>
										</li>
										<li>
											<select class="wd-100" name="interruption">
												<option value="" disabled selected>중단원명</option>
												<option value="곱셈">곱셈</option>
												<option value="평면도형과 그 구성 요소">평면도형과 그 구성 요소</option>
												<option value="여러 가지 삼각형">여러 가지 삼각형</option>

											</select>
										</li>
										<li>
											<select class="wd-100" name="subsection">
												<option value="" disabled selected>소단원명</option>
												<option value="세 자리 수의 덧셈과 뺄셈">세 자리 수의 덧셈과 뺄셈</option>
												<option value="자연수의 곱셈과 나눗셈">자연수의 곱셈과 나눗셈</option>
												<option value="원의 구성 요소">원의 구성 요소</option>
											</select>
										</li>
									</ul>
								</div>

								<button class="btn-white plus shadow" onclick='curriculum_add();'>+ 추가하기</button>
							</div>
                        </div>
<!-- 						<div class="flex"> -->
<!-- 							<h3>임상술기</h3> -->
<!-- 							<div class="cont-type3"> -->
<!-- 								<div class="search-type2"> -->
<!-- 									<div class="flex flex-space-between"> -->
<!-- 										<input type="text" name="clinical_technique" class="search-input mb-30" placeholder="Search"  onclick="popup2('./form/clinical_technique_search.jsp','clinical_technique_search','1350','900');"> -->
<!-- 										<button  onclick="popup2('./form/clinical_technique_search.jsp','clinical_technique_search','1350','900');"  class="bth-search">검색</button> -->
<!-- 									</div>                            -->
<!-- 								</div> -->
<!-- 								<button class="btn-white plus shadow" onclick="clinical_technique_add()">+ 추가하기</button> -->
<!-- 							</div> -->
<!--                         </div> -->
<!-- 						<div class="flex"> -->
<!-- 							<h3>임상표현명</h3> -->
<!-- 							<div class="cont-type3"> -->
<!-- 								<div class="search-type2"> -->
<!-- 									<div class="flex flex-space-between"> -->
<!-- 										<input type="text" name="clinical_expression" class="search-input mb-30" placeholder="Search"  onclick="popup2('./form/clinical_expression_search.jsp','clinical_expression_search','1350','900');"> -->
<!-- 										<button  onclick="popup2('./form/clinical_expression_search.jsp','clinical_expression_search','1350','900');" class="bth-search">검색</button> -->
<!-- 									</div>                            -->
<!-- 								</div> -->
<!-- 								<button class="btn-white plus shadow" onclick="clinical_expression_add()">+ 추가하기</button> -->
<!-- 							</div> -->
<!--                         </div> -->
<!-- 						<div class="flex"> -->
<!-- 							<h3>진단명</h3> -->
<!-- 							<div class="cont-type3"> -->
<!-- 								<div class="search-type2"> -->
<!-- 									<div class="flex flex-space-between"> -->
<!-- 										<input type="text" name="diagnosis_name" class="search-input mb-30" placeholder="Search"  onclick="popup2('./form/diagnosis_name_search.jsp','diagnosis_name_search','1350','900');"> -->
<!-- 										<button onclick="popup2('./form/diagnosis_name_search.jsp','diagnosis_name_search','1350','900');"  class="bth-search">검색</button> -->
<!-- 									</div>                            -->
<!-- 								</div> -->
<!-- 								<button class="btn-white plus shadow" onclick="diagnosis_name_add()">+ 추가하기</button> -->
<!-- 							</div> -->
<!--                         </div> -->
<!-- 						<div class="flex"> -->
<!-- 							<h3>보건용어</h3> -->
<!-- 							<div class="cont-type3"> -->
<!-- 								<div class="search-type2"> -->
<!-- 									<div class="flex flex-space-between"> -->
<!-- 										<input type="text" name="health_care_term" class="search-input mb-30" placeholder="Search"  onclick="popup2('./form/health_care_term_search.jsp','health_care_term_search','1350','900');"> -->
<!-- 										<button onclick="popup2('./form/health_care_term_search.jsp','health_care_term_search','1350','900');"  class="bth-search">검색</button> -->
<!-- 									</div>                            -->
<!-- 								</div> -->
<!-- 								<button class="btn-white plus shadow" onclick="health_care_term_add()">+ 추가하기</button> -->
<!-- 							</div> -->
<!--                         </div> -->
<!-- 						<div class="flex"> -->
<!-- 							<h3>참고문헌</h3> -->
<!-- 							<div class="cont-type3"> -->
<!-- 								<div class="search-type2"> -->
<!-- 									<div class="flex flex-space-between"> -->
<!-- 										<input type="text" name="reference" class="search-input mb-30" placeholder="Search"  onclick="popup2('./form/reference_search.jsp','reference_search','1350','900');"> -->
<!-- 										<button onclick="popup2('./form/reference_search.jsp','reference_search','1350','900');"  class="bth-search">검색</button> -->
<!-- 									</div>                            -->
<!-- 								</div> -->
<!-- 								<button class="btn-white plus shadow" onclick="reference_add()">+ 추가하기</button> -->
<!-- 							</div> -->
<!--                         </div> -->
<!-- 						<div class="flex"> -->
<!-- 							<h3>수술 및 기타 수기</h3> -->
<!-- 							<div class="cont-type3"> -->
<!-- 								<div class="search-type2"> -->
<!-- 									<div class="flex flex-space-between"> -->
<!-- 										<input type="text" name="etc" class="search-input mb-30" placeholder="Search"  onclick="popup2('./form/etc_search.jsp','etc_search','1350','900');"> -->
<!-- 										<button onclick="popup2('./form/etc_search.jsp','etc_search','1350','900');"  class="bth-search">검색</button> -->
<!-- 									</div>                            -->
<!-- 								</div> -->
<!-- 								<button class="btn-white plus shadow" onclick="etc_add()">+ 추가하기</button> -->
<!-- 							</div> -->
<!--                         </div> -->
						<div class="flex">
							<h3>주제어(자유 키워드)</h3>
							<div class="cont-type3">
								<div class="search">
									<input type="text" name="subject" class="input" placeholder="내용을 입력해주세요.">
								</div>
								<button class="btn-white plus shadow" onclick="subject_add();">+ 추가하기</button>
							</div>
                        </div>
						<div class="flex">
							<h3>개인별 문항카드</h3>
							<div class="cont-type3">

								<div class="search-list">
									<ul>
										<li>
											<select class="select2 wd-100" name="main_category" data-placeholder="대분류">
												<option value=""></option>
												<option value="우리초등학교 (2020년도)">우리초등학교 (2020년도)</option>
												 <option value="우리초등학교 (2019년도)">우리초등학교 (2019년도)</option>
											</select>
										</li>
										<li>
											<select class="select2 wd-100" name="sub_category" data-placeholder="중분류">
												<option value=""></option>
												<option value="5학년 3반 - A조 (김누리, 하예은, 최소은)">5학년 3반 - A조 (김누리, 하예은, 최소은)</option>
												<option value="5학년 3반 -B조 (이찬원, 정동원, 신인선)">5학년 3반 -B조 (이찬원, 정동원, 신인선)</option>
												<option value="5학년 3반 - C조 (조인성, 차인표, 이서진)">5학년 3반 - C조 (조인성, 차인표, 이서진)</option>
											</select>
										</li>
									</ul>
								</div>

								<button class="btn-white plus shadow" onclick='Individual_question_card_add();'>+ 추가하기</button>
							</div>
                        </div>

                        <form name="" method="" action=""></form>
                        
                        <div class="btn-wrap flex">
                            <button class="btn-white prev shadow" onclick="go_prev_card('multimedia_subject')"><i class="prev-gray"></i>이전단계</button> 
                            <div class="flex">
                                <button class="btn-gray delete shadow" onclick="card_cancel()">삭제</button>
                                <button class="btn-red next shadow" >자료등록</button> 
                            </div>
                        </div>

                    </article>

                    <aside>
                        <div class="selected">
                            <strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
                            <ul class="selected-list">
<!--                                 <li>#[시험과목] 연도 > 학급 > 학년 > 학기 > 대분류 > 중분류 > 소분류<i class="close"></i></li> -->
                            </ul>
                        </div>
            
                    </aside>
                </div>

                

            </div>
        </section> 
    </body>
</html>
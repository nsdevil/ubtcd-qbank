<%@ page contentType = "text/html;charset=utf-8" %>
		<script>
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawVisualization);

		  function drawVisualization() {
			// Some raw data (not necessarily accurate)
			var data = google.visualization.arrayToDataTable([
			  ['Month', '활용 횟수', {role : 'annotation'},  '찜 횟수', {role : 'annotation'}, '좋아요 횟수', {role : 'annotation'}],
			  ['2019년 01월', 132, '132', 34, '34', 97, '97'],
			  ['2019년 02월', 109, '109', 22, '22', 74, '74'],
			  ['2019년 03월', 73, '73', 52, '52', 38 ,'38'],
			  ['2019년 04월', 24,'24', 142,'142', 17,'17'],
			  ['2019년 05월', 52,'52', 42,'42', 17,'17'],
			  ['2019년 06월', 11,'11', 11,'11', 5,'5'],
			  ['2019년 07월', 262,'262', 227,'227', 52,'52'],
			  ['2019년 08월', 22,'22', 22,'22', 12,'12'],
			  ['2019년 09월', 163,'163', 128,'128', 19,'19'],
			  ['2019년 10월', 11,'11', 11,'11', 101,'101'],
			  ['2019년 11월', 252,'252', 217,'217', 35,'35'],
			  ['2019년 12월', 32,'32', 6,'6', 52,'52']
			]);



			var options = {
			  title : ' ',
			  vAxis: {title: ' '},
			  hAxis: {title: ' '},
			  seriesType: 'bars',
			  series: {
				  0: {type: 'bars' , color: '#f0f3f8', bar : {groupWidth : '27px'}}, 
				  1: {type: 'line' , color: '#4a9faf' , lineWidth : 1}, 
				  2: {type: 'line' , color: '#830e26'}
			  },
			  legend: {position :'top' , alignment :'end'},
			  width : 920,
			  height : 495,
		      chartArea: {
					width : '80%' // 그래프 너비 설정 %
				}


			};

			var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
			chart.draw(data, options);
		  }		

		  google.charts.setOnLoadCallback(drawChart1);
		  function drawChart1() {
			var data1 = google.visualization.arrayToDataTable([
			  ['Task', 'Hours per Day'],
			  ['객관식(단일/복수정답형)',     7],
			 // ['객관식(R형) ',      5],
			  ['주관식',  3],
			  //['CPX', 2]
			]);

			var options = {
			  title: '',
			  pieHole: 0.45,
			  width : 420,
			  height : 420,
			  legend: {position :'top' , alignment :'center' , textStyle: {color: '#000000', fontSize: 12}},
		      chartArea: {
					width : '100%',
					height: '70%',
					top: 70
				},
			  slices: {
				0: { color: '#959fb1' },
				1: { color: '#4a9faf' },
				2: { color: '#c1e3ea' },
				3: { color: '#ebd4cc' }
			  }
			};

			var pie_chart1 = new google.visualization.PieChart(document.getElementById('pie_chart1'));
			pie_chart1.draw(data1, options);
		  }
		  google.charts.setOnLoadCallback(drawChart2);
		  function drawChart2() {
			var data2 = google.visualization.arrayToDataTable([
			  ['Task', 'Hours per Day'],
			  ['제시자료',     7],
			  ['답가지 ',      2],
			  ['오답노트',  3]
			]);

			var options = {
			  title: '',
			  pieHole: 0.45,
			  width : 420,
			  height : 420,
			  legend: {position :'top' , alignment :'center' , textStyle: {color: '#000000', fontSize: 12}},
		      chartArea: {
					width : '100%',
					height: '70%',
					top: 70
				},
			  slices: {
				0: { color: '#9c95b1' },
				1: { color: '#b19595' },
				2: { color: '#b1a395' }
			  }
			};

			var pie_chart1 = new google.visualization.PieChart(document.getElementById('pie_chart2'));
			pie_chart1.draw(data2, options);
		  }



		$(document).ready(function(){		
			$('.btn-wish2').on('click',function(){		
				if ($(this).hasClass('btn-white')) {
					$(this).removeClass('btn-white');
					$(this).addClass('btn-red-line');
					$(this).children('.wish').addClass('on');
				} else {
					$(this).removeClass('btn-red-line');
					$(this).addClass('btn-white');
					$(this).children('.wish').removeClass('on');
				}
			});
			$('.btn-like2').on('click',function(){
				if ($(this).hasClass('btn-white')) {
					$(this).removeClass('btn-white');
					$(this).addClass('btn-red-line');
					$(this).children('.like').addClass('on');
				} else {
					$(this).removeClass('btn-red-line');
					$(this).addClass('btn-white');
					$(this).children('.like').removeClass('on');
				}
			});

			$('ul.statistics-tab li').click(function(){
				var tab_id = $(this).attr('tab-data');

				$('ul.statistics-tab li').removeClass('statistics-on');
				$('.statistics-cont').removeClass('current');

				$(this).addClass('statistics-on');
				$("#"+tab_id).addClass('current');
			});
		});
		</script>
        <div class="wrap wd1000">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>

			<div class="multi-content">
				<div class="flex multi-top flex-space-between">
					<div class="multi-thumb-area">
						<div class="thumb">
							<img src="${IMG}/icons/question_registration/play.png" class="auto">
							<!--
							<img src="${IMG}/icons/image.png" class="auto">
							<img src="${IMG}/icons/question_registration/stack.png" class="auto">
							<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
							-->
						</div>
						<button class="btn-red-line shadow">크게 보기</button>
					</div>
					<div class="multi-info">
						<dl class="multi-title flex flex-space-between">
							<dt>QB_2020_10000002333</dt>
							<dd class="flex">
								<button class="btn-white shadow btn-wish2"><i class="wish"></i>찜</button>
								<button class="btn-white shadow btn-like2"><i class="like"></i>좋아요</button>
							</dd>
						</dl>
						<div class="multi-disc">
							나눗셈 풀이 모습 동영상 01
						</div>
						<div class="multi-count table-wrap">
							<table>
								<thead class="border-red-top-2 border-gray-bottom-2">
									<tr>
										<th><i class="wish on"></i>“찜” 횟수 </th>
										<th><i class="like on"></i>“좋아요” 횟수</th>
										<th>활용된 횟수 (누적)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>123회</td>
										<td>234회</td>
										<td>1,234회</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			
				<div class="multi-card">
					<p class="title">등록된 문항카드</p>
					<div class="card-sec">
						<div class="masonry">
							<div class="card-item brick">
								# [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가
							</div>
							<div class="card-item brick">
								# [성취수준] 수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.
							</div>
							<div class="card-item brick">
								# [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈
							</div>
							<div class="card-item brick">
								# [성취수준] 수와 연산 > 나눗셈 > [4수01-09]나누는 수가 두 자리 수인 나눗셈의 계산 원리를 이해하고 그 계산을 할 수 있다.
							</div>
							<div class="card-item brick">
								# [지식수준] 해석(판단)
							</div>
							<div class="card-item brick">
								# [문항의 적절성] 필수적인(essential)
							</div>
							<div class="card-item brick">
								# [예상난이도] 매우 어려움
							</div>
						</div>
					</div>
				</div>

				<div class="multi-statistics">
					<ul class="statistics-tab flex">
						<li class="statistics-on" tab-data="statistics-type1">활용 빈도</li>
						<li tab-data="statistics-type2">활용 유형</li>
					</ul>
					<div class="statistics-cont current" id="statistics-type1">
						<div id="chart_div" ></div>
					</div>
					<div class="statistics-cont" id="statistics-type2">
						<div class="flex">
							<div class="pie-chart">
								<p class="pie-tt">문항 유형별</p>
								<p class="pie-disc">문항 유형별</p>
								<div id="pie_chart1"></div>
							</div>
							<div class="pie-chart">
								<p class="pie-tt">문항내 영역별</p>
								<p class="pie-disc">문항내 영역별</p>
								<div id="pie_chart2"></div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		
		</div>


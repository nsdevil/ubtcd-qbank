<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd1350">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
			<div class="default-list-pop">
				<h2>임상술기</h2>
                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2" name="type" data-placeholder="구분">
                                <option value=""></option>
                                <option value="all" selected>전체</option>
                                <option value="num">구분</option>
                                <option value="num">타입</option>
                                <option value="num">임상술기명(국문)</option>
                                <option value="num">임상술기명(영문)</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
                </div>
				<div class="table-wrap">
					<table>
						<colgroup>
							<col style="width : 95px;"/>
							<col style="width : 140px;"/>
							<col style="width : 135px;"/>
							<col />
							<col />
							<col style="width : 90px;"/>
						</colgroup>
						<thead class="border-red-top-2 border-gray-bottom-2">
							<tr>
								<th>번호</th>
								<th>구분</th>
								<th>타입</th>
								<th>임상술기명(국문)</th>
								<th>임상술기명(영문)</th>
								<th>선택</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>2</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>3</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>4</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>5</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>6</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>7</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>8</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>9</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>10</td>
								<td class="part">기본임상</td>
								<td class="type">A:진료</td>
								<td class="name_kr">복부 진찰</td>
								<td class="name_en">abdominal examination</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="clinical_technique_select(this);">선택</button></div></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="pagination">
					<button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
					<button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
					<ul class="paginate-list">
						<li class="page-num active"><a href="#">1</a></li>
						<li class="page-num"><a href="#">2</a></li>
						<li class="page-num"><a href="#">3</a></li>
						<li class="page-num"><a href="#">4</a></li>
						<li class="page-num"><a href="#">5</a></li>
						<li class="page-num"><a href="#">6</a></li>
						<li class="page-num"><a href="#">7</a></li>
						<li class="page-num"><a href="#">8</a></li>
						<li class="page-num"><a href="#">9</a></li>
						<li class="page-num"><a href="#">10</a></li>
					</ul>
					<button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
					<button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
				</div>
			</div>
        </div>

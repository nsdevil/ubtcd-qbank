<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd800">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
			<div class="default-list-pop">
				<h2>이미지</h2>
                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
                </div>
				<div class="table-wrap">
					<table>
						<colgroup>
							<col style="width : 800px;"/>
							<col style="width : 100px;"/>
							<col />
							<col style="width : 90px;"/>
						</colgroup>
						<thead class="border-red-top-2 border-gray-bottom-2">
							<tr>
								<th>번호</th>
								<th>썸네일</th>
								<th>소스이름</th>
								<th>선택</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td class="thumb">
									<div class="thumb-box">
										<img src="${IMG}/icons/question_registration/play.png" class="auto">
										<!--
										<img src="${IMG}/icons/image.png" class="auto">
										<img src="${IMG}/icons/question_registration/stack.png" class="auto">
										<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
										-->
									</div>
								</td>
								<td class="source-name">발열 환자 체크 이미지01</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="multimedia_select(this);">선택</button></div></td> <!-- 선택시 해당 멀티미디어 폼에 타겟팅 필요 -->
							</tr>
							<tr>
								<td>2</td>
								<td class="thumb">
									<div class="thumb-box">
										<img src="${IMG}/icons/question_registration/play.png" class="auto">
										<!--
										<img src="${IMG}/icons/image.png" class="auto">
										<img src="${IMG}/icons/question_registration/stack.png" class="auto">
										<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
										-->
									</div>
								</td>
								<td class="source-name">발열 환자 체크 이미지01</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="multimedia_select(this);">선택</button></div></td><!-- 선택시 해당 멀티미디어 폼에 타겟팅 필요 -->
							</tr>
							<tr>
								<td>3</td>
								<td class="thumb">
									<div class="thumb-box">
										<img src="${IMG}/icons/question_registration/play.png" class="auto">
										<!--
										<img src="${IMG}/icons/image.png" class="auto">
										<img src="${IMG}/icons/question_registration/stack.png" class="auto">
										<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
										-->
									</div>
								</td>
								<td class="source-name">발열 환자 체크 이미지01</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="multimedia_select(this);">선택</button></div></td><!-- 선택시 해당 멀티미디어 폼에 타겟팅 필요 -->
							</tr>
							<tr>
								<td>4</td>
								<td class="thumb">
									<div class="thumb-box">
										<img src="${IMG}/icons/question_registration/play.png" class="auto">
										<!--
										<img src="${IMG}/icons/image.png" class="auto">
										<img src="${IMG}/icons/question_registration/stack.png" class="auto">
										<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
										-->
									</div>
								</td>
								<td class="source-name">발열 환자 체크 이미지01</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="multimedia_select(this);">선택</button></div></td><!-- 선택시 해당 멀티미디어 폼에 타겟팅 필요 -->
							</tr>
							<tr>
								<td>5</td>
								<td class="thumb">
									<div class="thumb-box">
										<img src="${IMG}/icons/question_registration/play.png" class="auto">
										<!--
										<img src="${IMG}/icons/image.png" class="auto">
										<img src="${IMG}/icons/question_registration/stack.png" class="auto">
										<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
										-->
									</div>
								</td>
								<td class="source-name">발열 환자 체크 이미지01</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="multimedia_select(this);">선택</button></div></td><!-- 선택시 해당 멀티미디어 폼에 타겟팅 필요 -->
							</tr>
							<tr>
								<td>6</td>
								<td class="thumb">
									<div class="thumb-box">
										<img src="${IMG}/icons/question_registration/play.png" class="auto">
										<!--
										<img src="${IMG}/icons/image.png" class="auto">
										<img src="${IMG}/icons/question_registration/stack.png" class="auto">
										<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
										-->
									</div>
								</td>
								<td class="source-name">발열 환자 체크 이미지01</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="multimedia_select(this);">선택</button></div></td><!-- 선택시 해당 멀티미디어 폼에 타겟팅 필요 -->
							</tr>
						</tbody>
					</table>
				</div>

				<div class="pagination">
					<button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
					<button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
					<ul class="paginate-list">
						<li class="page-num active"><a href="#">1</a></li>
						<li class="page-num"><a href="#">2</a></li>
						<li class="page-num"><a href="#">3</a></li>
						<li class="page-num"><a href="#">4</a></li>
						<li class="page-num"><a href="#">5</a></li>
						<li class="page-num"><a href="#">6</a></li>
						<li class="page-num"><a href="#">7</a></li>
						<li class="page-num"><a href="#">8</a></li>
						<li class="page-num"><a href="#">9</a></li>
						<li class="page-num"><a href="#">10</a></li>
					</ul>
					<button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
					<button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
				</div>
			</div>
        </div>

<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd1350">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
			<div class="default-list-pop">
				<h2>참고문헌</h2>
                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2" name="type" data-placeholder="구분">
                                <option value=""></option>
                                <option value="all" selected>전체</option>
                                <option value="num">번호</option>
                                <option value="num">구분</option>
                                <option value="num">논문/도서명(국문)</option>
                                <option value="num">논문/도서명(영문)</option>
                                <option value="num">저자</option>
                                <option value="num">발행기관/출판사</option>
                                <option value="num">연도</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
                </div>
				<div class="table-wrap">
					<table>
						<colgroup>
							<col style="width : 95px;"/>
							<col style="width : 110px;"/>
							<col style="width : 290px;" />
							<col />
							<col style="width : 90px;"/>
							<col style="width : 140px;"/>
							<col style="width : 80px;"/>
							<col style="width : 90px;"/>
						</colgroup>
						<thead class="border-red-top-2 border-gray-bottom-2">
							<tr>
								<th>번호</th>
								<th>구분</th>
								<th>논문/도서명(국문)</th>
								<th>논문/도서명(영문)</th>
								<th>저자</th>
								<th>발행기관/출판사</th>
								<th>연도</th>
								<th>선택</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>2</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>3</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>4</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>5</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>6</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>7</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>8</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>9</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>10</td>
								<td class="division">논문/학술지</td>
								<td class="name_kr t-left">9월 알레르기비염의 호발과 공중 화분과의 상관관계에 대한 연구</td>
								<td class="name_en t-left">A study on the correlation between outbreak of allergic rhinitis and airborne pollen in September</td>
								<td class="writer">김종석 외</td>
								<td class="publish">대한 소아알레르기 <br/>호흡기학회</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="reference_select(this);">선택</button></div></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="pagination">
					<button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
					<button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
					<ul class="paginate-list">
						<li class="page-num active"><a href="#">1</a></li>
						<li class="page-num"><a href="#">2</a></li>
						<li class="page-num"><a href="#">3</a></li>
						<li class="page-num"><a href="#">4</a></li>
						<li class="page-num"><a href="#">5</a></li>
						<li class="page-num"><a href="#">6</a></li>
						<li class="page-num"><a href="#">7</a></li>
						<li class="page-num"><a href="#">8</a></li>
						<li class="page-num"><a href="#">9</a></li>
						<li class="page-num"><a href="#">10</a></li>
					</ul>
					<button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
					<button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
				</div>
			</div>
        </div>

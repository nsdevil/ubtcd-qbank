<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link href="${CSS}/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../lib/froala_editor/froala_editor.pkgd.min.css">
        <link rel="stylesheet" href="${CSS}/common/header.css">
        <link rel="stylesheet" href="${CSS}/common/footer.css">
        <link rel="stylesheet" href="${CSS}/multimedia/multimedia.css">
        <script src="${JS}/lib/froala_editor/froala_editor.pkgd.min.js"></script>
        <script src="${JS}/making.js"></script>
		<script type="text/javascript" src="   ${JS}/chart.js"></script>
		<script>


		</script>

    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                 <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href="">멀티미디어자료</a></span>
                </div>
                <div class="content-wrap flex">
                    <aside>
						<dl>
							<dt>검색 범위</dt>
							<dd class="flex flex-wrap">
								<div class="radio-wrap">
									<input type="radio" class="radio" name="range" id="range1" value="전체" checked>
									<label for="range1">전체</label>
								</div>
								<div class="radio-wrap">
									<input type="radio" class="radio" name="range" id="range2" value="내가 등록한 자료">
									<label for="range2">내가 등록한 자료</label>
								</div>
								<div class="radio-wrap">
									<input type="radio" class="radio" name="range" id="range3" value="'찜' 한 자료">
									<label for="range3">“찜” 한 자료</label>
								</div>
								<div class="radio-wrap">
									<input type="radio" class="radio" name="range" id="range4" value="'좋아요' 한 자료">
									<label for="range4">“좋아요” 한 자료</label>
								</div>
							</dd>
						</dl>
						<dl>
							<dt>검색 자료 유형</dt>
							<dd class="flex flex-wrap">
								<div class="checkbox-wrap wd100">
									<input type="checkbox" class="checkbox all-chk" name="data_type" id="data_type1" value="전체" checked>
									<label for="data_type1">전체</label>
								</div>
								<div class="checkbox-wrap">
									<input type="checkbox" class="checkbox" name="data_type" id="data_type2" value="이미지" checked>
									<label for="data_type2">이미지</label>
								</div>
								<div class="checkbox-wrap">
									<input type="checkbox" class="checkbox" name="data_type" id="data_type3" value="오디오" checked>
									<label for="data_type3">오디오</label>
								</div>
								<div class="checkbox-wrap">
									<input type="checkbox" class="checkbox" name="data_type" id="data_type4" value="동영상" checked>
									<label for="data_type4">동영상</label>
								</div>
								<div class="checkbox-wrap">
									<input type="checkbox" class="checkbox" name="data_type" id="data_type5" value="기타" checked>
									<label for="data_type5">기타</label>
								</div>
<!-- 								<div class="checkbox-wrap"> -->
<!-- 									<input type="checkbox" class="checkbox" name="data_type" id="data_type6" value="STACK" checked> -->
<!-- 									<label for="data_type6">STACK</label> -->
<!-- 								</div> -->
<!-- 								<div class="checkbox-wrap"> -->
<!-- 									<input type="checkbox" class="checkbox" name="data_type" id="data_type7" value="환자사례" checked> -->
<!-- 									<label for="data_type7">환자사례</label> -->
<!-- 								</div> -->
							</dd>
						</dl>
						<dl>
							<dt>선택한 검색 조건</dt>
							<dd>
								<ul class="selected-list">
<!-- 									<li>#[시험과목] 연도 &gt; 학급 &gt; 학년 &gt; 학기 &gt; 대분류 &gt; 중분류 &gt; 소분류<i class="close"></i></li> -->
<!-- 									<li>#[교육과정] 과정명 &gt; 대단원명 &gt; 중단원명 &gt; 소단원명<i class="close"></i></li> -->
<!-- 									<li>#[시험과목] 연도 &gt; 학급 &gt; 학년 &gt; 학기 &gt; 대분류 &gt; 중분류 &gt; 소분류<i class="close"></i></li> -->
<!-- 									<li>#[교육과정] 과정명 &gt; 대단원명 &gt; 중단원명 &gt; 소단원명<i class="close"></i></li> -->
								</ul>
							</dd>
						</dl>
						<div class="aside-btn-area flex flex-space-between">
							<button class="btn-white shadow" onclick="card_all_del();">조건 초기화</button>
							<button class="btn-red-line shadow">검색</button>
						</div>
						<div class="search-add-list">
							<dt>시험과목</dt>
							<dd>
								<ul>
									<li>
										<select name="year" id="year" class="select2">
										<option value="" disabled selected>연도</option>
										<option value="2020년">2020년</option>
                                        <option value="2019년">2019년</option>
										<option value="2018년">2018년</option>
										<option value="2017년">2017년</option>
										<option value="2016년">2016년</option>
										</select>
									</li>
									<li>
										<select name="major" id="major" class="select2">
										<option value="">학교(학급)</option>
                                        <option value="우리초등학교">우리초등학교</option>
                                        <option value="영훈초등학교">영훈초등학교</option>
										<option value="염리초등학교">염리초등학교</option>
										<option value="신월초등학교">신월초등학교</option>
										<option value="신석초등학교">신석초등학교</option>
										<option value="목운초등학교">목운초등학교</option>
										</select>
									</li>
									<li>
										<select name="grade" id="grade" class="select2">
                                        <option value="" disabled selected>학년</option>
                                        <option value="1학년">1학년</option>
                                        <option value="2학년">2학년</option>
										<option value="3학년">3학년</option>
										<option value="4학년">4학년</option>
										<option value="5학년">5학년</option>
										<option value="6학년">6학년</option>
										</select>
									</li>
									<li>
										<select name="term" id="term" class="select2">
                                        <option value="" disabled selected>학기</option>
                                        <option value="1학기">1학기</option>
                                        <option value="2학기">2학기</option>
										</select>
									</li>
									<li>
										<select name="main" id="main" class="select2">
                                        <option value="" disabled selected>대분류</option>
                                        <option value="1반">1반</option>
                                        <option value="2반">2반</option>
										<option value="3반">3반</option>
										<option value="4반">4반</option>
										<option value="5반">5반</option>
										<option value="6반">6반</option>
										</select>
									</li>
									<li>
										<select name="middle" id="middle" class="select2">
                                        <option value="" disabled selected>중분류</option>
										<option value="A조 (김누리, 하예은, 최소은)">A조 (김누리, 하예은, 최소은)</option>
										<option value="B조 (이찬원, 정동원, 신인선)">B조 (이찬원, 정동원, 신인선)</option>
                                        <option value="C조 (조인성, 차인표, 이서진)">C조 (조인성, 차인표, 이서진)</option>
<!--                                         <option value="기초치의학">기초치의학</option> -->
										</select>
									</li>
									<li>
										<select name="subclass" id="subclass" class="select2">
                                        <option value="" disabled selected>소분류</option>
                                        <option value="중간평가">중간평가</option>
                                        <option value="수시평가">수시평가</option>
										</select>
									</li>
								</ul>
								<div class="search-add">
									<button class="btn-red-line shadow" onclick="test_subject_add(this);">검색 조건 추가</button>
								</div>
							</dd>
							<dt>교육과정</dt>
							<dd>
								<ul>
									<li>
										<select name="course" id="course" class="select2">
                                        <option value="" disabled selected>교육과정명</option>
                                        <option value="수와 연산">수와 연산</option>
										<option value="도형">도형</option>
										<option value="측정">측정</option>
										<option value="규칙성">규칙성</option>
										</select>
									</li>
									<li>
										<select name="unit" id="unit" class="select2">
                                        <option value="" disabled selected>단원명</option>
                                        <option value="수의 체계">수의 체계</option>
										<option value="수의 연산">수의 연산</option>
										<option value="평면 도형">평면 도형</option>
										<option value="입체 도형">입체 도형</option>
										</select>
									</li>
									<li>
										<select name="interruption" id="interruption" class="select2">
                                        <option value="" disabled selected>중단원명</option>
                                        <option value="곱셈">곱셈</option>
										<option value="평면도형과 그 구성 요소">평면도형과 그 구성 요소</option>
										<option value="여러 가지 삼각형">여러 가지 삼각형</option>
										</select>
									</li>
									<li>
										<select name="subsection" id="subsection" class="select2">
                                        <option value="" disabled selected>소단원명</option>
                                        <option value="세 자리 수의 덧셈과 뺄셈">세 자리 수의 덧셈과 뺄셈</option>
										<option value="자연수의 곱셈과 나눗셈">자연수의 곱셈과 나눗셈</option>
										<option value="원의 구성 요소">원의 구성 요소</option>
										</select>
									</li>
								</ul>
								<div class="search-add">
									<button class="btn-red-line shadow" onclick="curriculum_add(this);">검색 조건 추가</button>
								</div>
							</dd>
<!-- 							<dt>임상술기</dt> -->
<!-- 							<dd> -->
<!-- 								<div class="search technique_sh"> -->
<!-- 									<input type="text" name="clinical_technique" class="search-input" placeholder="1click search" onkeyup="search_keyword_technique(this);"> -->
<!-- 									<div class="search-sub"> -->
<!-- 										<ul> -->
<!-- 											<li onclick="clinical_technique_select_type2(this);"> -->
<!-- 												<span class="part">임산부</span> -->
<!-- 												<span class="type">임산부의 두손 진찰</span> -->
<!-- 												<span class="lang1">임산부 초음파 검사</span> -->
<!-- 												<span class="lang2">임산부 기초 검진</span> -->
<!-- 											</li> -->
<!-- 										</ul> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="search-add"> -->
<!-- 									<button class="btn-red-line shadow" onclick="clinical_technique_add(this);">검색 조건 추가</button> -->
<!-- 								</div> -->
<!-- 							</dd> -->
<!-- 							<dt>임상표현명</dt> -->
<!-- 							<dd> -->
<!--  -->
<!-- 								<div class="search expression_sh"> -->
<!-- 									<input type="text" name="clinical_expression" class="search-input" placeholder="1click search" onkeyup="search_keyword_expression(this);"> -->
<!-- 									<div class="search-sub"> -->
<!-- 										<ul> -->
<!-- 											<li onclick="clinical_expression_select_type2(this);"> -->
<!-- 												<span class="code">H00</span> -->
<!-- 												<span class="lang1">맥립종 및 콩다래끼</span> -->
<!-- 												<span class="lang2">Hordeolum and chalazion</span> -->
<!-- 												<span class="source">KAMC</span> -->
<!-- 												<span class="year">2019</span> -->
<!-- 											</li> -->
<!-- 										</ul> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="search-add"> -->
<!-- 									<button class="btn-red-line shadow" onclick="clinical_expression_add(this);">검색 조건 추가</button> -->
<!-- 								</div> -->
<!--  -->
<!-- 							</dd> -->
							<dt>개인별 문항카드</dt>
							<dd>
								<ul>
									<li>
										<select name="main_category" id="main_category" class="select2">
                                        <option value="" disabled selected>선택</option>
                                        <option value="우리초등학교 (2020년도)">우리초등학교 (2020년도)</option>
										 <option value="우리초등학교 (2019년도)">우리초등학교 (2019년도)</option>
										</select>
									</li>
									<li>
										<select name="sub_category" id="sub_category" class="select2">
										<option value="" disabled selected>선택</option>
                                        <option value="5학년 3반 - A조 (김누리, 하예은, 최소은)">5학년 3반 - A조 (김누리, 하예은, 최소은)</option>
										<option value="5학년 3반 -B조 (이찬원, 정동원, 신인선)">5학년 3반 -B조 (이찬원, 정동원, 신인선)</option>
										<option value="5학년 3반 - C조 (조인성, 차인표, 이서진)">5학년 3반 - C조 (조인성, 차인표, 이서진)</option>
										</select>
									</li>
								</ul>
								<div class="search-add">
									<button class="btn-red-line shadow" onclick="Individual_question_card_add(this);">검색 조건 추가</button>
								</div>
							</dd>
						</div>

                    </aside>

                    <article>
						<h2>멀티미디어자료</h2>

						<div class="search-wrap flex">
							<div class="search-form">
								<form name="searchForm" method="get" action="">
									<input type="text" name="keyword" placeholder="결과내 검색">
									<button><img src="${IMG}/icons/search.png" class="auto"></button>
								</form>
							</div>
							<div class="btn-wrap flex">
								<button class="btn-card-list" onclick="location.href='./multimedia_card_search.jsp';"><i class="list-type-card-red"></i></button>
<!-- 								<button onclick="location.href='./multimedia_search.jsp';"><i class="list-type-list"></i></button> -->
<button onclick="location.href='';"><i class="list-type-list"></i></button>
							</div>
						</div>

						<div class="multi-card-list">
							<ul>
								<li>
									<div class="card-info flex flex-space-between">
										<span class="date">2020-02-15</span>
										<span class="name">홍길동(20180000)</span>
									</div>
									<div class="card-etc flex flex-space-between align-items-center">
										<span class="label">이미지</span>
										<div class="etc-btn">
											<button><i class="update"></i></button>
											<button><i class="delete"></i></button>
											<button class="btn-wish"><i class="wish"></i></button>
											<button class="btn-like"><i class="like"></i></button>
										</div>
									</div>
									<div class="card-cont" onclick="popup2('./form/multimedia_detail.jsp','multimedia_detail','1000','');" >
										<div class="thumb">
											<img src="${IMG}/icons/image.png" class="auto">
										</div>
										<div class="multi-data-code">QB_2020_10000000123</div>
										<div class="multi-data-name"><div class="t-ellipsis">A조-수업 활용 이미지 01</div></div>
										<div class="multi-data-list">
											<div class="data-item"><div class="t-ellipsis"># [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가</div></div>
										</div>
									</div>
								</li>
								<li>
									<div class="card-info flex flex-space-between">
										<span class="date">2020-02-15</span>
										<span class="name">홍길동(20180000)</span>
									</div>
									<div class="card-etc flex flex-space-between align-items-center">
										<span class="label">동영상</span>
										<div class="etc-btn">
											<button><i class="update"></i></button>
											<button><i class="delete"></i></button>
											<button class="btn-wish"><i class="wish"></i></button>
											<button class="btn-like"><i class="like"></i></button>
										</div>
									</div>
									<div class="card-cont" onclick="popup2('./form/multimedia_detail.jsp','multimedia_detail','1000','');" >
										<div class="thumb">
											<img src="${IMG}/icons/question_registration/play.png" class="auto">
											<!--
<%--											<img src="${IMG}/icons/image.png" class="auto">--%>
<%--											<img src="${IMG}/icons/question_registration/stack.png" class="auto">--%>
<%--											<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">--%>
											-->
										</div>
										<div class="multi-data-code">QB_2020_10000002333</div>
										<div class="multi-data-name"><div class="t-ellipsis">나눗셈 풀이 모습 동영상 01</div></div>
										<div class="multi-data-list">
											<div class="data-item"><div class="t-ellipsis"># [성취수준] 수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.</div></div>
										</div>
									</div>
								</li>
<!-- 								<li> -->
<!-- 									<div class="card-info flex flex-space-between"> -->
<!-- 										<span class="date">2020-02-15</span> -->
<!-- 										<span class="name">홍길동(20180000)</span> -->
<!-- 									</div> -->
<!-- 									<div class="card-etc flex flex-space-between align-items-center"> -->
<!-- 										<span class="label">VR</span> -->
<!-- 										<div class="etc-btn"> -->
<!-- 											<button><i class="update"></i></button> -->
<!-- 											<button><i class="delete"></i></button> -->
<!-- 											<button class="btn-wish"><i class="wish"></i></button> -->
<!-- 											<button class="btn-like"><i class="like"></i></button> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 									<div class="card-cont" onclick="popup2('./form/multimedia_detail.jsp','multimedia_detail','1000','');" > -->
<!-- 										<div class="thumb"> -->
<%--<!-- 											<img src="${IMG}/icons/question_registration/stack.png" class="auto"> -->--%>
<%--<!-- 											<!-- -->--%>
<%--<!-- 											<img src="${IMG}/icons/image.png" class="auto"> -->--%>
<%--<!-- 											<img src="${IMG}/icons/question_registration/stack.png" class="auto"> -->--%>
<%--<!-- 											<img src="${IMG}/icons/question_registration/type_audio.png" class="auto"> -->--%>
<!-- 											-->
<!-- 										</div> -->
<!-- 										<div class="multi-data-code">QB_2020_1000000722323</div> -->
<!-- 										<div class="multi-data-name"><div class="t-ellipsis">흉부 X-RAY 이미지_01 (자료 제목은 2줄 까지만 표기한 후 말줄임표 처리)흉부 X-RAY 이미지_01 (자료 제목은 2줄 까지만 표기한 후 말줄임표 처리)</div></div> -->
<!-- 										<div class="multi-data-list"> -->
<!-- 											<div class="data-item"><div class="t-ellipsis"># [교육과정]과정명 > 단원명 > 분류 3 꽃 길을 이것이야말로 살 귀는 그들에가 발휘하기..</div></div> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</li> -->
								<li>
									<div class="card-info flex flex-space-between">
										<span class="date">2020-02-15</span>
										<span class="name">홍길동(20180000)</span>
									</div>
									<div class="card-etc flex flex-space-between align-items-center">
										<span class="label">오디오</span>
										<div class="etc-btn">
											<button><i class="update"></i></button>
											<button><i class="delete"></i></button>
											<button class="btn-wish"><i class="wish on"></i></button>
											<button class="btn-like"><i class="like on"></i></button>
										</div>
									</div>
									<div class="card-cont" onclick="popup2('./form/multimedia_detail.jsp','multimedia_detail','1000','');" >
										<div class="thumb type-audio">
											<img src="${IMG}/icons/multi_audio.png" class="auto">
										</div>
										<div class="multi-data-code">QB_2020_10000083463</div>
										<div class="multi-data-name"><div class="t-ellipsis">풀이 설명 녹음본_20200214</div></div>
										<div class="multi-data-list">
											<div class="data-item"><div class="t-ellipsis"># [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈</div></div>
										</div>
									</div>
								</li>
								<li>
									<div class="card-info flex flex-space-between">
										<span class="date">2020-02-15</span>
										<span class="name">홍길동(20180000)</span>
									</div>
									<div class="card-etc flex flex-space-between align-items-center">
										<span class="label">동영상</span>
										<div class="etc-btn">
											<button><i class="update"></i></button>
											<button><i class="delete"></i></button>
											<button class="btn-wish"><i class="wish"></i></button>
											<button class="btn-like"><i class="like"></i></button>
										</div>
									</div>
									<div class="card-cont" onclick="popup2('./form/multimedia_detail.jsp','multimedia_detail','1000','');" >
										<div class="thumb">
											<img src="${IMG}/icons/question_registration/play.png" class="auto">
											<!--
											<img src="${IMG}/icons/image.png" class="auto">
											<img src="${IMG}/icons/question_registration/stack.png" class="auto">
											<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
											-->
										</div>
										<div class="multi-data-code">QB_2020_10007343</div>
										<div class="multi-data-name"><div class="t-ellipsis">20200217_수업 동영상</div></div>
										<div class="multi-data-list">
											<div class="data-item"><div class="t-ellipsis"># [성취수준] 수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.</div></div>
										</div>
									</div>
								</li>
								<li>
									<div class="card-info flex flex-space-between">
										<span class="date">2020-02-15</span>
										<span class="name">홍길동(20180000)</span>
									</div>
									<div class="card-etc flex flex-space-between align-items-center">
										<span class="label">동영상</span>
										<div class="etc-btn">
											<button><i class="update"></i></button>
											<button><i class="delete"></i></button>
											<button class="btn-wish"><i class="wish"></i></button>
											<button class="btn-like"><i class="like"></i></button>
										</div>
									</div>
									<div class="card-cont" onclick="popup2('./form/multimedia_detail.jsp','multimedia_detail','1000','');" >
										<div class="thumb">
											<img src="${IMG}/icons/question_registration/play.png" class="auto">
											<!--
											<img src="${IMG}/icons/image.png" class="auto">
											<img src="${IMG}/icons/question_registration/stack.png" class="auto">
											<img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
											-->
										</div>
										<div class="multi-data-code">QB_2020_100000000533</div>
										<div class="multi-data-name"><div class="t-ellipsis">곱셈과 나눗셈-개념 이해를 위한 설명 동영상</div></div>
										<div class="multi-data-list">
											<div class="data-item"><div class="t-ellipsis">수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈</div></div>
										</div>
									</div>
								</li>
								<li>
									<div class="card-info flex flex-space-between">
										<span class="date">2020-02-15</span>
										<span class="name">홍길동(20180000)</span>
									</div>
									<div class="card-etc flex flex-space-between align-items-center">
										<span class="label">오디오</span>
										<div class="etc-btn">
											<button><i class="update"></i></button>
											<button><i class="delete"></i></button>
											<button class="btn-wish"><i class="wish on"></i></button>
											<button class="btn-like"><i class="like on"></i></button>
										</div>
									</div>
									<div class="card-cont" onclick="popup2('./form/multimedia_detail.jsp','multimedia_detail','1000','');" >
										<div class="thumb type-audio">
											<img src="${IMG}/icons/multi_audio.png" class="auto">
										</div>
										<div class="multi-data-code">QB_2020_10000039433</div>
										<div class="multi-data-name"><div class="t-ellipsis">나눗셈-응용 풀이 설명 오디오(20200117)</div></div>
										<div class="multi-data-list">
											<div class="data-item"><div class="t-ellipsis">수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.</div></div>
										</div>
									</div>
								</li>
							</ul>
						</div>




						<div class="pagination">
							<button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
							<button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
							<ul class="paginate-list">
								<li class="page-num active"><a href="#">1</a></li>
								<li class="page-num"><a href="#">2</a></li>
								<li class="page-num"><a href="#">3</a></li>
								<li class="page-num"><a href="#">4</a></li>
								<li class="page-num"><a href="#">5</a></li>
								<li class="page-num"><a href="#">6</a></li>
								<li class="page-num"><a href="#">7</a></li>
								<li class="page-num"><a href="#">8</a></li>
								<li class="page-num"><a href="#">9</a></li>
								<li class="page-num"><a href="#">10</a></li>
							</ul>
							<button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
							<button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
						</div>


                    </article>

                </div>
            </div>
            <div id="preview"></div>
        </section>
    </body>
</html>

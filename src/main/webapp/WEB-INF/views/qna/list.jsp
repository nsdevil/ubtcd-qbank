<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/qna/list.css">
    </head>
    <body>

        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="../main.jsp">Home</a></span>
                    <span><a href="">MY</a></span>
                    <span><a href="../qna/list.jsp">1:1 문의</a></span>
                </div>

                <h2>1:1 문의</h2>
                
                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2" name="type" data-placeholder="구분">
                                <option value=""></option>
                                <option value="all" selected>전체</option>
                                <option value="notice-type">문의구분</option>
                                <option value="title">제목</option>
                                <option value="reply-status">답변상태</option>
                                <option value="date">문의일</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
                    <button class="btn-red shadow" onclick="location.href='./write.jsp';"><i class="plus"></i>문의글 작성</button>
                </div>
                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col style="width : 70px;">
                            <col style="width : 160px;">
                            <col style="width : 150px;">
                            <col>
                            <col style="width : 200px;">
                        </colgroup>
                        <thead class="border-red-top-2 border-gray-bottom-2">
                            <tr>
                                <th>
                                    No
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>
                                    구분
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>
                                    답변상태
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>
                                    제목
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>
                                    문의일
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr onclick="location.href='./show.jsp';">
                                <td>10</td>
                                <td>분류</td>
                                <td>상태</td>
                                <td>제목 최대 2줄까지 노출되며 2줄 넘으면 (...)처리</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr onclick="location.href='./show.jsp';">
                                <td>9</td>
                                <td>이용 관련</td>
                                <td>대기중</td>
                                <td>이미지를 추가하고 싶은데 어떻게 하나요?</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr onclick="location.href='./show.jsp';">
                                <td>8</td>
                                <td>이용 관련</td>
                                <td class="color-red">답변완료</td>
                                <td>추가했던 분류를 삭제하고 싶습니다..</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr onclick="location.href='./show.jsp';">
                                <td>7</td>
                                <td>분류</td>
                                <td>대기중</td>
                                <td>하나의 리스트에 10개씩 리스트 업 됨 ...</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr onclick="location.href='./show.jsp';">
                                <td>6</td>
                                <td>이용 관련</td>
                                <td class="color-red">답변완료</td>
                                <td>제목 최대 2줄까지 노출되며 2줄 넘으면 (...)처리</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr onclick="location.href='./show.jsp';">
                                <td>5</td>
                                <td>이용 관련</td>
                                <td class="color-red">답변완료</td>
                                <td>이미지를 추가하고 싶은데 어떻게 하나요?</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr onclick="location.href='./show.jsp';">
                                <td>4</td>
                                <td>분류</td>
                                <td class="color-red">답변완료</td>
                                <td>추가했던 분류를 삭제하고 싶습니다..</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr onclick="location.href='./show.jsp';">
                                <td>3</td>
                                <td>이용 관련</td>
                                <td class="color-red">답변완료</td>
                                <td>하나의 리스트에 0개씩 리스트 업 됨...</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr onclick="location.href='./show.jsp';">
                                <td>2</td>
                                <td>이용 관련</td>
                                <td class="color-red">답변완료</td>
                                <td>제목 최대 2줄까지 노출되며 2줄 넘으면 (...)처리</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr onclick="location.href='./show.jsp';">
                                <td>1</td>
                                <td>이용 관련</td>
                                <td class="color-red">답변완료</td>
                                <td>이미지를 추가하고 싶은데 어떻게 하나요?</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
                    <button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
                    <ul class="paginate-list">
                        <li class="page-num active"><a href="#">1</a></li>
                        <li class="page-num"><a href="#">2</a></li>
                        <li class="page-num"><a href="#">3</a></li>
                        <li class="page-num"><a href="#">4</a></li>
                        <li class="page-num"><a href="#">5</a></li>
                        <li class="page-num"><a href="#">6</a></li>
                        <li class="page-num"><a href="#">7</a></li>
                        <li class="page-num"><a href="#">8</a></li>
                        <li class="page-num"><a href="#">9</a></li>
                        <li class="page-num"><a href="#">10</a></li>
                    </ul>
                    <button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
                    <button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
                </div>

            </div>
        </section>
    </body>
</html>
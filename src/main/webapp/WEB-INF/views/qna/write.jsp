<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link href="${CSS}/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../lib/froala_editor/froala_editor.pkgd.min.css">
        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/qna/write.css">
        <script src="../lib/froala_editor/froala_editor.pkgd.min.js"></script>
    </head>
    <body>

        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="../main.jsp">Home</a></span>
                    <span><a href="">MY</a></span>
                    <span><a href="../qna/list.jsp">1:1 문의</a></span>
                </div>

                <form name="writeForm" class="writeForm" method="get" action="">
                    <div class="title">
                        <select class="select2" name="type" data-placeholder="구분" required>
                            <option value=""></option>
                            <option value="사이트 이용 불편">사이트 이용 불편</option>
                            <option value="개인정보 관련">개인정보 관련</option>
                            <option value="불건전 게시물 신고">불건전 게시물 신고</option>
                            <option value="버그&장애 신고">버그&장애 신고</option>
                            <option value="저작권 신고">저작권 신고</option>
                            <option value="제안">제안</option>
                            <option value="기타">기타</option>
                        </select>
                        <input type="text" name="title" placeholder="환자 사례는 어디에서 등록하면 되나요?">
                    </div>
                    <div class="content">
                        <div id="froala"></div>

                        <div class="btn-wrap">
                            <button class="btn-white shadow" onclick="location.href='./list.jsp';"><i class="menu-bars"></i>목록으로</button>
                            <div class="flex">
                                <button class="btn-gray shadow">취소</button>
                                <button class="btn-red shadow" onclick="history.go(-1);"><i class="check-white"></i>제출</button>
                            </div>
                        </div>
                    </div>
                </form>



            </div>
        </section>
    </body>
</html>

<%@ page contentType = "text/html;charset=utf-8" %>
        <div class="wrap">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
			<div class="pc-certi-cont">
				<p class="info-txt">아래 버튼을 클릭하시면 <strong>사용자 PC의 정보(IP, MAC Address)</strong>를 조회한 후 등록하실 수 있습니다.</p>
				<a href="#" class="btn-red shadow big-btn">내 PC정보 확인하기</a>
				<div class="certi-address-info">
					<ul>
						<li class="flex justify-content-center">
							<span class="label">IP Address</span>
							<span class="address">255.255.255.123</span>
						</li>
						<li class="flex justify-content-center">
							<span class="label">MAC Address</span>
							<span class="address">01-22-33-AC-AA</span>
						</li>
					</ul>
					<div class="btn-wrap">
						<button class="btn-gray shadow" onclick="popupClose()">취소</button>
						<button class="btn-red shadow">변경</button>
					</div>
				</div>
			</div>

        </div>

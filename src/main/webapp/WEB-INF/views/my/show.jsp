<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <link rel="stylesheet" href="${CSS}/common/header.css">
        <link rel="stylesheet" href="${CSS}/common/footer.css">
        <link rel="stylesheet" href="${CSS}/my/show.css">
    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="../main.jsp">Home</a></span>
                    <span><a href="">MY</a></span>
                    <span><a href="../my/show.jsp">정보관리</a></span>
                </div>

                <h2>내 정보 관리</h2>

                <article class="flex">
                    <div class="profile">
                        <div class="image"><img src="${IMG}/icons/profile.png" class="auto"></div>
                        <div class="update-btn"><button onclick="location.href='./update.jsp';">수정</button></div>
                    </div>
                    <div class="detail">
                        <div class="default">
                            <h3>기본 정보(직접 수정 불가)<p>변경이 필요한 사항은 학교 관리자에 문의해주세요.</p></h3>
                            <ul class="flex">
                                <li>
                                    <div class="title">이름</div>
                                    <div class="desc">박기남</div>
                                </li>
                                <li>
                                    <div class="title">ID</div>
                                    <div class="desc">katie</div>
                                </li>
                                <li>
                                    <div class="title">사번(학교)</div>
                                    <div class="desc">123456789</div>
                                </li>
                                <li>
                                    <div class="title">소속 기관(학교)</div>
                                    <div class="desc">한국대학교</div>
                                </li>
                                <li>
                                    <div class="title">교실</div>
                                    <div class="desc">해부학교실</div>
                                </li>
                                <li>
                                    <div class="title">구분</div>
                                    <div class="desc">기초</div>
                                </li>
                                <li>
                                    <div class="title">소속 기관(병원)</div>
                                    <div class="desc">한국병원</div>
                                </li>
                                <li>
                                    <div class="title">병원 소속</div>
                                    <div class="desc">인재개발원</div>
                                </li>
                                <li>
                                    <div class="title">사번(병원)</div>
                                    <div class="desc">123456789</div>
                                </li>
                                <li>
                                    <div class="title">시스템 권한</div>
                                    <div class="desc">객원교수/전임교수</div>
                                </li>
                                <li>
                                    <div class="desc only">해부학교실 책임교수</div>
                                </li>
                                <li>
                                    <div class="desc only">000000시험 책임교수</div>
                                </li>
                                <li class="long">
                                    <div class="desc">가나다라마바사아자차카타파하가나다라마 교육과정 책임교수</div>
                                </li>
                            </ul>
                        </div>
                        <div class="phone">
                            <h3>연락처<p>연락처를 변경하시려면 수정 버튼을 클릭해주세요.</p></h3>
                            <ul class="flex">
                                <li>
                                    <div class="title">휴대폰</div>
                                    <div class="desc">010-0000-0000</div>
                                </li>
                                <li>
                                    <div class="title">이메일</div>
                                    <div class="desc">katie@hankuk.ac.kr</div>
                                </li>
                            </ul>
                        </div>
                        <div class="password clear">
                            <h3>비밀번호<p>비밀번호를 변경하시려면 수정 버튼을 클릭해주세요.</p></h3>
                            <button class="btn-red shadow" onclick="location.href='./update.jsp';">수정</button>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </body>
</html>
<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <link rel="stylesheet" href="${CSS}/common/header.css">
        <link rel="stylesheet" href="${CSS}/common/footer.css">
        <link rel="stylesheet" href="${CSS}/notice/list.css">
    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="../main.jsp">Home</a></span>
                    <span><a href="">MY</a></span>
                    <span><a href="../my/pc_certification.jsp">PC인증 관리</a></span>
                </div>

                <h2>PC인증 관리</h2>

                <div class="my-certi flex flex-space-between">
					<ul>
						<li>인증 PC는 최대 2개까지 등록 가능합니다.</li>
						<li>보안 목적상 [PC 추가 인증 등록]은 관리자의 승인 절차가 필요합니다.</li>
					</ul>
                    <button type="button" class="btn-red shadow" onclick="popup('./form/pc_add.jsp','pc_add','500','350');"><i class="plus"></i>PC 추가 인증 요청</button>
                </div>
                <div class="table-wrap certi-table">
                    <table>
                        <colgroup>
                            <col style="width : 70px;">
                            <col style="width : 150px;">
                            <col>
                            <col style="width : 310px;">
                            <col style="width : 315px;">
                            <col style="width : 90px;">
                        </colgroup>
                        <thead class="border-red-top-2 border-gray-bottom-2">
                            <tr>
                                <th>번호</th>
                                <th>구분</th>
                                <th>IP Address</th>
                                <th>MAC Address</th>
                                <th>인증일시</th>
                                <th>변경</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>인증 완료</td>
                                <td>255.255.255.123</td>
                                <td>01-22-33-AC-AA</td>
                                <td>2019-12-01 14:23:14</td>
                                <td><button type="button" class="btn-red-line" onclick="popup('./form/pc_update.jsp','pc_update','500','350');">변경</button></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>승인 요청 중</td>
                                <td>255.255.255.101</td>
                                <td>01-11-22-BC-BA</td>
                                <td></td>
                                <td><button type="button" class="btn-red-line" onclick="popup('./form/pc_update.jsp','pc_update','500','350');">변경</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </body>
</html>

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <title>Засах</title>
    <link rel="stylesheet" href="${CSS}/common/header.css">
    <link rel="stylesheet" href="${CSS}/common/footer.css">
    <link rel="stylesheet" href="${CSS}/my/show.css">

    <script src="${JS}/vue/vue-router.js"></script>

    <style>
        .additional {
            display: flex;
            float: right;
        }

        .btn-red {
            color: #fff;
            background-color: #fb5f33;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 50px;
            border-radius: 2px;
            width: 80px;
        }

        .btn-gray {
            color: #fff;
            background-color: #a3a3a3;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 50px;
            margin-right: 30px;
            border-radius: 2px;
            width: 80px;
        }
        #se-pre-con {
            position: fixed;
            -webkit-transition: opacity 5s ease-in-out;
            -moz-transition: opacity 5s ease-in-out;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
        }
    </style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="userDetails">
    <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
        <div class="breadcrumb">
            <span><a href="../main.jsp">Home</a></span>
            <span><a href="">MY</a></span>
            <span><a href="../my/show.jsp">Details</a></span>
        </div>

        <h2><spring:message code="user.mainInfo" /> </h2>

        <article class="flex" style="margin-top: 25px;">
            <div class="profile">
                <div class="image"><img src="${IMG}/icons/profile.png" class="auto"></div>
                <div class="update-btn">
                    <button>Picture</button>
                </div>
                <p>** <spring:message code="user.require" />.</p>
            </div>
            <div class="detail">
                <div class="default">
                    <h3><spring:message code="examreg.mainInfo" /> </h3>
                    <ul class="flex">
                        <li>
                            <div class="title"><spring:message code="LastName"/>
                                <spring:message code="LastName" var="lasts"/></div>
                            <input type="text" class="desc" placeholder="${lasts}" v-model="user.lastName" name="updatelast"/>
                        </li>
                        <li>
                            <div class="title"><spring:message code="FirstName"/>
                                <spring:message code="FirstName" var="firsts"/></div>
                            <input type="text" class="desc" placeholder="${firsts}" v-model="user.name" name="updatename"/>
                        </li>
                        <li>
                            <div class="title"><spring:message code="email"/></div>
                            <spring:message code="email" var="emails"/>
                            <input type="text" class="desc" placeholder="${emails}" v-model="user.email" name="updateemail" />
                        </li>
                        <li>
                            <div class="title"><spring:message code="user.loginID"/> **</div>
                            <spring:message code="user.loginID" var="loginID"/>
                            <input type="text" class="desc" placeholder="loginID" v-model="user.loginId" disabled/>
                        </li>
<%--                        <li>--%>
<%--                            <div class="title"><spring:message code="user.subject"/>**</div>--%>
<%--                            <spring:message code="user.subject" var="subjects"/>--%>
<%--                            <input type="text" class="desc" placeholder="${subjects}" v-model="user.subject" disabled/>--%>
<%--                        </li>--%>
                        <li>
                            <div class="title"><spring:message code="phonenumber"/></div>
                            <spring:message code="phonenumber" var="phonenumber"/>
                            <input type="text" class="desc" placeholder="${phonenumber}" v-model="user.phone" name="updatephone"/>
                        </li>
                        <li>
                            <div class="title"><spring:message code="user.schCode"/> **</div>
                            <spring:message code="user.schCode" var="schCode"/>
                            <input type="text" class="desc" placeholder="${schCode}" v-model="user.schCode" disabled/>
                        </li>
                        <li>
                            <div class="title"><spring:message code="user.classID"/> **</div>
                            <spring:message code="user.classID" var="classID"/>
                            <input type="text" class="desc" placeholder="${classID}" v-model="user.groupNo" disabled/>
                        </li>
                        <li>
                            <div class="title"><spring:message code="user.gender"/></div>
                            <select class="wd-100" name="subclass" v-model="user.gender">
                                <option value="" disabled="" selected=""><spring:message code="user.select"/></option>
                                <option value="woman"><spring:message code="user.woman"/></option>
                                <option value="man"><spring:message code="user.man"/></option>
                            </select>
                        </li>
                    </ul>
                </div>

                <div class="btn-wrap additional">
                    <button class="btn-gray shadow" @click="back()"><spring:message code="all.close"/></button>
                    <button class="btn-red shadow" @click="updateUser()"><spring:message code="update"/></button>
                </div>
            </div>
        </article>
    </div>
</section>
<script type="text/javascript">
    let router = new VueRouter({
        mode: 'history',
        routes: []
    });
    let app = new Vue({
        router,
        data: {
            user: {
                loginId: '',
                email: '',
                password: '',
                passComp: '',
                phone: '',
                name: '',
                lastName: '',
                image: '',
                schCode: '',
                groupNo: '',
            },
        },
        computed: {},
        watch: {
            name(val) {
                this.user.password = val.replace(/\W/g,"");
            },
        },
        created() {
            if (typeof this.$route.query.id != 'undefined') {
                let id = this.$route.query.id;
                this.loaddata(id)
            } else {
                alert("Хэрэглэгчийн ID олдсонгүй. Та шалгаад дахиад оролдоно уу.")
            }
        },
        methods: {
            nameKeydown(e) {
                if (/^\W$/.test(e.key)) {
                    e.preventDefault();
                }
            },
            loading(cmd) {
                var l = document.getElementById("se-pre-con");
                l.style.display = cmd;
            },
            clearUserData() {
                this.user.email = ""
                this.user.groupNo = ""
                this.user.lastName = ""
                this.user.loginId = ""
                this.user.name = ""
                this.user.password = ""
                this.user.phone = ""
                this.user.role = ""
                this.user.schCode = ""
                this.user.gender = ''
            },
            async loaddata(userId) {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }
                    var _this = this;
                    _this.loading('block')
                    await axios.get('${BASEURL}/user/detail/' + userId, {
                        params: {},
                        headers: headers
                    }).then((response) => {
                        _this.loading('none')
                        if (response.status == 200) {
                            _this.user.loginId = response.data.loginId
                            _this.user.email = response.data.email
                            _this.user.gender = response.data.gender
                            _this.user.lastName = response.data.lastName
                            _this.user.name = response.data.name
                            _this.user.phone = response.data.phone
                            _this.user.schCode = response.data.schCode
                            _this.user.subject = response.data.subject
                            for (let i = 0; i < response.data.classes.length; i++) {
                                _this.user.groupNo = response.data.classes[i].groupName
                            }
                        } else {
                            alert("response status error")
                        }
                    }, function (error) {
                        _this.loading('none')
                        console.log(error)
                    });
                } catch (error) {
                    console.log(error)
                }
            },
            back(){
                window.location.href = "${HOME}/my/student"
            },
            updateUser() {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }
                    var _this = this
                    _this.loading('block')
                    axios.post('${BASEURL}/modify/user', {
                            name: this.user.name,
                            lastName: this.user.lastName,
                            phone: this.user.phone,
                            gender: this.user.gender,
                            email: this.user.email,
                            loginId : this.user.loginId
                        },
                        {headers: headers}).then((response) => {
                        if (response.data.status === 200) {
                            _this.loading('none')
                            alert(response.data.message);
                        } else {
                            _this.loading('none')
                            alert("ERROR: " + response.data.message)
                        }
                    }, (error) => {
                        _this.loading('none')
                        alert(error);
                    });
                } catch (error) {
                    alert(error);
                }
            },
        }
    });
    app.$mount('#userDetails')
</script>
</body>
</html>

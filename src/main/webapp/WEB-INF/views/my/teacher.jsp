<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <link rel="stylesheet" href="${CSS}/common/header.css">
    <link rel="stylesheet" href="${CSS}/common/footer.css">
    <link rel="stylesheet" href="${CSS}/notice/list.css">
    <link rel="stylesheet" href="${CSS}/my/show.css">
    <link rel="stylesheet" href="${CSS}/question_registration/register_information.css">
    <style>
        .displaydiv {
            display: block !important;
        }
        #se-pre-con {
            position: fixed;
            -webkit-transition: opacity 5s ease-in-out;
            -moz-transition: opacity 5s ease-in-out;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
        }
    </style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="teacherlist">
    <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
        <div class="breadcrumb">
            <span><a href="../main.jsp"><spring:message code="all.home"/></a></span>
            <span><a href=""><spring:message code="all.user.register"/></a></span>
            <span><a href="../my/pc_certification.jsp"><spring:message code="all.teacher"/></a></span>
        </div>
        <h2><spring:message code="teacher.list" /></h2>

<%--        <div class="my-certi flex flex-space-between"> --%>
<%--            <input style="position: absolute; height: 50px; width: 212px; opacity: 0;" name="file" type="file">--%>
<%--            <button type="button" class="btn-red-line shadow" style="width: 200px; height: 37px">--%>
<%--                <spring:message code="default.excelUpload"/>--%>
<%--            </button>--%>

<%--            <button type="button" class="btn-red shadow" style="height: 37px"--%>
<%--                    @click="popupClose()">--%>
<%--                <i class="plus"></i>--%>
<%--                <spring:message code="teacher.register"/>--%>
<%--            </button>--%>
<%--        </div>--%>
        <div class="table-wrap certi-table">
            <table>
                <colgroup>
                    <col style="width : 70px;">
                    <col style="width : 150px;">
                    <col style="width : 150px;">
                    <col style="width : 310px;">
                    <col style="width : 315px;">
                    <col style="width : 150px;">
                </colgroup>
                <thead class="border-red-top-2 border-gray-bottom-2">
                <tr>
                    <th><spring:message code="all.number"/></th>
                    <th><spring:message code="name"/></th>
                    <th><spring:message code="schoollist.schoolName"/></th>
                    <th><spring:message code="class"/></th>
                    <th><spring:message code="major"/></th>
                    <th>...</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item,index) in teacherlist">
                    <td>{{index+1}}</td>
                    <td>{{item.name}}</td>
                    <td>{{item.schname}}</td>
                    <td>{{item.group_name}}</td>
                    <td>_</td>
                    <td>
                        <button type="button" class="btn-red-line" @click="updateUserModal(item.id)">Edit</button>
                        <button type="button" class="btn-red-line" @click="classAdd(item.id)">Add</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="pagination">
            <button class="start" @click="firstQuestion()"><img src="${IMG}/icons/start.png" class="auto"></button>
            <button class="prev" @click="prevQuestion()"><img src="${IMG}/icons/prev.png" class="auto"></button>
            <ul class="paginate-list">
                <li class="page-num " v-for="(item,index) in pages " :key="index"
                    :class="{'active': currentPage === item}"
                    v-if="Math.abs(item - currentPage) < 3 || item == pages || item == 1">

                    <a href="javascript:void(0);" @click="questionCurrent(item)" :class="{
					last: (item == pages && Math.abs(item - currentPage) > 3),
					first: (item == 1 && Math.abs(item - currentPage) > 3)}">{{item}}</a>

                </li>
            </ul>
            <button class="next" @click="nextQuestion()"><img src="${IMG}/icons/next.png" class="auto"></button>
            <button class="end" @click="lastQuestion()"><img src="${IMG}/icons/end.png" class="auto"></button>
        </div>
    </div>


    <div v-show="userRegisterModal" class="modalDialog" :class="{'displaydiv': userRegisterModal}"
         style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <a href="javascript:void(0)" @click="popupClose()" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p style="font-weight: 800"><spring:message code="student.register"/></p>
                    </div>
                    <div class="modal-body">
                        <article class="flex">
                            <div class="profile" style="margin-top: 20px; padding-left: 50%;">
                                <div class="image"><img src="${IMG}/icons/profile.png" class="auto"></div>
                                <%--                                <div class="update-btn">--%>
                                <%--                                    <button onclick="location.href='./update.jsp';">--%>
                                <%--                                        <spring:message code="save" />--%>
                                <%--                                    </button>--%>
                                <%--                                </div>--%>
                            </div>
                            <%--                            style="margin-top: 30px;"--%>
                            <div class="detail">
                                <div class="default">
                                    <h3>** <spring:message code="user.require" />
                                    </h3>
                                    <ul class="flex">
                                        <li>
                                            <div class="title"><spring:message code="LastName"/> **
                                                <spring:message code="LastName" var="lasts"/></div>
                                            <input type="text" class="desc" placeholder="${lasts}" v-model="student.lastName"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="FirstName"/> **
                                                <spring:message code="FirstName" var="firsts"/></div>
                                            <input type="text" class="desc" placeholder="${firsts}" v-model="student.name" />
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="email"/></div>
                                            <spring:message code="email" var="emails"/>
                                            <input type="text" class="desc" placeholder="${emails}" v-model="student.email"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.loginID"/> **</div>
                                            <spring:message code="user.loginID" var="loginID"/>
                                            <input type="text" class="desc" placeholder="loginID" v-model="student.loginId"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.password"/>**</div>
                                            <spring:message code="user.password" var="password"/>
                                            <input type="password" class="desc" placeholder="${password}" v-model="student.password" />
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.passComp"/>**</div>
                                            <spring:message code="user.passComp" var="passComp"/>
                                            <input type="password" class="desc" placeholder="${passComp}" v-model="student.passComp"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="phonenumber"/></div>
                                            <spring:message code="phonenumber" var="phonenumber"/>
                                            <input type="text" class="desc" placeholder="${phonenumber}" v-model="student.phone"/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.schCode"/></div>
                                            <spring:message code="user.schCode" var="schCode"/>
                                            <input type="text" class="desc" placeholder="${schCode}" v-model="student.schCode" disabled/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.classID"/></div>
                                            <spring:message code="user.classID" var="classID"/>
                                            <input type="text" class="desc" placeholder="${classID}" v-model="student.groupNo" disabled/>
                                        </li>
                                        <li>
                                            <div class="title"><spring:message code="user.gender"/></div>
                                            <select class="wd-100" name="subclass" v-model="student.gender">
                                                <option value="" disabled="" selected=""><spring:message code="user.select"/></option>
                                                <option value="woman"><spring:message code="user.woman"/></option>
                                                <option value="man"><spring:message code="user.man"/></option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="password clear">
                                    <button type="button" class="btn-red shadow" @click="userRegister()" style="height: 39px;">
                                        <spring:message code="examlist.register" /></button>
                                    <button type="button" @click="popupClose()" class="btn-gray shadow"
                                            style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                                        <spring:message code="all.close" /></button>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    let app = new Vue({
        data: {
            userRegisterModal: false,
            teacherlist: [],
            currentPage: 1,
            pages: 0,
            itemsPerPage: 1,
            totalElement: 0,
            student:{
                loginId: '',
                email: '',
                password: '',
                passComp: '',
                phone: '',
                name: '',
                image: '',
                lastName:'',
                schCode: '',
                groupNo: ''
            },
        },
        computed: {
            paginate: function () {
                if (!this.teacherlist || this.teacherlist.length != this.teacherlist.length) {
                    return
                }
                this.resultCount = this.teacherlist.length
                if (this.currentPage >= this.pages) {
                    this.currentPage = this.pages
                }
                var index = this.currentPage * this.itemsPerPage - this.itemsPerPage
                return this.teacherlist.slice(index, index + this.itemsPerPage)
            }
        },

        created() {
            this.loaddata(this.currentPage)
        },
        methods: {
            clearUserData(){
                this.student.email = ""
                this.student.groupNo = ""
                this.student.lastName = ""
                this.student.loginId = ""
                this.student.name = ""
                this.student.password = ""
                this.student.phone = ""
                this.student.role = ""
                this.student.schCode = ""
                this.student.gender = ''
            },
            popupClose() {
                this.student.password = ''
                if (!this.userRegisterModal) {
                    document.body.style.position = 'fixed';
                } else {
                    document.body.style.position = 'relative';
                }
                this.userRegisterModal = !this.userRegisterModal;
            },
            updateUserModal(id) {
                window.location.href = "${HOME}/my/update?id=" + id
            },
            loading(cmd) {
                var l = document.getElementById("se-pre-con");
                l.style.display = cmd;
            },
            loaddata(page) {
                try {
                    if (page > 0) {
                        page = page - 1
                    }
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this;
                    _this.loading('block')
                    axios.post('${BASEURL}/users',
                        {
                            page : page,
                            schCode: '',
                            groupNo: '',
                            sort: '',
                            sortfield: '',
                            role: '3'
                        },
                        {headers: headers}).then((response) => {
                        if (response.status == 200) {
                            _this.loading('none')
                            _this.teacherlist = response.data.content;

                            for (let i = 0; i < _this.teacherlist.length; i++) {
                                _this.teacherlist[i].checked = false;
                            }

                            _this.pages = response.data.totalPages
                            _this.totalElement = response.data.totalElements
                        } else {
                            _this.loading('none')
                            alert("ERROR: ")
                        }
                    }, (error) => {
                        _this.loading('none')
                        console.log(error);
                    });
                } catch (error) {
                    console.log(error)
                    this.loading('none')
                }
            },
            userRegister(){
                try {
                    if (this.student.password == this.student.passComp)
                    {
                        if (this.student.email == '' && this.student.loginId == ''
                            && this.student.password == ''&& this.student.passComp == '')
                        {
                            alert("<spring:message code="user.alertRequire"/>")
                        }
                        else{
                            var language =  "<%=pageContext.getResponse().getLocale()%>"
                            var locale = ''
                            if (language == 'kr')
                            {
                                locale = 'kr-KR'
                            }
                            if(language == 'mn')
                            {
                                locale = 'mn-MN'
                            }
                            if(language == 'en'){
                                locale = 'en-EN'
                            }

                            const headers = {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                                'Accept-Language' : locale
                            }
                            var _this = this;

                            let item = {
                                // image : '',
                                loginId : this.student.loginId,
                                email : this.student.email,
                                password : this.student.password,
                                phone : this.student.phone,
                                name : this.student.name,
                                lastName : this.student.lastName,
                                schCode : this.student.schCode,
                                groupNo : this.student.groupNo,
                                role : '2',
                                gender: this.student.gender

                            };
                            axios.post('${BASEURL}/create/user', item, {
                                headers: headers
                            }).then(function (response) {
                                if (response.data.status == 200) {
                                    if (response.data.success== true)
                                    {
                                        alert(response.data.message)
                                        _this.userRegisterModal = false
                                        _this.clearUserData()
                                    }
                                    else{
                                        alert(response.data.message)
                                    }
                                } else {
                                    alert(response.data.message);
                                }
                            }, (error) => {
                                console.log(error);
                                alert('Error')
                            });
                        }
                    }
                    else{
                        alert("Нууц үгээ зөв оруулна уу.");
                        return
                    }

                } catch (error) {
                    console.log(error)
                }
            },
            nextQuestion() {
                if (this.pages > this.currentPage) {
                    this.currentPage = this.currentPage + 1
                    this.loaddata(this.currentPage)
                }
            },
            questionCurrent(item) {
                this.currentPage = item
                this.loaddata(this.currentPage)
            },
            prevQuestion() {
                if (this.currentPage > 1) {
                    this.currentPage = this.currentPage - 1
                    this.loaddata(this.currentPage)
                }
            }
            ,
            lastQuestion() {
                this.currentPage = this.pages
                this.loaddata(this.currentPage)
            }
            ,
            firstQuestion() {
                this.currentPage = 1
                this.loaddata(this.currentPage)
            }
            ,
            searchFilter() {
                this.loaddata(this.currentPage);
            },
            defaultSearch() {
                window.location.href = "${HOME}/question_card/questionbank" + this.examId
            },

        }
    });
    app.$mount('#teacherlist')
</script>
</body>
</html>

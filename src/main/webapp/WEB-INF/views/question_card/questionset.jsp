<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <title>Question set</title>
    <link rel="stylesheet" href="${CSS}/question_card/institutional/default_list.css">
    <link rel="stylesheet" href="${CSS}/question_card/questionset.css">
    <link rel="stylesheet" href="${CSS}/questionbank/additional.css">
    <link href="${CSS}/select2.min.css" rel="stylesheet" />
<%--    <script src="${JS}/questionbank/datepicker.min.js"></script>--%>
    <script src="${JS}/scroll-loader.js"></script>
    <script src="${JS}/vue/vue-router.js"></script>
    <script src="${JS}/select2.min.js"></script>
    <style>
        .mainCateSpace{
            margin-left: 35px;
        }
        .cateSpace{
            margin-left: 100px;
        }
        .myquest{
            font-size: 19px;
            font-weight: 800;
            background: none;
        }
        #se-pre-con {
            position: fixed;
            -webkit-transition: opacity 5s ease-in-out;
            -moz-transition: opacity 5s ease-in-out;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
        }
        .questionTitle{
            margin-top: 20px;
            text-align: center;
        }
        .questionTitle span{
            font-size: 18px;
            font-weight: 900;
            color: #fb5f33;
        }
    </style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="questionset">
    <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
        <h2><spring:message code="examAddQuestion" /></h2>
        <div class="table-wrap">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <colgroup>
                    <col width="15%">
                    <col width="24%">
                    <col width="10%">
                    <col width="26%">
                    <col width="10%">
                    <col width="8%">
                </colgroup>
                <tbody>
                <tr class="border-red-top-2 border-gray-bottom-2">
                    <td class="tit"><spring:message code="all.category"/></td>
                    <td colspan="5" style="text-align: left; padding-left: 15px;">
                        <span class="mainCateSpace"><spring:message code="all.main"/></span>
                        <select class="select2 select2-container select2-container--default"
                                style="width: 200px !important;"   v-model="search.cate1" id="selectSearchSet" >
                            <template v-for="(item, index) in mainCategory.main">
                                <option :value="item.id" :key="index">{{item.name}} - ({{item.regName}})</option>
                            </template>
                        </select>
                        <span class="cateSpace"><spring:message code="all.middle"/></span>
                        <select class="select2 select2-container select2-container--default"
                                style="width: 200px;" @change="categoryChange('exam', 'middle')"
                                v-model="search.cate2">
                            <template v-for="(item, index) in mainCategory.middle">
                                <option :value="item.id" :key="index">{{item.name}}</option>
                            </template>
                        </select>
                        <span class="cateSpace"><spring:message code="all.low"/></span>
                        <select class="select2 select2-container select2-container--default"
                                style="width: 200px;" @change="categoryChange('exam', 'low')" v-model="search.cate3">
                            <template v-for="(item, index) in mainCategory.low">
                                <option :value="item.id" :key="index">{{item.name}}</option>
                            </template>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tit"><spring:message code="all.qlevel"/></td>
                    <td colspan="1">
                        <div class="flex sh-radio-wrap" style="padding-left: 15px; margin-bottom: 0px;">
                            <%--                            <input  name="questionDiff" type="checkbox" :value="1" id="soeasy" class="checkbox"--%>
                            <%--                                    v-model="search.qlevel">--%>
                            <%--                            <label for="soeasy">매우 쉬움</label>--%>
                            <%--                            <input  name="questionDiff" type="checkbox" :value="2" id="easy" class="checkbox"--%>
                            <%--                                    v-model="search.qlevel">--%>
                            <%--                            <label for="easy">쉬움 </label>--%>
                            <%--                            <input  name="questionDiff" type="checkbox" :value="3" id="normal" class="checkbox"--%>
                            <%--                                    v-model="search.qlevel">--%>
                            <%--                            <label for="normal">보통 </label>--%>
                            <%--                            <input  name="questionDiff" type="checkbox" :value="4" id="diff" class="checkbox"--%>
                            <%--                                    v-model="search.qlevel">--%>
                            <%--                            <label for="diff">어려움 </label>--%>
                            <%--                            <input  name="questionDiff" type="checkbox" :value="5" id="verydiff" class="checkbox"--%>
                            <%--                                    v-model="search.qlevel">--%>
                            <%--                            <label for="verydiff">매우 어려움</label>--%>
                            <div class="radio-wrap">
                                <input name="search_difficultyLevel" id="search_difficultyLevel3" type="radio"
                                       class="radio" value="1" v-model="search.qlevel">
                                <label for="search_difficultyLevel3">
                                    <spring:message code="all.easy"/>
                                </label>
                            </div>
                            <div class="radio-wrap">
                                <input name="search_difficultyLevel" id="search_difficultyLevel2" type="radio"
                                       class="radio" value="2" v-model="search.qlevel">
                                <label for="search_difficultyLevel2">
                                    <spring:message code="all.normal"/>
                                </label>
                            </div>
                            <div class="radio-wrap">
                                <input name="search_difficultyLevel" id="search_difficultyLevel1" type="radio"
                                       class="radio" value="3" v-model="search.qlevel">
                                <label for="search_difficultyLevel1">
                                    <spring:message code="all.diff"/>
                                </label>
                            </div>
                        </div>
                    </td>
                    <td class="tit"><spring:message code="all.questionType"/></td>
                    <td colspan="3">
                        <div class="flex  sh-radio-wrap" style="padding-left: 15px; margin-bottom: 0px;">
                            <div class="radio-wrap">
                                <input type="radio" class="radio" name="questionType" id="questionType1" value="3"
                                       v-model="search.qtype">
                                <label for="questionType1">
                                    <spring:message code="all.multiple"/>
                                </label>
                            </div>
                            <div class="radio-wrap">
                                <input type="radio" class="radio" name="questionType" id="questionType3" value="1"
                                       v-model="search.qtype">
                                <label for="questionType3">
                                    <spring:message code="all.single"/>
                                </label>
                            </div>
                            <div class="radio-wrap">
                                <input type="radio" class="radio" name="questionType" id="questionType4" value="2"
                                       v-model="search.qtype">
                                <label for="questionType4">
                                    <spring:message code="all.shortAnswer"/>
                                </label>
                            </div>
                            <div class="radio-wrap">
                                <input type="radio" class="radio" name="questionType" id="questionType2" value="4"
                                       v-model="search.qtype">
                                <label for="questionType4"><spring:message code="all.drawing"/></label>
                            </div>
                            <%--                            <input  name="questionType" type="checkbox" :value="1" id="single" class="checkbox"--%>
                            <%--                                    v-model="search.qtype">--%>
                            <%--                            <label for="single">객관식 (단일)</label>--%>
                            <%--                            <input  name="questionType" type="checkbox" :value="3" id="multiple" class="checkbox"--%>
                            <%--                                    v-model="search.qtype">--%>
                            <%--                            <label for="multiple">객관식 (복수)</label>--%>
                            <%--                            <input  name="questionType" type="checkbox" :value="2" id="short" class="checkbox"--%>
                            <%--                                    v-model="search.qtype">--%>
                            <%--                            <label for="short">주관식</label>--%>
                            <%--                            <input  name="questionType" type="checkbox" :value="4" id="draw" class="checkbox"--%>
                            <%--                                    v-model="search.qtype">--%>
                            <%--                            <label for="draw">그리기 문제</label>--%>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tit"><spring:message code="all.examName"/></td>
                    <td><input value="" class="" size="30" v-model="search.examName"></td>
                    <td class="tit"><spring:message code="all.submitter"/></td>
                    <td><input value="" class="" size="30" v-model="search.submitter"></td>
                    <td class="tit"><spring:message code="all.number"/></td>
                    <td><input value="" class="" size="30" v-model="search.number" style="width: 74px;"></td>
                </tr>
                <%--                <tr>--%>
                <%--                    <td class="tit">--%>
                <%--                        <spring:message code="qBank.date"/>--%>
                <%--                    </td>--%>
                <%--                    <td colspan="3">--%>
                <%--                        <div class="flex" style="margin-left: 100px;">--%>
                <%--                            <vuejs-datepicker class="my-datepicker"--%>
                <%--                                              v-model="search.lifeStartDate">--%>
                <%--                            </vuejs-datepicker>--%>

                <%--                            <vuejs-datepicker   class="my-datepicker" v-model="search.lifeEndDate"--%>
                <%--                                                style="margin-left: 60px;"--%>
                <%--                            ></vuejs-datepicker>--%>
                <%--                        </div>--%>
                <%--                    </td>--%>
                <%--                    <td class="tit">--%>
                <%--                        <spring:message code="question.register.realNumber"/>--%>
                <%--                    </td>--%>
                <%--                    <td>--%>
                <%--                        <input value="" class="" size="30" v-model="search.controlNo" style="width: 74px;">--%>
                <%--                    </td>--%>
                <%--                </tr>--%>
                <tr>
                    <td class="tit"><spring:message code="all.question"/></td>
                    <td><input name="questionBranch" value="" class="" size="30" v-model="search.question"></td>
                    <td class="tit"><spring:message code="all.keyword"/></td>
                    <td><input name="searchHeadScript" value="" class="" size="30" v-model="search.keyword"></td>
                    <td class="tit"><spring:message code="all.point"/></td>
                    <td><input name="searchExaminer" value="" class="" size="3" v-model="search.point" style="width: 74px;"></td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="search-wrap flex">
            <div class="search-btn-area">
                <input class="checkbox" type="checkbox" name="myQueiosn" id="myQueiosn" v-model="onlyme" @change="myQuestions()"/>
                <label for="myQueiosn" class="myquest">Миний асуултууд</label>
            </div>
            <div class="search-btn-area" style="margin-top: 30px;">
            <span class="questionSelect" style="padding: 4px 10px 0 0;">
                <spring:message code="all.questionCount"/> :
                <span class="list_total_count">{{question2.length}}</span>
            </span>
                <button class="btn-white shadow" @click="defaultSearch()"><spring:message code="all.defaultSearch"/></button>
                <button class="btn-red shadow" @click="searchQues"><spring:message code="all.search"/></button>
            </div>
        </div>

        <div class="content">
            <div class="flex" style="width: 100%;">
                <div style="width: 47%;  margin-bottom: 30px;">
                    <div class="flex">
                        <a class="btnSelect" href="javascript:void(0);" @click="selectAll('left', true)"><spring:message code="qSet.selectAll"/></a>
                        <a class="btnDeselect" href="javascript:void(0);" @click="selectAll('left', false)"><spring:message code="qSet.unSelectAll"/></a>
                        <p class="questionSelect" style="padding: 5px 2px 0px 10px; font-size: 16px;"><spring:message code="qSet.quesionSelected"/></p>
                        <span class="totalCount" style="padding: 5px 5px 0px 5px; font-size: 16px">
							{{leftSelectCount}}</span>
                    </div>
                    <div class="questionTitle">
                        <span>Нийт асуултууд </span>
                    </div>
                    <div class="content questionList">
                        <ul class="list">
                            <template v-for="(item,index) in question2">
                                <li v-if="item.display" :class="{ 'checked': item.checked}" :key="index">
                                    <a class="questionClick" href="javascript:void(0);"
                                       @click="selectQuestion(item, 'left')">
                                        <div class="flex">
                                            <span class="optionNumber" style="width: 13%;">{{item.id}}</span>
                                            <span style="padding-top: 9px; width: 83%;" v-html="item.question"></span>
                                            <span style="padding-top: 9px;  width: 10%; text-align: center; border: 1px solid #fb5f33;
                                                    color: #fff;  background: #fb5f33;  border-radius: 5px; height: 35px" @click="preview(item.id)">
                                                <spring:message code="qBank.see"/>
                                            </span>
                                        </div>
                                    </a>
                                </li>
                            </template>
                        </ul>
                        <scroll-loader :loader-method="allQuestionList" :loader-disable="disable">
                            <div><spring:message code="all.pleaseWait" />...</div>
                        </scroll-loader>
                        <div v-if="disable" class="icon_finish">
                            ..................................................................................................
                        </div>
                    </div>
                </div>
                <div class="middleBtn">
                    <input type="number" class="btn-mid-select" v-model="randomNumber"
                    style="width: 58px; height: 35px; padding: 10px; border-radius: 10px; font-weight: 600;">
                    <button @click="insertQuestion" class="btn-mid-select2">
                        <img src="${IMG}/icons/right-arrow.png">
                    </button>
                    <button @click="deleteQuestion" class="btn-mid-select2">
                        <img src="${IMG}/icons/left.png">
                    </button>
                    <button class="btn-mid-select3" @click="groupQuestionCheck()">
                        Group
                    </button>
                </div>
                <div style="width: 47%;position:relative">
                    <div class="flex">
                        <a class="btnSelect" href="javascript:void(0);"
                           @click="selectAll('right', true)"><spring:message code="qSet.selectAll"/></a>
                        <a class="btnDeselect" class="btnSelect" href="javascript:void(0);"
                           @click="selectAll('right', false)"><spring:message code="qSet.unSelectAll"/></a>
                        <p class="questionSelect" style="padding: 5px 2px 0px 10px; font-size: 16px;">
                            <spring:message code="qSet.quesionSelected"/>
                        </p>
                        <span class="totalCount" style="padding: 5px 5px 0px 5px;
						font-size: 16px">{{rightSelectCount}}</span>
                    </div>
                    <div class="questionTitle">
                        <span>Шалгалтын асуултууд </span>
                    </div>
                    <div class="content questionList">
                        <ul class="list">
                            <template v-for="(item, index) in types[activeOption].questions">
                                <li v-if="item.display" :class="{ 'checked': item.checked}" :key="index">
                                    <a class="questionClick" @click="selectQuestion(item, 'right')">
                                        <div class="flex">
                                            <span class="optionNumber" style="width: 13%">{{item.id}}</span>
                                            <span style="padding-top: 9px; width: 83%;" v-html="item.question"></span>
                                            <button style="width: 10%; vertical-align: middle; border: 1px solid #fb5f33;
                                                    color: #fff;  background: #fb5f33;  border-radius: 5px; height: 35px;" @click="preview(item.id)">
                                                <spring:message code="qBank.see"/>
                                            </button>
                                        </div>
                                    </a>
                                </li>
                            </template>
                        </ul>
                    </div>

                    <div class="middleBtn" style="position: absolute;right: 0px;margin-right: -41px;top: 0;">
                        <button @click="sortChange('up')" class="btn-mid-select">
                            <img src="${IMG}/icons/right-arrow.png" style="transform: rotate(-90deg);">
                        </button>
                        <button @click="sortChange('down')" class="btn-mid-select2" style="transform: rotate(-90deg);">
                            <img src="${IMG}/icons/left.png">
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="content bottom-btn">
            <button class="btn-red-line" @click="backTo()">
                <spring:message code="all.back" />
            </button>
            <button class="btn-red shadow" @click="generate()">
                <%--				<span v-if="useFlag == 'Y">Generate</span>--%>
                <span><spring:message code="examreg.register"/></span>
            </button>
        </div>
    </div>
</section>
<script>
    let router = new VueRouter({
        mode: 'history',
        routes: []
    });
    let apps = new Vue({
        router,
        el: '#questionset',
        // components: {
        //     vuejsDatepicker
        // },
        data: {
            randomNumber: 0,
            countRandom: 0,
            count: 0,
            id: 1,
            examDetail: [],
            totalElements: '',
            examId: 0,
            useFlag: 'N',
            leftSelectCount: 0,
            rightSelectCount: 0,
            activeOption: 0,
            gType: 'manual',
            exam:{
                id: '',
                examName: '',
                examDesc: '',
                unit: '',
            },
            search: {
                number: '',
                controlNo: '',
                lifeStartDate: "",
                lifeEndDate: "",
                submitter: '',
                schoolCode: '',
                gradeGroup: '',
                gradeCode: '',
                termCode: '',
                subject: '',
                standart: '',
                qlevel: '',
                qtype: '',
                question: '',
                keyword: '',
                examName: '',
                point: '',
                unit: '',
                topic: '',
                sessionCode: '',
                use: ''
            },
            types: [
                {
                    type: "A",
                    questions: []
                }
            ],
            typelib: ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
                "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W",
                "X", "Y", "Z"],
            cateQuestionCount: 0,
            questions: [],
            question2: [],
            mainCategory: {
                main: [],
                middle: [],
                low: []
            },
            subjectCategory: {
                main: [],
                middle: [],
                low: []
            },
            disable: false,
            page: 0,
            pageSize: 20,
            examDetails: [],
            guestionGroupCount: 0,
            onlyme: false
        },
        watch: {
            $route(to, from) {
                if (from.params.examId !== to.params.examId) {
                    this.examId = to.params.examId
                    this.getExamPutedQuestionss()
                }
            }
        },
        computed: {},
        mounted() {
            if (typeof this.$route.query.id != 'undefined') {
                let id = this.$route.query.id;
                this.examId = id;
                this.getExamPutedQuestionss(id);
            } else {
                alert("Question or register id undefined")
            }
            this.getCates("main", 1, 0);
        },
        created() {

        },
        methods: {
            loading(cmd) {
                var l = document.getElementById("se-pre-con");
                l.style.display = cmd;
            },
            preview(id) {
                window.open('${HOME}/question_registration/each/preview/question_preview?id='+ id,'_blank',
                    'width= 1550, height= 760');
            },
            async sortChange(type) {

                let oldIndex = 0
                for (let i = 0; i < this.types[0].questions.length; i++) {
                    if (this.types[0].questions[i].checked) {
                        oldIndex = i
                    }
                }
                if (this.rightSelectCount > 1 || this.rightSelectCount == 0) {
                    alert("<spring:message code="selectOneQuestion"/>")
                    return;
                }

                let newIndex = 0
                if (type == 'up') {
                    newIndex = oldIndex - 1

                } else {
                    newIndex = oldIndex + 1
                }
                try {

                    if (newIndex == -1) {
                        alert("this is first");
                        return;
                    }

                    if (newIndex == this.types[0].questions.length) {
                        alert("this is last");
                        return;
                    }
                    var _this = this;
                    _this.loading('block')
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    // console.log(newIndex + "   " + this.types[0].questions.length);
                    if (this.types[0].questions[newIndex].examQId)
                        axios.post('${BASEURL}/kexam/change/order', {
                            qId1: this.types[0].questions[newIndex].examQId,
                            qId2: this.types[0].questions[oldIndex].examQId
                        }, {headers: headers}).then((response) => {
                            _this.loading('none')
                            if (response.data.status == 200) {
                                _this.types[0].questions.splice(newIndex, 0,
                                    _this.types[0].questions.splice(oldIndex, 1)[0]);
                            } else {
                                alert(response.data.message)
                            }
                        }, function (error) {
                            console.log("axios error found");
                            console.log(error);
                            _this.loading('none')
                        });


                } catch (error) {
                    console.log(error);
                }
            },
            searchQues() {
                this.disable = false;
                this.page = 0;
                this.question2 = [];
                this.allQuestionList();
            },
            clearData(){
                this.search.number = ''
                this.search.controlNo = ''
                this.search.lifeStartDate = ""
                this.search.lifeEndDate = ""
                this.search.submitter = ''
                this.search.schoolCode = ''
                this.search.gradeGroup = ''
                this.search.gradeCode = ''
                this.search.termCode = ''
                this.search.subject = ''
                this.search.standart = ''
                this.search.qlevel = ''
                this.search.qtype = ''
                this.search.question = ''
                this.search.keyword = ''
                this.search.examName = ''
                this.search.point = ''
                this.search.unit = ''
                this.search.topic = ''
                this.search.sessionCode = ''
                this.search.use = ''
            },
            myQuestions(){
                this.clearData();
                this.question2 = [];
                this.page = 0;
                this.allQuestionList()
            },

            async allQuestionList() {
                try {
                    var _this = this;
                    _this.loading('block')
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }


                    let item = {
                        page: this.page++,
                        size: this.pageSize,
                        examId: this.examId,
                        question: this.search.question,
                        point: this.search.point,
                        keyword: this.search.keyword,
                        qtype: this.search.qtype,
                        qlevel: this.search.qlevel,
                        cate1: this.search.cate1,
                        cate2: this.search.cate2,
                        cate3: this.search.cate3,
                        // cate1: "7730",
                        // cate2: 7733,
                        // cate3: 7734,
                        scate1: this.search.scate1,
                        scate2: this.search.scate2,
                        scate3: this.search.scate3,
                        onlyme: this.onlyme,
                        regUser : this.search.submitter,
                        examName: this.search.examName,
                        startDate: this.search.lifeStartDate,
                        endDate: this.search.lifeEndDate,
                        qId: this.search.number,
                        controlNo: this.search.controlNo,
                        sort: 'asc',
                        sortfield: ''
                    };
                    axios.post('${BASEURL}/questions', item, {headers: headers}).then((response) => {

                        _this.loading('none')
                        if (response.status == 200) {
                            for (let i = 0; i < response.data.content.length; i++) {
                                let item = {
                                    checked: false,
                                    groupCheck: false,
                                    question: response.data.content[i].question,
                                    id: response.data.content[i].id,
                                    display: true,
                                    groupCheck: false
                                }
                                _this.question2.push(item)
                            }
                            if (response.data.content.length < _this.pageSize) {
                                _this.disable = true
                            }
                            _this.boxleftDisplayItems();
                            _this.loading('none')
                        } else {
                            alert("request failed")
                        }
                    }, function (error) {
                        console.log("axios error found");
                        console.log(error);
                        _this.loading('none')
                    });

                } catch (error) {
                    console.log("try catch error found");
                    console.log(error)
                }
            },
            async getCates(type, cateStep, parentId) {
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    var _this = this;

                    axios.get('${BASEURL}/category/liststep', {
                        params: {
                            type: type,
                            cateStep: cateStep,
                            parentId: parentId
                        },
                        headers: headers
                    }).then(function (response) {
                        if (response.data.status == 200) {
                            if (type == "main") {
                                //exam category
                                if (cateStep == 1) {
                                    _this.mainCategory.main = response.data.result.cates;
                                }
                                if (cateStep == 2) {
                                    _this.mainCategory.middle = response.data.result.cates;
                                }
                                if (cateStep == 3) {
                                    _this.mainCategory.low = response.data.result.cates;
                                }
                            } else {
                                //subject category
                                if (cateStep == 1) {
                                    _this.subjectCategory.main = response.data.result.cates;
                                }
                                if (cateStep == 2) {
                                    _this.subjectCategory.middle = response.data.result.cates;
                                }
                                if (cateStep == 3) {
                                    _this.subjectCategory.low = response.data.result.cates;
                                }
                            }
                        }
                    }, function (error) {
                        console.log(" error found : category get requested ")
                        console.log(error);
                    });
                } catch (error) {
                    console.log("category error catch")
                    console.log(error)
                }
            },
            categoryChange(type, catelevel) {
                if (type == "exam") {
                    type = "main";
                } else {
                    type = "subject";
                }
                if (catelevel == "main") {
                    let parentId = 0;
                    if (type == "main") parentId = this.search.cate1;
                    else parentId = this.search.scate1;
                    this.getCates(type, 2, parentId);
                }
                if (catelevel == "middle") {
                    let parentId = 0;
                    if (type == "main") parentId = this.search.cate2;
                    else parentId = this.search.scate2;
                    this.getCates(type, 3, parentId);
                }
            },
            defaultSearch() {
                window.location.href = "${HOME}/question_card/questionset?id=" + this.examId
            },
            backTo() {
                window.location.href = "${HOME}/kexam/examlist"
            },
            async generate() {
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this;

                    axios.post('${BASEURL}/kexam/generate', {
                            examId: this.examId
                        }, {headers: headers},
                    ).then(function (response) {
                        if (response.data.status == 200) {
                            _this.useFlag = 'Y'
                            console.log("kexam/generate")
                            console.log(response);
                            alert(response.data.message)
                            window.location.href = "${HOME}/kexam/examlist"
                        } else {
                            alert(response.data.message)
                        }
                    }, function (error) {
                        alert("Error")
                        console.log(error);
                    });

                } catch (error) {
                    console.log(error);
                }
            },
            async insertQuestion() {
               this.loading('block')
               if (this.randomNumber > 0)
               {
                   if (this.randomNumber > this.question2.length)
                   {
                       alert("Таны сонгосон хариулт Random тооноос бага байна.")
                       this.loading('none')
                       return
                   }
                   await this.randomize();
                   for (let i = 0; i < this.question2.length; i++) {
                       let item = this.question2[i];
                       if (item.checked) {
                           let insitem = {
                               checked: false,
                               question: item.question,
                               id: item.id,
                               examQId: -1,
                               display: false,
                               groupCheck: false
                           }
                           this.selectQuestion(item, "left")
                           if (this.randomNumber != this.countRandom)
                           {
                               this.countRandom++
                               this.types[0].questions.push(insitem)
                           }
                       }
                   }
                   if (this.types[0].questions.length == 0) {
                       alert("<spring:message code="selectOneQuestion"/>")
                       this.loading('none')
                       return
                       // window.location.reload()
                   }
                   this.putExamQuestions()
               }
               else{
                   for (let i = 0; i < this.question2.length; i++) {
                       let item = this.question2[i];
                       if (item.checked) {
                           let insitem = {
                               checked: false,
                               question: item.question,
                               id: item.id,
                               examQId: -1,
                               display: false,
                               groupCheck: false
                           }
                           this.selectQuestion(item, "left")
                           this.types[0].questions.push(insitem)
                       }
                   }
                   if (this.types[0].questions.length == 0) {
                       alert("<spring:message code="selectOneQuestion"/>")
                       this.loading('none')
                       return
                       // window.location.reload()
                   }
                   this.putExamQuestions()
               }
            },
            async randomize() {
                for (let i = this.question2.length - 1; i > 0; i--) {
                    let randomIndex = Math.floor(Math.random() * i);
                    let temp = this.question2[i];
                    this.$set(this.question2, i, this.question2[randomIndex]);
                    this.$set(this.question2, randomIndex, temp);
                    return this.question2
                }
            },
            async deleteQuestion() {
                let qIds = [];
                this.loading('block')
                var language =  "<%=pageContext.getResponse().getLocale()%>"
                var locale = ''
                if (language == 'kr')
                {
                    locale = 'kr-KR'
                }
                if(language == 'mn')
                {
                    locale = 'mn-MN'
                }
                if(language == 'en'){
                    locale = 'en-EN'
                }
                const headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                    'Accept-Language' : locale
                }
                for (let i = 0; i < this.types[0].questions.length; i++) {
                    let item = this.types[0].questions[i];

                    if (item.checked) {
                        this.selectQuestion(item, "right");
                        qIds.push(item.examQId);
                        // this.types[0].questions.splice(i, 1)
                    }
                }
                if (qIds.length == 0) {
                    alert("Please check question");
                    window.location.reload()
                }
                try {
                    let sdata = {
                        examId: this.examId,
                        type: this.types[0].type,
                        qIds: qIds
                    }
                    var _this = this;
                    axios.post('${BASEURL}/kexam/delete/questions', sdata, {
                        headers: headers
                    }).then(function (response) {
                        _this.loading('none')
                        if (response.data.status == 200) {
                            alert(response.data.message)
                            _this.getExamPutedQuestionss(_this.examId)
                        } else {
                            alert(response.data.message)
                        }
                    }, function (error) {
                        _this.loading('none')
                        alert("Error");
                        console.log(error);
                    });
                } catch (error) {
                    console.log(error);
                }
            }
            ,
            selectAll(box, sord) {
                if (box == 'left') {

                    for (let i = 0; i < this.question2.length; i++) {
                        let item = this.question2[i]
                        item.checked = sord
                    }
                    if (sord) {
                        let selcount = 0;
                        for (let i = 0; i < this.question2.length; i++) {
                            if (this.question2[i].display) {
                                selcount++;
                            }
                        }
                        this.leftSelectCount = selcount;
                    } else {
                        this.leftSelectCount = 0
                    }
                }
                if (box == "right") {
                    for (let i = 0; i < this.types[0].questions.length; i++) {
                        let item = this.types[0].questions[i]
                        item.checked = sord
                    }
                    if (sord) {
                        this.rightSelectCount = this.types[0].questions.length
                    } else {
                        this.rightSelectCount = 0
                    }

                }
            },
            groupQuestionCheck(){
                let qIds = []

                for (let i = 0; i < this.types[0].questions.length; i++) {
                    let item = this.types[0].questions[i];
                    if (item.groupCheck) {
                        this.selectQuestion(item);
                        qIds.push(item.examQId);
                    }
                }
                if (qIds.length == 0) {
                    alert("<spring:message code="selectOneQuestion"/>");
                    return;
                }
                try{
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    this.guestionGroupCount = qIds.length
                    var _this = this
                    _this.loading('block')
                    axios.post('${BASEURL}/kexam/question/group', {
                        examId: this.examId,
                        qIds: qIds
                    }, {headers: headers}).then((response) => {
                        _this.loading('none')
                        if (response.data.status == 200) {
                            if (response.data.success == true)
                            {
                                alert(_this.guestionGroupCount + " <spring:message code="groupquestion"/>")
                                _this.guestionGroupCount = 0
                            }
                            else{
                                alert(response.message)
                            }
                        } else {
                            alert(response.data.message)
                        }
                    }, function (error) {
                        console.log(error);
                        _this.loading('none')
                    });
                }
                catch (error) {
                    console.log(error)
                }

            },
            selectQuestion(item, LorR) {

                item.checked = !item.checked
                item.groupCheck = !item.groupCheck

                if (LorR == 'left') {
                    if (item.checked)
                    {
                        this.leftSelectCount++;
                    }
                    else
                    {
                        this.leftSelectCount--;
                    }

                } else {
                    if (item.checked)
                        this.rightSelectCount++;
                    else
                        this.rightSelectCount--;
                }

            }
            ,
            shortQuestion(question) {
                if (question && question.length > 50) {
                    return question.substring(0, 50) + '...'
                } else {
                    return question
                }
            }
            ,
            boxleftDisplayItems() {
                for (let i = 0; i < this.question2.length; i++) {

                    let itemq = this.question2[i]

                    // itemq.checked = false
                    if (this.isExistQId(itemq.id)) {
                        itemq.display = false
                    } else {
                        itemq.display = true
                    }
                }
            }
            ,
            async getExamPutedQuestionss(examId) {
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this;

                    axios.get('${BASEURL}/kexam/puted/questions', {
                        params: {
                            examId: examId
                        },
                        headers: headers
                    }).then(function (response) {
                        _this.loading('none')
                        if (response.data.status == 200) {
                            let questions = response.data.result.putedQuestions
                            _this.types[0].questions = [];
                            for (let i = 0; i < questions.length; i++) {
                                let item = questions[i]
                                let insitem = {
                                    checked: false,
                                    question: item.question,
                                    id: item.pQid,
                                    examQId: item.qId,
                                    display: true,
                                    groupCheck: false
                                }
                                _this.types[0].questions.push(insitem)
                            }
                            _this.page = 0;
                            _this.disable = false;
                            _this.question2 = [];
                            _this.allQuestionList(examId)
                        }
                    }, function (error) {
                        alert("Error")
                        console.log(error);
                    });

                } catch (error) {
                    console.log(error);
                }
            }
            ,


            isExist: function (option) {
                for (let i = 0; i < this.types.length; i++) {
                    if (this.types[i].type === option)
                        return i
                }
                return -1
            }
            ,

            isExistQId: function (qId) {
                for (let i = 0; i < this.types[0].questions.length; i++) {
                    if (this.types[0].questions[i].id == qId)
                        return true

                }
                return false
            }
            ,
            async putExamQuestions() {
                try {
                    this.countRandom = 0
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    let data = this.types[0]

                    data.examId = this.examId;

                    var _this = this;

                    axios.post('${BASEURL}/kexam/put/questions', data, {
                        headers: headers
                    }).then(function (response) {
                        if (response.data.status == 200) {
                            alert(response.data.message)
                            _this.getExamPutedQuestionss(_this.examId)
                            _this.loading('none')
                        } else {
                            alert(response.data.message)
                        }
                    }, function (error) {
                        alert("Error")
                        console.log(error);
                    });

                } catch (error) {
                    console.log(error);
                }
            }
            ,

        }
    })
    // app.$mount('#questionset')
</script>
<script>
    $('#selectSearchSet').on("change",function(){
        apps.search.cate1 = $(this).val();
        apps.categoryChange('exam','main')
    });

    $(document).ready(function() {
        $('#selectSearchSet').select2({});
    });
</script>
<style scoped>
    .bottom-btn {
        display: inline-flex;
        text-align: center;
        width: 100%;
        justify-content: center;
    }

    .bottom-btn button {
        width: 110px;
        margin-left: 20px;
    }
</style>
</body>
</html>

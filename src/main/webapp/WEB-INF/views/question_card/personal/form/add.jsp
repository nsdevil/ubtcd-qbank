<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap subpopup">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
            <form name="" method="" action="">
                <input type="text" class="input" name="title" placeholder='문항카드 이름을 입력하세요.'>
                <div class="btn-wrap">
                    <button class="btn-gray shadow" onclick="popupClose()">취소</button>
                    <button class="btn-red shadow">추가</button>
                </div>
            </form>
        </div>

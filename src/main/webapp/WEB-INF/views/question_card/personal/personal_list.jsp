<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/notice/list.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/question_card.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/institutional/add.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/personal/personal_list.css">
    </head>
    <body>

        <section class="no_contents">
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a  onclick="location.href='${HOME}/main';">Home</a></span>
                    <span><a href="">문항카드</a></span>
                    <span><a href="">개인별 문항카드</a></span>
                </div>

                <h2>개인별 문항카드</h2>
            
                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2" name="type" data-placeholder="구분">
                                <option value=""></option>
                                <option value="idx" selected>번호</option>
                                <option value="name">문항카드명</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
                    <div class="btn-wrap flex">
                        <button class="btn-card-list" onclick="location.href='${HOME}/question_card/personal/main_card_list';"><i class="list-type-card"></i></button>
                        <button onclick="location.href='${HOME}/question_card/personal/personal_list';"><i class="list-type-list-red"></i></button>
                    </div>
                </div>

                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col style="width : 95px;">
                            <col style="width : 140px;">
                            <col>
                            <col style="width : 220px;">
                        </colgroup>
                        <thead class="border-red-top-2 border-gray-bottom-2">
                            <tr>
                                <th>
                                    번호
                                </th>
                                <th colspan="2">
                                    문항카드명
                                </th>
                                <th>
                                    관리
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
								<td colspan="2"></td>
                                <td colspan="2"  class="first-name">
									<div class="flex-sec">
										<input type="text" name="subclass" placeholder="문항카드(대분류)명을 입력하세요." >
										<button class="btn-red"><i class="plus"></i>추가</button>
									</div>

                                </td>
                            </tr>
                            <tr class="main">
                                <td>1</td>
                                <td onclick="submenu_hide(this);"><i class="down"></i></td>
                                <td class="name">우리초등학교 (2020년도)</td>
                                <td class="management">
                                    <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                    <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                                </td>
                            </tr>
                            <tr class="middle">
                                <td></td>
                                <td></td>
                                <td class="name">5학년 3반 - A조 (김누리, 하예은, 최소은)</td>
                                <td class="management">
                                    <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                    <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                                </td>
                            </tr>
                            <tr class="middle">
                                <td></td>
                                <td></td>
                                <td class="name">5학년 3반 -B조 (이찬원, 정동원, 신인선)</td>
                                <td class="management">
                                    <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                    <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                                </td>
                            </tr>
                            <tr class="middle">
                                <td></td>
                                <td></td>
                                <td class="name">5학년 3반 - C조 (조인성, 차인표, 이서진)</td>
                                <td class="management">
                                    <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                    <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                                </td>
                            </tr>
<!--                             <tr class="middle"> -->
<!--                                 <td></td> -->
<!--                                 <td></td> -->
<!--                                 <td class="name">여성간호학(중간)</td> -->
<!--                                 <td class="management"> -->
<!--                                     <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button> -->
<!--                                     <button onclick="deleteCard();"><i class="delete"></i></button> -->
<!--                                 <button onclick="copyCard();"><i class="copy"></i></button> -->
<!--                                 </td> -->
<!--                             </tr> -->
                            
                            <tr class="middle">
                                <td></td>
                                <td></td>
                                <td class="name">
									<div class="flex-sec">
										<input type="text" name="middle" placeholder="문항카드(중분류)명을 입력하세요.">
										<button class="btn-red"><i class="plus"></i>추가</button>
									</div>
                                </td>
                                <td class="management">
                                    <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                    <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                                </td>
                            </tr>
                            <tr class="main">
                                <td>2</td>
                                <td><i class="down"></i></td>
                                <td class="name">우리초등학교 (2019년도)</td>
                                <td class="management">
                                    <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                    <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                                </td>
                            </tr>
                            <tr class="main">
                                <td>3</td>
                                <td><i class="down"></i></td>
                                <td class="name">우리초등학교 (2018년도)</td>
                                <td class="management">
                                    <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                    <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
                    <button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
                    <ul class="paginate-list">
                        <li class="page-num active"><a href="#">1</a></li>
                        <li class="page-num"><a href="#">2</a></li>
                        <li class="page-num"><a href="#">3</a></li>
                        <li class="page-num"><a href="#">4</a></li>
                        <li class="page-num"><a href="#">5</a></li>
                        <li class="page-num"><a href="#">6</a></li>
                        <li class="page-num"><a href="#">7</a></li>
                        <li class="page-num"><a href="#">8</a></li>
                        <li class="page-num"><a href="#">9</a></li>
                        <li class="page-num"><a href="#">10</a></li>
                    </ul>
                    <button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
                    <button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
                </div>
            </div>
        </section>

    </body>
</html> 
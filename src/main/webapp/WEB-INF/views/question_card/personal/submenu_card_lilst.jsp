<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/notice/list.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/institutional/add.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/personal/main_card_list.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/question_card.css">

    </head>
    <body>

        <section class="no_contents">
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a  onclick="location.href='${HOME}/main';">Home</a></span>
                    <span><a href="">문항카드</a></span>
                    <span><a href="">개인별 문항카드</a></span>
                </div>

                <h2>개인별 문항카드</h2>
                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2" name="type" data-placeholder="구분">
                                <option value=""></option>
                                <option value="idx" selected>번호</option>
                                <option value="name">문항카드명</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
                    <div class="btn-wrap flex">
                        <button class="btn-card-list" onclick="location.href='${HOME}/question_card/personal/main_card_list';"><i class="list-type-card-red"></i></button>
                        <button onclick="location.href='${HOME}/question_card/personal/personal_list';"><i class="list-type-list"></i></button>
                    </div>
                </div>

                <div class="card-list submenu-list">
                    <ul>
<!--                         <li> -->
<!--                             <div class="content"> -->
<!--                                 <div>여성건강간호학(수시 1차) 여성 건강간호학(수시 1차)</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button> -->
<!--                                 <button onclick="deleteCard();"><i class="delete"></i></button> -->
<!--                                 <button onclick="copyCard();"><i class="copy"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li> -->
<!--                             <div class="content"> -->
<!--                                 <div>여성건강간호학(수시 1차) 여성 건강간호학(수시 1차)</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button> -->
<!--                                 <button onclick="deleteCard();"><i class="delete"></i></button> -->
<!--                                 <button onclick="copyCard();"><i class="copy"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li> -->
<!--                             <div class="content"> -->
<!--                                 <div>여성건강간호학(수시 1차) 여성 건강간호학(수시 1차)</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button> -->
<!--                                 <button onclick="deleteCard();"><i class="delete"></i></button> -->
<!--                                 <button onclick="copyCard();"><i class="copy"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li> -->
<!--                             <div class="content"> -->
<!--                                 <div>여성건강간호학(수시 1차) 여성 건강간호학(수시 1차)</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button> -->
<!--                                 <button onclick="deleteCard();"><i class="delete"></i></button> -->
<!--                                 <button onclick="copyCard();"><i class="copy"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li> -->
<!--                             <div class="content"> -->
<!--                                 <div>여성건강간호학(수시 1차) 여성 건강간호학(수시 1차)</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button> -->
<!--                                 <button onclick="deleteCard();"><i class="delete"></i></button> -->
<!--                                 <button onclick="copyCard();"><i class="copy"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
                        <li>
                            <div class="content">
                                <div>5학년 3반 - A조 (김누리, 하예은, 최소은)</div>
                            </div>
                            <div class="btn-wrap">
                                <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                            </div>
                        </li>
                        <li>
                            <div class="content">
                                <div>5학년 3반 -B조 (이찬원, 정동원, 신인선)</div>
                            </div>
                            <div class="btn-wrap">
                                <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                            </div>
                        </li>
                        <li>
                            <div class="content">
                                <div>5학년 3반 - C조 (조인성, 차인표, 이서진)</div>
                            </div>
                            <div class="btn-wrap">
                                <button onclick="popup('${HOME}/question_card/personal/form/update','update','440','185')"><i class="update"></i></button>
                                <button onclick="deleteCard();"><i class="delete"></i></button>
                                <button onclick="copyCard();"><i class="copy"></i></button>
                            </div>
                        </li>
                        <li class="add_card"><button onclick="popup('./form/add.jsp','add','440','185')"><img src="${IMG}/icons/question_card/card_list_add.png" class="auto"></button></li>
                    </ul>
                </div>

                <div class="pagination">
                    <button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
                    <button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
                    <ul class="paginate-list">
                        <li class="page-num active"><a href="#">1</a></li>
                        <li class="page-num"><a href="#">2</a></li>
                        <li class="page-num"><a href="#">3</a></li>
                        <li class="page-num"><a href="#">4</a></li>
                        <li class="page-num"><a href="#">5</a></li>
                        <li class="page-num"><a href="#">6</a></li>
                        <li class="page-num"><a href="#">7</a></li>
                        <li class="page-num"><a href="#">8</a></li>
                        <li class="page-num"><a href="#">9</a></li>
                        <li class="page-num"><a href="#">10</a></li>
                    </ul>
                    <button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
                    <button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
                </div>
            </div>
        </section>

    </body>
</html> 
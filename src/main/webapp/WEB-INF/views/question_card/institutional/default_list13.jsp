<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/question_card.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/institutional/default_list.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/institutional/add.css">
    </head>
    <body>

        <section class="no_contents">
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a  onclick="location.href='${HOME}/main';">Home</a></span>
                    <span><a href="">문항카드</a></span>
                    <span><a href="">기관별 문항카드</a></span>
                </div>

                <h2>기관별 문항카드 - 머리글</h2>

                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2">
                                <option value=""></option>
                                <option value="all" selected>전체</option>
                                <option value="num">번호</option>
                                <option value="type">유형</option>
                                <option value="header">머리글</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
					<div class="search-btn-area">
						<button class="btn-red-line shadow" onclick="location.href='${HOME}/question_card/institutional/default_card_list'">기관별 문항카드 메인</button>
						<button class="btn-red shadow" onclick="popup('./form/default13_add.jsp','update','650','235')"><i class="plus"></i>추가</button>
						<button class="btn-white shadow" onclick="popup('${HOME}/question_card/institutional/form/excel_upload','excel_upload','700','480')"><i class="upload"></i>엑셀 업로드</button>
					</div>
                </div>
                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col style="width : 95px;">
                            <col style="width : 330px;">
                            <col>
                            <col style="width : 180px;">
                        </colgroup>
                        <thead class="border-red-top-2 border-gray-bottom-2">
                            <tr>
                                <th>번호</th>
                                <th>유형</th>
                                <th>머리글</th>
                                <th>관리</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>10</td>
                                <td class="t-left">그림 보고 고르기</td>
                                <td class="t-left">그림을 보고 ( )에 가장 알맞은 것을 고르시오</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default13_update.jsp','update','650','235')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td class="t-left">지시문과 맞는 내용 고르기</td>
                                <td class="t-left">다음을 읽고 가장 관계 있는 것을 고르시오</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default13_update.jsp','update','650','235')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td class="t-left">대화를 읽고 반의어 고르기</td>
                                <td class="t-left">밑줄 친 부분과 반대되는 뜻을 가진 것을 고르시오</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default13_update.jsp','update','650','235')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td class="t-left">대화를 읽고 유의어 고르기</td>
                                <td class="t-left">밑줄 친 부분과 비슷한 뜻을 가진 것을 고르시오</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default13_update.jsp','update','650','235')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td class="t-left">짧은 글의 핵심 의미 파악하기</td>
                                <td class="t-left">무엇에 대한 이야기입니까? 가장 알맞은 것을 고르시오</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default13_update.jsp','update','650','235')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td class="t-left">어휘와 표현 의미 고르기</td>
                                <td class="t-left">( )에 들어갈 가장 알맞은 것을 고르시오</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default13_update.jsp','update','650','235')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td class="t-left">일치하는 내용 고르기 </td>
                                <td class="t-left">다음의 내용과 같은 것을 고르시오</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default13_update.jsp','update','650','235')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td class="t-left">일치하지 않는 내용 고르기 </td>
                                <td class="t-left">다음을 읽고 맞지 않는 것으로 고르시오</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default13_update.jsp','update','650','235')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
                    <button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
                    <ul class="paginate-list">
                        <li class="page-num active"><a href="#">1</a></li>
                        <li class="page-num"><a href="#">2</a></li>
                        <li class="page-num"><a href="#">3</a></li>
                        <li class="page-num"><a href="#">4</a></li>
                        <li class="page-num"><a href="#">5</a></li>
                        <li class="page-num"><a href="#">6</a></li>
                        <li class="page-num"><a href="#">7</a></li>
                        <li class="page-num"><a href="#">8</a></li>
                        <li class="page-num"><a href="#">9</a></li>
                        <li class="page-num"><a href="#">10</a></li>
                    </ul>
                    <button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
                    <button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
                </div>
            </div>
        </section>

    </body>
</html> 
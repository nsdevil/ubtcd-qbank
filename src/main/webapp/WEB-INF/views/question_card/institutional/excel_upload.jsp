<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/notice/list.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/institutional/excel_upload.css">
    </head>
    <body>

        <section class="no_contents">
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a  onclick="location.href='${HOME}/main';">Home</a></span>
                    <span><a href="">문항카드</a></span>
                    <span><a href="">기관별 문항카드</a></span>
                </div>

                <h2>기관별 문항카드</h2>
                
                <h3>엑셀 업로드</h3>
                
                <div class="step-wrap">
                    <ul>
                        <li>
                            <h4>STEP 1.</h4>
                            <p>엑셀 양식 파일을 다운로드 받습니다.</p>
                            <button class="btn-download btn-white shadow"><i class="download"></i>엑셀 파일 다운로드</button>
                        </li>
                        <li>
                            <h4>STEP 2.</h4>
                            <p>
                                엑셀 파일을 열어서 양식(항목)에 맞게 데이터를 입력합니다.
                                <span class="color-blue">(주의) 반드시 양식에 구성된 항목대로 입력해 주셔야 하며, 임의로 변경하시면 등록이 되지 않습니다.</span>
                            </p>
                        </li>
                        <li>
                            <h4>STEP 3.</h4>
                            <p>
                                엑셀 파일을 업로드합니다.
                                <span class="color-blue">(주의) 엑셀 외의 확장자 파일은 업로드 하실 수 없습니다.</span>
                            </p>
                            <button class="btn-download btn-white shadow"><i class="download"></i>엑셀 파일 다운로드</button>
                        </li>
                    </ul>
                </div>

                <button class="btn-white shadow btn-list"><i class="menu-bars"></i>목록으로</button>
            </div>
        </section>

    </body>
</html> 
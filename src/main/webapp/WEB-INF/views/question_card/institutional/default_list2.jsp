<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<link rel="stylesheet" href="${CSS}/common/header.css">
	<link rel="stylesheet" href="${CSS}/common/footer.css">
	<link rel="stylesheet" href="${CSS}/question_card/question_card.css">
	<link rel="stylesheet" href="${CSS}/question_card/institutional/default_list.css">
	<link rel="stylesheet" href="${CSS}/question_card/institutional/add.css">
</head>
<body>
<section class="no_contents" id="category">
	<div class="wrap bg-white pd-50 radius-5 shadow-wrap">
		<div class="breadcrumb">
			<span><a onclick="location.href='${HOME}/main';">
				<spring:message code="all.home"/></a></span>
			<span><a href=""><spring:message code="all.subjectcategory"/></a></span>
		</div>

		<h2><spring:message code="default.subtitle"/></h2>

		<%--		<div class="sort-wrap">--%>
		<%--			<div class="sort-ex flex">--%>
		<%--				<div class="sort-tt">--%>
		<%--					<span>연도</span>--%>
		<%--					<span>학교(학급)</span>--%>
		<%--					<span>학년</span>--%>
		<%--					<span>학기</span>--%>
		<%--				</div>--%>
		<%--				<div class="search-btn-area">--%>
		<%--					<button class="btn-red-line shadow"--%>
		<%--							onclick="popup('./form/default_list1_terms.jsp','default_list1_terms','1350','900')">--%>
		<%--						기간 분류 보기--%>
		<%--					</button>--%>
		<%--					<button class="btn-red shadow" onclick="popup('./form/default1_copy.jsp','update','400','227')"><i--%>
		<%--							class="plus"></i>연간 단위 복사 생성--%>
		<%--					</button>--%>
		<%--				</div>--%>
		<%--			</div>--%>
		<%--			<div class="sort-location">--%>
		<%--				<ul class="flex">--%>
		<%--					<li class="loca-item">--%>
		<%--						<select class="select2" name="">--%>
		<%--							<option value="">2019년도</option>--%>
		<%--							<option value="">2020년도</option>--%>
		<%--							<option value="">2021년도</option>--%>
		<%--						</select>--%>
		<%--					</li>--%>
		<%--					<li class="loca-item">--%>
		<%--						<select class="select2" name="">--%>
		<%--							<option value="">우리초등학교</option>--%>
		<%--							<option value="">영훈초등학교</option>--%>
		<%--							<option value="">경기남부초등학교</option>--%>
		<%--						</select>--%>
		<%--					</li>--%>
		<%--					<li class="loca-item">--%>
		<%--						<select class="select2" name="">--%>
		<%--							<option value="">1학년</option>--%>
		<%--							<option value="">2학년</option>--%>
		<%--							<option value="">3학년</option>--%>
		<%--							<option value="">4학년</option>--%>
		<%--							<option value="">5학년</option>--%>
		<%--							<option value="">6학년</option>--%>
		<%--						</select>--%>
		<%--					</li>--%>
		<%--					<li class="loca-item">--%>
		<%--						<select class="select2" name="">--%>
		<%--							<option value="">1학기</option>--%>
		<%--							<option value="">2학기</option>--%>
		<%--						</select>--%>
		<%--					</li>--%>
		<%--				</ul>--%>
		<%--			</div>--%>
		<%--		</div>--%>
		<%--		<div class="search-wrap flex">--%>
		<%--			<div class="search-form">--%>
		<%--				<div style="display: flex;">--%>
		<%--					<select class="select2" name="type" data-placeholder="구분">--%>
		<%--						<option value=""></option>--%>
		<%--						<option value="all" selected>전체</option>--%>
		<%--						<option value="num">번호</option>--%>
		<%--						<option value="title">문항카드명</option>--%>
		<%--					</select>--%>
		<%--					<input type="text" v-model="search" name="keyword" placeholder="Search">--%>
		<%--					<button type="button" v-on:click="loadCates"><img src="${IMG}/icons/search.png" class="auto">--%>
		<%--					</button>--%>
		<%--				</div>--%>
		<%--			</div>--%>

		<%--			<div class="search-btn-area">--%>
		<%--				<button class="btn-red-line shadow" onclick="location.href='default_card_list'">기관별 문항카드 메인</button>--%>
		<%--				<button class="btn-white shadow" onclick="popup('./form/excel_upload','excel_upload','700','480')">--%>
		<%--					<i class="upload"></i>엑셀 업로드--%>
		<%--				</button>--%>
		<%--			</div>--%>
		<%--		</div>--%>


		<div class="table-wrap">
			<table>
				<colgroup>
					<col style="width : 95px;">
					<col>
					<col style="width : 220px;">
				</colgroup>
				<thead class="border-red-top-2 border-gray-bottom-2">
				<tr>
					<th><spring:message code="default.number"/></th>
					<th><spring:message code="default.question"/></th>
					<th><spring:message code="default.settings"/></th>
				</tr>
				</thead>
				<tbody>
				<tr class="confirm_line">
					<td></td>
					<td colspan="2">
						<div class="flex-sec">
							<spring:message code="default.mainDesc" var="title"/>
							<input v-model="cateName" type="text" name="subclass" placeholder="${title}">
							<button v-on:click="cateAction(cateName, '1', '0', '')" class="btn-red"><i class="plus"></i>
								<spring:message code="default.add"/>
							</button>
						</div>
					</td>
				</tr>
				<!-- 1 depth 반복 -->
				<tr class="repeat_line" v-if="categories.length > 0">
					<td colspan="3" class="no-bd">
						<table class="inner-tb main-line">
							<colgroup>
								<col style="width : 95px;">
								<col style="width : 70px;">
								<col style="width : 70px;">
								<col>
								<col style="width : 220px;">
							</colgroup>
							<!-- 1depth -->
							<template v-for="(item, index) in categories">
								<tr class="edit-mode">
									<td>{{index+1}}</td>
									<td onclick="tbrow(this);"><i class="down"></i></td>
									<td></td>
									<td v-show="!item.isEdit" class="name">{{item.name}}</td>
									<td class="name" v-show="item.isEdit">
										<div class="flex-sec">
											<input v-model="item.name" type="text" name="" placeholder="" value="">
											<button v-on:click="update(item)" class="btn-red"><spring:message
													code="default.modify"/></button>
											<button v-on:click="close(item)" class="btn-gray btn-cancel"><spring:message
													code="default.cancel"/></button>
										</div>
									</td>
									<td class="management" v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN'
														|| '${sessionScope.S_LOGINID}' == item.regId ">
										<button v-on:click="editName(item)" class="btn-edit"><i class="update"></i>
										</button>
										<button v-on:click="remove(item)"><i class="delete"></i></button>
										<button><i class="copy"></i></button>
									</td>
									<td v-else>
										_
									</td>
								</tr>
								<!-- //1depth -->
								<tr>
									<td colspan="5" class="no-bd">
										<!-- 2depth -->
										<table class="inner-tb mid-line">
											<colgroup>
												<col style="width : 95px;">
												<col style="width : 70px;">
												<col style="width : 70px;">
												<col>
												<col style="width : 220px;">
											</colgroup>
											<template v-for="(itemm, indexx) in item.middlecates">
												<tr class="edit-mode">
													<td></td>
													<td></td>
													<td onclick="tbrow(this);"><i class="down"></i></td>
													<td v-show="!itemm.isEdit" class="name mid-name">{{itemm.name}}</td>
													<td v-show="itemm.isEdit" class="name">
														<div class="flex-sec">
															<input v-model="itemm.name" type="text">
															<button v-on:click="update(itemm)" class="btn-red">
																<spring:message code="default.modify"/>
															</button>
															<button v-on:click="close(itemm)"
																	class="btn-gray btn-cancel"><spring:message
																	code="default.cancel"/>
															</button>
														</div>
													</td>

													<td class="management" v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN'
														|| '${sessionScope.S_LOGINID}' == item.regId ">
														<button v-on:click="editName(itemm)" class="btn-edit"><i
																class="update"></i></button>
														<button v-on:click="remove(itemm)"><i class="delete"></i>
														</button>
													</td>
													<td v-else>
														_
													</td>
												</tr>
												<tr>
													<td colspan="5" class="no-bd">
														<!-- 3depth -->
														<table class="inner-tb sub-line">
															<colgroup>
																<col style="width : 95px;">
																<col style="width : 70px;">
																<col style="width : 70px;">
																<col>
																<col style="width : 220px;">
															</colgroup>
															<template v-for="(itemmm, indexxx) in itemm.lowcates">
																<tr class="edit-mode">
																	<td></td>
																	<td></td>
																	<td></td>
																	<td v-show="!itemmm.isEdit" class="name sub-name">
																		{{itemmm.name}}
																	</td>
																	<td v-show="itemmm.isEdit" class="name sub-name">
																		<div class="flex-sec">
																			<input v-model="itemmm.name" type="text">
																			<button v-on:click="update(itemmm)"
																					class="btn-red"><spring:message
																					code="default.modify"/>
																			</button>
																			<button v-on:click="close(itemmm)"
																					class="btn-gray btn-cancel">
																				<spring:message code="default.cancel"/>
																			</button>
																		</div>
																	</td>
																	<td class="management" v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN'
																						|| '${sessionScope.S_LOGINID}' == item.regId ">
																		<button v-on:click="editName(itemmm)"
																				class="btn-edit"><i class="update"></i>
																		</button>
																		<button v-on:click="remove(itemmm)"><i
																				class="delete"></i></button>
																	</td>
																	<td v-else>
																		_
																	</td>
																</tr>
															</template>
															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td class="name sub-name">
																	<div class="flex-sec">
																		<spring:message code="default.lowDesc"
																						var="low"/>
																		<input v-model="itemm.lowCateName" type="text"
																			   name="subclass" placeholder="${low}}">
																		<button v-on:click="cateAction(itemm.lowCateName, '3', itemm.id, itemm)"
																				class="btn-red"><i
																				class="plus"></i><spring:message
																				code="default.add"/>
																		</button>
																	</div>
																</td>
																<td class="management"></td>
															</tr>

														</table>
														<!-- //3depth -->
													</td>
												</tr>
											</template>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td class="name mid-name">
													<div class="flex-sec">
														<spring:message code="default.middleDesc" var="Mid"/>
														<input type="text" name="middle"
															   v-model="item.middleCateName"
															   placeholder="${Mid}">
														<button v-on:click="cateAction(item.middleCateName, '2', item.id, item)"
																class="btn-red"><i class="plus"></i><spring:message
																code="default.add"/>
														</button>
													</div>
												</td>
												<td class="management"></td>
											</tr>

										</table>
										<!-- //2depth -->

									</td>
								</tr>
							</template>
						</table>
					</td>
				</tr>
				<!-- //1 depth 반복 -->

				</tbody>
			</table>

		</div>


	</div>

	</div>
</section>

<script>
    // app Vue instance
    var app = new Vue({
        data() {
            return {
                cateName: '',
                categories: [],
                pagetype: 'subject',
                search: ''
            }
        },

        watch: {},

        computed: {},

        filters: {},

        async mounted() {
            this.loadCates()
        },

        methods: {
            clickCallback: function (pageNum) {
                console.log(pageNum)
            },
            async cateAction(catename, cateStep, parentId, item) {
                if (catename.length == 0) {
                    alert("please enter category name");
                    return;
                }
                try {
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}

                    axios.post('${BASEURL}/category/action', {
                        mode: "insert",
                        cateStep: cateStep,
                        cateCode: parentId,
                        cateName: catename,
                        type: this.pagetype,
                    }, {headers: headers}).then((response) => {
                        if (response.data.status === 200) {
                            if (response.data.success == false) {
                                alert(response.data.message)
                                console.log("item")
                                console.log(item)
                                if (item != '') {
                                    if (item.cateStep == 1)
                                        item.middleCateName = ""
                                    if (item.cateStep == 2)
                                        item.lowCateName = ""
                                }
                            } else {
                                this.loadCates()
                            }
                            if (parentId == '0') {
                                this.cateName = ""
                            }
                        } else {
                            alert("ERROR: " + response.data.message)
                        }
                    }, (error) => {
                        console.log(error);
                    });

                } catch (error) {
                    console.log(error)
                }

            },

            editName(item) {
                item.isEdit = true
            },
            close(item) {
                item.isEdit = false
            },

            async remove(item) {

                try {
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}

                    axios.post('${BASEURL}/category/action', {
                        mode: "delete",
                        cateStep: item.cateStep,
                        cateCode: item.id,
                        cateName: item.name,
                        type: this.pagetype
                    }, {headers: headers}).then((response) => {

                        if (response.data.status === 200) {
                            this.loadCates()
                        } else {
                            alert("ERROR: " + response.data.message)
                        }
                    }, (error) => {
                        console.log(error);
                    });

                } catch (error) {
                    console.log(error)
                }

            },

            async update(item) {

                try {
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}

                    axios.post('${BASEURL}/category/action', {
                        mode: "update",
                        cateStep: item.cateStep,
                        cateCode: item.id,
                        cateName: item.name,
                        type: this.pagetype
                    }, {headers: headers}).then((response) => {
                        if (response.data.status === 200) {
                            if (response.data.success == false) {
                                alert(response.data.message)
                            } else {
                                item.isEdit = false
                            }
                        } else {
                            alert("ERROR: " + response.data.message)
                        }
                    }, (error) => {
                        console.log(error);
                    });


                } catch (error) {
                    console.log(error)
                }

            },
            async loadCates(page) {
                try {
                    if (page > 0) {
                        page = page - 1
                    }
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}

                    axios.get('${BASEURL}/category/list', {
                        params: {
                            page: page,
                            type: this.pagetype,
                            name: this.search
                        },
                        headers: headers
                    }).then((response) => {
                        if (response.data.status === 200) {
                            let maincates = []
                            let middlecates = []
                            let lowcates = []
                            response.data.result.cates.forEach(item => {

                                let cateStep = item.cateStep
                                let name = item.name
                                let id = item.id
                                let parentId = item.parentId
								let regId = item.regId

                                if (cateStep === 3 && parentId !== 0) {
                                    let lowcate = {}
                                    lowcate.name = name
                                    lowcate.cateStep = cateStep
                                    lowcate.id = id
                                    lowcate.parentId = parentId
                                    lowcate.isEdit = false
									lowcate.regId = regId
                                    lowcates.push(lowcate)
                                }


                                if (cateStep === 2 && parentId !== 0) {
                                    var midcate = {}
                                    midcate.name = name
                                    midcate.cateStep = cateStep
                                    midcate.id = id
                                    midcate.isEdit = false
                                    midcate.lowCateName = ''
                                    midcate.parentId = parentId
									midcate.regId = regId
                                    midcate.lowcates = []

                                    middlecates.push(midcate)
                                }

                                if (cateStep === 1 && parentId === 0) {
                                    var mcate = {}
                                    mcate.name = name
                                    mcate.cateStep = cateStep
                                    mcate.id = id
                                    mcate.parentId = parentId
                                    mcate.isEdit = false
                                    mcate.middleCateName = ''
									mcate.regId = regId
                                    mcate.middlecates = []
                                    maincates.push(mcate)

                                }

                            })

                            middlecates.forEach(middle => {
                                lowcates.forEach(low => {
                                    if (middle.id === low.parentId) {
                                        middle.lowcates.push(low)
                                    }
                                })
                            })

                            this.categories = []

                            maincates.forEach(main => {
                                middlecates.forEach(middle => {
                                    if (main.id === middle.parentId) {
                                        main.middlecates.push(middle)

                                    }
                                })

                                this.categories.push(main)

                            })

                            console.log(this.categories)

                        } else {
                            alert("ERROR: " + response.data.message)
                        }
                    }, (error) => {
                        console.log(error);
                    });


                } catch (error) {
                    console.log(error)
                }

            }


        },
    })
    app.$mount('#category')
</script>
</body>
</html>

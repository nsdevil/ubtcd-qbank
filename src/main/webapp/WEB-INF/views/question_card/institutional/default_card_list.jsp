<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <link rel="stylesheet" href="${CSS}/common/header.css">
        <link rel="stylesheet" href="${CSS}/common/footer.css">
        <link rel="stylesheet" href="${CSS}/notice/list.css">
        <link rel="stylesheet" href="${CSS}/question_card/question_card.css">
        <link rel="stylesheet" href="${CSS}/question_card/institutional/add.css">
		<link rel="stylesheet" href="${CSS}/question_card/institutional/default_card_list.css">
    </head>
    <body>
        <section class="no_contents">
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="${JS}/main.jsp"><spring:message code="all.home"/></a></span>
                    <span><a href=""><spring:message code="category.questionCard"/></a></span>
                    <span><a href=""><spring:message code="category.orga.questionCard"/></a></span>
                </div>

                <h2><spring:message code="category.orga.questionCard"/></h2>
                <div class="sub">
                    <ul>
                        <li class="on">
                            <a onclick="location.href='${HOME}/question_card/institutional/default_card_list';">
                                <spring:message code="category.per"/>
                            </a>
                        </li>
<%--                        <li>--%>
<%--                            <a onclick="location.href='${HOME}/question_card/institutional/institution_add';">--%>
<%--                                기관추가--%>
<%--                            </a>--%>
<%--                        </li>--%>
                    </ul>
                </div>

                <div class="d-card-list">
                    <ul>
                        <li>
                            <div class="content" onclick="location.href='${HOME}/question_card/institutional/default_list1';">
								<div>1. <spring:message code="category.per"/></div>
                            </div>
                            <div class="btn-wrap">
                                <button onclick="location.href='${HOME}/question_card/institutional/default_list1';"><i class="sh-white"></i></button>
                                <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button>
                            </div>
                        </li>
                        <li>
                            <div class="content"
                                 onclick="location.href='${HOME}/question_card/institutional/default_list2';">
								<div>2. <spring:message code="category.examContent"/></div>
                            </div>
                            <div class="btn-wrap">
                                <button
                                        onclick="location.href='${HOME}/question_card/institutional/default_list2';"><i class="sh-white"></i></button>
                                <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button>
                            </div>
                        </li>
<%--                        <li>--%>
<%--                            <div class="content"--%>
<%--                                 onclick="location.href='${HOME}/question_card/institutional/default_list3'">--%>
<%--								<div>3. 성취기준</div>--%>
<%--                            </div>--%>
<%--                            <div class="btn-wrap">--%>
<%--                                <button onclick="location.href='${HOME}/question_card/institutional/default_list3'"><i--%>
<%--                                        class="sh-white"></i></button>--%>
<%--                                <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button>--%>
<%--                            </div>--%>
<%--                        </li>--%>
<%--                        <li>--%>
<%--                            <div class="content"--%>
<%--                                 onclick="location.href='${HOME}/question_card/institutional/default_list4';">--%>
<%--								<div>4. 지식수준</div>--%>
<%--                            </div>--%>
<%--                            <div class="btn-wrap">--%>
<%--                                <button--%>
<%--                                        onclick="location.href='${HOME}/question_card/institutional/default_list4';"><i class="sh-white"></i></button>--%>
<%--                                <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button>--%>
<%--                            </div>--%>
<%--                        </li>--%>
<%--                        <li>--%>
<%--                            <div class="content"--%>
<%--                                 onclick="location.href='${HOME}/question_card/institutional/default_list5';">--%>
<%--								<div>5. 예상 난이도</div>--%>
<%--                            </div>--%>
<%--                            <div class="btn-wrap">--%>
<%--                                <button--%>
<%--                                        onclick="location.href='${HOME}/question_card/institutional/default_list5';"><i class="sh-white"></i></button>--%>
<%--                                <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button>--%>
<%--                            </div>--%>
<%--                        </li>--%>
<%--                        <li>--%>
<%--                            <div class="content"--%>
<%--                                 onclick="location.href='${HOME}/question_card/institutional/default_list6';">--%>
<%--								<div>6. 문항의 적절성</div>--%>
<%--                            </div>--%>
<%--                            <div class="btn-wrap">--%>
<%--                                <button--%>
<%--                                        onclick="location.href='${HOME}/question_card/institutional/default_list6';"><i class="sh-white"></i></button>--%>
<%--                                <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button>--%>
<%--                            </div>--%>
<%--                        </li>--%>
<!--                         <li> -->
<!--                             <div class="content" onclick="location.href='default_list7.jsp';"> -->
<!-- 								<div>7. 임상술기</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="location.href='default_list7.jsp';"><i class="sh-white"></i></button> -->
<!--                                 <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li> -->
<!--                             <div class="content" onclick="location.href='default_list12.jsp';"> -->
<!-- 								<div>8. 임상표현명</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="location.href='default_list12.jsp';"><i class="sh-white"></i></button> -->
<!--                                 <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li> -->
<!--                             <div class="content" onclick="location.href='default_list8.jsp';"> -->
<!-- 								<div>9. 진단명</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="location.href='default_list8.jsp';"><i class="sh-white"></i></button> -->
<!--                                 <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li> -->
<!--                             <div class="content" onclick="location.href='default_list9.jsp';"> -->
<!-- 								<div>10. 보건용어</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="location.href='default_list9.jsp';"><i class="sh-white"></i></button> -->
<!--                                 <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li> -->
<!--                             <div class="content" onclick="location.href='default_list10.jsp';"> -->
<!-- 								<div>11. 참고문헌</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="location.href='default_list10.jsp';"><i class="sh-white"></i></button> -->
<!--                                 <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li> -->
<!--                             <div class="content" onclick="location.href='default_list11.jsp';"> -->
<!-- 								<div>12. 수술 및 기타수기</div> -->
<!--                             </div> -->
<!--                             <div class="btn-wrap"> -->
<!--                                 <button onclick="location.href='default_list11.jsp';"><i class="sh-white"></i></button> -->
<!--                                 <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button> -->
<!--                             </div> -->
<!--                         </li> -->

                        <li>
                            <div class="content"
                                 onclick="location.href='${HOME}/question_card/institutional/default_list13';">
								<div>7. 머리글</div>
                            </div>
                            <div class="btn-wrap">
                                <button
                                        onclick="location.href='${HOME}/question_card/institutional/default_list13';"><i class="sh-white"></i></button>
                                <button onclick="popup('./form/main_update.jsp','update','440','285')"><i class="update-white"></i></button>
                            </div>
                        </li>
					</ul>
                </div>
            </div>
        </section>
    </body>
</html>

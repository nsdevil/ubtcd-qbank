<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${CSS}/question_registration/each/making.css">
	<link rel="stylesheet" href="${CSS}/question_card/institutional/default_list.css">
	<link rel="stylesheet" href="${CSS}/question_registration/register_information.css">
	<link rel="stylesheet" href="${CSS}/notice/list.css">
	<link rel="stylesheet" href="${CSS}/my/show.css">

	<script src='${JS}/making.js'></script>
	<script src="${JS}/vue/vuejs-datepicker.js"></script>
	<style>
		.displaydiv {
			display: block !important;
		}
		#popup-modal form .filed-wrap .title span {
			font-size: 18px;
			font-weight: bold;
			width: 130px;
		}
		.classInput{
			border-top: 0px;
			border-left: 0px;
			border-right: 0px;
			width: 92%;
			margin-left: 10px;
			margin-right: 20px;
			border-bottom: 1px solid #dee2e6;
		}
		.btn-white-search {
			background-color: #fff;
			color: #000000;
			align-items: center;
			justify-content: center;
			height: 35px;
			border: 1px solid #dee2e6;
			border-radius: 2px;
			width: 150px;
			margin-right: 20px;
		}
		.btn-red-search {
			color: #fff;
			background-color: #fb5f33;
			align-items: center;
			justify-content: center;
			height: 35px;
			border: 0;
			border-radius: 2px;
			width: 80px;
		}
		#se-pre-con {
			position: fixed;
			-webkit-transition: opacity 5s ease-in-out;
			-moz-transition: opacity 5s ease-in-out;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
		}
	</style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="examlist">
	<div>
		<div class="wrap bg-white pd-50 radius-5 shadow-wrap">
			<div class="table-wrap">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<colgroup>
						<col width="8%">
						<col width="24%">
					</colgroup>
					<tbody>
					<tr class="border-red-top-2 border-gray-bottom-2">
						<td class="tit">
							<spring:message code="classlist.groupNo"/>
						</td>
						<td>
							<input name="" v-model="search.groupName" class="classInput">
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="table-wrap search-div" style="display: flex; float: right; margin-top: 20px; margin-bottom: 20px;">
				<button class="btn-white-search" @click="defaultSearch()">
					<spring:message code="schoollist.defaultSearch"/>
				</button>
				<button class="btn-red-search" @click="searchFilter()">
					<spring:message code="schoollist.search"/>
				</button>
			</div>
<%--			<div class="table-wrap flex" style=" margin-top: 60px; "> --%>
<%--				<button class="btn-red" @click="createModalShow()" style="width: 180px; height: 36px;">--%>
<%--					<i class="plus"></i>--%>
<%--					<spring:message code="schoollist.schoolAdd"/>--%>
<%--				</button>--%>
<%--			</div>--%>

			<div class="table-wrap" style="margin-top: 10px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					   style="table-layout: auto;">
					<colgroup>
						<col width="4%">
						<col width="15%">
						<col width="15%">
						<col width="10%">
<%--						<col width="10%">--%>
					</colgroup>
					<thead>
					<tr class="title_table border-red-top-2">
						<th>
							<span><spring:message code="all.number"/></span>
						</th>
						<th>
							<span><spring:message code="classlist.schoolName"/></span>
						</th>
						<th>
							<span><spring:message code="classlist.schoolID"/></span>
						</th>
						<th colspan="1">
							<span><spring:message code="student.register" /></span>
						</th>
					</tr>
					</thead>
					<tbody v-if="classList.length > 0">
					<tr v-for="(item,index) in classList" :key="index"
					>
						<td>
							{{index+1}}
						</td>
						<td>
							{{item.group_name}}
						</td>
						<td>
							{{item.class_id}}
						</td>
						<td>
							<button class="btn-red-search" @click="popupClose(item.sch_code, item.class_id)"><spring:message code="default.add"/></button>
						</td>
<%--						<td>--%>
<%--							<button class="btn-red-search" @click="excelClose(item.sch_code, item.class_id)"><spring:message code="default.excelUpload"/></button>--%>
<%--						</td>--%>
					</tr>
					</tbody>
					<tbody  v-else>
					<tr>
						<td colspan="4">
							<spring:message code="all.empty" />
						</td>
					</tr>
					</tbody>
				</table>
			</div>

<%--			<div class="pagination">--%>
<%--				<button class="start" @click="firstQuestion()"><img src="${IMG}/icons/start.png" class="auto"></button>--%>
<%--				<button class="prev" @click="prevQuestion()"><img src="${IMG}/icons/prev.png" class="auto"></button>--%>
<%--				<ul class="paginate-list">--%>
<%--					<li class="page-num " v-for="(item,index) in pages " :key="index"--%>
<%--						:class="{'active': currentPage === item}"--%>
<%--						v-if="Math.abs(item - currentPage) < 3 || item == pages || item == 1">--%>

<%--						<a href="javascript:void(0);" @click="questionCurrent(item)" :class="{--%>
<%--					last: (item == pages && Math.abs(item - currentPage) > 3),--%>
<%--					first: (item == 1 && Math.abs(item - currentPage) > 3)}">{{item}}</a>--%>

<%--					</li>--%>
<%--				</ul>--%>
<%--				<button class="next" @click="nextQuestion()"><img src="${IMG}/icons/next.png" class="auto"></button>--%>
<%--				<button class="end" @click="lastQuestion()"><img src="${IMG}/icons/end.png" class="auto"></button>--%>
<%--			</div>--%>
		</div>
	</div>

	<%--    --%>
	<div v-show="excelModal" class="modalDialog" :class="{'displaydiv': excelModal}"
		 style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
		<div class="modal-mask">
			<div class="modal-wrapper">
				<div class="modal-container">
					<a href="javascript:void(0)" @click="excelClose()" title="modelclose" class="modelclose">X</a>
					<div class="modal-header">
						<p style="font-weight: 800"><spring:message code="all.user.register"/></p>
					</div>
					<div class="modal-body">
						<article class="flex">
							<div class="detail">
								<div class="default">
									<h3>** <spring:message code="user.require" />
									</h3>
									<ul class="flex">
										<li>
											<div class="title"><spring:message code="user.schCode"/>**</div>
											<spring:message code="user.schCode" var="schCode"/>
											<input type="text" class="desc" placeholder="${schCode}" v-model="excel.schCode" disabled/>
										</li>
										<li>
											<div class="title"><spring:message code="user.classID"/>**</div>
											<spring:message code="user.classID" var="classID"/>
											<input type="text" class="desc" placeholder="${classID}" v-model="excel.groupNo" disabled/>
										</li>
									</ul>
								</div>
								<div class="form-box" style="text-align: center">
									<h3><spring:message code="default.excelUpload" /> </h3>
									<div class="file" style="position: relative;">
										<p><spring:message code="question.register.fileUpload" /> (XLS, 10MB)</p>
										<%--<button class="btn-white shadow" onclick=";"><i class="download2"></i>양식 다운로드</button>--%>
										<button class="btn-white shadow" onclick="file_load(event);"
												style="margin-left: 40%; margin-top: 20px;  margin-bottom: 20px;">
											<i class="upload"></i>
											<spring:message code="default.excelUpload" />
										</button>
										<input style="display: none;" @change="previewFileExcel($event)" name="file" type="file">
										<p style="position: absolute;
    top: 0;
    left: 0;
    margin-top: 65px;
    margin-left: 30px;
    font-weight: 500;
    color: #000;">{{excel.fileName}}</p>
									</div>
								</div>
								<div class="password clear">
									<button type="button" class="btn-red shadow" @click="userExcel()" style="height: 39px;">
										<spring:message code="examlist.register" /></button>
									<button type="button" @click="excelModal()" class="btn-gray shadow"
											style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
										<spring:message code="all.close" /></button>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div v-show="userRegisterModal" class="modalDialog" :class="{'displaydiv': userRegisterModal}"
		 style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
		<div class="modal-mask">
			<div class="modal-wrapper">
				<div class="modal-container">
					<a href="javascript:void(0)" @click="popupClose()" title="modelclose" class="modelclose">X</a>
					<div class="modal-header">
						<p style="font-weight: 800"><spring:message code="student.register"/></p>
					</div>
					<div class="modal-body">
						<article class="flex">
							<div class="profile" style="margin-top: 20px; padding-left: 50%;">
								<div class="image"><img src="${IMG}/icons/profile.png" class="auto"></div>
							</div>
							<div class="detail">
								<div class="default">
									<h3>** <spring:message code="user.require" />
									</h3>
									<ul class="flex">
										<li>
											<div class="title"><spring:message code="LastName"/> **
												<spring:message code="LastName" var="LastNames"/></div>
											<input type="text" class="desc" placeholder="${LastNames}" v-model="student.lastname" />
										</li>
										<li>
											<div class="title"><spring:message code="FirstName"/> **
												<spring:message code="FirstName" var="firsts"/></div>
											<input type="text" class="desc" placeholder="${firsts}" v-model="student.name" />
										</li>

										<li>
											<div class="title"><spring:message code="email"/></div>
											<spring:message code="email" var="emails"/>
											<input type="text" class="desc" placeholder="${emails}" v-model="student.email"/>
										</li>
										<li>
											<div class="title"><spring:message code="user.loginID"/> **</div>
											<spring:message code="user.loginID" var="loginID"/>
											<input type="text" class="desc" placeholder="loginID" v-model="student.loginId"/>
										</li>
										<li>
											<div class="title"><spring:message code="user.password"/>**</div>
											<spring:message code="user.password" var="password"/>
											<input type="password" class="desc" placeholder="${password}" v-model="student.password" />
										</li>
										<li>
											<div class="title"><spring:message code="user.passComp"/>**</div>
											<spring:message code="user.passComp" var="passComp"/>
											<input type="password" class="desc" placeholder="${passComp}" v-model="student.passComp"/>
										</li>
										<li>
											<div class="title"><spring:message code="phonenumber"/></div>
											<spring:message code="phonenumber" var="phonenumber"/>
											<input type="text" class="desc" placeholder="${phonenumber}" v-model="student.phone"/>
										</li>
										<li>
											<div class="title"><spring:message code="user.schCode"/></div>
											<spring:message code="user.schCode" var="schCode"/>
											<input type="text" class="desc" placeholder="${schCode}" v-model="student.schCode" disabled/>
										</li>
										<li>
											<div class="title"><spring:message code="user.classID"/></div>
											<spring:message code="user.classID" var="classID"/>
											<input type="text" class="desc" placeholder="${classID}" v-model="student.groupNo" disabled/>
										</li>
										<li>
											<div class="title"><spring:message code="user.gender"/></div>
											<select class="wd-100" name="subclass" v-model="student.gender">
												<option value="" disabled="" selected=""><spring:message code="user.select"/></option>
												<option value="0"><spring:message code="user.woman"/></option>
												<option value="1"><spring:message code="user.man"/></option>
											</select>
										</li>
									</ul>
								</div>
								<div class="password clear">
									<button type="button" class="btn-red shadow" @click="userRegister()" style="height: 39px;">
										<spring:message code="examlist.register" /></button>
									<button type="button" @click="popupClose()" class="btn-gray shadow"
											style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
										<spring:message code="all.close" /></button>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	let app = new Vue({
		components: {},
		data: {
			search: {
				schoolId: '',
				schoolName: '',
				groupNo: '',
				groupName: ''
			},
			create: {
				schId: '',
				schoolName: '',
				schoolId: '',
				image: 'https://mongolia-today.mn/wp-content/uploads/2017/08/%D1%85%D1%83%D1%83%D0%BB%D1%8C-%D1%81%D0%B0%D1%85%D0%B8%D1%83%D0%BB%D0%B0%D1%85%D0%B8%D0%B9%D0%BD-%D1%81%D1%83%D1%80%D0%B3%D1%83%D1%83%D0%BB%D1%8C.jpg',
				groupNo: '',
				groupName: ''
			},
			student:{
				loginId: '',
				email: '',
				password: '',
				passComp: '',
				phone: '',
				lastname: '',
				name: '',
				image: '',
				lastName:'',
				schCode: '',
				groupNo: '',
				gender: ''
			},
			excel:{
				fileName: '',
				excelFile: null,
				schCode: '',
				groupNo: ''
			},
			classList: [],
			currentPage: 1,
			itemsPerPage: 1,
			schoolCode: '',
			schoolName: '',
			pages: 0,
			totalElement: 0,
			flag: false,
			userRegisterModal: false,
			excelModal: false
		},
		computed: {
			// paginate: function () {
			// 	if (!this.classList || this.classList.length != this.classList.length) {
			// 		return
			// 	}
			// 	this.resultCount = this.classList.length
			// 	if (this.currentPage >= this.pages) {
			// 		this.currentPage = this.pages
			// 	}
			// 	var index = this.currentPage * this.itemsPerPage - this.itemsPerPage
			// 	return this.classList.slice(index, index + this.itemsPerPage)
			// }
		},
		mounted() {

		},
		created() {
			this.loaddata()
		},
		methods: {
			previewFileExcel(event) {
				this.excel.excelFile = event.target.files[0];
				this.excel.fileName = event.target.files[0].name;

				if (this.excel.excelFile){
					if ( /\.(xlsx)$/i.test( item.fileName) ) {
					}
					else{
						alert("File format is wrong please check your file !");
						return
					}
				}
			},
			loading(cmd) {
				var l = document.getElementById("se-pre-con");
				l.style.display = cmd;
			},
			userExcel() {
				try {
					let formData = new FormData()

					if (this.excel.fileName == '') {
						alert("choose excel file")
						return;
					}

					if (this.excel.schCode == '') {
						alert("chooose School Code")
						return;
					}
					if (this.excel.groupNo == '') {
						alert("chooose class id")
						return;
					}
					formData.append('efile', this.excel.excelFile);
					formData.append('schCode', this.excel.schCode);
					formData.append('groupNo', this.excel.groupNo);

					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}
					this.loading('block')
					axios.post('${BASEURL}/create/excel/users', formData, {headers: headers}).then((response) => {
						;
						if (response.data.status === 200) {
							this.loading('none')
							alert("Success")
							this.excel.excelFile = null;
							this.excel.fileName = "";
							this.excelModal = false
							this.clearExcelData
						} else {
							this.loading('none')
							alert("ERROR: " + response.data.message)
						}
					}, (error) => {
						this.loading('none')
						console.log(error);
					});
				} catch (e) {
					console.log(e)
					alert(e.response.data.message)
					this.loading('none')
				}
			}
			,
			excelClose(schCode, groupNo) {
				this.excel.schCode = schCode
				this.excel.groupNo = groupNo
				if (!this.excelModal) {
					document.body.style.position = 'fixed';
				} else {
					document.body.style.position = 'relative';
				}
				this.excelModal = !this.excelModal;
			},
			popupClose(schCode, groupNo) {
				this.student.schCode = schCode
				this.student.groupNo = groupNo
				this.student.password = ''
				if (!this.userRegisterModal) {
					document.body.style.position = 'fixed';
				} else {
					document.body.style.position = 'relative';
				}
				this.userRegisterModal = !this.userRegisterModal;
			},
			<%--saveClass(){--%>
			<%--	try {--%>
			<%--		if (this.create.groupName != '' && this.create.groupNo != '')--%>
			<%--		{--%>
			<%--			var language =  "<%=pageContext.getResponse().getLocale()%>"--%>
			<%--			var locale = ''--%>
			<%--			if (language == 'kr')--%>
			<%--			{--%>
			<%--				locale = 'kr-KR'--%>
			<%--			}--%>
			<%--			if(language == 'mn')--%>
			<%--			{--%>
			<%--				locale = 'mn-MN'--%>
			<%--			}--%>
			<%--			if(language == 'en'){--%>
			<%--				locale = 'en-EN'--%>
			<%--			}--%>
			<%--			const headers = {--%>
			<%--				'Content-Type': 'application/json',--%>
			<%--				'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',--%>
			<%--				'Accept-Language' : locale--%>
			<%--			}--%>
			<%--			var _this = this;--%>

			<%--			let item = {--%>
			<%--				groupNo: this.create.groupNo,--%>
			<%--				groupName: this.create.groupName,--%>
			<%--				schCode: this.schoolCode--%>
			<%--			};--%>
			<%--			axios.post('${BASEURL}/kexam/addnewclass', item, {--%>
			<%--				headers: headers--%>
			<%--			}).then(function (response) {--%>
			<%--				if (response.data.status == 200) {--%>
			<%--					alert(response.data.message);--%>
			<%--					window.location.href = "#close"--%>
			<%--					_this.loaddata(_this.currentPage);--%>
			<%--				} else {--%>
			<%--					alert(response.data.message);--%>
			<%--				}--%>
			<%--			}, (error) => {--%>
			<%--				console.log(error);--%>
			<%--				alert('Error')--%>
			<%--			});--%>
			<%--		}--%>
			<%--		else{--%>
			<%--			if (this.create.groupName == '')--%>
			<%--			{--%>
			<%--				alert("Та ангийн нэрээ оруулна уу.")--%>
			<%--			}--%>
			<%--			else{--%>
			<%--				alert("Та ангийн кодоо оруулна уу.")--%>
			<%--			}--%>
			<%--		}--%>

			<%--	} catch (error) {--%>
			<%--		console.log(error)--%>
			<%--	}--%>
			<%--},--%>
			userRegister(){
				try {
					if (this.student.password == this.student.passComp)
					{
						if (this.student.email == '' && this.student.loginId == ''
								&& this.student.password == ''&& this.student.passComp == '')
						{
							alert("<spring:message code="user.alertRequire"/>")
						}
						else{
							var language =  "<%=pageContext.getResponse().getLocale()%>"
							var locale = ''
							if (language == 'kr')
							{
								locale = 'kr-KR'
							}
							if(language == 'mn')
							{
								locale = 'mn-MN'
							}
							if(language == 'en'){
								locale = 'en-EN'
							}

							const headers = {
								'Content-Type': 'application/json',
								'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
								'Accept-Language' : locale
							}
							var _this = this;

							let item = {
								// image : '',
								loginId : this.student.loginId,
								email : this.student.email,
								password : this.student.password,
								phone : this.student.phone,
								name : this.student.name,
								lastName: this.student.lastname,
								lastName : this.student.lastName,
								schCode : this.student.schCode,
								groupNo : this.student.groupNo,
								role : '4',
								gender: this.student.gender

							};
							axios.post('${BASEURL}/create/user', item, {
								headers: headers
							}).then(function (response) {
								if (response.data.status == 200) {
									if (response.data.success== true)
									{
										alert(response.data.message)
										_this.userRegisterModal = false
										_this.clearUserData()
									}
									else{
										alert(response.data.message)
									}
								} else {
									alert(response.data.message);
								}
							}, (error) => {
								console.log(error);
								alert('Error')
							});
						}
					}
					else{
						alert("Please check your password.");
						return
					}

				} catch (error) {
					alert(error)
				}
			},
			clearUserData(){
				this.student.email = ""
				this.student.groupNo = ""
				this.student.lastName = ""
				this.student.loginId = ""
				this.student.name = ""
				this.student.lastname = ""
				this.student.password = ""
				this.student.phone = ""
				this.student.role = ""
				this.student.schCode = ""
				this.student.gender = ""
			},
			clearExcelData(){
				this.excel.fileName = ""
				this.excel.fileName = ""
				this.excel.groupNo = ""
				this.excel.schCode = ""
			},
			loaddata(){
				try {

					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}

					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}

					let schCode = '${sessionScope.S_SCHCODE}'

					let item = {
						groupName: this.search.groupName,
						schCode: schCode,
					}
					var _this = this

					axios.get('${BASEURL}/kexam/school/classes',{
						params: item,
						headers: headers
					}).then(function (response) {
						if (response.status == 200) {
							_this.classList = response.data
							window.location.href = "#classSeeModal"
						} else {
							console.log(response.data.message);
						}
					},(error) => {
						console.log(error);
						alert('Error')
					});

				} catch (error) {
					console.log(error)
					alert(error)
				}
			},
			nextQuestion() {
				if (this.pages > this.currentPage) {
					this.currentPage = this.currentPage + 1
					this.loaddata(this.currentPage)
				}
			}
			,
			prevQuestion() {
				if (this.currentPage > 1) {
					this.currentPage = this.currentPage - 1
					this.loaddata(this.currentPage)
				}
			}
			,
			lastQuestion() {
				this.currentPage = this.pages
				this.loaddata(this.currentPage)
			}
			,
			firstQuestion() {
				this.currentPage = 1
				this.loaddata(this.currentPage)
			}
			,
			async searchFilter() {
				this.currentPage = 1
				this.loaddata(this.currentPage);
			}
			,
			defaultSearch() {
				window.location.reload()
			}
		}
	});
	app.$mount('#examlist')
</script>
<style scoped>
	.my-datepicker input {
		padding: 9px 9px 9px 0;
		border-radius: 8px;
		text-align: center;
	}
</style>
</body>
</html>

<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd1350">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
			<div class="default-list-pop">
				 <h2>기관별 문항카드 - 시험과목</h2>

                <div class="table-wrap">
					<table>
                        <colgroup>
                            <col style="width : 95px;">
                            <col>
                            <col style="width : 220px;">
                        </colgroup>
						<thead class="border-red-top-2 border-gray-bottom-2">
							<tr>
								<th>번호</th>
								<th>문항카드명</th>
								<th>관리</th>
							</tr>
						</thead>
						<tbody>
							<tr class="confirm_line type4">
								<td></td>
								<td colspan="2">
									<div class="flex-sec">
										<input type="text" name="subclass" placeholder="년도를 입력하세요">
										<button class="btn-red"><i class="plus"></i>추가</button>
									</div>
								</td>
							</tr>
							<!-- 1 depth 반복 -->
							<tr class="repeat_line">
								<td colspan="3" class="no-bd">
									<table class="inner-tb main-line">
										<colgroup>
											<col style="width : 95px;">
											<col style="width : 70px;">
											<col style="width : 70px;">
											<col style="width : 70px;">
											<col>
											<col style="width : 220px;">
										</colgroup>
										<!-- 1depth -->
										<tr>
											<td>1</td>
											<td onclick="tbrow(this);"><i class="down"></i></td>
											<td></td>
											<td></td>
											<td class="name">2019년도</td>
											<td class="management">
												<button class="btn-edit"><i class="update"></i></button>
												<button><i class="delete"></i></button>
												<button><i class="copy"></i></button>
											</td>
										</tr>
										<!-- //1depth -->
										<tr>
											<td colspan="6" class="no-bd">
												<!-- 2depth -->
												<table class="inner-tb mid-line">
													<colgroup>
														<col style="width : 95px;">
														<col style="width : 70px;">
														<col style="width : 70px;">
														<col style="width : 70px;">
														<col>
														<col style="width : 220px;">
													</colgroup>

													<tr>
														<td></td>
														<td></td>
														<td onclick="tbrow(this);"><i class="down"></i></td>
														<td></td>
														<td class="name mid-name">우리초등학교</td>
														<td class="management">
															<button class="btn-edit"><i class="update"></i></button>
															<button><i class="delete"></i></button>
														</td>
													</tr>
													<tr>
														<td colspan="6" class="no-bd">
															<!-- 3depth -->
															<table class="inner-tb sub-line">
																<colgroup>
																	<col style="width : 95px;">
																	<col style="width : 70px;">
																	<col style="width : 70px;">
																	<col style="width : 70px;">
																	<col>
																	<col style="width : 220px;">
																</colgroup>
																<tr>
																	<td></td>
																	<td></td>																	
																	<td></td>
																	<td onclick="tbrow(this);"><i class="down"></i></td>
																	<td class="name sub-name">4학년</td>
																	<td class="management">
																		<button class="btn-edit"><i class="update"></i></button>
																		<button><i class="delete"></i></button>
																	</td>
																</tr>
																<tr>
																	<td colspan="6" class="no-bd">
																		<!-- 4depth -->
																		<table class="inner-tb low-line">
																			<colgroup>
																				<col style="width : 95px;">
																				<col style="width : 70px;">
																				<col style="width : 70px;">
																				<col style="width : 70px;">
																				<col>
																				<col style="width : 220px;">
																			</colgroup>
																			<tr>
																				<td></td>
																				<td></td>
																				<td></td>
																				<td></td>
																				<td class="name low-name">1학기</td>
																				<td class="management">
																					<button class="btn-edit"><i class="update"></i></button>
																					<button><i class="delete"></i></button>
																				</td>																			
																			</tr>	
																			<tr>
																				<td></td>
																				<td></td>
																				<td></td>
																				<td></td>
																				<td class="name low-name">
																					<div class="flex-sec">
																						<input type="text" name="subclass" placeholder="학기를 입력하세요.">
																						<button class="btn-red"><i class="plus"></i>추가</button>
																					</div>
																				</td>
																				<td class="management"></td>
																			</tr>
																		</table>
																		<!-- //4depth -->
																	</td>
																</tr>
																<tr>
																	<td></td>
																	<td></td>
																	<td></td>
																	<td onclick="tbrow(this);"><i class="down"></i></td>
																	<td class="name sub-name">3학년</td>
																	<td class="management">
																		<button class="btn-edit"><i class="update"></i></button>
																		<button><i class="delete"></i></button>
																	</td>
																</tr>
																<tr>
																	<td colspan="6" class="no-bd">
																		<!-- 4depth -->
																		<table class="inner-tb low-line">
																			<colgroup>
																				<col style="width : 95px;">
																				<col style="width : 70px;">
																				<col style="width : 70px;">
																				<col style="width : 70px;">
																				<col>
																				<col style="width : 220px;">
																			</colgroup>
																			<tr>
																				<td></td>
																				<td></td>
																				<td></td>
																				<td></td>
																				<td class="name low-name">1학기</td>
																				<td class="management">
																					<button class="btn-edit"><i class="update"></i></button>
																					<button><i class="delete"></i></button>
																				</td>																			
																			</tr>	
																			<tr>
																				<td></td>
																				<td></td>
																				<td></td>
																				<td></td>
																				<td class="name low-name">
																					<div class="flex-sec">
																						<input type="text" name="subclass" placeholder="학기를 입력하세요.">
																						<button class="btn-red"><i class="plus"></i>추가</button>
																					</div>
																				</td>
																				<td class="management"></td>
																			</tr>
																		</table>
																		<!-- //4depth -->
																	</td>
																</tr>
																<tr>
																	<td></td>
																	<td></td>
																	<td></td>
																	<td></td>
																	<td class="name sub-name">
																		<div class="flex-sec">
																			<input type="text" name="subclass" placeholder="학년을 입력하세요.">
																			<button class="btn-red"><i class="plus"></i>추가</button>
																		</div>
																	</td>
																	<td class="management"></td>
																</tr>
															</table>
															<!-- //3depth -->
														</td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td class="name mid-name">
															<div class="flex-sec">
																<input type="text" name="middle" placeholder="학교(학급)을 입력하세요">
																<button class="btn-red"><i class="plus"></i>추가</button>
															</div>
														</td>
														<td class="management"></td>													
													</tr>
												</table>
												<!-- //2depth -->

											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- //1 depth 반복 -->
						</tbody>
					</table>

                </div>


				<div class="pagination">
					<button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
					<button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
					<ul class="paginate-list">
						<li class="page-num active"><a href="#">1</a></li>
						<li class="page-num"><a href="#">2</a></li>
						<li class="page-num"><a href="#">3</a></li>
						<li class="page-num"><a href="#">4</a></li>
						<li class="page-num"><a href="#">5</a></li>
						<li class="page-num"><a href="#">6</a></li>
						<li class="page-num"><a href="#">7</a></li>
						<li class="page-num"><a href="#">8</a></li>
						<li class="page-num"><a href="#">9</a></li>
						<li class="page-num"><a href="#">10</a></li>
					</ul>
					<button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
					<button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
				</div>
			</div>
        </div>

<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd400">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
            <form name="" method="" action="">
				<div class="filed-wrap">
					<div class="title">복사대상 연도</div>
					<div class="select-form">
						<select class="select2" name="year">
							<option value="2019">2019</option>
							<option value="2018">2018</option>
							<option value="2017">2017</option>
							<option value="2016">2016</option>
						</select>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">생성할 연도</div>
					<div class="select-form">
						<select class="select2" name="year">
							<option value="2019">2019</option>
							<option value="2018">2018</option>
							<option value="2017">2017</option>
							<option value="2016">2016</option>
						</select>
					</div>
				</div>
                <div class="btn-wrap">
                    <button class="btn-gray shadow" onclick="popupClose()">취소</button>
                    <button class="btn-red shadow">생성</button>
                </div>
            </form>
        </div>

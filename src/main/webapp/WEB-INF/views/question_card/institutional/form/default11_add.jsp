<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd650">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
            <form name="" method="" action="">
                <div class="filed-wrap">
                    <div class="title">수술 및 기타 수기(국문)</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='수술 및 기타 수기(국문)을 입력하세요.' value=''>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">수술 및 기타 수기(영문)</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='수술 및 기타 수기(영문)을 입력하세요.' value=''>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">저자</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='저자를 입력하세요.' value=''>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">소속기관</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='저자의 소속기관을 입력하세요.' value=''>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">연도</div>
					<div class="select-form">
						<select class="select2" name="year">
							<option value="2019">2019</option>
							<option value="2018">2018</option>
							<option value="2017">2017</option>
							<option value="2016">2016</option>
						</select>
					</div>
				</div>
                <div class="btn-wrap">
                    <button class="btn-gray shadow" onclick="popupClose()">취소</button>
                    <button class="btn-red shadow">추가</button>
                </div>
            </form>
        </div>

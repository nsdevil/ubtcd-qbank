<%@ page contentType = "text/html;charset=utf-8" %>
        <div class="wrap">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
            <form name="" type="" action="">
                <input type="text" name="name" class="input" placeholder="문항 카드 이름을 입력하세요.">
                <div class="radio-wrap">
                    <div class="title">사용 여부</div>
                    <div class="radio">
                        
                        <input type="radio" name="userable" id="userable1" value="사용">
                        <label for="userable1">사용</label>
                    </div>
                    <div class="radio">
                        <input type="radio" name="userable" id="userable2" value="미사용">
                        <label for="userable2">미사용</label>
                    </div>
                </div>
                <div class="radio-wrap">
                    <div class="title">시험 유형</div>
                    <div class="radio">
                        <input type="radio" name="test_type" id="test_type1" value="지필고사">
                        <label for="test_type1">지필고사</label>
                    </div>
<!--                     <div class="radio"> -->
<!--                         <input type="radio" name="test_type" id="test_type2" value="실기시험 (CPX)"> -->
<!--                         <label for="test_type2">실기시험 (CPX)</label> -->
<!--                     </div> -->
                    <div class="radio mgl15">
                        <input type="radio" name="test_type" id="test_type3" value="전체">
                        <label for="test_type3">전체</label>
                    </div>
                </div>
                <div class="radio-wrap">
                    <div class="title">필수 여부</div>
                    <div class="radio">
                        <input type="radio" name="essential" id="essential" value="필수">
                        <label for="essential">필수</label>
                    </div>
                    <div class="radio">
                        <input type="radio" name="essential" id="essential2" value="권장">
                        <label for="essential2">권장</label>
                    </div>
                </div>
                <div class="btn-wrap">
                    <button class="btn-gray shadow" onclick="popupClose()">취소</button>
                    <button class="btn-red shadow">추가</button>
                </div>
            </form>
        </div>

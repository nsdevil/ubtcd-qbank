<%@ page contentType = "text/html;charset=utf-8" %>
		
		<div class="wrap pop-excel-wrap">
			 <div class="btn-close"><button onclick="popupClose();"><i class="close-wh"></i></button></div>
			 <div class="title">엑셀 업로드</div>
			 <form action="">
			 <div class="pop-excel-cont">			 
				<dl>
					<dt>STEP 1.</dt>
					<dd>아래의 양식을 다운로드 받으신 후 형식에 맞게 작성하세요. <p class="color-blue">※ 반드시 양식에 맞춰서 입력해 주셔야 하며, 임의로 변경하시면 등록이 되지 않습니다.</p>
						 <button class="btn-download btn-white shadow"><i class="download"></i>양식 다운로드</button>
					</dd>
				</dl>
 				<dl>
					<dt>STEP 2.</dt>
					<dd>[파일 선택]을 클릭하셔서 작성하신 엑셀 파일을 선택해 주세요.<p class="color-blue">※ 엑셀 이외의 파일은 업로드 하실 수 없습니다.</p>
						<div class="flex-sec">
							<input type="text" placeholder="">
							<button class="btn-white shadow">파일선택</button>							
						</div>
					</dd>
				</dl>
			 </div>
			<div class="btn-wrap">
				<button class="btn-gray shadow" onclick="popupClose()">취소</button>
				<button class="btn-red shadow">업로드</button>
			</div>
			</form>
		</div>

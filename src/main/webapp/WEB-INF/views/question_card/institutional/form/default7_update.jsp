<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd650">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
            <form name="" method="" action="">
                <div class="radio-wrap filed-wrap">
                    <div class="title">구분</div>
                    <div class="radio">                        
                        <input type="radio" name="division" id="division1" value="기본임상">
                        <label for="division1">기본임상</label>
                    </div>
                    <div class="radio">
                        <input type="radio" name="division" id="division2" value="관찰임상">
                        <label for="division2">관찰임상</label>
                    </div>
                </div>
                <div class="radio-wrap filed-wrap">
                    <div class="title">타입</div>
                    <div class="radio">                        
                        <input type="radio" name="type" id="type1" value="A">
                        <label for="type1">A</label>
                    </div>
                    <div class="radio">
                        <input type="radio" name="type" id="type2" value="B">
                        <label for="type2">B</label>
                    </div>
                    <div class="radio">
                        <input type="radio" name="type" id="type3" value="C">
                        <label for="type3">C</label>
                    </div>                
				</div>
				<div class="filed-wrap">
					<div class="title">임상술기(국문)</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='임상술기(국문)을 입력하세요.' value=''>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">임상술기(영문)</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='임상술기(영문)을 입력하세요.' value=''>
					</div>
				</div>
                <div class="btn-wrap">
                    <button class="btn-gray shadow" onclick="popupClose()">취소</button>
                    <button class="btn-red shadow">수정</button>
                </div>
            </form>
        </div>

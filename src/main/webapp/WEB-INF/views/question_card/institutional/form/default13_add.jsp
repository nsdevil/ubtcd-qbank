<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd650">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
            <form name="" method="" action="">
                <div class="filed-wrap">
                    <div class="title">머리글 유형</div>
					<div class="input-form flex-grow">
						<input type="text" class="input" name="title" placeholder='머리글 유형을 입력하세요.' value=''>
					</div>
                </div>
                <div class="filed-wrap">
                    <div class="title">머리글</div>
					<div class="input-form flex-grow">
						<input type="text" class="input" name="title" placeholder='머리글을 입력하세요.' value=''>
					</div>
				</div>
                <div class="btn-wrap">
                    <button class="btn-gray shadow" onclick="popupClose()">취소</button>
                    <button class="btn-red shadow">추가</button>
                </div>
            </form>
        </div>

<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd650">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
            <form name="" method="" action="">
                <div class="radio-wrap filed-wrap">
                    <div class="title">구분</div>
                    <div class="radio">                        
                        <input type="radio" name="division" id="division1" value="논문/학술지">
                        <label for="division1">논문/학술지</label>
                    </div>
                    <div class="radio">
                        <input type="radio" name="division" id="division2" value="도서">
                        <label for="division2">도서</label>
                    </div>
                </div>
                <div class="filed-wrap">
                    <div class="title">참고문헌/도서명(국문)</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='참고문헌/도서명(국문)을 입력하세요.' value=''>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">참고문헌/도서명(영문)</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='참고문헌/도서명(영문)을 입력하세요.' value=''>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">저자</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='저자를 입력하세요.' value=''>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">발행기관/출판사</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='발행기관/출판사를 입력하세요.' value=''>
					</div>
				</div>
				<div class="filed-wrap">
					<div class="title">연도</div>
					<div class="select-form">
						<select class="select2" name="year">
							<option value="2019">2019</option>
							<option value="2018">2018</option>
							<option value="2017">2017</option>
							<option value="2016">2016</option>
						</select>
					</div>
				</div>
                <div class="btn-wrap">
                    <button class="btn-gray shadow" onclick="popupClose()">취소</button>
                    <button class="btn-red shadow">추가</button>
                </div>
            </form>
        </div>

<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap subpopup wd500">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
            <form name="" method="" action="">
				<div class="filed-wrap">
					<div class="title">예상 난이도</div>
					<div class="input-form">
						<input type="text" class="input" name="title" placeholder='예상 난이도를 입력하세요.' value=''>
					</div>
				</div>
                <div class="btn-wrap">
                    <button class="btn-gray shadow" onclick="popupClose()">취소</button>
                    <button class="btn-red shadow">추가</button>
                </div>
            </form>
        </div>

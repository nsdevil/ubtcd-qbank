<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/question_card.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/institutional/default_list.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/institutional/add.css">
    </head>
    <body>

        <section class="no_contents">
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a  onclick="location.href='${HOME}/main';">Home</a></span>
                    <span><a href="">문항카드</a></span>
                    <span><a href="">기관별 문항카드</a></span>
                </div>

                <h2>기관별 문항카드 - 문항의 적절성</h2>

                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2">
                                <option value=""></option>
                                <option value="all" selected>전체</option>
                                <option value="num">번호</option>
                                <option value="adequacy_question">문항의 적절성</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
					<div class="search-btn-area">
						<button class="btn-red-line shadow" onclick="location.href='${HOME}/question_card/institutional/default_card_list'">기관별 문항카드 메인</button>
						<button class="btn-red shadow" onclick="popup('./form/default6_add.jsp','update','500','185')"><i class="plus"></i>추가</button>
						<button class="btn-white shadow" onclick="popup('${HOME}/question_card/institutional/form/excel_upload','excel_upload','700','480')"><i class="upload"></i>엑셀 업로드</button>
					</div>
                </div>
                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col style="width : 95px;">
                            <col>
                            <col style="width : 180px;">
                        </colgroup>
                        <thead class="border-red-top-2 border-gray-bottom-2">
                            <tr>
                                <th>번호</th>
                                <th>문항의 적절성</th>
                                <th>관리</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>10</td>
                                <td class="t-left">해당 사항 없음</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default6_update.jsp','update','500','185')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td class="t-left">필수적인 (Essential)</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default6_update.jsp','update','500','185')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td class="t-left">중요한 (Important)</td>
                                <td>
									<div class="flex flex-space-between wd-50 mg-auto">
										<button onclick="popup('./form/default6_update.jsp','update','500','185')"><i class="update"></i></button>
										<button onclick="deleteCard();"><i class="delete"></i></button>
									</div>
								</td>
                            </tr>
<!--                             <tr> -->
<!--                                 <td>7</td> -->
<!--                                 <td class="t-left">해당 사항 없음</td> -->
<!--                                 <td> -->
<!-- 									<div class="flex flex-space-between wd-50 mg-auto"> -->
<!-- 										<button onclick="popup('./form/default6_update.jsp','update','500','185')"><i class="update"></i></button> -->
<!-- 										<button onclick="deleteCard();"><i class="delete"></i></button> -->
<!-- 									</div> -->
<!-- 								</td> -->
<!--                             </tr> -->
<!--                             <tr> -->
<!--                                 <td>6</td> -->
<!--                                 <td class="t-left">해당 사항 없음</td> -->
<!--                                 <td> -->
<!-- 									<div class="flex flex-space-between wd-50 mg-auto"> -->
<!-- 										<button onclick="popup('./form/default6_update.jsp','update','500','185')"><i class="update"></i></button> -->
<!-- 										<button onclick="deleteCard();"><i class="delete"></i></button> -->
<!-- 									</div> -->
<!-- 								</td> -->
<!--                             </tr> -->
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
                    <button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
                    <ul class="paginate-list">
                        <li class="page-num active"><a href="#">1</a></li>
                        <li class="page-num"><a href="#">2</a></li>
                        <li class="page-num"><a href="#">3</a></li>
                        <li class="page-num"><a href="#">4</a></li>
                        <li class="page-num"><a href="#">5</a></li>
                        <li class="page-num"><a href="#">6</a></li>
                        <li class="page-num"><a href="#">7</a></li>
                        <li class="page-num"><a href="#">8</a></li>
                        <li class="page-num"><a href="#">9</a></li>
                        <li class="page-num"><a href="#">10</a></li>
                    </ul>
                    <button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
                    <button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
                </div>
            </div>
        </section>

    </body>
</html> 
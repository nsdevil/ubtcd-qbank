<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/question_card.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/institutional/default_list.css">
        <link rel="stylesheet" href="   ${CSS}/question_card/institutional/add.css">
    </head>
    <body>

        <section class="no_contents">
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a  onclick="location.href='${HOME}/main';">Home</a></span>
                    <span><a href="">문항카드</a></span>
                    <span><a href="">기관별 문항카드</a></span>
                </div>

                <h2>기관별 문항카드 - KAMC학습성과</h2>
                <div class="sub mb-30">
                    <ul>
                        <li><a onclick="location.href='${HOME}/question_card/institutional/default_list3'">과학적 개념과 원리</a></li>
                        <li class="on"><a onclick="location.href='${HOME}/question_card/institutional/default_list3_2'">진료역량 중심</a></li>
                        <li><a onclick="location.href='${HOME}/question_card/institutional/default_list3_3'">사람과 사회 중심</a></li>
                    </ul>
                </div>

                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2" name="type" data-placeholder="구분">
                                <option value=""></option>
                                <option value="all" selected>전체</option>
                                <option value="num">번호</option>
                                <option value="title">문항카드명</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
					<div class="search-btn-area">
						<button class="btn-red-line shadow" onclick="location.href='${HOME}/question_card/institutional/default_card_list'">기관별 문항카드 메인</button>
						<button class="btn-white shadow" onclick="popup('${HOME}/question_card/institutional/form/excel_upload','excel_upload','700','480')"><i class="upload"></i>엑셀 업로드</button>
					</div>
                </div>
                <div class="table-wrap">
					<table>
                        <colgroup>
                            <col style="width : 95px;">
                            <col>
                            <col style="width : 220px;">
                        </colgroup>
						<thead class="border-red-top-2 border-gray-bottom-2">
							<tr>
								<th>번호</th>
								<th>문항카드명</th>
								<th>관리</th>
							</tr>
						</thead>
						<tbody>
							<tr class="confirm_line type2">
								<td></td>
								<td colspan="2">
									<div class="flex-sec">
										<input type="text" name="subclass" placeholder="임상표현을 입력하세요.">
										<button class="btn-red"><i class="plus"></i>추가</button>
									</div>
								</td>
							</tr>
							<!-- 1 depth 반복 -->
							<tr class="repeat_line">
								<td colspan="3" class="no-bd">
									<table class="inner-tb main-line">
										<colgroup>
											<col style="width : 95px;">
											<col style="width : 70px;">
											<col>
											<col style="width : 220px;">
										</colgroup>
										<!-- 1depth -->
										<tr>
											<td>1</td>
											<td onclick="tbrow(this);"><i class="down"></i></td>
											<td class="name">[임상표현] 발열환자</td>
											<td class="management">
												<button class="btn-edit"><i class="update"></i></button>
												<button><i class="delete"></i></button>
												<button><i class="copy"></i></button>
											</td>
										</tr>
										<!-- //1depth -->
										<tr>
											<td colspan="4" class="no-bd">
												<!-- 2depth -->
												<table class="inner-tb mid-line">
													<colgroup>
														<col style="width : 95px;">
														<col style="width : 70px;">
														<col>
														<col style="width : 220px;">
													</colgroup>

													<tr>
														<td></td>
														<td></td>
														<td class="name mid-name">[실행 학습목표] 발열과 과체온(Hyperthermia)</td>
														<td class="management">
															<button class="btn-edit"><i class="update"></i></button>
															<button><i class="delete"></i></button>
														</td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td class="name mid-name">
															<div class="flex-sec">
																<input type="text" name="middle" placeholder="실행 학습목표를 입력하세요.">
																<button class="btn-red"><i class="plus"></i>추가</button>
															</div>
														</td>
														<td class="management"></td>													
													</tr>
												</table>
												<!-- //2depth -->

											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- //1 depth 반복 -->
							<!-- 1 depth 반복 -->
							<tr class="repeat_line">
								<td colspan="3" class="no-bd">
									<table class="inner-tb main-line">
										<colgroup>
											<col style="width : 95px;">
											<col style="width : 70px;">
											<col>
											<col style="width : 220px;">
										</colgroup>
										<!-- 1depth -->
										<tr>
											<td>1</td>
											<td onclick="tbrow(this);"><i class="down"></i></td>
											<td class="name">[임상표현] 발열환자</td>
											<td class="management">
												<button class="btn-edit"><i class="update"></i></button>
												<button><i class="delete"></i></button>
												<button><i class="copy"></i></button>
											</td>
										</tr>
										<!-- //1depth -->
										<tr>
											<td colspan="4" class="no-bd">
												<!-- 2depth -->
												<table class="inner-tb mid-line">
													<colgroup>
														<col style="width : 95px;">
														<col style="width : 70px;">
														<col>
														<col style="width : 220px;">
													</colgroup>

													<tr>
														<td></td>
														<td></td>
														<td class="name mid-name">[실행 학습목표] 발열과 과체온(Hyperthermia)</td>
														<td class="management">
															<button class="btn-edit"><i class="update"></i></button>
															<button><i class="delete"></i></button>
														</td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td class="name mid-name">
															<div class="flex-sec">
																<input type="text" name="middle" placeholder="실행 학습목표를 입력하세요.">
																<button class="btn-red"><i class="plus"></i>추가</button>
															</div>
														</td>
														<td class="management"></td>													
													</tr>
												</table>
												<!-- //2depth -->

											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- //1 depth 반복 -->
						</tbody>
					</table>

                </div>

                <div class="pagination">
                    <button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
                    <button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
                    <ul class="paginate-list">
                        <li class="page-num active"><a href="#">1</a></li>
                        <li class="page-num"><a href="#">2</a></li>
                        <li class="page-num"><a href="#">3</a></li>
                        <li class="page-num"><a href="#">4</a></li>
                        <li class="page-num"><a href="#">5</a></li>
                        <li class="page-num"><a href="#">6</a></li>
                        <li class="page-num"><a href="#">7</a></li>
                        <li class="page-num"><a href="#">8</a></li>
                        <li class="page-num"><a href="#">9</a></li>
                        <li class="page-num"><a href="#">10</a></li>
                    </ul>
                    <button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
                    <button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
                </div>
            </div>
        </section>

    </body>
</html> 
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
	<title>Question bank</title>/
	<link rel="stylesheet" href="${CSS}/questionbank/popup.css">
	<link rel="stylesheet" href="${CSS}/questionbank/default_list.css">
	<link rel="stylesheet" href="${CSS}/questionbank/additional.css">
	<link rel="stylesheet" href="${CSS}/question_card/institutional/default_list.css">
	<script src="${JS}/vue/vuejs-datepicker.js"></script>
	<link href="${CSS}/select2.min.css" rel="stylesheet" />
	<script src="${JS}/select2.min.js"></script>
	<style>
		.my-datepicker input {
			padding: 9px 9px 9px 0;
			border-radius: 8px;
			text-align: center;
		}
		.mainCateSpace{
			margin-left: 30px;
		}
		.cateSpace{
			margin-left: 25px;
		}
		.myquest{
			font-size: 19px;
			font-weight: 800;
			background: none;
		}
		.myquestinput{
			background: no-repeat;
			background: none;
		}
		#se-pre-con {
			position: fixed;
			-webkit-transition: opacity 5s ease-in-out;
			-moz-transition: opacity 5s ease-in-out;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
		}
	</style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="questionList">
	<div class="wrap bg-white pd-50 radius-5 shadow-wrap">
		<div class="breadcrumb">
			<span><a href=""><spring:message code="all.home"/></a></span>
			<span><a href=""><spring:message code="header.qBank"/></a></span>
		</div>
		<h2><spring:message code="header.qBank"/></h2>
		<div class="table-wrap">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col width="15%">
					<col width="24%">
					<col width="10%">
					<col width="26%">
					<col width="10%">
					<col width="8%">
				</colgroup>
				<tbody>
				<tr class="border-red-top-2 border-gray-bottom-2">
					<td class="tit"><spring:message code="all.category"/></td>
					<td colspan="5" style="text-align: left; padding-left: 15px;">
						<span class="mainCateSpace"><spring:message code="all.main"/></span>
						<select class='select2 select2-container select2-container--default' v-model='search.cate1'
								style="width: 250px; !important;" id="selectSearch">
							<template v-for="(item, index) in mainCategory.main">
								<option :value="item.id" :key="index">{{item.name}} - ({{item.regName}})</option>
							</template>
						</select>
						<span class="cateSpace"><spring:message code="all.middle"/></span>
						<select class=" select2 select2-container select2-container--default"
								style="width: 250px !important;" @change="categoryChange('exam', 'middle')"
								v-model="search.cate2">
							<template v-for="(item, index) in mainCategory.middle">
								<option :value="item.id" :key="index">{{item.name}}</option>
							</template>
						</select>
						<span class="cateSpace"><spring:message code="all.low"/></span>
						<select class="select2 select2-container select2-container--default"
								style="width: 250px !important;" @change="categoryChange('exam', 'low')" v-model="search.cate3" >
							<template v-for="(item, index) in mainCategory.low">
								<option :value="item.id" :key="index">{{item.name}}</option>
							</template>
						</select>
					</td>
				</tr>
<%--				<tr>--%>
<%--					<td class="tit"><spring:message code="all.subjectcategory"/></td>--%>
<%--					<td colspan="5" style="text-align: left; padding-left: 15px;">--%>
<%--						<span class="mainCateSpace"><spring:message code="all.main"/></span>--%>
<%--						<select class="select2 select2-container select2-container--default"--%>
<%--								style="width: 200px;"--%>
<%--								@change="categoryChange('subject', 'main')"--%>
<%--								v-model="search.scate1">--%>
<%--							<template v-for="(item, index) in subjectCategory.main">--%>
<%--								<option :value="item.id" :key="index">{{item.name}}</option>--%>
<%--							</template>--%>
<%--						</select>--%>
<%--						<span class="cateSpace"> <spring:message code="all.middle"/></span>--%>
<%--						<select class="select2 select2-container select2-container--default"--%>
<%--								style="width: 200px;"--%>
<%--								@change="categoryChange('subject', 'middle')" v-model="search.scate2">--%>
<%--							<template v-for="(item, index) in subjectCategory.middle">--%>
<%--								<option :value="item.id" :key="index">{{item.name}}</option>--%>
<%--							</template>--%>
<%--						</select>--%>
<%--						<span class="cateSpace"><spring:message code="all.low"/></span>--%>
<%--						<select class="select2 select2-container select2-container--default"--%>
<%--								style="width: 200px;" @change="categoryChange('subject', 'low')"--%>
<%--								v-model="search.scate3">--%>
<%--							<template v-for="(item, index) in subjectCategory.low">--%>
<%--								<option :value="item.id" :key="index">{{item.name}}</option>--%>
<%--							</template>--%>
<%--						</select>--%>
<%--					</td>--%>
<%--				</tr>--%>
				<tr>
					<td class="tit"><spring:message code="all.qlevel"/></td>
					<td colspan="1">
						<div class="flex sh-radio-wrap" style="padding-left: 15px; margin-bottom: 0px;">
							<div class="radio-wrap">
								<input name="search_difficultyLevel" id="search_difficultyLevel3" type="radio"
									   class="radio" value="1" v-model="search.qlevel">
								<label for="search_difficultyLevel3">
									<spring:message code="all.easy"/>
								</label>
							</div>
							<div class="radio-wrap">
								<input name="search_difficultyLevel" id="search_difficultyLevel2" type="radio"
									   class="radio" value="2" v-model="search.qlevel">
								<label for="search_difficultyLevel2">
									<spring:message code="all.normal"/>
								</label>
							</div>
							<div class="radio-wrap">
								<input name="search_difficultyLevel" id="search_difficultyLevel1" type="radio"
									   class="radio" value="3" v-model="search.qlevel">
								<label for="search_difficultyLevel1">
									<spring:message code="all.diff"/>
								</label>
							</div>
						</div>
					</td>
					<td class="tit"><spring:message code="all.questionType"/></td>
					<td colspan="3">
						<div class="flex  sh-radio-wrap" style="padding-left: 15px; margin-bottom: 0px;">
							<div class="radio-wrap">
								<input type="radio" class="radio" name="questionType" id="questionType1" value="3"
									   v-model="search.qtype">
								<label for="questionType1">
									<spring:message code="all.multiple"/>
								</label>
							</div>
							<!--<input type="radio" name="questionType" value="2" class=""/> 주관식 단답형&nbsp; -->
							<div class="radio-wrap">
								<input type="radio" class="radio" name="questionType" id="questionType3" value="1"
									   v-model="search.qType">
								<label for="questionType3">
									<spring:message code="all.single"/>
								</label>
							</div>
							<div class="radio-wrap">
								<input type="radio" class="radio" name="questionType" id="questionType4" value="2"
									   v-model="search.qtype">
								<label for="questionType4">
									<spring:message code="all.shortAnswer"/>
								</label>
							</div>
							<div class="radio-wrap">
								<input type="radio" class="radio" name="questionType" id="questionType2" value="4"
									   v-model="search.qtype">
								<label for="questionType4"><spring:message code="all.drawing"/></label>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="tit"><spring:message code="all.examName"/></td>
					<td><input value="" class="" size="30" v-model="search.examName"></td>
					<td class="tit"><spring:message code="all.submitter"/></td>
					<td><input value="" class="" size="30" v-model="search.submitter"></td>
					<td class="tit"><spring:message code="all.number"/></td>
					<td><input value="" class="" size="30" v-model="search.number" style="width: 74px;"></td>
				</tr>
				<tr>
					<td class="tit">
						<spring:message code="qBank.date"/>
					</td>
					<td colspan="3">
						<div class="flex" style="margin-left: 100px;">
							<vuejs-datepicker class="my-datepicker"
											  v-model="search.lifeStartDate">
							</vuejs-datepicker>

							<vuejs-datepicker   class="my-datepicker" v-model="search.lifeEndDate"
											  style="margin-left: 60px;"
							></vuejs-datepicker>
						</div>
					</td>
					<td class="tit">
						<spring:message code="question.register.realNumber"/>
					</td>
					<td>
						<input value="" class="" size="30" v-model="search.controlNo" style="width: 74px;">
					</td>
				</tr>
				<tr>
					<td class="tit"><spring:message code="all.question"/></td>
					<td><input name="questionBranch" value="" class="" size="30" v-model="search.question"></td>
					<td class="tit"><spring:message code="all.keyword"/></td>
					<td><input name="searchHeadScript" value="" class="" size="30" v-model="search.keyword"></td>
					<td class="tit"><spring:message code="all.point"/></td>
					<td><input name="searchExaminer" value="" class="" size="3" v-model="search.point" style="width: 74px;"></td>
				</tr>
				</tbody>
			</table>
		</div>

		<div class="search-wrap flex" style="margin-top: 20px;">
			<div class="search-btn-area">
				<input class="checkbox" type="checkbox" name="myQueiosn" id="myQueiosn" v-model="onlyme" @change="myQuestions()"/>
				<label for="myQueiosn" class="myquest"><spring:message code="myQuestions" /></label>
			</div>
			<div class="search-btn-area">
				<span class="table_summary" style="   margin-right: 10px;  margin-top: 6px;  font-size: 15px;">
					<spring:message code="all.questionCount"/> :
					<span class="list_total_count">{{totalElement}}</span>
				</span>
				<button class="btn-white shadow" @click="defaultSearch">
					<spring:message code="all.defaultSearch"/>
				</button>
				<button class="btn-red shadow" @click="searchFilter()">
					<spring:message code="all.search"/>
				</button>
			</div>
		</div>

		<div class="table-wrap list">
			<table>
				<colgroup>
					<col width="3%"/>
					<col width="10%"/>
					<col width="5%"/>
					<col width="6%"/>
					<col width="20%"/>
					<col width="6%"/>
					<col width="6%"/>
					<col width="5%"/>
				</colgroup>
				<thead class="border-red-top-2 border-gray-bottom-2">
				<tr id="qTableHeader">
					<%--						<th class="thleft"><a><span id="checkAll" class="checkBt"></span></a></th>--%>
					<th>№</th>
					<th class="thleft">
						<div class="qTitleCate"><a><span class="arrow">
							<spring:message code="all.category"/>
						</span></a></div>
					</th>
<%--					<th class="thleft">--%>
<%--						<div class="qTitleCate"><a><span class="arrow">교과목 분류</span></a></div>--%>
<%--					</th>--%>
					<th><span class="arrow">
						<spring:message code="qBank.qId"/>
					</span></th>
<%--					<th><span class="arrow"><spring:message code="qBank.qnumber"/></span></th>--%>
					<th><span class="arrow"><spring:message code="qBank.type"/></span></th>
					<th><span class="arrow"><spring:message code="qBank.question"/></span></th>
<%--					<th><span class="arrow">자료제시</span></th>--%>
					<th><span><spring:message code="qBank.register"/></span></th>
					<th><span class="arrow"><spring:message code="qBank.date"/></span></th>
					<th class="thright"><spring:message code="qBank.see"/></th>
				</tr>
				</thead>
				<tbody v-if="questions.length > 0">
					<tr v-for="(item,index) in questions" :key="index">
						<th>{{index+1}}</th>
						<th style="font-size: 13px; font-weight: 400;">{{item.topcate}} > {{item.midcate}} > {{item.lowcate}}
						</th>
	<%--					<th>_</th>--%>
						<th style="font-size: 13px; font-weight: 400;">{{item.id}}</th>
	<%--					<th>_</th>--%>
						<th style="font-size: 13px; font-weight: 400;">
							<label v-if="item.question_type == '1'">
								<spring:message code="all.single"/>
							</label>
							<label v-if="item.question_type == '2'">
								<spring:message code="all.shortAnswer"/>
							</label>
							<label v-if="item.question_type == '3'">
								<spring:message code="all.multiple"/>
							</label>
							<label v-if="item.question_type == '4'">
								<spring:message code="all.drawing"/>
							</label>
						</th>
						<th v-html="item.question.substring(0,150)"
							style="font-size: 13px; font-weight: 400;"></th>
						<th style="font-size: 13px; font-weight: 400;">{{item.reg_user}}</th>
						<th style="font-size: 13px; font-weight: 400;">{{item.created_at.substring(0,10)}}</th>
	<%--					.substring(0,10)--%>
						<th>
							<a class="btn-red-small preview" style="color: #fff;
										background-color: #fb5f33;
										border: 0;
										border-radius: 2px;
										padding: 8px 12px;
										margin: 2px;
										font-weight: 700;
										font-size: 12px;
										box-shadow: 1px 5px 7px 0px rgba(0,0,0,0.15);"
							   v-on:click="preview(item.id)"
							<%--							   onclick="location.href='${HOME}/question_registration/each/preview/preview?id='+item.id"--%>
							><spring:message code="qBank.see"/></a>
						</th>
					</tr>
				</tbody>
				<tbody v-else>
					<tr>
						<td colspan="8">
							<spring:message code="all.empty" />
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="pagination">
			<button class="start" @click="firstQuestion()"><img src="${IMG}/icons/start.png" class="auto"></button>
			<button class="prev" @click="prevQuestion()"><img src="${IMG}/icons/prev.png" class="auto"></button>
			<ul class="paginate-list">
				<li class="page-num " v-for="(item,index) in pages " :key="index"
					:class="{'active': currentPage === item}"
					v-if="Math.abs(item - currentPage) < 3 || item == pages || item == 1">

					<a href="javascript:void(0);" @click="questionCurrent(item)" :class="{
					last: (item == pages && Math.abs(item - currentPage) > 3),
					first: (item == 1 && Math.abs(item - currentPage) > 3)}">{{item}}</a>

				</li>
			</ul>
			<button class="next" @click="nextQuestion()"><img src="${IMG}/icons/next.png" class="auto"></button>
			<button class="end" @click="lastQuestion()"><img src="${IMG}/icons/end.png" class="auto"></button>
		</div>

	</div>
	<div id="previewss"></div>
</section>


<%--<div class="exam_view1" id="questionCardPreview" style="display:none;">--%>
<%--	--%>
<%--	<div class="pop_top">--%>
<%--		<span class="view_a">미리보기</span>--%>
<%--		<span class="view_b">--%>
<%--    		<a name="prevBtn"><span class="btn-red-small left">이전문제</span></a>--%>
<%--    		<a id="questionEditBtn"><span class="btn-red-line-small right">수정</span></a>--%>
<%--    		<a name="nextBtn"><span class="btn-red-small right">다음문제</span></a>--%>
<%--    	</span>--%>
<%--		<a><span class="exitimg"><i class="close-red"></i></span></a>--%>
<%--	</div>--%>
<%--	--%>
<%--	<div class="qu_in">--%>
<%--		<div id="scriptArea"></div>--%>
<%--		<p class="v_q"></p>--%>
<%--		<p class="pre_q"></p>--%>
<%--		<div id="headScript"></div>--%>
<%--		<div id="mediaList"></div>--%>
<%--		--%>
<%--		<table class="v_example" width="100%">--%>
<%--			<tbody id="exampleList"></tbody>--%>
<%--		</table> --%>
<%--		<div class="essay" style="display:none;">--%>
<%--			<span class="essay_q">주관식 정답</span>--%>
<%--			<span class="essay_memo"></span>--%>
<%--		</div> --%>
<%--		<br/><br/>--%>
<%--	</div> --%>
<%--</div>--%>

<script>
    var app = new Vue({
		components: {
			vuejsDatepicker
		},
        data() {
            return {
                questions: '',
                search: {
                    cate1: "",
                    cate2: "",
                    cate3: "",
                    scate1: "",
                    scate2: "",
                    scate3: "",
                    question: "",
                    keyword: "",
                    point: "",
                    qType: '',
                    qlevel: '',
					submitter: "",
				 	examName: "",
				 	lifeStartDate: "",
				 	lifeEndDate: "",
				 	number: "",
				 	controlNo: "",
					searchMainCate: ''
                },
                mainCategory: {
                    main: [],
                    middle: [],
                    low: []
                },
                subjectCategory: {
                    main: [],
                    middle: [],
                    low: []
                },
                currentPage: 1,
                pages: 0,
                itemsPerPage: 1,
                totalQuestionNumber: 0,
				totalElement: '',
				localPath: '${BASEHOST}/exam',
				onlyme: false,
            }
        },

        watch: {},

        computed: {
            paginate: function () {
                if (!this.questions || this.questions.length != this.questions.length) {
                    return
                }
                this.resultCount = this.questions.length
                if (this.currentPage >= this.pages) {
                    this.currentPage = this.pages
                }
                var index = this.currentPage * this.itemsPerPage - this.itemsPerPage
                return this.questions.slice(index, index + this.itemsPerPage)
            },
        },
        mounted() {
            this.getCates("main", 1, 0);
            // this.getCates("subject", 1, 0);
        },
        created() {
            this.loaddata(this.currentPage);
        },
        filters: {},
        methods: {
			loading(cmd) {
				var l = document.getElementById("se-pre-con");
				l.style.display = cmd;
			},
            preview(id) {
				window.open('${HOME}/question_registration/each/preview/question_preview?id=' + id, '_blank',
						'width= 1550, height= 680');
			},
			clearData(){
            	this.currentPage = 1
				this.search.cate1 = ""
				this.search.cate2 = ""
				this.search.cate3 = ""
				this.search.scate1 = ""
				this.search.scate2 = ""
				this.search.scate3 = ""
				this.search.question = ""
				this.search.keyword = ""
				this.search.point = ""
				this.search.qType = ''
				this.search.qlevel = ''
				this.search.submitter = ""
				this.search.examName = ""
				this.search.lifeStartDate = ""
				this.search.lifeEndDate = ""
				this.search.number = ""
				this.search.controlNo = ""
			},
			myQuestions(){
            	this.clearData()
				this.pages = 0;
				this.loaddata(this.currentPage)
			},
            async loaddata(page) {
                try {
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}

                    var _this = this;
					_this.loading('block')
                    if (page > 0) {
                        page = page - 1
                    }
                    axios.post('${BASEURL}/questions', {
                        page: page,
                        question: this.search.question,
                        point: this.search.point,
                        keyword: this.search.keyword,
                        qtype: this.search.qtype,
                        qlevel: this.search.qlevel,
                        cate1: this.search.cate1,
                        cate2: this.search.cate2,
                        cate3: this.search.cate3,
                        scate1: this.search.scate1,
                        scate2: this.search.scate2,
                        scate3: this.search.scate3,
						onlyme: this.onlyme,
						regUser : this.search.submitter,
						examName: this.search.examName,
						startDate: this.search.lifeStartDate,
						endDate: this.search.lifeEndDate,
						qId: this.search.number,
						controlNo: this.search.controlNo,
						sort: '',
						sortfield: ''
                    }, {headers: headers}).then((response) => {
						_this.loading('none')
                        _this.questions = response.data.content
                        // localStorage.setItem('questionData', JSON.stringify(response.data));
                        _this.pages = response.data.totalPages;
                        _this.totalElement = response.data.totalElements;
                    }, (error) => {
                    	console.log(error)
                        this.loading('none')
                    });

                } catch (error) {
                    console.log(error)
                        this.loading('none')
                }
            },
			categoryChange(type, catelevel) {
				if (type == "exam") {
                    type = "main";
                } else {
                    type = "subject";
                }
                if (catelevel == "main") {
                    let parentId = 0;
                    if (type == "main") parentId = this.search.cate1;
                    else parentId = this.search.scate1;
                    this.getCates(type, 2, parentId);
                }
                if (catelevel == "middle") {
                    let parentId = 0;
                    if (type == "main") parentId = this.search.cate2;
                    else parentId = this.search.scate2;
                    this.getCates(type, 3, parentId);
                }
            },
            async getCates(type, cateStep, parentId) {
                try {
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}

                    axios.get('${BASEURL}/category/liststep', {
                        params: {
                            type: type,
                            cateStep: cateStep,
                            parentId: parentId
                        },
                        headers: headers
                    }).then((response) => {
                        if (response.data.status == 200) {
                            if (type == "main") {
                                //exam category
                                if (cateStep == 1) {
                                    this.mainCategory.main = response.data.result.cates;
                                }
                                if (cateStep == 2) {
                                    this.mainCategory.middle = response.data.result.cates;
                                }
                                if (cateStep == 3) {
                                    this.mainCategory.low = response.data.result.cates;
                                }
                            } else {
                                //subject category
                                if (cateStep == 1) {
                                    this.subjectCategory.main = response.data.result.cates;
                                }
                                if (cateStep == 2) {
                                    this.subjectCategory.middle = response.data.result.cates;
                                }
                                if (cateStep == 3) {
                                    this.subjectCategory.low = response.data.result.cates;
                                }
                            }
                        }
                    }, (error) => {
                        console.log(" error found : category get requested ")
                        console.log(error);
                    });
                } catch (error) {
                    console.log("category error catch")
                    console.log(error)
                }
            },
            questionCurrent(item) {
                this.currentPage = item
                this.loaddata(this.currentPage)
            },
            nextQuestion() {
                if (this.pages > this.currentPage) {
                    this.currentPage = this.currentPage + 1
                    this.loaddata(this.currentPage)
                }
            },
            prevQuestion() {
                if (this.currentPage > 1) {
                    this.currentPage = this.currentPage - 1
                    this.loaddata(this.currentPage)
                }
            },
            lastQuestion() {
                this.currentPage = this.pages
                this.loaddata(this.currentPage)
            },
            firstQuestion() {
                this.currentPage = 1
                this.loaddata(this.currentPage)
            },

            async searchFilter() {
                this.loaddata(this.currentPage);
            },
            defaultSearch() {
                window.location.href = "${HOME}/question_card/questionbank"
            }
        }
    })
    app.$mount('#questionList')
</script>
<script>
	$('#selectSearch').on("change",function(){
		app.search.cate1 = $(this).val();
		app.categoryChange('exam','main')
	});

	$(document).ready(function() {
		$('#selectSearch').select2({});
	});
</script>
</body>
</html>

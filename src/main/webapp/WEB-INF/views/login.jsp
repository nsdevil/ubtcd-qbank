<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="Publisher" content="Nsdevil consulting">
	<meta name="Keywords" content="Nsdevil consulting">
	<meta name="Description" content="Nsdevil consulting">
	<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
	<meta name="viewport" content="user-scalable=no, width=1200">
	<link rel="stylesheet" href="${CSS}/login.css">
	<%@ include file="./common/head.jsp" %>
<%--	<title><spring:message code="all.home"/></title>--%>
	<script language="javascript" type="text/javascript">

		$(document).ready(function () {
			$.ajax({
				type: "POST",
				url: "${BASEURL}/auth/public/qcount",
				data: null,
				contentType: "application/json; charset=utf-8",
				success: function (response) {
					if (response.status == 200) {
						var qcount = response.result.qcount + "";
						var numbers = qcount.split('');
						var html = "";
						if (numbers.length < 5) {
							for (let j = 0; j < 5 - numbers.length; j++) {
								html += '<span class="red-mini-box">0</span>';
							}
						}
						for (let i = 0; i < numbers.length; i++) {
							let num = numbers[i];
							html += '<span class="red-mini-box">' + num + '</span>';
						}

						$('#qcount').html(html);

						$('#qcount>.red-mini-box').each(function () {
							$(this).prop('Counter', 0).animate({
								Counter: $(this).text()
							}, {
								duration: 1000,
								easing: 'swing',
								step: function (now) {
									$(this).text(Math.ceil(now));
								}
							});
						});
					}
				}
			});
			// if (document.getElementsByClassName('.circle-graph .circle')) {
			//     $('.circle-graph .circle').circlechart();
			// }
		});
		function modalClose() {
			var modal = document.querySelector('#popup-modal');
			modal.style.display = 'none'
		}
	</script>

</head>
<body>
<style>
	#se-pre-con {
		position: fixed;
		-webkit-transition: opacity 5s ease-in-out;
		-moz-transition: opacity 5s ease-in-out;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
	}
</style>
<header>
	<div id="se-pre-con" style="display: none"></div>
	<div class="wrap-login">
		<div class="logo"><img src="${IMG}/header/nslogo.png" class="auto"></div>
<%--	exam.edu.mn todo check --%>
<%--	Color : ubt.mn - #fb5f33   .......... exam.edu.mn - #fb5f33--%>
<%--		<div class="flex" style="align-items: center; justify-content: center;">--%>
<%--			<div class="logo"><img src="${IMG}/header/esis1.png" class="auto"></div>--%>
<%--			<div class="logo" style="margin-left: 50px;"><img src="${IMG}/header/edu1.png" class="auto"></div>--%>
<%--		</div>--%>
		<div class="flex description">
			<div id="qcount" class="flex">
				<span class="red-mini-box">0</span>
				<span class="red-mini-box">0</span>
				<span class="red-mini-box">0</span>
				<span class="red-mini-box">0</span>
				<span class="red-mini-box">0</span>
				<span class="red-mini-box">0</span>
			</div>
			<spring:message code="login.totalQuestion"/>
		</div>
	</div>
</header>
<section id="login">
	<div class="wrap-login flex flex-space-between">
		<div class="left">
<%--			exam.edu.mn todo check --%>
			<div>
				<span style="font-size: 16px; padding: 19px;">
					<spring:message code="login.description"/></span>
				<h1><spring:message code="login.title"/></h1>
			</div>
		</div>
		<div class="right">
			<div class="login">
				<h2><spring:message code="login.submit"/></h2>
				<form name="loginForm" @submit.prevent="login">
					<spring:message code="login.email" var="logEmail"/>
					<input type="text" v-model="looginId" placeholder="${logEmail}" name="loginlooginId" name="loginlooginId"class="input wd-100"
						   autofocus/>
					<spring:message code="login.pass" var="logPass"/>
					<input type="password" v-model="password" placeholder="${logPass}" name="password"
						   class="input wd-100"/>

					<spring:message code="login.submit" var="logPass"/>
					<input type="submit" class="btn-gray wd-100" value="${logPass}"/>
				</form>
				<div>
					<input type="checkbox" name="remember" id="remember" class="checkbox" v-model="rememberMe" @click="rememberTrue()">
					<label for="remember"><spring:message code="rememberMe" /> </label>
<%--					<div class="certify-pc">--%>
<%--						<a href="javascript:void(0);" onclick="location='${HOME}/my/pc_certification'">임시저장 목록</a>--%>
<%--						<img src="${IMG}/icons/alert.png" class="auto">--%>
<%--					</div>--%>
				</div>
			</div>
<%--			<p class="number">--%>
<%--				<a href="tel:1661-1878">헬프데스크 1661-1878</a>--%>
<%--				<a href="">온라인헬프데스크 바로가기</a>--%>
<%--			</p>--%>
<%--			<p class="time">(평일 09:00~18:00, 12:00~13:00 점심시간, 주말/공휴일 휴무)</p>--%>
<%--			<div class="notice">--%>
<%--				<h2>공지사항</h2>--%>
<%--				<ul>--%>
<%--					<li>--%>
<%--						<div><a href="">시험 출제 일정 공지</a></div>--%>
<%--						<div>2019.10.01</div>--%>
<%--					</li>--%>
<%--					<li>--%>
<%--						<div><a href="">시험 출제 일정 공지</a></div>--%>
<%--						<div>2019.10.01</div>--%>
<%--					</li>--%>
<%--					<li>--%>
<%--						<div><a href="">시험 일정 변경안내입니다.</a></div>--%>
<%--						<div>2019.10.01</div>--%>
<%--					</li>--%>
<%--					<li>--%>
<%--						<div><a href="">시험 출제 일정 공지</a></div>--%>
<%--						<div>2019.10.01</div>--%>
<%--					</li>--%>
<%--					<li>--%>
<%--						<div><a href="">시험 일정 변경안내입니다.</a></div>--%>
<%--						<div>2019.10.01</div>--%>
<%--					</li>--%>
<%--					<li>--%>
<%--						<div><a href="">시험 출제 일정 공지</a></div>--%>
<%--						<div>2019.10.01</div>--%>
<%--					</li>--%>
<%--				</ul>--%>
<%--			</div>--%>
		</div>

<%--		<div id="popup-modal"--%>
<%--			 style="position: absolute;--%>
<%--			display: none;--%>
<%--			min-width: 800px;--%>
<%--			min-height: 600px;--%>
<%--			left: 22%;--%>
<%--			top: 6%;--%>
<%--			border-radius: 15px;">--%>
<%--			<div class="wrap subpopup wd500" style="border-radius: 15px;">--%>
<%--				<div class="btn-close">--%>
<%--					<button onclick="modalClose()" ><i class="close-red"></i></button>--%>
<%--				</div>--%>
<%--				<form name="" method="" action="">--%>
<%--					<div class="filed-wrap" style="display: none;">--%>
<%--						<div class="title" style="text-align: center;  display: block;--%>
<%--                                            font-size: 27px;--%>
<%--                                            width: 100%;--%>
<%--                                            border-bottom: 1px solid #000;--%>
<%--                                            padding-bottom: 15px;">Мэдэгдэл--%>
<%--						</div>--%>
<%--						<br>--%>
<%--						<div class="exambody" style="font-size: 15px; line-height: 1.6em;">--%>
<%--							<p style="margin: 15px;">--%>
<%--								Сайн байцгаана уу?--%>
<%--								<br>--%>
<%--								<br>--%>
<%--								Юуны өмнө 2020 оны 11 сарын 1-ний өдрөөс өнөөдрийг хүртэл  идэвхтэй хамтран ажилласан нийт--%>
<%--								<br>сургуулийн удирдлага, менежер, багш, сурагчиддаа талархсанаа илэрхийлье.--%>
<%--								<br>--%>
<%--								<br>--%>
<%--								Мөн UBT.mn цахим сургалт, үнэлгээний систем нь цар тахлын хүнд цаг үед хэрэглэгчиддийнхээ <br>хэрэгцээ шаардлагыг үнэ төлбөргүйгээр бүрэн хангаж ажилласныг дуулгахад таатай байна.--%>
<%--								<br>--%>
<%--								UBT.mn нь нийт сургуулийн удирдлага, менежерүүдтэй ярилцсаны үндсэн дээр 3 сарын 17 -ны өдрөөс эхлэн төлбөртэй болж байгаагаа энэхүү мэдэгдлээрээ илэрхийлж байна.--%>
<%--								<br>--%>
<%--								<br>--%>
<%--								Бид эдийн засгийн нөхцөл байдлыг харгалзан үзэж цаашид системийн нэг хүүхдийн сарын <br>--%>
<%--								төлбөрийг 2020-2021 оны хичээлийн жилийг дуусах хүртэлх хугацаанд 500 төгрөг байхаар тогтоож<br> үйлчилгээгээ үзүүлэхээр шийдвэрлэлээ. (төлбөрийг 2021.04.10ны дотор урьдчилан төлөхийг <br>анхаарна уу.)--%>
<%--								<br>--%>
<%--								<br>--%>
<%--								Та бүхний орон зай, цаг хугацаанаас үл хамаарах мэдлэгийг хүртээмжтэй, үр ашигтайгаар түгээх,<br> үнэлэх боломжийн төлөө системээ улам бүр сайжруулсаар байх болно.--%>
<%--								<br>--%>
<%--								<br>--%>
<%--								Та бүхнийг хүндэтгэсэн : <b>Мэксим Консалтинг ХХК-ийн хамт олон</b>--%>
<%--							</p>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<br>--%>
<%--					<div class="btn-wrap">--%>
<%--						<button class="btn-gray shadow" type="button" onclick="modalClose()">Хаах</button>--%>
<%--					</div>--%>
<%--					<br>--%>
<%--					<br>--%>
<%--				</form>--%>
<%--			</div>--%>
<%--		</div>--%>
	</div>
</section>
<script type="text/javascript">
    var app = new Vue({
        data: {
            looginId: "",
            password: "",
			rememberMe: false
		},

        watch: {},

        computed: {},

        filters: {},

        mounted: function () {
        },
		created(){
        	this.remembered()
		},
        methods: {
			loading(cmd) {
				var l = document.getElementById("se-pre-con");
				l.style.display = cmd;
			},
			rememberTrue(){
				this.rememberMe = !this.rememberMe
			},
			remembered(){
				var username = localStorage.getItem('looginId')
				var password = localStorage.getItem('password')

				if(username != null && password != null)
				{
					this.looginId = username
					this.password = password
				}
			},
            login: function () {
                try {
                    if (this.looginId != '' && this.password != '')
                    {
						if(this.rememberMe == true){
							localStorage.setItem('looginId', this.looginId)
							localStorage.setItem('password', this.password)
						}
						var _this = this
						_this.loading('block')
                        var language =  "<%=pageContext.getResponse().getLocale()%>"
                        var locale = ''
                        if (language == 'kr')
                        {
                            locale = 'kr-KR'
                        }
                        if(language == 'mn')
                        {
                            locale = 'mn-MN'
                        }
                        if(language == 'en'){
                            locale = 'en-EN'
                        }
                        const headers = {
                            'Accept-Language' : locale
                        }

                        axios.post('${HOME}/app/auth/signin', {
                            loginId: this.looginId,
                            password: this.password
                        },{headers: headers}).then(function (response) {
                            if(response.data.status == 888)
							{
								_this.loading('none')
								window.location.href = "${HOME}/payment";
								// var modal = UIkit.modal("#payment");
								// modal.show()
							}
							if (response.data.status == 200) {
								_this.loading('none')
								window.location.href = "${HOME}";
							}
							else{
								alert(response.data.msg)
								_this.loading('none')
							}
							_this.loading('none')
                        });
                    }
                    else{
                        if (this.looginId != '')
                        {
                            alert("<spring:message code="login.incorrectId"/>")
                        }
                        else{
                            alert("<spring:message code="login.incorrectPass"/>")
                        }
                    }
                } catch (error) {
                    console.log(error);
					_this.loading('none')
                }
            }
        }
    });
    app.$mount('#login')
</script>

</body>
</html>

<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <link rel="stylesheet" href="./css/common/header.css">
        <link rel="stylesheet" href="./css/common/footer.css">
    </head>
    <body>
		<header class="bdb10">
			<div class="wrap top">
<%--				<div class="logo">--%>
<%--					<a href="./sample.jsp"><img src="${IMG}/header/nslogo.png" class="auto"></a>--%>
<%--				</div>--%>
<%--	exam.edu.mn todo check --%>
				<div class="flex" style="align-items: center;">
					<div class="logo"><img src="${IMG}/header/esis1.png" class="auto"></div>
					<div class="logo" style="margin-left: 50px;"><img src="${IMG}/header/edu1.png" class="auto"></div>
				</div>
<%--	exam.edu.mn todo check --%>
<%--				<div class="flex" style="align-items: center;">--%>
<%--					<div class="logo"><img src="${IMG}/header/esis1.png" class="auto"></div>--%>
<%--					<div class="logo" style="margin-left: 50px;"><img src="${IMG}/header/edu1.png" class="auto"></div>--%>
<%--				</div>--%>
			</div>
			<div class="submenu-bg"></div>
		</header>
        <section>

			<div class="radius-10 bg-white shadow-wrap wrap">
				<div class="certi-box">
					<p class="certi-tt"><span>PC인증하기</span></p>
					<div class="certi-cont">
						<dl class="certi-info flex">
							<dt><img src="./images/certi-icon.png" alt=""></dt>
							<dd>
								<ul>
									<li>Q-BANK 시스템을 이용하시려면 PC인증을 받으셔야 합니다. </li>
									<li>최초 1회 진행되며, 추후 PC정보 변경 및 추가는 <strong>MY> PC인증관리</strong> 메뉴에서 하실 수 있습니다.</li>
									<li>아래 버튼을 클릭하시면 <strong>사용자 PC의 정보(IP, MAC Address)</strong>를 조회한 후 등록하실 수 있습니다.</li>
								</ul>
							</dd>
						</dl>
						<a href="#" class="btn-red shadow big-btn">내 PC정보 확인하기</a>
						<div class="certi-address-info">
							<ul>
								<li class="flex justify-content-center">
									<span class="label">IP Address</span>
									<span class="address">255.255.255.123</span>
								</li>
								<li class="flex justify-content-center">
									<span class="label">MAC Address</span>
									<span class="address">01-22-33-AC-AA</span>
								</li>
							</ul>
							<a href="#" class="btn-red shadow">등록하기</a>
						</div>
					</div>
				</div>
			</div>

		</section>
    </body>
</html>

<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <link rel="stylesheet" href="./css/common/header.css">
        <link rel="stylesheet" href="./css/common/footer.css">
        <link rel="stylesheet" href="./css/main.css">
    </head>
    <body>
        <section class="no_contents">
            <div class="wrap">
                <div class="contents-description flex">
                    홍길동 교수님, 
                    <div class="flex">
                        <span class="red-mini-box">0</span><span class="red-mini-box">1</span><span class="red-mini-box">2</span><span class="red-mini-box">3</span><span class="red-mini-box">4</span>
                    </div>
                    콘텐츠를 보유하고 있습니다.
                </div>

                <article class="check bg-white radius-5">
                    <div class="notice">
                        <h2 class="flex">공지사항<a href="" class="more"><span class="dot"></span><span class="dot"></span><span class="dot"></span></a></h2>
                        <ul>
                            <li>
                                <a href="">
                                    <div class="title">한국교육학술정보원 의과대학 시험 출제 일정 공지</div>
                                    <div class="date">2019-00-00</div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="title">시험 출제 일정 공지</div>
                                    <div class="date">2019-00-00</div>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <div class="title">한국교육학술정보원 의과대학 시험 출제 일정 공지</div>
                                    <div class="date">2019-00-00</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="box flex">
                        <div class="detail">
                            <div>진행중 요청</div>
                            <div>0 / 0</div>
                        </div>
                        <div class="detail">
                            <div>미확인 피드백</div>
                            <div>0</div>
                        </div>
                        <div class="detail">
                            <div>임시저장/반려</div>
                            <div>0 / 0</div>
                        </div>
                        <div class="cok" onclick="location.href='';">
                            <div>문항 등록하기</div>
                            <div>+</div>
                        </div>
                    </div>
                </article>

                <div class="flex flex-wrap">
                    <article class="ing wd-100">
                        <h2 class="flex">진행 중 & 완료 요청 내역<a href="" class="more"><span class="dot"></span><span class="dot"></span><span class="dot"></span></a></h2>
                        <div class="flex content">
                            <div class="bg-white radius-5 flex-1">
                                <div class="top flex">
                                    <div class="clock"><img src="${IMG}/icons/main/clock.png" class="auto"></div>
                                    <ul class="flex-1">
                                        <li>출제 일정 초과!!! (MM.DD. 까지)</li>
                                        <li>시험명</li>
                                    </ul>
                                </div>
                                
                                <div class="middle flex flex-center">
                                    <div class="title">현재 요청 내역이 없습니다.</div>
                                    <div class="circle border-gray">
                                        <div><span class="">0</span> / 0</div>
                                    </div>
                                </div>
                                <div class="btn">
                                    <button class="btn-red wd-100 shadow">간편 출제하기</button>
                                </div>
                            </div>
                            <div class="bg-white radius-5 flex-1">
                                <div class="top flex">
                                    <div class="clock"><img src="${IMG}/icons/main/clock.png" class="auto"></div>
                                    <ul class="flex-1">
                                        <li>출제 일정 초과!!! (MM.DD. 까지)</li>
                                        <li>시험명</li>
                                    </ul>
                                </div>
                                <div class="middle flex flex-center">
                                    <div class="title">현재 요청 내역이 없습니다.</div>
                                    <div class="circle border-gray">
                                        <div><span class="">0</span> / 0</div>
                                    </div>
                                </div>
                                <div class="btn">
                                    <button class="btn-white wd-100 shadow">확인하기</button>
                                </div>
                            </div>
                            <div class="bg-white radius-5 flex-1">
                                <div class="top flex">
                                    <div class="clock"><img src="${IMG}/icons/main/clock.png" class="auto"></div>
                                    <ul class="flex-1">
                                        <li>출제 일정 초과!!! (MM.DD. 까지)</li>
                                        <li>시험명</li>
                                    </ul>
                                </div>
                                <div class="middle flex flex-center">
                                    <div class="title">현재 요청 내역이 없습니다.</div>
                                    <div class="circle border-gray">
                                        <div><span class="">0</span> / 0</div>
                                    </div>
                                </div>
                                <div class="btn">
                                    <button class="btn-white wd-100 shadow">확인하기</button>
                                </div>
                            </div>
                            <div class="bg-white radius-5 flex-1">
                                <div class="top flex">
                                    <div class="clock"><img src="./images/icons/main/clock.png" class="auto"></div>
                                    <ul class="flex-1">
                                        <li>출제 일정 초과!!! (MM.DD. 까지)</li>
                                        <li>시험명</li>
                                    </ul>
                                </div>
                                <div class="middle flex flex-center">
                                    <div class="title">현재 요청 내역이 없습니다.</div>
                                    <div class="circle border-gray">
                                        <div><span class="">0</span> / 0</div>
                                    </div>
                                </div>
                                <div class="btn">
                                    <button class="btn-white wd-100 shadow">확인하기</button>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="recent wd-50 left">
                        <h2 class="flex">최근 작업 내역<a href="" class="more"><span class="dot"></span><span class="dot"></span><span class="dot"></span></a></h2>
                        <div class="bg-white radius-5 flex-1">
                            <div class="no">최근 작업 내역이 없습니다.</div>
                        </div>
                    </article>
                    <article class="share wd-50 right">
                        <h2 class="flex">최근 내 문항을 활용한 교수<a href="" class="more"><span class="dot"></span><span class="dot"></span><span class="dot"></span></a></h2>
                        <div class="flex content">
                            <div class="bg-white radius-5 flex-1">
                                <div class="top">
                                    
                                </div>
                                <div class="profile">
                                    내 문항을 활용한<br>교수가 없습니다.
                                </div>
                            </div>
                            <div class="bg-white radius-5 flex-1">
                                <div class="top">
                                    
                                </div>
                                <div class="profile">
                                    내 문항을 활용한<br>교수가 없습니다.
                                </div>
                            </div>
                            <div class="bg-white radius-5 flex-1">
                                <div class="top">
                                    
                                </div>
                                <div class="profile">
                                    내 문항을 활용한<br>교수가 없습니다.
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="kind wd-100">
                        <h2 class="flex wd-100">나의 문항카드 관리<a href="" class="more"><span class="dot"></span><span class="dot"></span><span class="dot"></span></a></h2>
                        <div class="flex wd-100">
                            <div class="content">
                                <div class="bg-white radius-5 flex-1">
                                    <div class="title">등록하신 문항카드가 없습니다.</div>
                                    <div class="detail">
                                        <div class="detail-title">보유 문항 수</div>
                                        <div class="num">0</div>
                                    </div>
                                </div>
                                <div>중분류 <span class="color-red">0</span>개</div>
                            </div>
                            <div class="content">
                                <div class="bg-white radius-5 flex-1">
                                    <div class="title">등록하신 문항카드가 없습니다.</div>
                                    <div class="detail">
                                        <div class="detail-title">보유 문항 수</div>
                                        <div class="num">0</div>
                                    </div>
                                </div>
                                <div>중분류 <span class="color-red">0</span>개</div>
                            </div>
                            <div class="content">
                                <div class="bg-white radius-5 flex-1">
                                    <div class="title">등록하신 문항카드가 없습니다.</div>
                                    <div class="detail">
                                        <div class="detail-title">보유 문항 수</div>
                                        <div class="num">0</div>
                                    </div>
                                </div>
                                <div>중분류 <span class="color-red">0</span>개</div>
                            </div>
                            <div class="content">
                                <div class="bg-white radius-5 flex-1">
                                    <div class="title">등록하신 문항카드가 없습니다.</div>
                                    <div class="detail">
                                        <div class="detail-title">보유 문항 수</div>
                                        <div class="num">0</div>
                                    </div>
                                </div>
                                <div>중분류 <span class="color-red">0</span>개</div>
                            </div>
                        </div>
                    </article>
                </div>
            </div> 
        </section>
    </body>
</html>
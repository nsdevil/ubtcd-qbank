<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="${CSS}/common/header.css">
        <link rel="stylesheet" href="${CSS}/common/footer.css">
        <link rel="stylesheet" href="${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href="${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href="${CSS}/question_registration/each/making.css">
        <link rel="stylesheet" href="${CSS}/question_card/institutional/default_list.css">
        <script src='${JS}/making.js'></script>

    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                 <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href="">문항등록</a></span>
                    <span><a href="">임시저장 목록</a></span>
                </div>
                <h2>임시저장 목록</h2>

                <div class="search-wrap ">
					<div class="flex sh-radio-wrap">
						<span>문항 유형</span>
						<div class="radio-wrap"><input class="radio" type="radio" name="item_type" id="item_type1" value="전체"><label for="item_type1">전체</label></div>
						<div class="radio-wrap"><input class="radio" type="radio" name="item_type" id="item_type2" value="객관식(단일/복수정답형)"><label for="item_type2">객관식(단일/복수정답형)</label></div>
<!-- 						<div class="radio-wrap"><input class="radio" type="radio" name="item_type" id="item_type3" value="객관식(R형)"><label for="item_type3">객관식(R형)</label></div> -->
						<div class="radio-wrap"><input class="radio" type="radio" name="item_type" id="item_type4" value="주관식(단일정답형)"><label for="item_type4">주관식(단일정답형)</label></div>
<!-- 						<div class="radio-wrap"><input class="radio" type="radio" name="item_type" id="item_type5" value="CPX"><label for="item_type5">CPX</label></div> -->
					</div>
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2" name="type" data-placeholder="구분">
                                <option value="all" selected>전체</option>
                                <option value="num">번호</option>
                                <option value="type">문항 유형</option>
                                <option value="id">문항 ID</option>
                                <option value="version">버전</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
                </div>
                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col style="width : 100px;">
                            <col style="width : 180px;">
                            <col>
                            <col style="width : 100px;">
                            <col style="width : 310px;">
                            <col style="width : 130px;">
                            <col style="width : 130px;">
                        </colgroup>
                        <thead class="border-red-top-2 border-gray-bottom-2">
                            <tr>
                                <th>
                                    번호
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>
                                    문항 유형
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>
                                    문항 ID
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>
                                    버전
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>
                                    문항 줄기
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>
                                    최초 등록일
                                    <a href=""><i class="up"></i></a>
                                    <a href=""><i class="down"></i></a>
                                </th>
                                <th>관리</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>10</td>
                                <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td>
                                <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td>
                                <td onclick="tmp_preview();">1.3</td>
                                <td class="t-left" onclick="tmp_preview();">다음 도형에 변 ㄱㄴ 과 변 ㄱㄷ 은 길이가 같습니다. 안에 들어갈 각도로 옳은 것은 어떤 것입니까?</td>
                                <td onclick="tmp_preview();">2020-02-10</td>
								<td class="management">
									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button>
									<button><i class="delete"></i></button>
								</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td>
                                <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td>
                                <td onclick="tmp_preview();">1.2</td>
                                <td class="t-left" onclick="tmp_preview();">다음 도형에 변 ㄱㄴ 과 변 ㄱㄷ 은 길이가 같습니다. 안에 들어갈 각도로 옳은 것은 어떤 것입니까?</td>
                                <td onclick="tmp_preview();">2020-02-10</td>
								<td class="management">
									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button>
									<button><i class="delete"></i></button>
								</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td>
                                <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td>
                                <td onclick="tmp_preview();">1.1</td>
                                <td class="t-left" onclick="tmp_preview();">다음 도형에 변 ㄱㄴ 과 변 ㄱㄷ 은 길이가 같습니다. 안에 들어갈 각도로 옳은 것은 어떤 것입니까?</td>
                                <td onclick="tmp_preview();">2020-02-10</td>
								<td class="management">
									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button>
									<button><i class="delete"></i></button>
								</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td>
                                <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td>
                                <td onclick="tmp_preview();">1.0</td>
                                <td class="t-left" onclick="tmp_preview();">다음 도형에 변 ㄱㄴ 과 변 ㄱㄷ 은 길이가 같습니다. 안에 들어갈 각도로 옳은 것은 어떤 것입니까?</td>
                                <td onclick="tmp_preview();">2020-02-10</td>
								<td class="management">
									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button>
									<button><i class="delete"></i></button>
								</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td>
                                <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td>
                                <td onclick="tmp_preview();">1.0</td>
                                <td class="t-left" onclick="tmp_preview();">다음 도형에 변 ㄱㄴ 과 변 ㄱㄷ 은 길이가 같습니다. 안에 들어갈 각도로 옳은 것은 어떤 것입니까?</td>
                                <td onclick="tmp_preview();">2020-02-10</td>
								<td class="management">
									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button>
									<button><i class="delete"></i></button>
								</td>
                            </tr>
<!--                             <tr> -->
<!--                                 <td>5</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td> -->
<!--                                 <td onclick="tmp_preview();">1.0</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">다음 치주조직의 손상에 대해 옳은 것을 고르시오.</td> -->
<!--                                 <td onclick="tmp_preview();">2020-02-10</td> -->
<!-- 								<td class="management"> -->
<!-- 									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button> -->
<!-- 									<button><i class="delete"></i></button> -->
<!-- 								</td> -->
<!--                             </tr> -->
<!--                             <tr> -->
<!--                                 <td>4</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td> -->
<!--                                 <td onclick="tmp_preview();">1.0</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">다음 치주조직의 손상에 대해 옳은 것을 고르시오.</td> -->
<!--                                 <td onclick="tmp_preview();">2020-02-10</td> -->
<!-- 								<td class="management"> -->
<!-- 									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button> -->
<!-- 									<button><i class="delete"></i></button> -->
<!-- 								</td> -->
<!--                             </tr> -->
<!--                             <tr> -->
<!--                                 <td>3</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td> -->
<!--                                 <td onclick="tmp_preview();">1.0</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">다음 치주조직의 손상에 대해 옳은 것을 고르시오.</td> -->
<!--                                 <td onclick="tmp_preview();">2020-02-10</td> -->
<!-- 								<td class="management"> -->
<!-- 									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button> -->
<!-- 									<button><i class="delete"></i></button> -->
<!-- 								</td> -->
<!--                             </tr> -->
<!--                             <tr> -->
<!--                                 <td>2</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td> -->
<!--                                 <td onclick="tmp_preview();">1.0</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">다음 치주조직의 손상에 대해 옳은 것을 고르시오.</td> -->
<!--                                 <td onclick="tmp_preview();">2020-02-10</td> -->
<!-- 								<td class="management"> -->
<!-- 									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button> -->
<!-- 									<button><i class="delete"></i></button> -->
<!-- 								</td> -->
<!--                             </tr> -->
<!--                             <tr> -->
<!--                                 <td>1</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">객관식(단일/복수정답형)</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">G0000000001 > k000000011 > s00000000001</td> -->
<!--                                 <td onclick="tmp_preview();">1.0</td> -->
<!--                                 <td class="t-left" onclick="tmp_preview();">다음 치주조직의 손상에 대해 옳은 것을 고르시오.</td> -->
<!--                                 <td onclick="tmp_preview();">2020-02-10</td> -->
<!-- 								<td class="management"> -->
<!-- 									<button class="btn-edit" onclick="popup2('./popup/edit.jsp','edit','1350','800');"><i class="update"></i></button> -->
<!-- 									<button><i class="delete"></i></button> -->
<!-- 								</td> -->
<!--                             </tr> -->
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
                    <button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
                    <ul class="paginate-list">
                        <li class="page-num active"><a href="#">1</a></li>
                        <li class="page-num"><a href="#">2</a></li>
                        <li class="page-num"><a href="#">3</a></li>
                        <li class="page-num"><a href="#">4</a></li>
                        <li class="page-num"><a href="#">5</a></li>
                        <li class="page-num"><a href="#">6</a></li>
                        <li class="page-num"><a href="#">7</a></li>
                        <li class="page-num"><a href="#">8</a></li>
                        <li class="page-num"><a href="#">9</a></li>
                        <li class="page-num"><a href="#">10</a></li>
                    </ul>
                    <button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
                    <button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
                </div>

            </div>
			<div id="preview"></div>
        </section>

    </body>
</html>

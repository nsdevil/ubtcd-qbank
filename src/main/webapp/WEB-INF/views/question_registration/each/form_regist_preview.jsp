<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href="   ${CSS}/question_registration/each/essential.css">
		<link rel="stylesheet" href="   ${CSS}/question_registration/each/preview.css">
		<link rel="stylesheet" href="   ${CSS}/question_registration/each/preview/preview.css">

        <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="   ${JS}/essential.js"></script>
        <script src="/ftp/kucm/xredu/js/script.js"></script>
        
    </head>
    <body>

        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap pdb-0">
                <div class="breadcrumb">
                    <span><a  onclick="location.href='${HOME}/main';">Home</a></span>
                    <span><a href="">문항등록</a></span>
                    <span><a href="../../question_registration/each/form_regist.jsp">양식등록</a></span>
                </div>
            
                <nav>
                    <ul>
                        <li>필수정보</li>
                        <li>문항카드</li>
                        <li>양식등록</li>
                        <li class="on">미리보기</li>
                    </ul>

					<div class="group_breadcrumb">
                        그룹 ID : g000001 &gt; 문제 ID : k000001 &gt; Sub ID : s0001 &gt; Version &gt; 1.0
                    </div>
                </nav>

                
                <div class="content-wrap pdtb-0 flex bg-light-gray2">
					<aside class="type-r-left">
						<ul>
							<li class="on"><a href="javascript:;">1</a></li>
							<li><a href="javascript:;">2</a></li>
							<li><a href="javascript:;">3</a></li>
							<li><a href="javascript:;">4</a></li>
							<li><a href="javascript:;">5</a></li>
							<li><a href="javascript:;">6</a></li>
							<li><a href="javascript:;">100</a></li>
							<li><a href="javascript:;">101</a></li>
						</ul>
					</aside>
					<div class="preview-cont">
						<div class="flex top-btn-area">
							<button class="mgl-auto btn-red shadow">전체 문항 등록</button>
						</div>
						<div class="wrap bg-white">
							<div class="flex content-wrap flex-wrap">
								<div class="left">
									<div class="profile">
										<div class="name">
											<div>123456788</div>
											<div>Hong-gildong</div>
										</div>
										<div class="test-info">
											<div class="test-question">
												<div class="total">Total Question <span>100</span> | <span>50</span></div>
												<div class="time">00:50:00</div>
											</div>
											<div class="test-type">
												<div class="type"><span>A type</span>TEST Name</div>
												<div class="icon">
													<i class="speaker"></i>
												</div>
											</div>
										</div>
									</div>

									<div class="question">
										<div class="content">
											<p>다음 도형에 변 ㄱㄴ 과 변 ㄱㄷ 은 길이가 같습니다. 안에 들어갈 각도로 옳은 것은 어떤 것입니까?</p>
			<!--                                 <p class="content"><img src="${IMG}/icons/preview/example-image.png"></p> -->
											<p class="content"><img src="${IMG}/etc/img-sample.jpg" width="274"></p>
											<p class="refer"><i class="capture"></i>이미지를 Touch하시면 크게 보실 수 있습니다.</p>
										</div>
									</div>

									<div class="answer">
							<ul>
								<li><span>1</span>130º
									<div class="delete">
										<button class="btn-gray" onclick="delete_type(event);">
											<i class="close"></i>
										</button>
									</div>
								</li>
								<li><span>2</span>132º
									<div class="delete">
										<button class="btn-gray" onclick="delete_type(event);">
											<i class="close"></i>
										</button>
									</div>
								</li>
								<li class="on"><span>3</span>134º
									<div class="delete">
										<button class="btn-gray" onclick="delete_type(event);">
											<i class="close"></i>
										</button>
									</div>
								</li>
								<li><span>4</span>131º
									<div class="delete">
										<button class="btn-gray" onclick="delete_type(event);">
											<i class="close"></i>
										</button>
									</div>
								</li>
							</ul>
						</div>

									<div class="incoreect_note">
										<div class="title">오답노트</div>
										<div></div>
									</div>
								</div>

								                    <div class="right">
                        <div class="default">
                            <strong>기본정보<button onclick="default_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li>그룹 ID : g000001</li>
                                <li>문제 ID : qa000001</li>
                                <li>출제자 :홍길동(20180000)</li>
                                <li>등록일 : 2020-02-10</li>
                            </ul>
                        </div>

                        <div class="essential">
                            <strong>필수정보<button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li>[시험유형] 필기시험</li>
                                <li>[문제유형] 객관식(단일정답형)<i class="close"></i></li>
<!--                                 <li>[문항공개범위] 내가 속한 대학 내 전체 공유<i class="close"></i></li> -->
                                <li>[외부공유] 해당사항 없음<i class="close"></i></li>
								<li>[멀티미디어 자료 등록] 해당사항 없음<i class="close"></i></li>
<!--                                 <li>[편집권한] 타 출제자 편집 미허용(사용만가능)<i class="close"></i></li> -->
<!--                                 <li>[마켓 공유 시 가격] 해당사항 없음<i class="close"></i></li> -->
                            </ul>
                        </div>

                        <div class="selected-list">
                            <strong>문항카드</strong>
                            <div class="flex">
                                <ul>
									<li># [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가<i class="close"></i></li>
									<li># [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈<i class="close"></i></li>
									<li># [성취기준] 수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.<i class="close"></i></li>									
                                </ul>
                                <ul>
									<li># [지식수준] 해석(판단)<i class="close"></i></li>
									<li># [예상난이도] 어려움(4)<i class="close"></i></li>
									<li># [문항의 적절성] 필수적인(essential)<i class="close"></i></li>
									<li># [주제어(자유 키워드)] 평가(임시)<i class="close"></i></li>
                                </ul>
                            </div>
                        </div>
									<div class="btn-wrap flex">
										<button class="btn-white prev shadow" onclick="location.href='../../question_registration/each/form_regist.jsp';">
											<i class="prev-gray"></i>이전단계
										</button> 
										<div class="flex">
											<button class="btn-red-line next shadow" >문항수정</button><!-- onclick="popup2('./popup/edit.jsp','edit','1350','800');" -->
											<button class="btn-red next shadow" onclick="location.href='#';"><i class="check-white"></i>임시저장</button>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>

                </div>
            </div>
        </section>

    </body>
</html>
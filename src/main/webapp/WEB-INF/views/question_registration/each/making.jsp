<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link href="${CSS}/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../lib/froala_editor/froala_editor.pkgd.min.css">
        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href="   ${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href="   ${CSS}/question_registration/each/making.css">
        <script src="../../lib/froala_editor/froala_editor.pkgd.min.js"></script>
        <script src=/js/making.js></script>
    </head>
    <body>

        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                 <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href="">문항등록</a></span>
                    <span><a href="">개별등록</a></span>
                </div>

                <nav>
                    <ul>
                        <li><a href="../../question_registration/each/essential">필수정보</a></li>
                        <li><a href="../../question_registration/each/question_card/test_subject1">문항카드</a></li>
                        <li class="on"><a href="../../question_registration/each/making">문항제작</a></li>
                        <li><a href="../../question_registration/each/preview/preview">미리보기</a></li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                <div class="content-wrap flex">

                    <article>
                        <h3>문항줄기</h3>
                        <div class="content froala-wrap">
                            <div id="froala"></div>
                        </div>
                        <h3>제시자료</h3>
                        <div class="content">
                            <ul class="icon-list">
                                <li class="image"><img src="${IMG}/icons/question_registration/image.png"></li>
                                <li><img src="${IMG}/icons/question_registration/video.png"></li>
                                <li><img src="${IMG}/icons/question_registration/audio.png"></li>
                                <li><img src="${IMG}/icons/question_registration/A.png"></li>
                                <li><img src="${IMG}/icons/question_registration/A-bg-white.png"></li>
                                <li>STACK</li>
                                <li>Table</li>
                                <li>VR</li>
                                <li>환자사례</li>
                            </ul>
                            <ul class="content-list">
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            1
                                        </div>
                                        <div class="count">
                                            <a href=""><i class="up-pointing"></i></a>
                                            <a href=""><i class="down-pointing"></i></a>
                                        </div>
                                    </div>
                                    <div class="type">
                                        <img src="${IMG}/icons/question_registration/play.png" class="auto">
                                    </div>
                                    <div class="file">
                                        <p>Upload your File( MP4, 10MB)</p>
                                        <button class="btn-white shadow" onclick="file_load(event);">불러오기<i class="load"></i></button>
                                        <input type="file" name="file">
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            2
                                        </div>
                                        <div class="count">
                                            <a href=""><i class="up-pointing"></i></a>
                                            <a href=""><i class="down-pointing"></i></a>
                                        </div>
                                    </div>
                                    <div class="type">
                                        <img src="${IMG}/icons/image.png" class="auto">
                                    </div>
                                    <div class="file">
                                        <p>Upload your File( MP4, 10MB)</p>
                                        <button class="btn-white shadow" onclick="file_load(event);">불러오기<i class="load"></i></button>
                                        <input type="file" name="file">
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            3
                                        </div>
                                        <div class="count">
                                            <a href=""><i class="up-pointing"></i></a>
                                            <a href=""><i class="down-pointing"></i></a>
                                        </div>
                                    </div>
                                    <div class="type">
                                        <img src="${IMG}/icons/question_registration/type_audio.png" class="auto">
                                    </div>
                                    <div class="file">
                                        <p>Upload your File( MP4, 10MB)</p>
                                        <button class="btn-white shadow" onclick="file_load(event);">불러오기<i class="load"></i></button>
                                        <input type="file" name="file">
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            4
                                        </div>
                                        <div class="count">
                                            <a href=""><i class="up-pointing"></i></a>
                                            <a href=""><i class="down-pointing"></i></a>
                                        </div>
                                    </div>
                                    <div class="type">
                                        <img src="${IMG}/icons/question_registration/stack.png" class="auto">
                                    </div>
                                    <div class="file">
                                        <p>Upload your File( MP4, 10MB)</p>
                                        <button class="btn-white shadow" onclick="file_load(event);">불러오기<i class="load"></i></button>
                                        <input type="file" name="file">
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <h3>답가지 및 정답 등록</h3>
                        <div class="content">
                            <ul class="icon-list">
                                <li>
                                    <img src="${IMG}/icons/question_registration/text.png">
                                </li>
                                <li>
                                    <img src="${IMG}/icons/question_registration/image-letter.png">
                                </li>
                                <li>
                                    <img src="${IMG}/icons/question_registration/video-letter.png">
                                </li>
                                <li class="desc">
                                    ** 왼쪽 버튼을 클릭하면 유형을 변경할 수 있습니다.
                                </li>
                            </ul>
                            <ul class="content-list answer-froala-wrap">
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            1
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <label for="answer_1"></label>
                                        <input type="checkbox" name="answer_1" class="checkbox" id="answer_1" value="y">
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            2
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <label for="answer_1"></label>
                                        <input type="checkbox" name="answer_1"  class="checkbox" id="answer_1" value="y">
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            3
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <label for="answer_1"></label>
                                        <input type="checkbox" name="answer_1" class="checkbox" id="answer_1" value="y">
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            4
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <label for="answer_1"></label>
                                        <input type="checkbox" name="answer_1" class="checkbox" id="answer_1" value="y">
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <h3>오답노트</h3>
                        <div class="content">
                            <ul class="icon-list">
                                <li>
                                    <img src="${IMG}/icons/question_registration/image-letter.png">
                                </li>
                                <li>
                                    <img src="${IMG}/icons/question_registration/audio-letter.png">
                                </li>
                                <li>
                                    <img src="${IMG}/icons/question_registration/video-letter.png">
                                </li>
                                <li>
                                    <img src="${IMG}/icons/question_registration/text.png">
                                </li>
                                <li class="desc">
                                    ** 왼쪽 버튼을 클릭하면 유형을 변경할 수 있습니다.
                                </li>
                            </ul>
                            <ul class="content-list incorrect_note_wrap">
                                <li class="flex">
                                    <div class="select-wrap">
                                        <div class="num">

                                            <select class="select2" name="">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="btn-wrap flex">
                            <button class="btn-white prev shadow">이전단계<i class="prev-gray"></i></button>
                            <div class="flex">
                                <button class="btn-gray delete shadow">삭제</button>
                                <button class="btn-red next shadow">저장하고 다음으로<i class="next-white"></i></button>
                            </div>
                        </div>
                    </article>

                    <aside>
                        <div class="essential">
                            <strong>선택한 필수정보 <button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li class="test_type">[시험 유형] 의학교육 <span>지필고사</span></li>
                                <li class="question_type">[문제 유형] <span>객관식(단일정답형)</span></li>
<!--                                 <li class="public_scope">[문항공개범위] <span>내가 속한 대학 내 전체 공유</span></li> -->
<!--                                 <li class="edit_rights">[편집권한] <span>타 출제자(일반교수) 편집 미허용(사용만가능)</span></li> -->
                                <li class="external_sharing">[외부공유] <span>해당사항 없음</span></li>
								<li class="multimedia">[멀티미디어 자료 등록] <span>해당사항 없음</span></li>
<!--                                 <li class="price">[마켓 공유 시 가격] <span>해당사항 없음</span></li> -->
                            </ul>
                        </div>

                        <div class="selected">
                            <strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
                            <ul class="selected-list">
                                <li># [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가<i class="close"></i></li>
                                <li># [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈<i class="close"></i></li>
								<li># [성취수준] 수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.<i class="close"></i></li>
								<li># [지식수준] 해석(판단)<i class="close"></i></li>
								<li># [예상난이도] 어려움(4)<i class="close"></i></li>
								<li># [문항의 적절성] 필수적인(essential)<i class="close"></i></li>
								<li># [주제어(자유 키워드)] 평가(임시)<i class="close"></i></li>
                            </ul>
                        </div>
                    </aside>
                </div>


            </div>
        </section>

    </body>
</html>

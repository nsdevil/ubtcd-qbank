<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="${CSS}/common/header.css">
    <link rel="stylesheet" href="${CSS}/common/footer.css">
    <link rel="stylesheet" href="${CSS}/question_registration/question_registration.css">
    <link rel="stylesheet" href="${CSS}/question_registration/each/essential.css">
    <link href="${CSS}/select2.min.css" rel="stylesheet" />
    <script src="${JS}/select2.min.js"></script>
    <script src="${JS}/essential.js"></script>
    <style>
        #se-pre-con {
            position: fixed;
            -webkit-transition: opacity 5s ease-in-out;
            -moz-transition: opacity 5s ease-in-out;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
        }
    </style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="excelinsert">
    <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
        <div class="breadcrumb">
            <span><a onclick="location.href='${HOME}/question_card/questionbank';"><spring:message code="all.home" /> </a></span>
            <span><a href=""><spring:message code="question.register.name" /> </a></span>
            <span><a href="../../question_registration/each/form_regist.jsp"><spring:message code="default.excelUpload" /> </a></span>
        </div>

        <nav>
            <ul>
                <%--				<li>필수정보</li>--%>
                <%--				<li>문항카드</li>--%>
                <li class="on"><spring:message code="default.excelUpload"/></li>
                <%--				<li>미리보기</li>--%>
            </ul>
        </nav>


        <div class="content-wrap">
            <div class="form-contents">
                <div class="form-box">
                    <ul>
                        <p class="form-tt">STEP 1.</p>
                        <h3><spring:message code="all.mainCate"/></h3>
                        <select class='select2 select2-container select2-container--default' v-model='excel.topcate'
                                style="width: 250px; !important;" id="selectSearch">
                            <option value="0">Үндсэн</option>
                            <template v-for="(item, index) in examCategory.main">
                                <option :value="item.id" :key="index">{{item.name}} - ({{item.regName}})</option>
                            </template>
                        </select>
                        <select class=" select2 select2-container select2-container--default"
                                style="width: 250px !important; margin-left: 20px;" @change="categoryChange('exam', 'middle')"
                                v-model="excel.midcate">
                            <option value="0" disabled>Дунд</option>
                            <template v-for="(item, index) in examCategory.middle">
                                <option :value="item.id" :key="index">{{item.name}}</option>
                            </template>
                        </select>
                        <select class="select2 select2-container select2-container--default"
                                style="width: 250px !important; margin-left: 20px;" @change="categoryChange('exam', 'low')"
                                v-model="excel.lowcate" >
                            <option value="0" disabled>Бага</option>
                            <template v-for="(item, index) in examCategory.low">
                                <option :value="item.id" :key="index">{{item.name}}</option>
                            </template>
                        </select>
                    </ul>
                </div>
                <%--				<div class="form-box">--%>
                <%--					<ul>--%>
                <%--                        <h3><spring:message code="all.subjectcategory"/></h3>--%>
                <%--						<li>--%>
                <%--							<select class="wd-100" name="main" v-on:change="categoryChange('child', 'main')"--%>
                <%--									v-model="excel.topcateS">--%>
                <%--								<option value="0" disabled="" selected=""><spring:message code="all.main"/></option>--%>
                <%--								<template v-for="(item, index) in subjectCategory.main">--%>
                <%--									<option :value="item.id" :key="index">{{item.name}}</option>--%>
                <%--								</template>--%>
                <%--							</select>--%>
                <%--						</li>--%>
                <%--						<li>--%>
                <%--							<select class="wd-100" name="middle" v-on:change="categoryChange('child', 'middle')"--%>
                <%--									v-model="excel.midcateS">--%>
                <%--								<option value="0" disabled="" selected=""><spring:message code="all.middle"/></option>--%>
                <%--								<template v-for="(item, index) in subjectCategory.middle">--%>
                <%--									<option :value="item.id" :key="index">{{item.name}}</option>--%>
                <%--								</template>--%>
                <%--							</select>--%>
                <%--						</li>--%>
                <%--						<li>--%>
                <%--							<select class="wd-100" name="subclass" v-on:change="categoryChange('child', 'low')"--%>
                <%--									v-model="excel.lowcateS">--%>
                <%--								<option value="0" disabled="" selected=""><spring:message code="all.low"/></option>--%>
                <%--								<template v-for="(item, index) in subjectCategory.low">--%>
                <%--									<option :value="item.id" :key="index">{{item.name}}</option>--%>
                <%--								</template>--%>
                <%--							</select>--%>
                <%--						</li>--%>
                <%--					</ul>--%>
                <%--				</div>--%>

                <div class="form-box">
                    <p class="form-tt">STEP 2.</p>
                    <h3><spring:message code="default.excelUpload" /> </h3>
                    <div class="file" style="position: relative;">
                        <p><spring:message code="question.register.fileUpload" /> (XLS, 10MB)</p>
                        <%--<button class="btn-white shadow" onclick=";"><i class="download2"></i>양식 다운로드</button>--%>
                        <button class="btn-white shadow" onclick="file_load(event);"><i class="upload"></i>
                            <spring:message code="default.excelUpload" />
                        </button>
                        <input style="display: none;" @change="previewFileExcel($event)" name="file" type="file">
                        <p style="position: absolute;
    top: 0;
    left: 0;
    margin-top: 65px;
    margin-left: 30px;
    font-weight: 500;
    color: #000;">{{excel.fileName}}</p>
                    </div>
                </div>
                <div class="btn-wrap flex flex-space-between">
                    <button class="btn-white prev shadow" onclick=";">
                        <i class="prev-gray"></i> <spring:message code="all.back"/>
                    </button>
                    <div class="flex">
                        <button class="btn-red next shadow" @click="createQuestionExcel">
                            <i class="check-white"></i> <spring:message code="all.generate"/>
                        </button>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

<script>
    var app = new Vue({
        data() {
            return {
                excel: {
                    excelFile: null,
                    fileName: "",
                    topcate: "0",
                    midcate: "0",
                    lowcate: "0",
                    topcateS: "0",
                    midcateS: "0",
                    lowcateS: "0"

                },
                examCategory: {
                    main: [],
                    middle: [],
                    low: []
                },
                subjectCategory: {
                    main: [],
                    middle: [],
                    low: []
                }
            }
        },

        watch: {},

        computed: {},

        filters: {},

        mounted() {

            this.loadCates("main", 1, 0);
            this.loadCates("subject", 1, 0);
        },

        methods: {
            previewFileExcel(event) {
                this.excel.excelFile = event.target.files[0];
                this.excel.fileName = event.target.files[0].name;

                if ( this.excel.excelFile){
                    if ( /\.(xlsx)$/i.test( this.excel.fileName) ) {
                    }
                    else{
                        alert("File format is wrong please check your file !");
                        this.excel.fileName = ''
                        return
                    }
                }
            },
            createQuestionExcel() {
                try {
                    let formData = new FormData()
                    if (this.excel.fileName == '') {
                        alert("choose excel questions")
                        return;
                    }

                    if (this.excel.topcate == '0') {
                        alert("select top category")
                        return;
                    }
                    if (this.excel.midcate == '0') {
                        alert("select middle category")
                        return;
                    }
                    if (this.excel.lowcate == '0') {
                        alert("select low category")
                        return;
                    }

                    formData.append('efile', this.excel.excelFile);
                    formData.append('topcate', this.excel.topcate);
                    formData.append('midcate', this.excel.midcate);
                    formData.append('lowcate', this.excel.lowcate);

                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }

                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }

                    if(language == 'en'){
                        locale = 'en-EN'
                    }

                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    this.loading('block')
                    axios.post('${BASEURL}/questions/create/file', formData, {headers: headers}).then((response) => {
                        if (response.data.status === 200) {
                            this.loading('none')
                            alert("Success")
                            this.excel.excelFile = null;
                            this.excel.fileName = "";
                        } else {
                            this.loading('none')
                            alert("ERROR: " + response.data.message)
                        }

                    }, (error) => {
                        this.loading('none')
                        console.log(error);
                    });


                } catch (e) {
                    console.log(e)
                    alert(e.response.data.message)
                }
            }
            ,
            categoryChange(type, catelevel) {

                if (type == 'exam') {
                    type = 'main'
                } else {
                    type = 'subject'
                }

                if (catelevel == 'main') {
                    let parentId = 0
                    if (type == 'main')
                        parentId = this.excel.topcate
                    else
                        parentId = this.excel.topcateS


                    this.loadCates(type, 2, parentId)
                }
                if (catelevel == 'middle') {
                    let parentId = 0
                    if (type == 'main')
                        parentId = this.excel.midcate
                    else
                        parentId = this.excel.midcateS
                    this.loadCates(type, 3, parentId)
                }
            },
            loading(cmd) {
                var l = document.getElementById("se-pre-con");
                l.style.display = cmd;
            },
            loadCates(type, cateStep, parentId) {
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    axios.get('${BASEURL}/category/liststep', {
                        params: {
                            type: type,
                            cateStep: cateStep,
                            parentId: parentId
                        },
                        headers: headers
                    }).then((response) => {
                        if (response.data.status === 200) {
                            if (type == "main") {
                                //exam category
                                if (cateStep == 1) {
                                    this.examCategory.main = response.data.result.cates
                                }
                                if (cateStep == 2) {
                                    this.examCategory.middle = response.data.result.cates
                                }
                                if (cateStep == 3) {
                                    this.examCategory.low = response.data.result.cates
                                }
                            } else {
                                //subject category
                                if (cateStep == 1) {
                                    this.subjectCategory.main = response.data.result.cates
                                }
                                if (cateStep == 2) {
                                    this.subjectCategory.middle = response.data.result.cates
                                }
                                if (cateStep == 3) {
                                    this.subjectCategory.low = response.data.result.cates
                                }
                            }


                        } else {
                            alert("ERROR: " + response.data.message)
                        }
                    }, (error) => {
                        console.log(error);
                    });


                } catch (error) {
                    console.log(error)
                }
            },
        },
    })
    app.$mount('#excelinsert')
</script>
<script>
    $('#selectSearch').on("change",function(){
        app.excel.topcate = $(this).val();
        app.categoryChange('exam','main')
    });

    $(document).ready(function() {
        $('#selectSearch').select2({});
    });
</script>
</body>
</html>

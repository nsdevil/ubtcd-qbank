<%@ page contentType = "text/html;charset=utf-8" %>

	<div class="wrap">
		<div class="btn-close"><button onclick="popupClose2();"><i class="close-red"></i></button></div>
		<div class="card_edit flex flex-space-between">

			<article>
				<div class="card-content">


					<div class="card-sec">
                        <h3>시험과목</h3>
                        <div class="search test-sbj">
                            <input type="text" name="search" class="search-input" placeholder="1click search" onkeyup="search_keyword1(this);">
                            <div class="search-sub">
                                <ul>
                                    <li onclick="test_subject_select(this);">
                                        <span class="year">2019년</span>
                                        <span class="major">고려대 의예과</span>
                                        <span class="grade">1학년</span>
                                        <span class="term">1학기</span>
                                        <span class="main">내분비학</span>
                                        <span class="middle">호르몬과 남성 생식생리학</span>
                                        <span class="subclass">기말시험</span>
                                    </li>
                                    <li onclick="test_subject_select(this);">
                                        <span class="year">2019년</span>
                                        <span class="major">의학과</span>
                                        <span class="grade">2학년</span>
                                        <span class="term">2학기</span>
                                        <span class="main">치의학과</span>
                                        <span class="middle">기초치의학</span>
                                        <span class="subclass">중간고사</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="search-list">
                            <ul>
                                <li>
                                    <select class="wd-100" name="year">
                                        <option value="" disabled selected>연도</option>
                                        <option value="2019년">2019년</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="major">
                                        <option value="">학교(학급)</option>
                                        <option value="고려대 의예과">고려대 의예과</option>
                                        <option value="의학과">의학과</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="grade">
                                        <option value="" disabled selected>학년</option>
                                        <option value="1학년">1학년</option>
                                        <option value="2학년">2학년</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="term">
                                        <option value="" disabled selected>학기</option>
                                        <option value="1학기">1학기</option>
                                        <option value="2학기">2학기</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="main">
                                        <option value="" disabled selected>대분류</option>
                                        <option value="내분비학">내분비학</option>
                                        <option value="치의학과">치의학과</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="middle">
                                        <option value="" disabled selected>중분류</option>
                                        <option value="호르몬과 남성 생식생리학">호르몬과 남성 생식생리학</option>
                                        <option value="기초치의학">기초치의학</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="subclass">
                                        <option value="" disabled selected>소분류</option>
                                        <option value="기말시험">기말시험</option>
                                        <option value="중간고사">중간고사</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                        <button class="btn-white plus shadow" onclick='test_subject_add_pop();'>+ 추가하기</button>
					</div>


					<div class="card-sec">

						<h3>교육과정</h3>
						<div class="cont-type3">
							<div class="search curriculum-sh">
								<input type="text" name="search" class="search-input" placeholder="1click search" onkeyup="search_keyword2(this);">
								<div class="search-sub">
									<ul>
										<li onclick="curriculum_select(this);">
											<span class="course">교육과정명</span>
											<span class="unit">단원명1</span>
											<span class="interruption">중단원명1</span>
											<span class="subsection">소단원명1</span>
										</li>
										<li onclick="curriculum_select(this);">
											<span class="course">교육과정명</span>
											<span class="unit">단원명2</span>
											<span class="interruption">중단원명2</span>
											<span class="subsection">소단원명2</span>
										</li>
									</ul>
								</div>
							</div>
							<div class="search-list">
								<ul>
									<li>
										<select class="wd-100" name="course">
											<option value="" disabled selected>교육과정명</option>
											<option value="교육과정명">교육과정명</option>
										</select>
									</li>
									<li>
										<select class="wd-100" name="unit">
											<option value="">단원명</option>
											<option value="단원명1">단원명1</option>
											<option value="단원명2">단원명2</option>
										</select>
									</li>
									<li>
										<select class="wd-100" name="interruption">
											<option value="" disabled selected>중단원명</option>
											<option value="중단원명1">중단원명1</option>
											<option value="중단원명2">중단원명2</option>
										</select>
									</li>
									<li>
										<select class="wd-100" name="subsection">
											<option value="" disabled selected>소단원명</option>
											<option value="소단원명1">소단원명1</option>
											<option value="소단원명2">소단원명2</option>
										</select>
									</li>
								</ul>
							</div>
							<button class="btn-white plus shadow" onclick='curriculum_add_pop();'>+ 추가하기</button>
						</div>
					</div>
					<div class="card-sec">
                        <h3>KAMC학습성과</h3>
                        <div class="search learining-sh">
                            <input type="text" name="search" class="search-input" placeholder="1click search" onkeyup="search_keyword3(this);">
                            <div class="search-sub">
                                <ul>
                                    <li onclick="learining_result_select(this);">
                                        <span class="clinical_expression">가</span>
                                        <span class="learning_outcomes">나</span>
                                        <span class="learning_objective">다</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="search-list">
                            <ul>
                                <li>
                                    <select class="wd-100" name="clinical_expression">
                                        <option value="" disabled selected>임상표현 선택</option>
                                        <option value="가">가</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="learning_outcomes">
                                        <option value="" disabled selected>KAMC학습성과 선택</option>
                                        <option value="나">나</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="learning_objective">
                                        <option value="" disabled selected>KAMC학습성과목표 선택</option>
                                        <option value="다">다</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                        <button class="btn-white plus shadow" onclick="learining_result_add_pop()">+ 추가하기</button>
					</div>


					<div class="card-sec">
                        <h3>지식수준</h3>
						<div class="content">
							<ul class="click-btn">
								<li class="on" onclick="knowledge_level_select_pop(this);">해당 사항 없음</li>
								<li onclick="knowledge_level_select_pop(this);">암기</li>
								<li onclick="knowledge_level_select_pop(this);">해석(판단)</li>
								<li onclick="knowledge_level_select_pop(this);">해결</li>
							</ul>
                        </div>
					</div>
					<div class="card-sec">
                        <h3>예상 난이도</h3>
						<div class="content">
							<div class="level-stick flex flex-wrap" id="level-bar">
								<div>
									<span class="one">
										<span onclick="circle_left_pop(this);"></span>
									</span>
								</div>
								<div>
									<span class="two">
										<span onclick="circle_left_pop(this);"></span>
									</span>
								</div>
								<div>
									<span class="three">
										<span onclick="circle_left_pop(this);"></span>
									</span>
								</div>
								<div>
									<span class="four">
										<span onclick="circle_left_pop(this);"></span>
										<span onclick="circle_left_pop(this,1);"></span>
									</span>
								</div>
								<div class="circle" draggable="true" id="circle"></div>
							</div>
							<div class="level-stick-label flex flex-wrap">
								<div class="one"><span onclick="circle_left_pop(this);">매우 쉬움(1)</span></div>
								<div class="two"><span onclick="circle_left_pop(this);">쉬움(2)</span></div>
								<div class="three"><span onclick="circle_left_pop(this);">보통(3)</span></div>
								<div class="four"><span onclick="circle_left_pop(this);">어려움(4)</span></div>
								<div class="five"><span onclick="circle_left_pop(this);">매우 어려움(5)</span></div>
							</div>
						</div>
					</div>
					<div class="card-sec">
                        <h3>문항의 적절성</h3>
						<div class="content">
							<ul class="click-btn">
								<li class="on" onclick="question_relevance_select_pop(this);">해당사항없음</li>
								<li onclick="question_relevance_select_pop(this);">필수적인(essential)</li>
								<li onclick="question_relevance_select_pop(this);">중요한(important)</li>
							</ul>
						</div>
					</div>
					<div class="card-sec">
						<h3>임상술기</h3>
						<div class="cont-type3">
							<div class="search-type2">
								<div class="flex flex-space-between">
									<input type="text" name="clinical_technique" class="search-input mb-30" placeholder="Search"  onclick="popup2('./question_card/form/clinical_technique_search.jsp','clinical_technique_search','1350','900');">
									<button  onclick="popup2('./question_card/form/clinical_technique_search.jsp','clinical_technique_search','1350','900');"  class="bth-search">검색</button>
								</div>                           
							</div>
							<button class="btn-white plus shadow" onclick="clinical_technique_add_pop()">+ 추가하기</button>
						</div>
					</div>
					<div class="card-sec">
						<h3>임상표현명</h3>
						<div class="cont-type3">
							<div class="search-type2">
								<div class="flex flex-space-between">
									<input type="text" name="clinical_expression" class="search-input mb-30" placeholder="Search"  onclick="popup2('./question_card/form/clinical_expression_search.jsp','clinical_expression_search','1350','900');">
									<button  onclick="popup2('./question_card/form/clinical_expression_search.jsp','clinical_expression_search','1350','900');" class="bth-search">검색</button>
								</div>                           
							</div>
							<button class="btn-white plus shadow" onclick="clinical_expression_add_pop()">+ 추가하기</button>
						</div>
					</div>
					<div class="card-sec">
						<h3>진단명</h3>
						<div class="cont-type3">
							<div class="search-type2">
								<div class="flex flex-space-between">
									<input type="text" name="diagnosis_name" class="search-input mb-30" placeholder="Search"  onclick="popup2('./question_card/form/diagnosis_name_search.jsp','diagnosis_name_search','1350','900');">
									<button onclick="popup2('./question_card/form/diagnosis_name_search.jsp','diagnosis_name_search','1350','900');"  class="bth-search">검색</button>
								</div>                           
							</div>
							<button class="btn-white plus shadow" onclick="diagnosis_name_add_pop()">+ 추가하기</button>
						</div>
					</div>
					<div class="card-sec">
						<h3>보건용어</h3>
						<div class="cont-type3">
							<div class="search-type2">
								<div class="flex flex-space-between">
									<input type="text" name="health_care_term" class="search-input mb-30" placeholder="Search"  onclick="popup2('./question_card/form/health_care_term_search.jsp','health_care_term_search','1350','900');">
									<button onclick="popup2('./question_card/form/health_care_term_search.jsp','health_care_term_search','1350','900');"  class="bth-search">검색</button>
								</div>                           
							</div>
							<button class="btn-white plus shadow" onclick="health_care_term_add_pop()">+ 추가하기</button>
						</div>
					</div>
					<div class="card-sec">
						<h3>참고문헌</h3>
						<div class="cont-type3">
							<div class="search-type2">
								<div class="flex flex-space-between">
									<input type="text" name="reference" class="search-input mb-30" placeholder="Search"  onclick="popup2('./question_card/form/reference_search.jsp','reference_search','1350','900');">
									<button onclick="popup2('./question_card/form/reference_search.jsp','reference_search','1350','900');"  class="bth-search">검색</button>
								</div>                           
							</div>
							<button class="btn-white plus shadow" onclick="reference_add_pop()">+ 추가하기</button>
						</div>
					</div>
					<div class="card-sec">
						<h3>수술 및 기타 수기</h3>
						<div class="cont-type3">
							<div class="search-type2">
								<div class="flex flex-space-between">
									<input type="text" name="etc" class="search-input mb-30" placeholder="Search"  onclick="popup2('./question_card/form/etc_search.jsp','etc_search','1350','900');">
									<button onclick="popup2('./question_card/form/etc_search.jsp','etc_search','1350','900');"  class="bth-search">검색</button>
								</div>                           
							</div>
							<button class="btn-white plus shadow" onclick="etc_add_pop()">+ 추가하기</button>
						</div>
					</div>
					<div class="card-sec">

						<h3>주제어(자유 키워드)</h3>
						<div class="cont-type3">
							<div class="search">
								<input type="text" name="subject" class="input" placeholder="내용을 입력해주세요.">
							</div>
							<button class="btn-white plus shadow" onclick="subject_add_pop();">+ 추가하기</button>
						</div>
					</div>
					<div class="card-sec">

						<h3>개인별 문항카드</h3>
						<div class="cont-type3">
							<div class="search-list">
								<ul>
									<li>
										<select class="select2 wd-100" name="main_category" data-placeholder="대분류">
											<option value=""></option>
											<option value="대분류">대분류</option>
										</select>
									</li>
									<li>
										<select class="select2 wd-100" name="sub_category" data-placeholder="중분류">
											<option value=""></option>
											<option value="중분류">중분류</option>
										</select>
									</li>
								</ul>
							</div>
							<button class="btn-white plus shadow" onclick='Individual_question_card_add_pop();'>+ 추가하기</button>
						</div>
					</div>

					<form name="" method="" action="">
						<input type="hidden" name="expected_difficulty">
					</form>
					
				</div>
			</article>

			<aside>
				<div class="btn-wrap ">
					<button class="btn-red-line shadow" onclick="btn_all_change();">문항카드 변경 적용</button> 
					<button class="btn-white shadow" onclick="btn_all_del();">문항카드 모두 삭제</button> 
				</div>
				<div class="selected">
					<strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
					<ul class="selected-list">
						<li>#[시험과목] 연도 > 학급 > 학년 > 학기 > 대분류 > 중분류 > 소분류<i class="close"></i></li>
					</ul>
				</div>
	
			</aside>
		</div>
	</div>
	
	<script src="/ftp/kucm/xredu/js/expected_difficulty2.js"></script>
	

<%@ page contentType = "text/html;charset=utf-8" %>
<link rel="stylesheet" href="/ftp/kucm/xredu/css/question_registration/question_registration.css">
<link rel="stylesheet" href="/ftp/kucm/xredu/css/question_registration/each/preview.css">
<link rel="stylesheet" href="/ftp/kucm/xredu/css/question_registration/each/preview/preview.css">

<section>
    <button class="btn-close" onclick="preview_close();"><i class="close-red"></i></button>
    <div class="wrap bg-white radius-5">
        <div class="flex content-wrap flex-wrap">
            <div class="left">
                <div class="profile">
                    <div class="name">
                        <div>123456788</div>
                        <div>Hong-gildong</div>
                    </div>
                    <div class="test-info">
                        <div class="test-question">
                            <div class="total">Total Question <span>100</span> | <span>50</span></div>
                            <div class="time">00:50:00</div>
                        </div>
                        <div class="test-type">
                            <div class="type"><span>A type</span>TEST Name</div>
                            <div class="icon">
                                <i class="speaker"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="question">
                    <div class="title">01</div>
                    <div class="content">
                        <p>다음 도형에 변 ㄱㄴ 과 변 ㄱㄷ 은 길이가 같습니다. 안에 들어갈 각도로 옳은 것은 어떤 것입니까?</p>
			<!--                                 <p class="content"><img src="${IMG}/icons/preview/example-image.png"></p> -->
											<p class="content"><img src="${IMG}/etc/img-sample.jpg" width="274"></p>
											<p class="refer"><i class="capture"></i>이미지를 Touch하시면 크게 보실 수 있습니다.</p>
                    </div>
                </div>

                <div class="answer">
                    <ul>
                        <li><span>1</span>130º</li>
                        <li><span>2</span>132º</li>
                        <li class="on"><span>3</span>134º</li>
                        <li><span>4</span>131º</li>
                    </ul>
                </div>

                <div class="incoreect_note">
                    <div class="title">오답노트</div>
                    <div></div>
                </div>
            </div>

            <div class="right">
                <div class="default">
                    <strong>기본정보<button onclick="default_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li>그룹 ID : g000001</li>
                                <li>문제 ID : qa000001</li>
                                <li>출제자 :홍길동(20180000)</li>
                                <li>등록일 : 2020-02-10</li>
                            </ul>
                </div>

                <div class="essential">
                    <strong>필수정보<button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li>[시험유형] 필기시험</li>
                                <li>[문제유형] 객관식(단일정답형)<i class="close"></i></li>
<!--                                 <li>[문항공개범위] 내가 속한 대학 내 전체 공유<i class="close"></i></li> -->
                                <li>[외부공유] 해당사항 없음<i class="close"></i></li>
								<li>[멀티미디어 자료 등록] 해당사항 없음<i class="close"></i></li>
<!--                                 <li>[편집권한] 타 출제자 편집 미허용(사용만가능)<i class="close"></i></li> -->
<!--                                 <li>[마켓 공유 시 가격] 해당사항 없음<i class="close"></i></li> -->
                            </ul>
                </div>

                <div class="selected-list">
                    <strong>문항카드</strong>
                    <div class="flex">
                                <ul>
									<li># [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가</li>
									<li># [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈</li>
									<li># [성취기준] 수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.</li>									
                                </ul>
                                <ul>
									<li># [지식수준] 해석(판단)</li>
									<li># [예상난이도] 어려움(4)</li>
									<li># [문항의 적절성] 필수적인(essential)</li>
									<li># [주제어(자유 키워드)] 평가(임시)</li>
                                </ul>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
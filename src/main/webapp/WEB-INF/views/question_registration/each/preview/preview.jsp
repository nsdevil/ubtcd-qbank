<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<link rel="stylesheet" href="${CSS}/common/header.css">
	<link rel="stylesheet" href="${CSS}/common/footer.css">
	<link rel="stylesheet" href="${CSS}/question_registration/question_registration.css">
	<link rel="stylesheet" href="${CSS}/question_registration/each/preview/preview.css">
	<link rel="stylesheet" href="${CSS}/question_registration/each/preview.css">
	<link rel="stylesheet" href="${CSS}/questionbank/vue-plyr.css">
	<link rel="stylesheet" href="${JS}/vue/mathlive/dist/mathlive.core.css">
	<link rel="stylesheet" href="${JS}/vue/mathlive/dist/mathlive.css">
	<script src="${JS}/preview.js"></script>
<%--	<script src="${JS}/vue/vueplayer/vue-plyr.mjs"></script>--%>
	<script src="${JS}/vue-viaudio.umd.js"></script>
	<script src="${JS}/vue/vue-router.js"></script>
	<style>
		.selectedAnswer{
			background-color : #0084ff;
			color : #fff;
			width: 38px;
			height: 38px;
			padding: 5px;
			justify-content: center;
			align-items: center;
			text-align: center;
			margin-right: 15px;
			font-size: 18px;
			border : 1px solid #e3e7ea;
			border-radius: 50%;
		}
		.answerFlag{
			width: 38px;
			height: 38px;
			padding: 5px;
			background-color : #fff;
			color: #e2e6e9;
			justify-content: center;
			align-items: center;
			text-align: center;
			margin-right: 15px;
			font-size: 18px;
			border : 1px solid #e3e7ea;
			border-radius: 50%;
		}
		.flex{
			display: flex;
		}
		.center{
			text-align: center;
		}
		#se-pre-con {
			position: fixed;
			-webkit-transition: opacity 5s ease-in-out;
			-moz-transition: opacity 5s ease-in-out;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
		}
	</style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="questionPreview">
	<div class="wrap bg-white pd-50 radius-5 shadow-wrap">
		<div class="breadcrumb">
			<span><a href=""><spring:message code="all.home"/></a></span>
			<span><a href=""><spring:message code="all.questionSet"/></a></span>
		</div>

		<nav>
			<ul>
<%--				<li>필수정보</li>--%>
<%--				<li>문항카드</li>--%>
<%--				<li>문항제작</li>--%>
				<li class="on"><spring:message code="all.questionSet"/></li>
			</ul>
<%--			<div class="group_breadcrumb">--%>
<%--				그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0--%>
<%--			</div>--%>
		</nav>

		<div class="content-wrap flex-wrap">
			<div class="left">
<%--				<div class="profile">--%>
<%--					<div class="name">--%>
<%--						<div>123456788</div>--%>
<%--						<div>Hong-gildong</div>--%>
<%--					</div>--%>
<%--					<div class="test-info">--%>
<%--						<div class="test-question">--%>
<%--							<div class="total">Total Question <span>100</span> | <span>50</span></div>--%>
<%--							<div class="time">00:50:00</div>--%>
<%--						</div>--%>
<%--						<div class="test-type">--%>
<%--							<div class="type"><span>A type</span>TEST Name</div>--%>
<%--							<div class="icon">--%>
<%--								<button><i class="setting"></i></button>--%>
<%--								<button><i class="speaker"></i></button>--%>
<%--								<button><i class="help"></i></button>--%>
<%--							</div>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--				</div>--%>
				<div class="question">
					<div class="title" style=" border: 1px solid #ccc; margin-bottom:
                                       10px; border-radius: 25px; padding: 5px; text-align: center; width: 57px;
                                       position: absolute;">{{questionDetail.id}}
					</div>
					<div class="content">
						<div class="incoreect_note"
							 style="margin-bottom: 15px !important; margin-left:60px; height: 100%;"
							 v-if="questionDetail.headerText != ''">
							<div class="title">
								<spring:message code="preview.headerText"/>
							</div>
							<p style="padding: 15px; margin-bottom: 0px;"
							   v-html="questionDetail.headerText">
							</p>
						</div>
						<p v-else></p>
					</div>
					<div class="content">
						<p v-html="questionDetail.question" style=" margin-left: 100px;
                                   margin-bottom: 0px !important; font-size: 16px;"></p>
						<%--                                   <!--                                 <p class="content"><img src="${IMG}/icons/preview/example-image.png"></p> -->--%>
						<div class="content" v-if="questionDetail.question_datas.length > 0">
							<div v-for="(qData,qIndex) in questionDetail.question_datas" :key="qIndex">
								<div v-if="qData.dataType == 'text'">
									<div class="incoreect_note" style="margin-bottom: 15px !important;">
										<div class="title">
											<spring:message code="preview.pDataText"/>
										</div>
										<p v-html="qData.data_text" style="padding: 15px; margin-bottom: 0px;"></p>
									</div>
								</div>
								<img :src="'${BASEHOST}/uploadingDir/question/' + qData.media.filename "
									 width="100%"
									 v-if="qData.dataType == 'image'"/>
								<div v-else-if="qData.dataType== 'audio'" class="center">
									<Media
											:kind="'audio'"
											:src="'${BASEHOST}/uploadingDir/question/' + qData.media.fileName"
											:controls="true"
											:ref="'video_player'"
											:muted="muted"
											style="width: 700px; outline: none">
									</Media>
								</div>
								<div v-else-if="qData.dataType== 'video'" class="center">
									<Media
											:kind="'video'"
											:src="'${BASEHOST}/uploadingDir/question/' + qData.media.fileName"
											:controls="true"
											:ref="'video_player'"
											:muted="muted"
											style="width: 700px; outline: none">
									</Media>
								</div>
								<div class="froala" v-if="qData.dataType == 'math'">
									<mathlive-mathfield
											:id="'mf'+qIndex"
											:ref="'mathfield'+qIndex"
											:config="{smartMode:true}"
											@focus="ping"
											:on-keystroke="displayKeystroke"
											v-model="qData.data_text"
									>{{qData.data_text}}
									</mathlive-mathfield>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="answer">
					<ul>
						<li v-for="(answer,aIndex) in questionDetail.question_answers" :key="aIndex"
							style="height: 100% !important;  min-height: 80px;">
							<template v-if="questionDetail.question_type == '1'" :key="aIndex">
								<div v-if="questionDetail.answerType == 'image'">
									<div class="flex">
										<span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
										<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
										</span>
										<img :src="'${BASEHOST}/uploadingDir/answer/'+answer.media.filename"
											 :alt="answer.media.filename"/>
									</div>
								</div>
								<div v-else-if="questionDetail.answerType == 'audio'" class="flex">
									<span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
									<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
									</span>
									<Media
											:kind="'audio'"
											:src="'${BASEHOST}/uploadingDir/answer/' + answer.media.fileName"
											:controls="true"
											:ref="'video_player'"
											:muted="muted"
											style="width: 700px; outline: none">
									</Media>
								</div>
								<div v-else-if="questionDetail.answerType == 'video'" class="flex">
									<span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
									<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
									</span>
									<Media
											:kind="'video'"
											:src="'${BASEHOST}/uploadingDir/answer/' + answer.media.fileName"
											:controls="true"
											:ref="'video_player'"
											:muted="muted"
											style="width: 700px; outline: none">
									</Media>
								</div>
								<div v-else-if="questionDetail.answerType == 'text'">
									<div class="flex">
										<span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
										<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
										</span>
										<p v-html="answer.answer" style="padding-top: 13px;"></p>
									</div>
								</div>
								<div v-else-if="questionDetail.answerType == 'math'">
									<div class="flex">
                                         <span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
										<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
										</span>
										<div class="froala">
											<mathlive-mathfield
													:id="'mf'+aIndex"
													:ref="'mathfield'+aIndex"
													:config="{smartMode:true, virtualKeyboardMode:'manual'}"
													@focus="ping"
													:on-keystroke="displayKeystroke"
													v-model="answer.answer"
											>{{answer.answer}}
											</mathlive-mathfield>
										</div>
									</div>
								</div>
							</template>
							<template v-if="questionDetail.question_type == '3'" :key="aIndex">
								<div v-if="questionDetail.answerType == 'image'">
									<div  class="flex">
									  	<span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
										<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
										</span>
										<img :src="'${BASEHOST}/uploadingDir/answer/'+answer.media.filename"
											 :alt="answer.media.filename"/>
									</div>
									<div v-else class="flex">
                                       <span class="answerFlag" :class="{'selectedAnswer': answer.answerFlag}">
											{{answer.optionNumber}}</span>
										<img :src="'${BASEHOST}/webapps/uploadingDir/answer/'+answer.media.filename"
											 :alt="answer.media.filename"/>
									</div>
								</div>
								<div v-else-if="questionDetail.answerType == 'audio'" class="flex">
									<span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
									<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
									</span>
									<Media
											:kind="'audio'"
											:src="'${BASEHOST}/uploadingDir/answer/' + answer.media.fileName"
											:controls="true"
											:ref="'video_player'"
											:muted="muted"
											style="width: 700px; outline: none">
									</Media>
								</div>
								<div v-else-if="questionDetail.answerType == 'video'" class="flex">
									<span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
									<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
										</span>
									<Media
											:kind="'video'"
											:src="'${BASEHOST}/uploadingDir/answer/' + answer.media.fileName"
											:controls="true"
											:ref="'video_player'"
											:muted="muted"
											style="width: 700px; outline: none">
									</Media>
								</div>
								<div v-else-if="questionDetail.answerType == 'text'">
									<div  class="flex">
										 <span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
										<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
										</span>
									<p v-html="answer.answer" style="padding-top: 13px;"></p>
									</div>
								</div>
								<div v-else-if="questionDetail.answerType == 'math'">
									<div class="flex">
									   <span v-if="answer.answerFlag == 'false'" class="answerFlag">
											{{answer.optionNumber}}</span>
										<span v-else class="selectedAnswer">
											{{answer.optionNumber}}
										</span>
										<div class="froala">
											<mathlive-mathfield
													:id="'mf'+aIndex"
													:ref="'mathfield'+aIndex"
													:config="{smartMode:true, virtualKeyboardMode:'manual'}"
													@focus="ping"
													:on-keystroke="displayKeystroke"
													v-model="answer.answer"
											>{{answer.answer}}
											</mathlive-mathfield>
										</div>
									</div>
								</div>
							</template>
							<template v-if="questionDetail.question_type == '2'" :key="aIndex">
								<div style="padding-top: 20px;">
									<p style="font-size: 16px; display: flex; color: #fb5f33;">
										<spring:message code="preview.trueAnswer"/> :</p>
									<div class="froala" v-if="answer.answerFlag == 'math'">
										<mathlive-mathfield
												:id="'mf'+aIndex"
												:ref="'mathfield'+aIndex"
												:config="{smartMode:true, virtualKeyboardMode:'manual'}"
												@focus="ping"
												:on-keystroke="displayKeystroke"
												v-model="answer.trueData"
										>{{answer.trueData}}
										</mathlive-mathfield>
									</div>

									<p v-else style="margin-left: 10px;  font-size: 16px;"
									   v-html="answer.trueData"></p>

								</div>
								<div style="padding-top: 20px;">
									<p style="font-size: 16px; display: flex; color: #fb5f33;">
										<spring:message code="preview.hintText"/> :</p>
									<div class="froala" v-if="answer.answerFlag == 'math'">
										<mathlive-mathfield
												:id="'mf'+aIndex"
												:ref="'mathfield'+aIndex"
												:config="{smartMode:true, virtualKeyboardMode:'manual'}"
												@focus="ping"
												:on-keystroke="displayKeystroke"
												v-model="answer.answer"
										>{{answer.answer}}
										</mathlive-mathfield>
									</div>
									<p v-else style="margin-left: 10px;  font-size: 16px;"
									   v-html="answer.answer"></p>
								</div>
							</template>
							<template v-if="questionDetail.question_type == '4'" :key="aIndex">
								<div style=" display: block !important;">
									<p><spring:message code="preview.drawingTrue"/> : </p>
									<img
											:src="'${BASEHOST}/uploadingDir/answer/' + answer.trueData">
									<p>
										<spring:message code="preview.drawingPic"/>: </p>
									<img style=" padding-top: 30px; padding-bottom: 30px; "
										 :src="'${BASEHOST}/uploadingDir/answer/' + answer.drawingData">
								</div>
							</template>
						</li>
					</ul>
				</div>

				<div class="incoreect_note" v-if="questionDetail.question_wrongs.length > 0">
					<div class="title"><spring:message code="all.wrongAnswer"/></div>
					<template v-for="(wrong,wIndex) in questionDetail.question_wrongs">
						<div v-if="wrong.dataType == 'text'">
							<p v-html="wrong.data_text" style="padding: 5px;"></p>
						</div>
						<div v-if="wrong.dataType == 'math'" class="froala">
							<mathlive-mathfield
									:id="'mf'+wIndex"
									:ref="'mathfield'+wIndex"
									:config="{smartMode:true, virtualKeyboardMode:'manual'}"
									@focus="ping"
									:on-keystroke="displayKeystroke"
									v-model="wrong.data_text"
							>{{wrong.data_text}}
							</mathlive-mathfield>
						</div>
						<div v-if="wrong.dataType == 'image'">
							<img style=" padding-top: 30px; padding-bottom: 30px; "
								 :src="'${BASEHOST}/uploadingDir/wrong/' + wrong.media.filename">
						</div>
						<div v-else-if="wrong.answerType == 'audio'">
							<Media
									:kind="'audio'"
									:src="'${BASEHOST}/uploadingDir/wrong/' + wrong.media.fileName"
									:controls="true"
									:ref="'video_player'"
									:muted="muted"
									style="width: 700px; outline: none">
							</Media>
						</div>
						<div v-else-if="wrong.answerType == 'video'">
							<Media
									:kind="'video'"
									:src="'${BASEHOST}/uploadingDir/wrong/' + wrong.media.fileName"
									:controls="true"
									:ref="'video_player'"
									:muted="muted"
									style="width: 700px; outline: none">
							</Media>
						</div>
					</template>
				</div>
				</div>

				<div class="btn-wrap flex" style="float: right;">
					<button class="btn-white shadow" @click="backQuestion()" style="width: 150px; height: 45px; margin-right: 15px;"
							v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN'
							|| '${sessionScope.S_LOGINID}' == userId">
						<i class="prev-gray" style="margin-right: 15px;"></i>
						<spring:message code="preview.fix"/>
					</button>
					<%--                            <button class="btn-white shadow" @click="prevQuestion" ><i class="prev-gray"></i>이전문제--%>
					<%--                            </button>--%>
					<%--                            <button class="btn-white shadow"--%>
					<%--                                    @click="nextQuestion"><i class="">다음문제</i></button>--%>
					<a class="btn-red shadow" href="${HOME}/question_card/questionbank" style="width: 150px;  height: 45px;">
						<i class="check-white" style="margin-right: 15px;"></i>
						<spring:message code="preview.qBank"/></a>
				</div>
			</div>

<%--			<div class="right">--%>
<%--				<div class="default">--%>
<%--					<strong>기본정보--%>
<%--						<button onclick="default_show(this);"><i class="plus-bg-gray"></i></button>--%>
<%--					</strong>--%>
<%--					<ul>--%>
<%--						<li>그룹 ID : g000001</li>--%>
<%--						<li>문제 ID : qa000001</li>--%>
<%--						<li>출제자 :홍길동(20180000)</li>--%>
<%--						<li>등록일 : 2020-02-10</li>--%>
<%--					</ul>--%>
<%--				</div>--%>
<%--				--%>
<%--				<div class="essential">--%>
<%--					<strong>필수정보--%>
<%--						<button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button>--%>
<%--					</strong>--%>
<%--					<ul>--%>
<%--						<li>[시험유형] 필기시험</li>--%>
<%--						<li>[문제유형] 객관식(단일정답형)<i class="close"></i></li>--%>
<%--						<!--                                 <li>[문항공개범위] 내가 속한 대학 내 전체 공유<i class="close"></i></li> -->--%>
<%--						<li>[외부공유] 해당사항 없음<i class="close"></i></li>--%>
<%--						<li>[멀티미디어 자료 등록] 해당사항 없음<i class="close"></i></li>--%>
<%--						<!--                                 <li>[편집권한] 타 출제자 편집 미허용(사용만가능)<i class="close"></i></li> -->--%>
<%--						<!--                                 <li>[마켓 공유 시 가격] 해당사항 없음<i class="close"></i></li> -->--%>
<%--					</ul>--%>
<%--				</div>--%>
<%--				--%>
<%--				<div class="selected-list">--%>
<%--					<strong>문항카드</strong>--%>
<%--					<div>--%>
<%--						<ul class="flex flex-wrap flex-space-between">--%>
<%--							<li># [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가<i--%>
<%--									class="close"></i></li>--%>
<%--							<li># [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈 <i class="close"></i></li>--%>
<%--							<li># [성취기준] 수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.<i--%>
<%--									class="close"></i></li>--%>
<%--							<li># [지식수준] 해석(판단)<i class="close"></i></li>--%>
<%--							<li># [예상난이도] 어려움(4)<i class="close"></i></li>--%>
<%--							<li># [문항의 적절성] 필수적인(essential)<i class="close"></i></li>--%>
<%--							<li># [주제어(자유 키워드)] 평가(임시)<i class="close"></i></li>--%>
<%--						</ul>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				--%>
<%--				<div class="proposer">--%>
<%--					<ul>--%>
<%--						<li class="on"><a href="../../../question_registration/each/preview/preview.jsp">추천 문항카드</a>--%>
<%--						</li>--%>
<%--						<!--                                 <li><a href="../../../question_registration/each/preview/exam_history.jsp">시험이력</a></li> -->--%>
<%--						<li><a href="../../../question_registration/each/preview/change_history.jsp">변경이력</a></li>--%>
<%--						<!--                                 <li><a href="../../../question_registration/each/preview/share_history.jsp">공유이력</a></li> -->--%>
<%--					</ul>--%>
<%--					<div class="content">--%>
<%--						<div>--%>
<%--							<div class="image"><img src="${IMG}/icons/preview/robot.png"></div>--%>
<%--							<div class="check">--%>
<%--								<p>--%>
<%--									선생님께서 작성해주신 문항카드 정보와 문항내용을 토대로 Q-BOT이 아래와 같이 추천드립니다.<br>--%>
<%--									마음에 드시는 문항카드가 있다면 선택 후 담기 버튼을 클릭하시고,<br>--%>
<%--									더 다양한 추천을 원하시면 다른 추천 버튼을 눌러주세요.--%>
<%--								</p>--%>
<%--							</div>--%>
<%--							<div class="check previewChk">--%>
<%--								<ol>--%>
<%--									<li>--%>
<%--										<input type="checkbox" class="checkbox" name="proposer1" id="proposer1"--%>
<%--											   value="proposer1">--%>
<%--										<label for="proposer1">[시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > C조 (조인성, 차인표,--%>
<%--											이서진) > 수시평가</label>--%>
<%--									</li>--%>
<%--									<li>--%>
<%--										<input type="checkbox" class="checkbox" name="proposer2" id="proposer2"--%>
<%--											   value="proposer2">--%>
<%--										<label for="proposer2"> [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈</label>--%>
<%--									</li>--%>
<%--									<li>--%>
<%--										<input type="checkbox" class="checkbox" name="proposer3" id="proposer3"--%>
<%--											   value="proposer3">--%>
<%--										<label for="proposer3">[지식수준] 암기</label>--%>
<%--									</li>--%>
<%--									<li>--%>
<%--										<input type="checkbox" class="checkbox" name="proposer4" id="proposer4"--%>
<%--											   value="proposer4">--%>
<%--										<label for="proposer4">[성취기준] 수와 연산 > 나눗셈 > [4수01-09]나누는 수가 두 자리 수인 나눗셈의 계산 원리를--%>
<%--											이해하고 그 계산을 할 수 있다.</label>--%>
<%--									</li>--%>
<%--								</ol>--%>
<%--							</div>--%>
<%--						</div>--%>
<%--						<div class="btn-wrap">--%>
<%--							<button class="btn-white">다른 추천<i class="next"></i></button>--%>
<%--							<button class="btn-white" onclick="btn_add_preview();">담기<i class="next"></i></button>--%>
<%--						</div>--%>
<%--					--%>
<%--					</div>--%>
<%--				</div>--%>
<%--			</div>--%>
		</div>

	</div>
</section>
<script type="module">
    import MathLive from '${JS}/vue/mathlive/dist/src/mathlive.js';
    import Mathfield from '${JS}/vue/mathlive/dist/vue-mathlive.js';

    <%--import VuePlyr from '${JS}/vue/vueplayer/vue-plyr.js';--%>
    Vue.use(Mathfield, MathLive);
    // Vue.use(VuePlyr);

    var router = new VueRouter({
        mode: 'history',
        routes: []
    });

    var app = new Vue({
        router,
        el: '#questionPreview',
        components: {},
        data() {
            return {
                formula: 'x=-b\\pm \\frac {\\sqrt{b^2-4ac}}{2a}',
                keystroke: '',
                questionId: '',
                questionDetail: [],
				muted: false,
				loginId: '',
				userId: ''
            }
        },
        mounted() {
            let parameters = this.$route.query
            console.log(parameters)

            if (typeof this.$route.query.id != 'undefined') {
                let id = this.$route.query.id;
                // this.questionId =
                this.quesId(id)
            } else {
                alert("Question id undefined")
            }
        },
        created() {
            // this.firstQuestion()
        },
        methods: {
            backQuestion() {
                window.location.href = "${HOME}/question_registration/each/essential?id=" +
						this.questionDetail.id + '&loginId=' + this.questionDetail.userId;
            },
            sayIt: function (event) {
                this.$refs['mathfield'].$el.mathfield.$perform([
                    'speak',
                    'all',
                ]);
            },
            setIt: function (event) {
                this.formula = 'x=-b\\pm \\frac {\\sqrt{b^2-4ac}}{2a}';
            },
            ping: function () {
                console.log('ping');
            },
            displayKeystroke: function (keystroke, _ev) {
                this.keystroke = keystroke;
                return true;
            },
            asSpokenText: function () {
                return (
                    (this.$refs['mathfield'] &&
                        this.$refs['mathfield'].$el.mathfield &&
                        this.$refs['mathfield'].$el.mathfield.$text(
                            'spoken'
                        )) ||
                    ''
                );
            },
			loading(cmd) {
				var l = document.getElementById("se-pre-con");
				l.style.display = cmd;
			},
            quesId(item) {
                var _this = this;
				_this.loading('block')
				this.loginId = '${sessionScope.S_LOGINID}'
				var language =  "<%=pageContext.getResponse().getLocale()%>"
				var locale = ''
				if (language == 'kr')
				{
					locale = 'kr-KR'
				}
				if(language == 'mn')
				{
					locale = 'mn-MN'
				}
				if(language == 'en'){
					locale = 'en-EN'
				}
				const headers = {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
					'Accept-Language' : locale
				}

                axios.get('${BASEURL}/questions/' + item, {
                    params: {
                    },
                    headers: headers,
                }).then(function (response) {
					_this.loading('none')
                    if (response.data.status == 200) {
                        _this.questionDetail = response.data.result.questionDetail
						_this.userId = response.data.result.questionDetail.userId
                    }
                }, function (error) {
                    console.log("axios error found")
                    console.log(error);
					this.loading('block')
                });
            }
        }
    })
    // app.$mount('#questionPreview')
</script>
</body>
</html>

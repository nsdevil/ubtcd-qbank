<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <title></title>
    <link rel="stylesheet" href="${CSS}/question_registration/each/preview/question_preview.css">
    <link rel="stylesheet" href="${JS}/vue/mathlive/dist/mathlive.core.css">
    <link rel="stylesheet" href="${JS}/vue/mathlive/dist/mathlive.css">
<%--    <link rel="stylesheet" href="${CSS}/photoswipe.min.css">--%>
<%--    <link rel="stylesheet" href="${CSS}/default-skin.min.css">--%>

    <script src="${JS}/jquery3.3.1.js"></script>
    <script src="${JS}/blockUI.js" type="text/javascript"></script>

    <script src="${JS}/vue/vue.js"></script>
    <script src="${JS}/vue/vue-router.js"></script>
    <script src="${JS}/vue/axios.js"></script>
    <script src="${JS}/vue-viaudio.umd.js"></script>

    <style>
        .sectionnone {
            display: none;
        }
        .sectiondisplay {
            display: block;
        }
        .tit{
            text-align: left;
        }
        .my-gallery figcaption {
            display: none;
        }
        .spliter {
            margin-top: 50px;
            margin-bottom: 50px;
            padding-top: 50px;
            padding-bottom: 50px;
        }
        #se-pre-con {
             position: fixed;
             -webkit-transition: opacity 5s ease-in-out;
             -moz-transition: opacity 5s ease-in-out;
             left: 0px;
             top: 0px;
             width: 100%;
             height: 100%;
             z-index: 9999;
             background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
         }
    </style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<div id="solving">
    <section class="solving-wrap sectionnone" id="questionPreview">
        <h1 class="title" style="margin: 0px">
            <span><spring:message code="questionId"/> : <span class="top-title">{{questions.id}}</span></span>
            <span><spring:message code="question.register.realNumber"/>  : <span class="top-title">{{questions.controlNo}}</span></span>
            <%--			<button class="btn-top" v-if="'${sessionScope.S_LOGINID}' == questions.regUserId ||--%>
            <%--                            '${sessionScope.S_ROLE}'=='ROLE_ADMIN'"> 적합 (사용)</button>--%>
            <%--			<button class="btn-top-second"> 문항상태 변경하기</button>--%>
            <button class="btn-top-close" @click="closeWindow()">
                <spring:message code="all.close"/>
            </button>
        </h1>

        <div class="solving-layout">
            <div class="explain-wrap">
                <div class="explain-area left">
                    <div class="top-area" style="overflow:visible; padding-left: 12px;">
<%--                        과목 : <span class="top-title">{{questions.subj}}</span>--%>
<%--                        학교급 : <span class="top-title">{{questions.school}} > {{questions.gradeGroupName}} >--%>
<%--							{{questions.grade}} > {{questions.term}}</span>--%>
<%--                        출제일 : <span class="top-title">{{questions.createdAt}}</span>--%>
                        <spring:message code="all.submitter" /> : <span class="top-title">{{questions.regUserId}}</span>
                        <spring:message code="all.point" /> : <span class="top-title">{{questions.point}}</span>
                    </div>
                    <div class="class-area popup-scroll ">
                        <div class="pdl60">
                            <div v-if="questions.headerText != ''" style="margin-top: 20px;">
                                <p class="" style="border: 1px solid #999; padding: 8px; margin-bottom: 20px;
								text-align: center;" v-html="questions.headerText">
                                </p>
                            </div>
                            <div class=" uk-padding-remove flex">
                                <span class="uk-padding-remove questionOptionNumber">{{questions.id}}.</span>
                                <p class="txt-type01 mgt20 uk-margin-top"
                                   style="width: 100%; margin-left: 10px; margin-top: 19px;"
                                   v-html="questions.questionData">
                                </p>
                            </div>
                            <div class="question-area mgb0 my-gallery" itemscope
                                 itemtype="http://schema.org/ImageObject" v-if="questions.presentDatas.length > 0">
                                <template v-for="(apps, mindex) in questions.presentDatas">
                                    <div v-if="apps.type == 'image'">
                                        <img :src="'${BASEHOST}/uploadingDir/question/' + apps.fileName"
                                        :alt="apps.fileName" style="max-width: 900px !important; margin-bottom: 15px;"
                                        class="uk-margin-top uk-margin-left"/>
                                    </div>
                                    <div v-else-if="apps.type== 'audio'">
                                        <Media
                                                :kind="'audio'"
                                                :src="'${BASEHOST}/uploadingDir/question/' + apps.fileName"
                                                :controls="true"
                                                :ref="'video_player'"
                                                :muted="muted"
                                                style="width: 700px; outline: none">
                                        </Media>
                                    </div>
                                    <div v-else-if="apps.type== 'video'">
                                        <Media
                                                :kind="'video'"
                                                :src="'${BASEHOST}/uploadingDir/question/' + apps.fileName"
                                                :controls="true"
                                                :ref="'video_player'"
                                                :muted="muted"
                                                style="width: 700px; outline: none">
                                        </Media>
                                    </div>
                                    <div v-else-if="apps.type == 'math'">
                                        <mathlive-mathfield
                                                :id="'mf'+mindex"
                                                :ref="'mathfield'+mindex"
                                                @focus="ping"
                                                :on-keystroke="displayKeystroke"
                                                :config=" {smartMode:true, readOnly: true}"
                                                v-model="apps.text"
                                                style="font-size: 22px; font-weight: 400; margin-bottom: 20px">
                                            <span>{{apps.text}}</span>
                                        </mathlive-mathfield>
                                    </div>

                                    <p v-else style="border: 1px solid #999; padding: 8px; margin-bottom: 20px">
                                        <span v-html="apps.text"></span>
                                    </p>
                                </template>
                            </div>
                            <div class="uk-margin-large-top" v-else>

                            </div>
                            <div class="example-list uk-margin-top" v-if="questions.questionType == '1'">
                                <div class="example-wrap" v-for="(aItem,index) in questions.answers" :key="index">
                                    <span v-if="questions.answerType == 'image' " style=" margin-top: 5px;" class="flex">
                                        <input type="button" name="numRadio" class="checkBtn uk-margin-remove"
                                               style="padding-left: 0px;"
                                               :value="aItem.optionNumber"
                                               :class="{'answerFlag': aItem.correctAnswer}"/>
                                         <img :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                              :alt="aItem.fileName" style="max-width: 900px !important;"
                                              class="uk-margin-top uk-margin-left"/>
                                    </span>
                                    <div v-else-if="questions.answerType  == 'audio'" class="flex">
                                        <input type="button" name="numRadio" class="checkBtn uk-margin-remove"
                                               style="padding-left: 0px;"
                                               :value="aItem.optionNumber"
                                               :class="{'answerFlag': aItem.correctAnswer}"/>
                                        <Media
                                                :kind="'audio'"
                                                :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                :controls="true"
                                                :ref="'video_player'"
                                                :muted="muted"
                                                style="width: 700px; outline: none">
                                        </Media>
                                    </div>
                                    <div v-else-if="questions.answerType  == 'video'" class="flex">
                                        <input type="button" name="numRadio" class="checkBtn uk-margin-remove"
                                               style="padding-left: 0px;"
                                               :value="aItem.optionNumber"
                                               :class="{'answerFlag': aItem.correctAnswer}"/>
                                        <Media
                                                :kind="'video'"
                                                :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                :controls="true"
                                                :ref="'video_player'"
                                                :muted="muted"
                                                style="width: 700px; outline: none">
                                        </Media>
                                    </div>
                                    <div class="radio-numbers" v-else style=" margin-top: 5px;" >
                                        <div style="display: inline-flex" v-if="questions.answerType == 'math'">
                                            <input type="button" name="numRadio" class="checkBtn uk-margin-remove"
                                                   style="padding-left: 0px;  margin-right: 15px !important;" disabled
                                                   :value="aItem.optionNumber"
                                                   :class="{'answerFlag': aItem.correctAnswer}"/>
                                            <mathlive-mathfield
                                                    :id="'mf'+index"
                                                    :ref="'mathfield'+index"
                                                    :config="{smartMode:true, readOnly: true}"
                                                    @focus="ping"
                                                    :on-keystroke="displayKeystroke"
                                                    v-model="aItem.text"
                                                    class="theoremFont"
                                                    style="text-align: left; margin-top: -6px;"
                                            ><span>{{aItem.text}}</span>
                                            </mathlive-mathfield>
                                        </div>
                                        <div class="flex" style="flex-wrap: unset; padding-left: 0px;" v-else>
                                            <input type="button" disabled
                                                   class="checkBtn"
                                                   :value="aItem.optionNumber"
                                                   :class="{'answerFlag': aItem.correctAnswer}"
                                            />
                                            <span class="answerMargin" name="numRadio"
                                                  style="width: 98% !important;"
                                                  :class="{'answerTrue': aItem.correctAnswer}"
                                                  v-html="aItem.text"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="example-list uk-margin-top" v-else-if="questions.questionType == '3'">
                                <div class="example-wrap" v-for="(aItem,index) in questions.answers" :key="index">
                                    <span v-if="questions.answerType == 'image' " class="flex" style=" margin-top: 5px;">
                                        <input type="button" disabled class="checkBtn uk-margin-remove"
                                               :value="aItem.optionNumber"
                                               :class="{'answerFlag': aItem.correctAnswer}"/>
                                         <img :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                              :alt="aItem.fileName" style="max-width: 900px !important;"
                                              class="uk-margin-top uk-margin-left"/>
                                    </span>
                                    <div v-else-if="questions.answerType  == 'audio'" class="flex">
                                        <input type="button" name="numRadio" class="checkBtn uk-margin-remove"
                                               style="padding-left: 0px;"
                                               :value="aItem.optionNumber"
                                               :class="{'answerFlag': aItem.correctAnswer}"/>
                                        <Media
                                                :kind="'audio'"
                                                :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                :controls="true"
                                                :ref="'video_player'"
                                                :muted="muted"
                                                style="width: 700px; outline: none">
                                        </Media>
                                    </div>
                                    <div v-else-if="questions.answerType  == 'video'" class="flex">
                                        <input type="button" name="numRadio" class="checkBtn uk-margin-remove"
                                               style="padding-left: 0px;"
                                               :value="aItem.optionNumber"
                                               :class="{'answerFlag': aItem.correctAnswer}"/>
                                        <Media
                                                :kind="'video'"
                                                :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                :controls="true"
                                                :ref="'video_player'"
                                                :muted="muted"
                                                style="width: 700px; outline: none">
                                        </Media>
                                    </div>
                                    <div class="radio-numbers" v-else style=" margin-top: 5px;">
                                        <div style="display: inline-flex" v-if="questions.answerType == 'math'">
                                            <input type="button" name="numRadio" class="checkBtn uk-margin-remove"
                                                   style="padding-left: 0px;  margin-right: 15px !important;" disabled
                                                   :value="aItem.optionNumber"
                                                   :class="{'answerFlag': aItem.correctAnswer}"/>
                                            <mathlive-mathfield
                                                    :id="'mf'+index"
                                                    :ref="'mathfield'+index"
                                                    :config="{smartMode:true, readOnly: true}"
                                                    @focus="ping"
                                                    :on-keystroke="displayKeystroke"
                                                    v-model="aItem.text"
                                                    class="theoremFont"
                                                    style="text-align: left; margin-top: -6px;"
                                            ><span>{{aItem.text}}</span>
                                            </mathlive-mathfield>
                                        </div>
                                        <div class="flex" style="flex-wrap: unset; padding-left: 0px;" v-else>
                                            <input type="button" disabled
                                                   class="checkBtn"
                                                   :value="aItem.optionNumber"
                                                   :class="{'answerFlag': aItem.correctAnswer}"
                                            />
                                            <span class="answerMargin" name="numRadio"
                                                  style="width: 98% !important;"
                                                  :class="{'answerTrue': aItem.correctAnswer}"
                                                  v-html="aItem.text"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-else-if="questions.questionType == '4'" class="uk-margin-bottom uk-margin-top" style="display: flex">
                                <div class="uk-width-1-1 uk-margin-remove-left" uk-grid style="width: 50%; text-align: center"
                                     v-for="(aItem,index) in questions.answerCanvas" :key="index">
                                    <div class="uk-width-1-2@l uk-text-center uk-padding-remove" v-show="aItem.correntAnswer == false">
                                        <p class="btm_Tx" ><span class="tx_orange">[<spring:message code="student.drawing" />]</span></p>
                                        <img :src="'${BASEHOST}/uploadingDir/answer/'+aItem.fileName"
                                                class="uk-flex uk-flex-center" style="width:370px !important;
                                                 margin: 0 !important; display: initial;"/>
                                    </div>
                                    <div class="uk-width-1-2@l uk-text-center uk-padding-remove" v-show="aItem.correntAnswer == true">
                                        <p class="btm_Tx" ><span class="tx_blue">[Correct picture]</span></p>
                                        <img  :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                              class="uk-flex uk-flex-center" style="width:370px !important;
                                                 margin: 0 !important; display: initial;"/>
                                    </div>
                                </div>
                            </div>
                            <div v-else-if="questions.questionType == '5'" class="uk-margin-bottom uk-margin-top">
                                <div class="uk-width-1-1 uk-margin-remove-left" >
                                    <div class="uk-padding-remove" style="text-align: center;">
                                        <p class="btm_Tx">
                                            <img src="${IMG}/notice/photoUpload.png"
                                                 class="uk-flex uk-flex-center" style="width:190px !important;
                                                 margin: 0 !important; display: initial;"/>
                                        </p>
                                        <span> <spring:message code="student.upload.picture"/></span>
                                    </div>
                                </div>
                            </div>
                            <div class="mgt60 uk-margin-large-bottom uk-margin-top" v-else>
                                <div v-for="(aItem,index) in questions.shortAnswers" :key="index">
                                    <div v-if="aItem.fieldType == 'math'">
                                        <mathlive-mathfield
                                                :id="'mf'+index"
                                                :ref="'mathfield'+index"
                                                :config="{smartMode:true, readOnly: true}"
                                                @focus="ping"
                                                :on-keystroke="displayKeystroke"
                                                v-model="aItem.trueData"
                                                class="mathliveType2"
                                                style="font-size: 22px">
                                            <span>{{aItem.trueData}}</span>
                                        </mathlive-mathfield>
                                    </div>
                                    <div v-else>
                                        <span>{{aItem.trueData}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="answers-exp uk-margin-top" v-if="questions.questionType != '5'">
                                <p class=" uk-margin-small-bottom" style="color: #3ac4fc !important;
								font-size: 18px"><spring:message code="all.trueData" /></p>
                                <div>
                                    <div class="example-list" v-if="questions.questionType == '1'">
                                        <div class="example-wrap" v-for="(aItem,index) in questions.answers"
                                             :key="index"  v-if="aItem.correctAnswer == true">
                                            <span v-if="questions.answerType == 'image' ">
												 <img alt=""
                                                      :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                      :alt="aItem.fileName" style="max-width: 700px !important; height: auto; max-height: 300px; width: auto;margin-bottom: 15px;"
                                                      class="uk-margin-top uk-margin-left"/>
											</span>
                                            <div v-else-if="questions.answerType  == 'audio'" class="flex">
                                                <Media
                                                        :kind="'audio'"
                                                        :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                        :controls="true"
                                                        :ref="'video_player'"
                                                        :muted="muted"
                                                        style="width: 700px; outline: none">
                                                </Media>
                                            </div>
                                            <div v-else-if="questions.answerType  == 'video'">
                                                <Media
                                                        :kind="'video'"
                                                        :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                        :controls="true"
                                                        :ref="'video_player'"
                                                        :muted="muted"
                                                        style="width: 700px; outline: none">
                                                </Media>
                                            </div>
                                            <div class="radio-numbers" v-else style=" margin-top: 5px;">
                                                <div style="display: inline-flex" v-if="questions.answerType == 'math'">
                                                    <mathlive-mathfield
                                                            :id="'mf'+index"
                                                            :ref="'mathfield'+index"
                                                            :config="{smartMode:true, readOnly: true}"
                                                            @focus="ping"
                                                            :on-keystroke="displayKeystroke"
                                                            v-model="aItem.text"
                                                            class="theoremFont"
                                                            style="text-align: left; margin-top: -6px;"
                                                    ><span>{{aItem.text}}</span>
                                                    </mathlive-mathfield>
                                                </div>
                                                <div class="flex" style="flex-wrap: unset; padding-left: 0px;" v-else>
                                                    <span class="answerMargin" name="numRadio"
                                                          style="width: 98% !important;"
                                                          v-html="aItem.text"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="example-list" v-else-if="questions.questionType == '3'">
                                        <div class="example-wrap" v-for="(aItem,index) in questions.answers"
                                             :key="index"
                                             v-if="aItem.correctAnswer == true">
                                            <span v-if="questions.answerType == 'image' ">
												 <img alt=""
                                                      :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                      :alt="aItem.fileName" style="max-width: 700px !important; height: auto; max-height: 300px; width: auto;margin-bottom: 15px;"
                                                      class="uk-margin-top uk-margin-left"/>
											</span>
                                            <div v-else-if="questions.answerType  == 'audio'" class="flex">
                                                <Media
                                                        :kind="'audio'"
                                                        :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                        :controls="true"
                                                        :ref="'video_player'"
                                                        :muted="muted"
                                                        style="width: 700px; outline: none">
                                                </Media>
                                            </div>
                                            <div v-else-if="questions.answerType  == 'video'">
                                                <Media
                                                        :kind="'video'"
                                                        :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                        :controls="true"
                                                        :ref="'video_player'"
                                                        :muted="muted"
                                                        style="width: 700px; outline: none">
                                                </Media>
                                            </div>
                                            <div class="radio-numbers" v-else style=" margin-top: 5px;">
                                                <div style="display: inline-flex" v-if="questions.answerType == 'math'">
                                                    <mathlive-mathfield
                                                            :id="'mf'+index"
                                                            :ref="'mathfield'+index"
                                                            :config="{smartMode:true, readOnly: true}"
                                                            @focus="ping"
                                                            :on-keystroke="displayKeystroke"
                                                            v-model="aItem.text"
                                                            class="theoremFont"
                                                            style="text-align: left; margin-top: -6px;"
                                                    ><span>{{aItem.text}}</span>
                                                    </mathlive-mathfield>
                                                </div>
                                                <div class="flex" style="flex-wrap: unset; padding-left: 0px;" v-else>
                                                    <span class="answerMargin" name="numRadio"
                                                          style="width: 98% !important;"
                                                          v-html="aItem.text"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-else-if="questions.questionType == '4'" class="uk-margin-bottom">
                                        <div class="uk-width-1-1" uk-grid v-for="(aItem,index) in questions.answerCanvas"
                                             :key="index">
                                            <div class="uk-width-1-2 uk-text-center" v-show="aItem.correntAnswer == true">
                                                <img
                                                        :src="'${BASEHOST}/uploadingDir/answer/' + aItem.fileName"
                                                        class="uk-flex uk-flex-center" style="margin: 0 !important"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="answers-exp mgt60" v-else>
                                        <div v-for="(aItem,index) in questions.shortAnswers" :key="index">
                                            <p class="" style="width: 90%" v-if="aItem.fieldType == 'math'">
                                                <mathlive-mathfield
                                                        :id="'mf'+index"
                                                        :ref="'mathfield'+index"
                                                        :config="{smartMode:true,  readOnly: true}"
                                                        @focus="ping"
                                                        :on-keystroke="displayKeystroke"
                                                        v-model="aItem.trueData"
                                                        style="border: 1px solid #ccc; font-size: 24px;">
													<span class="answerMargin"
                                                          style="margin-left: 0px;">{{aItem.trueData}}</span>
                                                </mathlive-mathfield>
                                            </p>
                                            <span v-else>
                                                <span class="answerMargin"
                                                      style="margin-left: 0px;">{{aItem.trueData}}</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="txt-subject uk-margin-top" v-if="questions.wrongNotes.length > 0">
                                    <p class="txt-exp uk-margin-remove uk-padding-top"
                                       style="font-size: 18px; color: #3ac4fc !important;"><spring:message code = "all.wrongAnswer" /></p>
                                    <div v-for="(wwItem, wwIndex) in questions.wrongNotes">
                                        <div v-if="wwItem.type  == 'audio'" class="flex">
                                            <Media
                                                    :kind="'audio'"
                                                    :src="'${BASEHOST}/uploadingDir/wrong/' + wwItem.fileName"
                                                    :controls="true"
                                                    :ref="'video_player'"
                                                    :muted="muted"
                                                    style="width: 700px; outline: none">
                                            </Media>
                                        </div>
                                        <div v-if=" wwItem.type == 'video'">
                                            <Media
                                                    :kind="'video'"
                                                    :src="'${BASEHOST}/uploadingDir/wrong/' + wwItem.fileName"
                                                    :controls="true"
                                                    :ref="'video_player'"
                                                    :muted="muted"
                                                    style="width: 700px; outline: none">
                                            </Media>
                                        </div>
                                        <div v-if=" wwItem.type == 'image' ">
                                            <img :src="'${BASEHOST}/uploadingDir/wrong/' + wwItem.fileName"
                                                 :alt="wwItem.fileName" :key="wwIndex" style="width:auto"/>
                                        </div>
                                        <div v-if="wwItem.type == 'text'" style="font-size: 18px !important;"
                                             v-html="wwItem.text">
                                        </div>
                                        <div v-if="wwItem.type == 'math'">
                                            <mathlive-mathfield
                                                    :id="'mf'+wwItem"
                                                    :ref="'mathfield'+wwItem"
                                                    @focus="ping"
                                                    :on-keystroke="displayKeystroke"
                                                    :config=" {smartMode:true, readOnly: true}"
                                                    v-model="wwItem.text"
                                                    style="font-size: 22px; font-weight: 400;">
                                                <span>{{wwItem.text}}</span>
                                            </mathlive-mathfield>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1 uk-flex uk-flex-right uk-margin-large-top"
                             style=" border-top: 1px solid #d2d2d2;">
                            <%--							<img alt="" src="${ASSETS}/img/viewer/no_scroll.png">--%>
                        </div>
                    </div>
                    <div class="next-area">
                        <div class="solving-nav tac pc">
                            <%--							<button class="prev mobile-view" @click="prevQuestion">이전문제</button>--%>
                            <%--							v-if="'${sessionScope.S_LOGINID}' == item.reg_agent ||
                                                        '${sessionScope.S_ROLE}'=='ROLE_ADMIN'"--%>
                            <button class="btn-red-line mobile-view" @click="backQuestion"
                                    v-if="'${sessionScope.S_LOGINID}' == questions.regUserId ||'${sessionScope.S_ROLE}'=='ROLE_ADMIN'">
                                <spring:message code="question.register.edit" />
                            </button>
<%--                            <button class="btn-red-line mobile-view" @click="copyModel()">--%>
<%--                                <spring:message code="question.register.copy" />--%>
<%--                            </button>--%>
<%--                            <button class="btn-red-line mobile-view" @click="logsee()">--%>
<%--                                <spring:message code="examLog" />--%>
<%--                            </button>--%>
                        </div>
                        <%--						<div class="solving-nav tac mob" style="text-align:center;">--%>
                        <%--							<button class="prev mobile-view" style="margin-right:0;" @click="prevQuestion">이전문제</button>--%>
                        <%--							<button class="next mobile-view" style="margin-left:0;" @click="nextQuestion">다음문제</button>--%>
                        <%--						</div>--%>
                    </div>
                </div>
                <div class="explain-area right mobile-blind">
<%--                    <div class="top-area-div">--%>
<%--                        <button class="btn-red-long" @click="createQuestion()"--%>
<%--                                v-if="'${sessionScope.S_LOGINID}' == questions.regUserId ||--%>
<%--                            '${sessionScope.S_ROLE}'=='ROLE_ADMIN'">--%>
<%--                            <spring:message code="save" />--%>
<%--                        </button>--%>
<%--                    </div>--%>
                    <div class="list-area on" style="overflow:auto;">
                        <div class="height-type03 mgt15">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <colgroup>
                                    <col width="40%">
                                    <col width="60%">
                                </colgroup>
                                <tbody>
                                <tr>
                                    <td colspan="2" class="tit-th">
                                        <span><spring:message code="all.questionType" /></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                        <span><spring:message code="qBank.type" /></span>
                                    </td>
                                    <td>
                                        <select class="small-width" v-model="questions.questionType">
                                            <option :value="1">
                                                <spring:message code="question.register.single" />
                                            </option>
                                            <option :value="2">
                                                <spring:message code="question.register.shortAnswer" />
                                            </option>
                                            <option :value="3">
                                                <spring:message code="question.register.multiple" />
                                            </option>
                                            <option :value="4">
                                                <spring:message code="question.register.drawData" />
                                            </option>
                                            <option :value="5">
                                                <spring:message code="question.register.image" />
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="tit-th">
                                        <span><spring:message code="all.mainCate" /></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit" style="border-top-left-radius: 10px;">
                                        <p><spring:message code="all.main"/> <span class="top-title">*</span></p>
                                    </td>
                                    <td style="border-top-right-radius: 10px;">
                                        <select class="small-width" v-model="questions.examCategory.main"
                                                @change="categoryChange('exam', 'main')">
                                            <option value="0" disabled><spring:message code="question.register.main"/></option>
                                            <template v-for="(item, index) in examCategory.main">
                                                <option :value="item.id" :key="index">{{item.name}}</option>
                                            </template>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit" style="border-top-left-radius: 10px;">
                                        <p><spring:message code="all.middle"/> <span class="top-title">*</span></p>
                                    </td>
                                    <td style="border-top-right-radius: 10px;">
                                        <select class="small-width" v-model="questions.examCategory.middle"
                                                @change="categoryChange('exam', 'middle')">
                                            <option value="0"
                                                    disabled><spring:message code="question.register.middle"/></option>
                                            <template v-for="(item, index) in examCategory.middle">
                                                <option :value="item.id" :key="index">{{item.name}}</option>
                                            </template>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit" style="border-top-left-radius: 10px;">
                                        <p><spring:message code="all.low"/> <span class="top-title">*</span></p>
                                    </td>
                                    <td style="border-top-right-radius: 10px;">
                                        <select class="small-width" v-model="questions.examCategory.low"
                                                @change="categoryChange('exam', 'low')">
                                            <option value="0" disabled><spring:message code="question.register.low"/></option>
                                            <template v-for="(item, index) in examCategory.low">
                                                <option :value="item.id" :key="index">{{item.name}}</option>
                                            </template>
                                        </select>
                                    </td>
                                </tr>
<%--                                <tr>--%>
<%--                                    <td colspan="2" class="tit-th">--%>
<%--                                        <span><spring:message code="all.subjectcategory" /></span>--%>
<%--                                    </td>--%>
<%--                                </tr>--%>
<%--                                <tr>--%>
<%--                                    <td class="tit">--%>
<%--                                        <p>--%>
<%--                                            <spring:message code="all.main" />--%>
<%--                                        </p>--%>
<%--                                    </td>--%>
<%--                                    <td>--%>
<%--                                        <select class="small-width" v-model="questions.subjectCategory.main"--%>
<%--                                                @change="categoryChange('subject', 'main')">--%>
<%--                                            <option value="0" disabled><spring:message code="question.register.main"/></option>--%>
<%--                                            <template v-for="(item, index) in subjectCategory.main">--%>
<%--                                                <option :value="item.id" :key="index">{{item.name}}</option>--%>
<%--                                            </template>--%>
<%--                                        </select>--%>
<%--                                    </td>--%>
<%--                                </tr>--%>
<%--                                <tr>--%>
<%--                                    <td class="tit">--%>
<%--                                        <p>--%>
<%--                                            <spring:message code="all.middle" />--%>
<%--                                        </p>--%>
<%--                                    </td>--%>
<%--                                    <td>--%>
<%--                                        <select class="small-width" v-model="questions.subjectCategory.middle"--%>
<%--                                                @change="categoryChange('subject', 'middle')">--%>
<%--                                            <option value="0"--%>
<%--                                                    disabled><spring:message code="question.register.middle"/></option>--%>
<%--                                            <template v-for="(item, index) in subjectCategory.middle">--%>
<%--                                                <option :value="item.id" :key="index">{{item.name}}</option>--%>
<%--                                            </template>--%>
<%--                                        </select>--%>
<%--                                    </td>--%>
<%--                                </tr>--%>
<%--                                <tr>--%>
<%--                                    <td class="tit">--%>
<%--                                        <p>--%>
<%--                                            <spring:message code="all.low" />--%>
<%--                                        </p>--%>
<%--                                    </td>--%>
<%--                                    <td>--%>
<%--                                        <select class="small-width" v-model="questions.subjectCategory.low"--%>
<%--                                                @change="categoryChange('subject', 'low')">--%>
<%--                                            <option value="0" disabled><spring:message code="question.register.low"/></option>--%>
<%--                                            <template v-for="(item, index) in subjectCategory.low">--%>
<%--                                                <option :value="item.id" :key="index">{{item.name}}</option>--%>
<%--                                            </template>--%>
<%--                                        </select>--%>
<%--                                    </td>--%>
<%--                                </tr>--%>
                                <tr>
                                    <td colspan="2" class="tit-th">
                                        <p>
                                            <spring:message code="others" />
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                        <p><spring:message code="question.register.score"/><span class="red">*</span></p>
                                    </td>
                                    <td>
                                        <input name="question_score" v-model="questions.point" class="qbank-input" size="30"
                                               type="number">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                        <p><spring:message code="question.register.diff" /><span class="top-title">*</span></p>
                                    </td>
                                    <td>
                                        <select class="small-width" v-model="questions.questionLevel">
                                            <option :value="1">
                                                <spring:message code="question.register.vEasy" />
                                            </option>
                                            <option :value="2">
                                                <spring:message code="question.register.easy" />
                                            </option>
                                            <option :value="3">
                                                <spring:message code="question.register.normal" />
                                            </option>
                                            <option :value="4">
                                                <spring:message code="question.register.diff" />
                                            </option>
                                            <option :value="5">
                                                <spring:message code="question.register.vDiff" />
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                        <p>
                                            <spring:message code="question.register.knowledge" />
                                        </p>
                                    </td>
                                    <td>
                                        <select class="small-width" v-model="questions.knowledgeLevel">
                                            <option :value="1">
                                                <spring:message code="question.register.memoriz" />
                                            </option>
                                            <option :value="2">
                                                <spring:message code="question.register.analyz" />
                                            </option>
                                            <option :value="3">
                                                <spring:message code="question.register.solution" />
                                            </option>
                                            <option :value="4">
                                                <spring:message code="question.register.na" />
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit"><spring:message code="question.register.rela"/></td>
                                    <td>
                                        <div class="flex margin20">
                                            <input id="essential" type="checkbox" class="checkbox"
                                                   v-model="questions.questionRevevance" value="1"/>
                                            <label for="essential" style="margin-top: -4px; padding-left: 5px;"><spring:message code="question.register.essential"/></label>
                                            <input id="important" type="checkbox" class="checkbox" style="margin-left: 10px;"
                                                   v-model="questions.questionRevevance" value="2"/>
                                            <label class="margin10" for="important" style="margin-top: -4px; padding-left: 5px;"><spring:message
                                                    code="question.register.important"/></label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                        <spring:message code="question.register.cRate"/>
                                    </td>
                                    <td>
                                        <input name="question_correctRate" v-model="questions.correctRate" class="qbank-input">
                                        <span>%</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                        <p>
                                            <spring:message code="question.register.keyword" />
                                        </p>
                                    </td>
                                    <td>
                                        <div>
                                            <p v-for="(item,index) in questions.keywords" :key="index"
                                               style="padding-left: 10px;">
                                                <span>{{item}}</span>
                                            </p>
                                        </div>
                                    </td>
                                <tr>
                                    <td class="tit">
                                        <p>
                                            <spring:message code="question.register.topic" />
                                        </p>
                                    </td>
                                    <td>
                                        <div>
                                            <input name="question_topic" v-model="questions.topic" class="qbank-input">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                        <p>
                                            <spring:message code="question.register.ref" />
                                        </p>
                                    </td>
                                    <td>
                                        <div>
                                            <input name="question_reference" v-model="questions.reference" class="qbank-input">
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <%--		Exam list is here --%>
<%--        <div id="examlist-modal" class="modalDialog">--%>
<%--            <div class="modal-mask">--%>
<%--                <div class="modal-wrapper">--%>
<%--                    <div class="modal-container">--%>
<%--                        <a href="#close" title="modelclose" class="modelclose">X</a>--%>
<%--                        <div class="modal-header">--%>
<%--                            <p>시험지 이력</p>--%>
<%--                        </div>--%>

<%--                        <div class="modal-body">--%>
<%--                            <div class="modal-table">--%>
<%--                                <table width="100%" border="0" cellspacing="0" cellpadding="0"--%>
<%--                                       style="table-layout: auto;">--%>
<%--                                    <colgroup>--%>
<%--                                        <col width="4%">--%>
<%--                                        <col width="15%">--%>
<%--                                        <col width="15%">--%>
<%--                                        <col width="15%">--%>
<%--                                        <col width="15%">--%>
<%--                                        <col width="15%">--%>
<%--                                        <col width="15%">--%>
<%--                                    </colgroup>--%>
<%--                                    <thead>--%>
<%--                                    <tr class="title_table border-red-top-2">--%>
<%--                                        <th class="title-th">--%>
<%--                                            <span>№</span>--%>
<%--                                        </th>--%>
<%--                                        <th class="title-th">--%>
<%--                                            <span><spring:message code="all.startDate" /></span>--%>
<%--                                        </th>--%>
<%--                                        <th class="title-th">--%>
<%--                                            <span><spring:message code="examreg.examName" /></span>--%>
<%--                                        </th>--%>
<%--                                        <th class="title-th">--%>
<%--                                            <span><spring:message code="all.submitter" /></span>--%>
<%--                                        </th>--%>
<%--                                        <th class="title-th">--%>
<%--                                            <span><spring:message code="class" /></span>--%>
<%--                                        </th>--%>
<%--                                        <th class="title-th">--%>
<%--                                            <span><spring:message code="all.questionCount" /></span>--%>
<%--                                        </th>--%>
<%--                                    </tr>--%>
<%--                                    </thead>--%>
<%--                                    <tbody>--%>
<%--                                    <tr v-if="examList.length == 0">--%>
<%--                                        <td colspan="6" style="text-align: center">--%>
<%--                                            <spring:message code="all.empty" />--%>
<%--                                        </td>--%>
<%--                                    </tr>--%>
<%--                                    <tr v-for="(item, index) in examList" :key="index" v-else>--%>
<%--                                        <td>--%>
<%--										 	<span>--%>
<%--												{{index+1}}--%>
<%--											</span>--%>
<%--                                        </td>--%>
<%--                                        <td>--%>
<%--                                            <span>{{item.created_at}}</span>--%>
<%--                                        </td>--%>
<%--                                        <td>--%>
<%--                                            <span>{{item.exam_name}}</span>--%>
<%--                                        </td>--%>
<%--                                        <td>--%>
<%--                                            <span>{{item.unit}}</span>--%>
<%--                                        </td>--%>
<%--                                        <td>--%>
<%--                                            <span>{{item.reguser}}</span>--%>
<%--                                        </td>--%>
<%--                                        <td>--%>
<%--                                            <span>{{item.sent_count}}</span>--%>
<%--                                        </td>--%>
<%--                                        <td>--%>
<%--                                            <span >--%>
<%--												<a href="javascript:void(0)" class=""--%>
<%--                                                   @click="newtab('questionlist', item.id)">--%>
<%--													{{item.qcount}}--%>
<%--												</a>--%>
<%--											</span>--%>
<%--                                        </td>--%>
<%--                                    </tr>--%>

<%--                                    </tbody>--%>
<%--                                </table>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="modal-footer flex">--%>
<%--                            <button @click="callnameClose()" class="modal-default-button" type="button">--%>
<%--                                <spring:message code="all.close" />--%>
<%--                            </button>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>

        <%--		modal exam questions--%>
<%--        <div id="copyModal" class="modalDialog">--%>
<%--            <div class="modal-mask" style="top: 25%">--%>
<%--                <div class="modal-wrapper">--%>
<%--                    <div class="modal-container" style="width: 600px">--%>
<%--                        <a href="#close" title="modelclose" class="modelclose">X</a>--%>
<%--                        <div class="modal-header">--%>
<%--                            <p>문항복사</p>--%>
<%--                        </div>--%>
<%--                        <div class="modal-body" style="text-align: center; padding: 40px;">--%>
<%--                            <p style="font-size: 16px">해당 문항을 복사하여 출제하시겠습니까?</p>--%>
<%--                        </div>--%>
<%--                        <div class="modal-footer flex">--%>
<%--                            <button @click="callnameClose()" class="modal-default-button" type="button">아니오</button>--%>
<%--                            <button @click="questionCopy()" class="modal-default-button" type="button"--%>
<%--                                    style="margin-left: 10px; color: #fff; background-color: #3ac4fc"> 예</button>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
        <%--	load  achiement modal--%>
<%--        <div id="openModal-about" class="modalDialog">--%>
<%--            <div class="modal-mask">--%>
<%--                <div class="modal-wrapper">--%>
<%--                    <div class="modal-container">--%>
<%--                        <a href="#close" title="modelclose" class="modelclose">X</a>--%>
<%--                        <div class="modal-header">--%>
<%--                            <p>성취기준 코드 검색</p>--%>
<%--                        </div>--%>

<%--                        <div class="modal-body">--%>
<%--                            <div class="modal-search flex">--%>
<%--                                <input name="model-search" value="" class="" placeholder="Write something"--%>
<%--                                       v-model="questions.search">--%>
<%--                                <button class="modal-default-button" style="margin-left: 20px" type="button"--%>
<%--                                        @click="loadAshimet('open')">--%>
<%--                                    Search--%>
<%--                                </button>--%>
<%--                            </div>--%>
<%--                            <div class="modal-table">--%>
<%--                                <table width="100%" border="0" cellspacing="0" cellpadding="0"--%>
<%--                                       style="table-layout: auto;">--%>
<%--                                    <colgroup>--%>
<%--                                        <col width="4%">--%>
<%--                                        <col width="15%">--%>
<%--                                        <col width="40%">--%>
<%--                                    </colgroup>--%>
<%--                                    <thead>--%>
<%--                                    <tr class="title_table border-red-top-2">--%>
<%--                                        <th class="title-th">--%>
<%--                                            <span>선택</span>--%>
<%--                                        </th>--%>
<%--                                        <th class="title-th">--%>
<%--                                            <span>성취기준 코드</span>--%>
<%--                                        </th>--%>
<%--                                        <th class="title-th">--%>
<%--                                            <span>성취기준 </span>--%>
<%--                                        </th>--%>
<%--                                    </tr>--%>
<%--                                    </thead>--%>
<%--                                    <tbody>--%>
<%--                                    <tr v-for="(item,index) in achievementlist" :key="index"--%>
<%--                                        v-if="achievementlist.length > 0">--%>
<%--                                        <td>--%>
<%--                                            <input name="groupQuestion" :id="'item.achievement_code'+index" type="radio"--%>
<%--                                                   class="radio"--%>
<%--                                                   :value="item.achievement_code" v-model="questions.achievement">--%>
<%--                                            <label :for="'item.achievement_code'+index" class="modal-label"></label>--%>
<%--                                        </td>--%>
<%--                                        <td>--%>
<%--                                            {{item.achievement_code}}--%>
<%--                                        </td>--%>
<%--                                        <td>--%>
<%--                                            {{item.achievement}}--%>
<%--                                        </td>--%>
<%--                                    </tr>--%>
<%--                                    <tr v-else>--%>
<%--                                        <td colspan="4" style="text-align: center">Empty</td>--%>
<%--                                    </tr>--%>
<%--                                    </tbody>--%>
<%--                                </table>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="modal-footer flex">--%>
<%--                            <button @click="callnameClose()" class="modal-default-button" type="button">닫기</button>--%>
<%--                            <button @click="callname()" class="modal-default-button margin-left" type="button">선택 등록--%>
<%--                            </button>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
        <div class="spliter">
        </div>
        <!--***** PHOTOSWIPE *****-->
        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap">
                <div class="pswp__container">
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                </div>

                <div class="pswp__ui pswp__ui--hidden">
                    <div class="pswp__top-bar">
                        <div class="pswp__counter"></div>

                        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                        <div class="pswp__preloader">
                            <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                        <div class="pswp__share-tooltip">
                        </div>
                    </div>

                    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                    <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>

                    <div class="pswp__caption">
                        <div class="pswp__caption__center"></div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <script type="module">
        import MathLive from '${JS}/vue/mathlive/dist/src/mathlive.js';
        import Mathfield from '${JS}/vue/mathlive/dist/vue-mathlive.js';

        Vue.use(Mathfield, MathLive);

        let router = new VueRouter({
            mode: 'history',
            routes: []
        });

        let app = new Vue({
            router,
            el: '#questionPreview',
            components: {},
            computed: {},
            data: {
                muted: false,
                formula: 'x=-b\\pm \\frac {\\sqrt{b^2-4ac}}{2a}',
                keystroke: '',
                classModal: false,
                examCategory: {
                    main: [],
                    middle: [],
                    low: []
                },
                subjectCategory: {
                    main: [],
                    middle: [],
                    low: []
                },
                questions: {
                    questionType: '1',
                    correctRate: '100',
                    controlNo: '',
                    regUserId: '',
                    examCategory: {
                        main: '0',
                        middle: '0',
                        low: '0'
                    },
                    bulletedListType: [
                        {
                            first: 'A',
                            second: '',
                            third: '',
                            fourth: '',
                            fifth: ''
                        }
                    ],
                    point: 1,
                    subjectCategory: {
                        main: '0',
                        middle: '0',
                        low: '0'
                    },
                    questionLevel: 3,
                    estimatedCorrectRate: '',
                    knowledgeLevel: 4,
                    questionRevevance: [],
                    topic: '',
                    keywords: [],
                    reference: '',
                    headerText: '',
                    questionData: '',
                    presentDatas: [],
                    wrongNotes: [],
                    shortAnswers: [
                        {
                            hint: '',
                            trueData: '',
                            fieldType: 'text'
                        },
                    ],
                    answerCanvas: [
                        {
                            file: null,
                            text: '',
                            imgDesc: '',
                            correntAnswer: false,
                            fileName: '',
                        },
                        {
                            file: null,
                            text: '',
                            imgDesc: '',
                            correntAnswer: false,
                            fileName: '',
                        }
                    ],
                    answers: [
                        {
                            fileName: '',
                            file: null,
                            imgDesc: '',
                            text: '',
                            correctAnswer: false,
                            optionNumber: '1'
                        },
                        {
                            fileName: '',
                            imgDesc: '',
                            file: null,
                            text: '',
                            correctAnswer: false,
                            optionNumber: '2'
                        },
                        {
                            fileName: '',
                            imgDesc: '',
                            file: null,
                            text: '',
                            correctAnswer: false,
                            optionNumber: '3'
                        },
                        {
                            fileName: '',
                            imgDesc: '',
                            file: null,
                            text: '',
                            correctAnswer: false,
                            optionNumber: '4'
                        },
                        {
                            fileName: '',
                            imgDesc: '',
                            file: null,
                            text: '',
                            correctAnswer: false,
                            optionNumber: '5'
                        },

                    ],
                    cloudQType: 'public',
                    questionPrice: 'true',
                    qInputPrice: 0
                },
                btnhide: false,
                subjects: [],
                behaviors: [],
                actionType: '',
                func: [],
                examList: [],
                alertsuccess: '',
                imgPath: '',
                userId: 0,
                localPath: '${BASEHOST}/exam'
                // localPath: 'http://localhost:8088/keris/kexam'
            },
            mounted() {
                var element = document.getElementById('questionPreview');
                element.classList.remove("sectionnone");
                element.classList.add("sectiondisplay");

                if (typeof this.$route.query.id != 'undefined') {
                    let id = this.$route.query.id;
                    this.getQuestionDetail(id)
                    this.actionType = 'update'
                } else {
                    alert("Question id undefined")
                }
                this.loadCates("main", 1, 0)
                this.loadCates("subject", 1, 0)
            },
            created() {

            },
            methods: {
                showFunc(item){
                    if (item.checked)
                    {
                        item.checked = false
                    }
                    else{
                        item.checked = true
                    }
                },
                newtab(link,id){
                    var loginId = '${sessionScope.S_LOGINID}';
                    this.userId = loginId
                    console.log(this.userId)

                    window.open(this.localPath + "/" + link + "?examId=" + id + "&userId=" + this.userId,
                        '_blank', 'width=1240,height=700')
                },

                questionCopy(){
                    try {
                        var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                        axios.post('${BASEURL}/questions/copy',{
                            qId: this.questions.id }, {
                            headers: headers }).then((response) => {
                            if (response.data.status == 200){
                                if (response.data.success == true)
                                {
                                    // alert("questions copied!! ")
                                    // if (window.opener && !window.opener.closed) {
                                    window.opener.location.href = "${HOME}/question_registration/each/essential?id=" + response.data.result.newId;
                                    window.close()
                                    // }
                                    <%--window.opener.location.href = "${HOME}/question_registration/each/essential?id=" + response.data.result.newId;--%>
                                }
                                else{
                                    alert(response.data.message)
                                }
                            } else {
                                alert(response.data.message)
                            }
                        }, (error) => {
                            console.log(error);
                        });
                    }
                    catch (error) {
                        console.log(error)
                    }
                },
                copyModel(){
                    window.location.href = "#copyModal";
                },
                logsee(){
                    try {
                        var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                        axios.get('${BASEURL}/kexam/questions/used/exams', {
                            params: {
                                qId: this.questions.id
                            },
                            headers: headers
                        }).then((response) => {
                            if (response.data.status == 200){
                                if (response.data.success == true)
                                {
                                    this.examList = response.data.result.exams
                                    window.location.href = "#examlist-modal";
                                }
                                else{
                                    alert(response.data.message)
                                }
                            } else {
                                alert(response.data.message)
                            }
                        }, (error) => {
                            console.log(error);
                        });
                    }
                    catch (error) {
                        console.log(error)
                    }
                },
                backQuestion() {
                    window.close();
                    if (window.opener && !window.opener.closed) {
                        window.opener.location.href = "${HOME}/question_registration/each/essential?id=" + this.questions.id
                    }
                },
                closeWindow() {
                    window.close();
                    // if (window.opener && !window.opener.closed) {
                    //     window.opener.location.reload();
                    // }
                },
                categoryChange(type, catelevel) {

                    if (type == 'exam') {
                        type = 'main'
                    } else {
                        type = 'subject'
                    }

                    if (catelevel == 'main') {
                        let parentId = 0
                        if (type == 'main')
                            parentId = this.questions.examCategory.main
                        else
                            parentId = this.questions.subjectCategory.main

                        this.loadCates(type, 2, parentId)
                    }
                    if (catelevel == 'middle') {
                        let parentId = 0
                        if (type == 'main')
                            parentId = this.questions.examCategory.middle
                        else
                            parentId = this.questions.subjectCategory.middle

                        this.loadCates(type, 3, parentId)
                    }

                },
                async loadCates(type, cateStep, parentId) {
                    try {
                        var language =  "<%=pageContext.getResponse().getLocale()%>"
                        var locale = ''
                        if (language == 'kr')
                        {
                            locale = 'kr-KR'
                        }
                        if(language == 'mn')
                        {
                            locale = 'mn-MN'
                        }
                        if(language == 'en'){
                            locale = 'en-EN'
                        }
                        const headers = {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                            'Accept-Language' : locale
                        }

                        axios.get('${BASEURL}/category/liststep', {
                            params: {
                                type: type,
                                cateStep: cateStep,
                                parentId: parentId
                            },
                            headers: headers
                        }).then((response) => {
                            if (response.data.status === 200) {
                                if (type == "main") {
                                    //exam category
                                    if (cateStep == 1) {
                                        this.examCategory.main = response.data.result.cates
                                    }
                                    if (cateStep == 2) {
                                        this.examCategory.middle = response.data.result.cates
                                    }
                                    if (cateStep == 3) {
                                        this.examCategory.low = response.data.result.cates
                                    }
                                } else {
                                    //subject category
                                    if (cateStep == 1) {
                                        this.subjectCategory.main = response.data.result.cates
                                    }
                                    if (cateStep == 2) {
                                        this.subjectCategory.middle = response.data.result.cates
                                    }
                                    if (cateStep == 3) {
                                        this.subjectCategory.low = response.data.result.cates
                                    }
                                }


                            } else {
                                alert("ERROR: " + response.data.message)
                            }
                        }, (error) => {
                            console.log(error);
                        });


                    } catch (error) {
                        console.log(error)
                    }
                },
                getQuestionDetail(qId) {
                    try{
                        var language =  "<%=pageContext.getResponse().getLocale()%>"
                        var locale = ''
                        if (language == 'kr')
                        {
                            locale = 'kr-KR'
                        }
                        if(language == 'mn')
                        {
                            locale = 'mn-MN'
                        }
                        if(language == 'en'){
                            locale = 'en-EN'
                        }
                        const headers = {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                            'Accept-Language' : locale
                        }
                        axios.get('${BASEURL}/questions/' + qId, {
                            params: {},
                            headers: headers
                        }).then((response) => {

                            if (response.data.status == 200) {
                                let questionDetail = response.data.result.questionDetail
                                this.questions.examCategory.main = questionDetail.cate1_code
                                if (questionDetail.cate1_code != 0)
                                    this.categoryChange('exam', 'main')
                                this.questions.examCategory.middle = questionDetail.cate2_code
                                if (questionDetail.cate2_code != 0)
                                    this.categoryChange('exam', 'middle')
                                this.questions.examCategory.low = questionDetail.cate3_code

                                this.questions.correctRate = questionDetail.correct_rate
                                this.questions.questionLevel = questionDetail.difficulty_level
                                this.questions.cloudQType = questionDetail.exportType
                                this.questions.questionPrice = questionDetail.free + ''
                                this.questions.qInputPrice = questionDetail.price
                                this.questions.headerText = questionDetail.headerText
                                this.questions.id = questionDetail.id
                                this.questions.keywords = []

                                if (questionDetail.keywords.length > 0) {
                                    if (questionDetail.keywords.includes(","))
                                        this.questions.keywords = questionDetail.keywords.split(',')
                                    else
                                        this.questions.keywords[0] = questionDetail.keywords
                                }

                                this.questions.knowledgeLevel = questionDetail.question_level
                                this.questions.point = questionDetail.point
                                if (questionDetail.qRelevance.length > 0) {
                                    if (questionDetail.qRelevance.includes(","))
                                        this.questions.questionRevevance = questionDetail.qRelevance.split(',')
                                    else
                                        this.questions.questionRevevance[0] = questionDetail.qRelevance
                                }

                                this.questions.questionData = questionDetail.question
                                this.questions.questionType = questionDetail.question_type
                                this.questions.reference = questionDetail.reference
                                this.questions.regUserId = questionDetail.userId

                                this.questions.subjectCategory.main = questionDetail.scate1_code
                                if (questionDetail.scate1_code != 0)
                                    this.categoryChange('subject', 'main')
                                this.questions.subjectCategory.middle = questionDetail.scate2_code
                                if (questionDetail.scate2_code != 0)
                                    this.categoryChange('subject', 'middle')
                                this.questions.subjectCategory.low = questionDetail.scate3_code
                                this.questions.topic = questionDetail.topic
                                this.questions.controlNo = questionDetail.controlNo
                                if (questionDetail.question_type != '4' && questionDetail.question_type != '2' && questionDetail.question_type != '5') {
                                    if (questionDetail.question_answers[0].optionNumber == 'A') {
                                        // alert("A")
                                        this.answerOptionType = 'eletter'
                                    } else if (questionDetail.question_answers[0].optionNumber == 'А') {
                                        this.answerOptionType = 'kletter'
                                    } else if (questionDetail.question_answers[0].optionNumber == '1') {
                                        this.answerOptionType = 'number'
                                    }
                                } else {
                                    this.answerOptionType = 'number'
                                }
                                this.questions.answerType = questionDetail.answerType


                                this.questions.answers = []
                                for (let a = 0; a < questionDetail.question_answers.length; a++) {
                                    let item = {
                                        fileName: '',
                                        file: null,
                                        imgDesc: '',
                                        text: questionDetail.question_answers[a].answer,
                                        correctAnswer: false,
                                        optionNumber: questionDetail.question_answers[a].optionNumber
                                    }
                                    if (questionDetail.question_answers[a].media != null) {
                                        item.fileName = questionDetail.question_answers[a].media.filename
                                        item.imgDesc = questionDetail.question_answers[a].media.keyword
                                    }

                                    item.correctAnswer = questionDetail.question_answers[a].answerFlag == 'true' ? true : false
                                    this.questions.answers.push(item)

                                }

                                if (questionDetail.question_type === '4' && questionDetail.question_answers.length > 0) {
                                    this.questions.answerCanvas = []
                                    let item1 = {
                                        file: null,
                                        text: '',
                                        imgDesc: '',
                                        correntAnswer: true,
                                        fileName: questionDetail.question_answers[0].trueData,
                                    }
                                    let item2 = {
                                        file: null,
                                        text: '',
                                        imgDesc: '',
                                        correntAnswer: true,
                                        fileName: questionDetail.question_answers[0].drawingData,
                                    }

                                    this.questions.answerCanvas.push(item1)
                                    this.questions.answerCanvas.push(item2)


                                    this.questions.answers = []
                                    for (let i = 0; i < 5; i++) {
                                        let item = {
                                            fileName: '',
                                            file: null,
                                            imgDesc: '',
                                            text: '',
                                            correctAnswer: false,
                                            optionNumber: 1
                                        }

                                        this.questions.answers.push(item)
                                    }
                                }
                                if (questionDetail.question_type === '2' && questionDetail.question_answers.length > 0) {
                                    this.questions.shortAnswers = []
                                    for (let i = 0; i < questionDetail.question_answers.length; i++) {
                                        let item = {
                                            trueData: questionDetail.question_answers[i].trueData,
                                            hint: questionDetail.question_answers[i].answer,
                                            fieldType: questionDetail.question_answers[i].fieldType
                                        }
                                        this.questions.shortAnswers.push(item)
                                    }

                                    this.questions.answers = []
                                    for (let i = 0; i < 5; i++) {
                                        let item = {
                                            fileName: '',
                                            file: null,
                                            imgDesc: '',
                                            text: '',
                                            correctAnswer: false,
                                            optionNumber: 1
                                        }

                                        this.questions.answers.push(item)
                                    }
                                }
                                this.questions.presentDatas = []

                                for (let p = 0; p < questionDetail.question_datas.length; p++) {
                                    let type = questionDetail.question_datas[p].dataType;
                                    let filename = ""
                                    let imgDesc = ""
                                    let file = null
                                    if (questionDetail.question_datas[p].media != null) {
                                        type = questionDetail.question_datas[p].media.mediatype
                                        filename = questionDetail.question_datas[p].media.filename
                                        imgDesc = questionDetail.question_datas[p].media.keyword
                                        file = ""
                                    }
                                    let item = {
                                        fileName: filename,
                                        type: type,
                                        file: file,
                                        fileChanged: false,
                                        text: questionDetail.question_datas[p].data_text,
                                        imgDesc: imgDesc
                                    }

                                    this.questions.presentDatas.push(item)
                                }

                                this.questions.wrongNotes = []
                                for (let p = 0; p < questionDetail.question_wrongs.length; p++) {
                                    let type = questionDetail.question_wrongs[p].dataType;
                                    let filename = "";
                                    let imgDesc = "";
                                    let file = null;
                                    if (questionDetail.question_wrongs[p].media != null) {
                                        type = questionDetail.question_wrongs[p].media.mediatype
                                        filename = questionDetail.question_wrongs[p].media.filename
                                        imgDesc = questionDetail.question_wrongs[p].media.keyword
                                        file = ""
                                    }
                                    let item = {
                                        fileName: filename,
                                        type: type,
                                        file: file,
                                        fileChanged: false,
                                        text: questionDetail.question_wrongs[p].data_text,
                                        imgDesc: imgDesc
                                    }
                                    this.questions.wrongNotes.push(item)
                                }

                                console.log("questions")
                                console.log(this.questions)

                            } else {
                                alert("ERROR: " + response.data.message)
                            }
                        }, (error) => {
                            console.log(error);
                        });
                    }
                    catch(error){
                        alert(error)
                    }

                },
                callnameClose(){
                    window.location.href="#close"
                },
                loading(cmd) {
                    var l = document.getElementById("se-pre-con");
                    l.style.display = cmd;
                },
                async createQuestion() {
                    try {
                        let formData = new FormData()
                        let attachedPFile = true;
                        let attachedPText = true;
                        for (let i = 0; i < this.questions.presentDatas.length; i++) {
                            if (this.questions.presentDatas[i].type != 'text' && this.questions.presentDatas[i].type != 'math') {

                                if (this.questions.presentDatas[i].fileName == "") {
                                    attachedPFile = false;
                                    break;
                                } else {
                                    console.log(this.questions.presentDatas[i].file)
                                    formData.append('presentDataFiles', this.questions.presentDatas[i].file)
                                }
                            } else {
                                if (this.questions.presentDatas[i].text == '') {
                                    attachedPText = false;
                                    break;
                                }
                            }
                        }
                        if (!attachedPText) {
                            alert("<spring:message code="selectPresentDataText" />")
                            return;
                        }
                        if (!attachedPFile) {
                            alert("<spring:message code="selectPresentDataFiles" />")
                            return;
                        }

                        if (this.questions.questionType == 2) {
                            let attachedFileC = true;
                            for (let i = 0; i < this.questions.shortAnswers.length; i++) {

                                if (this.questions.shortAnswers[i].trueData == '' || this.questions.shortAnswers[i].hint == '') {
                                    attachedFileC = false;
                                    break;
                                }
                            }
                            if (!attachedFileC) {
                                alert("please enter short answer data")
                                return;
                            }
                        }
                        if (this.questions.questionType == 4 || this.questions.questionType == 2) {
                            this.questions.answers = []
                            this.answerType = 'text'
                            if (this.questions.questionType == 4) {
                                let attachedFileC = true;
                                for (let i = 0; i < this.questions.answerCanvas.length; i++) {

                                    if (this.questions.answerCanvas[i].fileName == '') {
                                        attachedFileC = false;
                                        break;
                                    } else {
                                        // console.log(this.questions.answers[i].file)
                                        formData.append('answerFiles', this.questions.answerCanvas[i].file)
                                    }
                                }
                                if (!attachedFileC) {
                                    alert("<spring:message code="essentail.chooseFiles"/>")
                                    return;
                                }
                            }
                        }

                        if (this.answerType != 'text' && this.answerType != 'math') {
                            let attachedFile = true;
                            for (let i = 0; i < this.questions.answers.length; i++) {

                                if (this.questions.answers[i].fileName == '') {
                                    attachedFile = false;
                                    break;
                                } else {
                                    // console.log(this.questions.answers[i].file)
                                    formData.append('answerFiles', this.questions.answers[i].file)
                                }

                            }
                            if (!attachedFile) {
                                alert("<spring:message code="essentail.chooseFiles"/>")
                                return;
                            }
                        }

                        let attachedWFile = true;
                        let attachedWText = true;
                        for (let i = 0; i < this.questions.wrongNotes.length; i++) {
                            if (this.questions.wrongNotes[i].type != 'text' && this.questions.wrongNotes[i].type != 'math') {

                                if (this.questions.wrongNotes[i].fileName == "") {
                                    attachedWFile = false;
                                    break;
                                } else {
                                    console.log(this.questions.wrongNotes[i].file)
                                    formData.append('wrongDataFiles', this.questions.wrongNotes[i].file)
                                }
                            } else {
                                if (this.questions.wrongNotes[i].text == '') {
                                    attachedWText = false;
                                    break;
                                }
                            }
                        }
                        if (!attachedWText) {
                            alert("<spring:message code="selectPresentDataText" />")
                            return;
                        }
                        if (!attachedWFile) {
                            alert("<spring:message code="selectPresentDataFiles"/>")
                            return;
                        }

                        if (this.questions.questionType == 1 || this.questions.questionType == 3) {
                            let rightAnswerFlag = false;

                            for (let i = 0; i < this.questions.answers.length; i++) {
                                if (this.questions.answers[i].correctAnswer == true) {
                                    rightAnswerFlag = true;
                                    break;
                                }
                            }
                            if (!rightAnswerFlag) {
                                alert("<spring:message code="essential.chooseAnswer" />")
                                return;
                            }
                        }

                        if (this.questions.examCategory.main == 0) {
                            alert("<spring:message code="essential.cateChoose"/>")
                            this.scrollToTop()
                            return;
                        }
                        if (this.questions.examCategory.middle == 0) {
                            alert("<spring:message code="essentail.midChoose"/>")
                            this.scrollToTop()
                            return;
                        }
                        if (this.questions.examCategory.low == 0) {
                            alert("<spring:message code="essentail.lowChoose"/>")
                            this.scrollToTop()
                            return;
                        }

                        if (this.questions.questionData.length == 0) {
                            alert("<spring:message code="essentail.questionChoose"/>")
                            this.scrollToTop()
                            return;
                        }

                        for (let i = 0; i < this.questions.answers.length; i++) {

                            if (this.answerOptionType == 'number') {
                                this.questions.answers[i].optionNumber = i + 1
                            }
                            if (this.answerOptionType == 'eletter') {
                                this.questions.answers[i].optionNumber = this.options.eletter[i]
                            }
                            if (this.answerOptionType == 'kletter') {
                                this.questions.answers[i].optionNumber = this.options.kletter[i]
                            }
                        }

                        this.questions.actionType = this.actionType
                        this.questions.answerType = this.answerType

                        formData.append('datas', JSON.stringify(this.questions))

                        var language =  "<%=pageContext.getResponse().getLocale()%>"
                        var locale = ''
                        if (language == 'kr')
                        {
                            locale = 'kr-KR'
                        }
                        if(language == 'mn')
                        {
                            locale = 'mn-MN'
                        }
                        if(language == 'en'){
                            locale = 'en-EN'
                        }
                        const headers = {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                            'Accept-Language' : locale
                        }
                        this.loading('block')
                        axios.post('${BASEURL}/questions/create', formData, {headers: headers}).then((response) => {
                            if (response.data.status == 200) {
                                this.loading('none')
                                if (response.data.success == true) {
                                    if (this.actionType == 'insert') {
                                        this.clearData()
                                        this.scrollToTop()
                                        alert("<spring:message code="essentail.success"/>")
                                    } else {
                                        if (response.data.result.seted.length > 0) {
                                            this.seted = response.data.result.seted
                                            alert("this questions belongs to some exams !!")
                                            window.location.href = "#examListModal"
                                        } else {
                                            alert("<spring:message code="essentail.success"/>")
                                        }
                                    }
                                }
                            } else {
                                this.loading('none')
                                alert("<spring:message code="error.error" />!")
                            }
                        }, (error) => {
                            this.loading('none')
                            alert("<spring:message code="error.error" />!")
                            console.log(error);
                        });
                    } catch (error) {
                        alert("<spring:message code="error.error" />!")
                        console.log(error)
                    }
                },

                sayIt: function (event) {
                    this.$refs['mathfield'].$el.mathfield.$perform([
                        'speak',
                        'all',
                    ]);
                },
                setIt: function (event) {
                    this.formula = 'x=-b\\pm \\frac {\\sqrt{b^2-4ac}}{2a}';
                },
                ping: function () {
                    console.log('ping');
                },
                displayKeystroke: function (keystroke, _ev) {
                    this.keystroke = keystroke;
                    return true;
                },
                asSpokenText: function () {
                    return (
                        (this.$refs['mathfield'] &&
                            this.$refs['mathfield'].$el.mathfield &&
                            this.$refs['mathfield'].$el.mathfield.$text(
                                'spoken'
                            )) ||
                        ''
                    );
                },
            }
        });
    </script>
</div>
</body>
</html>

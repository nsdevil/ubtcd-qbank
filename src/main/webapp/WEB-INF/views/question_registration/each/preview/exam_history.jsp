<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href=" ${CSS}/common/header.css">
        <link rel="stylesheet" href=" ${CSS}/common/footer.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/preview.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/preview/preview.css">
        <script src="${JS}/preview.js"></script>
    </head>
    <body>

        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href="">문항등록</a></span>
                    <span><a href="">개별등록</a></span>
                </div> 
                
                <nav>
                    <ul>
                        <li>필수정보</li>
                        <li>문항카드</li>
                        <li>문항제작</li>
                        <li class="on">미리보기</li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                <div class="flex content-wrap flex-wrap">
                    <div class="left">

                        <div class="profile">
                            <div class="name">
                                <div>123456788</div>
                                <div>Hong-gildong</div>
                            </div>
                            <div class="test-info">
                                <div class="test-question">
                                    <div class="total">Total Question <span>100</span> | <span>50</span></div>
                                    <div class="time">00:50:00</div>
                                </div>
                                <div class="test-type">
                                    <div class="type"><span>A type</span>TEST Name</div>
                                    <div class="icon">
                                        <button><i class="setting"></i></button>
                                        <button><i class="speaker"></i></button>
                                        <button><i class="help"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="question">
                            <div class="title">01</div>
                            <div class="content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting Industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of</p>
                                <p class="content"><img src="${IMG}/icons/preview/example-image.png"></p>
                                <p class="refer"><i class="capture"></i>이미지를 Touch하시면 크게 보실 수 있습니다.</p>
                            </div>
                        </div>

                        <div class="answer">
							<ul>
								<li><span>1</span>Visual 
									<div class="delete">
										<button class="btn-gray" onclick="delete_type(event);">
											<i class="close"></i>
										</button>
									</div>
								</li>
								<li><span>2</span>Circumstantial
									<div class="delete">
										<button class="btn-gray" onclick="delete_type(event);">
											<i class="close"></i>
										</button>
									</div>
								</li>
								<li class="on"><span>3</span>Lorem Ipsum
									<div class="delete">
										<button class="btn-gray" onclick="delete_type(event);">
											<i class="close"></i>
										</button>
									</div>
								</li>
								<li><span>4</span>Lorem Ipsum
									<div class="delete">
										<button class="btn-gray" onclick="delete_type(event);">
											<i class="close"></i>
										</button>
									</div>
								</li>
							</ul>
						</div>

                        <div class="incoreect_note">
                            <div class="title">오답노트</div>
                            <div></div>
                        </div>

                        <div class="btn-wrap">
                            <button class="btn-white shadow" onclick="history.go(-1);"><i class="prev-gray"></i>이전단계</button> 
                            <button class="btn-red shadow"><i class="check-white"></i>이대로 등록</button>
                        </div>
                    </div>

                    <div class="right">
                        <div class="default">
                            <strong>기본정보<button onclick="default_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li>그룹 ID : g000001</li>
                                <li>문제 ID : qa000001</li>
                                <li>출제자 : OOO교수tept[pp)</li>
                                <li>등록일 : 2019-06-31</li>
                            </ul>
                        </div>

                        <div class="essential">
                            <strong>필수정보<button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li>[시험유형] 의학교육 필기시험</li>
                                <li>[문제유형] 객관식(단일정답형)<i class="close"></i></li>
                                <li>[문항공개범위] 내가 속한 대학 내 전체 공유<i class="close"></i></li>
                                <li>[외부공유] 해당사항 없음<i class="close"></i></li>
                                <li>[편집권한] 타 출제자 편집 미허용(사용만가능)<i class="close"></i></li>
                                <li>[마켓 공유 시 가격] 해당사항 없음<i class="close"></i></li>
                            </ul>
                        </div>

                        <div class="selected-list">
                            <strong>문항카드</strong>
                            <div class="flex">
                                <ul>
                                    <li># [교육과정]과정명 > 단원명 > 분류 3 꽃 길을 이것이야말로 살 귀는 그들에가 발휘하기 풍부하게<i class="close"></i></li>
                                    <li># [KAMC학습성과] 과학적 개념과 원리 중심 > 세포와 대사 > 세포의 구조를 설명할 수 있다. > 세포의 기본적인 구조를 그려서 설명할 수 있다 > 진핵 세포와 원핵 세포의 차이점을 설명할 수 있다.<i class="close"></i></li>
                                </ul>
                                <ul>
                                    <li># [지식수준] 암기<i class="close"></i></li>
                                    <li># [문항의 적설성] 필수적인(essential)<i class="close"></i></li>
                                    <li># [예상난이도] 매우 어려움<i class="close"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="proposer">
                            <ul>
                                <li><a href="../../../question_registration/each/preview/preview.jsp">추천 문항카드</a></li>
                                <li class="on"><a href="../../../question_registration/each/preview/exam_history.jsp">시험이력</a></li>
                                <li><a href="../../../question_registration/each/preview/change_history.jsp">변경이력</a></li>
                                <li><a href="../../../question_registration/each/preview/share_history.jsp">공유이력</a></li>
                            </ul>
                            <div class="table-wrap">
                                <table>
                                    <colgroup>
                                        <col>
                                        <col style="width : 110px;">
                                        <col style="width : 80px;">
                                        <col style="width : 85px;">
                                        <col style="width : 85px;">
                                        <col style="width : 70px;">
                                    </colgroup>
                                    <thead class="border-red-top-2 border-gray-bottom-2">
                                        <tr>
                                            <th>
												<div class="ps-relative">
													시험명
													<a href=""><i class="up"></i></a>
													<a href=""><i class="down"></i></a>
												</div>
                                            </th>
                                            <th>
												<div class="ps-relative">
                                                시험일
                                                <a href=""><i class="up"></i></a>
                                                <a href=""><i class="down"></i></a>
												</div>
                                            </th>
                                            <th>
												<div class="ps-relative">
                                                교시
                                                <a href=""><i class="up"></i></a>
                                                <a href=""><i class="down"></i></a>
												</div>
                                            </th>
                                            <th>
												<div class="ps-relative">
                                                응시자수
                                                <a href=""><i class="up"></i></a>
                                                <a href=""><i class="down"></i></a>
												</div>
                                            </th>
                                            <th>
												<div class="ps-relative">
                                                출제번호
                                                <a href=""><i class="up"></i></a>
                                                <a href=""><i class="down"></i></a>
												</div>
                                            </th>
                                            <th>
												<div class="ps-relative">
                                                정답
                                                <a href=""><i class="up"></i></a>
                                                <a href=""><i class="down"></i></a>
												</div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>내분비학 중간고사</td>
                                            <td>YYYY-MM-DD</td>
                                            <td>1교시</td>
                                            <td>100명</td>
                                            <td>1번</td>
                                            <td>3번</td>
                                        </tr>
                                        <tr>
                                            <td>내분비학 중간고사</td>
                                            <td>YYYY-MM-DD</td>
                                            <td>1교시</td>
                                            <td>100명</td>
                                            <td>1번</td>
                                            <td>3번</td>
                                        </tr>
                                        <tr>
                                            <td>내분비학 중간고사</td>
                                            <td>YYYY-MM-DD</td>
                                            <td>1교시</td>
                                            <td>100명</td>
                                            <td>1번</td>
                                            <td>3번</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </section>

    </body>
</html>
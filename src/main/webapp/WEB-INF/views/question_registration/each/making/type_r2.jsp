<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <link href="${CSS}/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href=" ${CSS}/lib/froala_editor/froala_editor.pkgd.min.css">
        <link rel="stylesheet" href=" ${CSS}/common/header.css">
        <link rel="stylesheet" href=" ${CSS}/common/footer.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/making.css">
        <script src=" ${JS}/lib/froala_editor/froala_editor.pkgd.min.js"></script>
        <script src="${JS}/making.js"></script>
    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                 <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href="">문항등록</a></span>
                    <span><a href="">개별등록</a></span>
                </div>

                <nav>
                    <ul>
                        <li>필수정보</li>
                        <li>문항카드</li>
                        <li class="on">문항제작</li>
                        <li>미리보기</li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                <div class="content-wrap flex">
					<aside class="type-r-left">
						<ul>
							<li><a href="javascript:;">공통</a></li>
							<li><a href="javascript:;">1</a></li>
							<li><a href="javascript:;">2</a></li>
							<li><a href="javascript:;">3</a></li>
							<li class="on"><a href="javascript:;">4</a></li>
							<li><a href="javascript:;">5</a></li>
							<li><a href="javascript:;">6</a></li>
							<li><a href="javascript:;">100</a></li>
							<li><a href="javascript:;">101</a></li>
						</ul>
						<button type="button" class="btn-add" onclick="btn_add_typeR();"><i class="add"></i></button>
					</aside>
                    <article class="type_r_cont">
						 <h2>객관식(R형) – 개별 요소</h2>
                        <h3>지식수준</h3>
						<div class="content">
							<ul class="click-btn">
								<li class="on" onclick="knowledge_level_select(this);">해당 사항 없음</li>
								<li onclick="knowledge_level_select(this);">암기</li>
								<li onclick="knowledge_level_select(this);">해석(판단)</li>
								<li onclick="knowledge_level_select(this);">해결</li>
							</ul>
                        </div>

                        <h3>예상 난이도</h3>
						<div class="content">
							<div class="level-stick flex flex-wrap" id="level-bar">
								<div>
									<span class="one">
										<span onclick="circle_left_dm(this);"></span>
									</span>
								</div>
								<div>
									<span class="two">
										<span onclick="circle_left_dm(this);"></span>
									</span>
								</div>
								<div>
									<span class="three">
										<span onclick="circle_left_dm(this);"></span>
									</span>
								</div>
								<div>
									<span class="four">
										<span onclick="circle_left_dm(this);"></span>
										<span onclick="circle_left_dm(this,1);"></span>
									</span>
								</div>
								<div class="circle" draggable="true" id="circle"></div>
							</div>
							<div class="level-stick-label flex flex-wrap">
								<div class="one"><span onclick="circle_left_dm(this);">매우 쉬움(1)</span></div>
								<div class="two"><span onclick="circle_left_dm(this);">쉬움(2)</span></div>
								<div class="three"><span onclick="circle_left_dm(this);">보통(3)</span></div>
								<div class="four"><span onclick="circle_left_dm(this);">어려움(4)</span></div>
								<div class="five"><span onclick="circle_left_dm(this);">매우 어려움(5)</span></div>
							</div>
						</div>
						<form name="" method="" action="">
							<input type="hidden" name="expected_difficulty">
						</form>

                        <h3>문항의 적절성</h3>
						<div class="content">
							<ul class="click-btn">
								<li class="on" onclick="question_relevance_select(this);">해당사항없음</li>
								<li onclick="question_relevance_select(this);">필수적인(essential)</li>
								<li onclick="question_relevance_select(this);">중요한(important)</li>
							</ul>
						</div>
                        <h3>문항 줄기  (필수)</h3>
                        <div class="content froala-wrap">
                            <div class="answer_froala"></div>
                        </div>

                        <h3>제시 자료(선택)</h3>
                        <div class="content">
                            <ul class="icon-list">
                                <li class="image" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/image.png"></li>
                                <li class="video" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/video.png"></li>
                                <li class="audio" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/audio.png"></li>
                                <li class="text" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/A.png"></li>
                                <li class="box-text" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/A-bg-white.png"></li>
                                <li class="stack" onclick="suggest_type_select(this);">STACK</li>
                                <li class="table" onclick="suggest_type_select(this);">Table</li>
                                <li class="VR" onclick="suggest_type_select(this);">VR</li>
                                <li class="example" onclick="suggest_type_select(this);">환자사례</li>
                                <li class="desc">
                                    ** 왼쪽 버튼을 클릭하면 제시 자료를 추가할 수 있습니다.
                                </li>
                            </ul>
                            <ul class="content-list">
                                <!-- 컨텐츠 추가영역 -->
                            </ul>
                        </div>

                        <h3>정답 체크(필수)</h3>
                        <div class="content">
                            <ul class="content-list answer-text">
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            1
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_1" class="checkbox" id="answer_1" value="y">
                                        <label for="answer_1"></label>
                                    </div>
									<div class="answer-text">
									Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다. Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다.
									</div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            2
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_2"  class="checkbox" id="answer_2" value="y">
                                        <label for="answer_2"></label>
                                    </div>
									<div class="answer-text">
									Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다. Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다.
									</div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            3
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_3" class="checkbox" id="answer_3" value="y">
                                        <label for="answer_3"></label>
                                    </div>
									<div class="answer-text">
									Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다. Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다.
									</div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            4
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_4" class="checkbox" id="answer_4" value="y">
                                        <label for="answer_4"></label>
                                    </div>
									<div class="answer-text">
									Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다. Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다.
									</div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            5
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_5" class="checkbox" id="answer_5" value="y">
                                        <label for="answer_5"></label>
                                    </div>
									<div class="answer-text">
									Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다. Heparin 투여받는 환자들은 투여 중지 혹은 protamine으로 heparin을 역전했다면 적어도 그 6시간 후까지 수술을 연기한다.
									</div>
                                </li>
                            </ul>

                        </div>

                        <h3>오답 노트 (선택)</h3>
                        <div class="content">
                            <ul class="icon-list">
                                <li class="text" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/A.png"></li>
                                <li class="image" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/image.png"></li>
                                <li class="video" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/video.png"></li>
                                <li class="audio" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/audio.png"></li>
                                <li class="desc">
                                    ** 왼쪽 버튼을 클릭하면 오답 노트를 추가할 수 있습니다.
                                </li>
                            </ul>

                            <ul class="content-list incorrect_note_wrap">
                                <!--컨텐츠내용추가-->
                            </ul>
                        </div>

                        <form name="" method="" action="">
                            <input type="hidden" name="knowledge_level">
                            <input type="hidden" name="expected_difficulty">
                        </form>

                        <div class="btn-wrap flex">
                            <button class="btn-white prev shadow" onclick="location.href='../../../question_registration/each/question_card/Individual_question_card.jsp';">
                                <i class="prev-gray"></i>이전단계
                            </button>
                            <div class="flex">
                                <button class="btn-red next shadow" onclick="location.href='../../../question_registration/each/preview/preview.jsp';">저장하고 다음으로<i class="next-white"></i></button>
                            </div>
                        </div>
                    </article>

                    <aside>
                        <button class="btn-red btn-temp shadow" onclick="tmp_preview();"><i class="check-white"></i>임시저장 후 미리보기</button>
                        <div class="essential">
                            <strong>선택한 필수정보<button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li class="test_type">[시험 유형] 의학교육 <span>지필고사</span></li>
                                <li class="question_type">[문제 유형] <span>객관식(단일정답형)</span></li>
                                <li class="public_scope">[문항공개범위] <span>내가 속한 대학 내 전체 공유</span></li>
                                <li class="edit_rights">[편집권한] <span>타 출제자(일반교수) 편집 미허용(사용만가능)</span></li>
                                <li class="external_sharing">[외부공유] <span>해당사항 없음</span></li>
								<li class="multimedia">[멀티미디어 자료 등록] <span>해당사항 없음</span></li>
                                <li class="price">[마켓 공유 시 가격] <span>해당사항 없음</span></li>
                            </ul>
                        </div>

                        <div class="selected">
                            <strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
                            <ul class="selected-list">
                                <li># [시험과목] 연도 > 학급 > 학년 > 학기 > 대분류 > 중분류 > 소분류<i class="close"></i></li>
                                <li># [교육과정] 과정명 > 대단원명 > 중단원명 > 소단원명<i class="close"></i></li>
                                <li># [KAMC학습성과] 아기가 보채요 > 보채는 아기를 진료할 때 신속한 치료가 필요한 전신성 원인들을 찾아낼 수 있고 신경계 원인인 경우 그 원인을 구별할 수 있다. > 1. 늘어진 아이의 의식을 평가할 수 있다.<i class="close"></i></li>
                                <li># [KAMC학습성과] 아기가 보채요 > 보채는 아기를 진료할 때 신속한 치료가 필요한 전신성 원인들을 찾아낼 수 있고 신경계 원인인 경우 그 원인을 구별할 수 있다. > 5. 진단과 치료계획을 설명할 수 있다.<i class="close"></i></li>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
            <div id="preview"></div>

            <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
            <script src="${JS}/expected_difficulty2.js"></script>
        </section>
    </body>
</html>

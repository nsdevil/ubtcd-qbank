<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link href="${CSS}/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href=" ${CSS}/lib/froala_editor/froala_editor.pkgd.min.css">
        <link rel="stylesheet" href=" ${CSS}/common/header.css">
        <link rel="stylesheet" href=" ${CSS}/common/footer.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/making.css">
        <script src=" ${JS}/lib/froala_editor/froala_editor.pkgd.min.js"></script>
        <script src="${JS}/making.js"></script>
    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                 <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href="">문항등록</a></span>
                    <span><a href="">개별등록</a></span>
                </div>

                <nav>
                    <ul>
                        <li>필수정보</li>
                        <li>문항카드</li>
                        <li class="on">문항제작</li>
                        <li>미리보기</li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                <div class="content-wrap flex">
                    <article>
						 <h2>객관식 (단일/복수정답형)</h2>
                        <h3>머리글 (선택)</h3>
                        <ul class="icon-list">
                            <li onclick="head_title_show()">
                                <img src="${IMG}/icons/question_registration/text.png">
                            </li>
                            <li class="desc">
                                ** 왼쪽 버튼을 클릭하면 머리글을 추가할 수 있습니다.
                            </li>
                        </ul>
                        <div class="content">
							<div class="flex align-items-center head-title-type">
								<span>머리글</span>
								<div class="radio-wrap">
									<input type="radio" class="radio" name="head-tt-type" id="head-tt-type1" value="직접입력">
									<label for="head-tt-type1" onclick="type_head_tt(this);">직접입력</label>
								</div>
								<div class="radio-wrap">
									<input type="radio" class="radio" name="head-tt-type" id="head-tt-type2" value="유형선택">
									<label for="head-tt-type2" onclick="type_head_tt(this);">유형선택</label>
								</div>
							</div>
							<div class="froala-wrap head-title-edit">
								<div id="froala"></div>
							</div>
							<div class="head-title-select select-list">
								<div class="flex align-items-center">
									<span>머리글 유형</span>
									<div class="form-slc flex-grow">
										<select name="headTitle" id="headTitle" class="select2">
											<option value="유형선택">유형선택</option>
											<option value="그림을 보고 ( )에 가장 알맞은 것을 고르시오">그림 보고 고르기</option>
											<option value="다음을 읽고 가장 관계 있는 것을 고르시오">지시문과 맞는 내용 고르기</option>
											<option value="밑줄 친 부분과 반대되는 뜻을 가진 것을 고르시오">대화를 읽고 반의어 고르기</option>
											<option value="밑줄 친 부분과 비슷한 뜻을 가진 것을 고르시오">대화를 읽고 유의어 고르기</option>
											<option value="무엇에 대한 이야기입니까? 가장 알맞은 것을 고르시오">짧은 글의 핵심 의미 파악하기</option>
											<option value="( )에 들어갈 가장 알맞은 것을 고르시오">어휘와 표현 의미 고르기</option>
											<option value="다음의 내용과 같은 것을 고르시오">일치하는 내용 고르기 </option>
											<option value="다음을 읽고 맞지 않는 것으로 고르시오">일치하지 않는 내용 고르기</option>
										</select>
									</div>
								</div>
								<div class="flex align-items-center">
									<span>머리글</span>
									<div class="form-ip flex-grow">
										<input type="text" id="headTitleIp" name="headTitleIp" value="" class="input" placeholder="유형을 선택하세요." readonly>
									</div>
								</div>
							</div>
                        </div>

                        <h3>문항 줄기  (필수)</h3>
                        <div class="content froala-wrap">
                            <div class="answer_froala"></div>
                        </div>

                        <h3>제시 자료(선택)</h3>
                        <div class="content">
                            <ul class="icon-list">
								<li class="text" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/A.png"></li>
                                <li class="image" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/image.png"></li>
                                <li class="video" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/video.png"></li>
                                <li class="audio" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/audio.png"></li>
<!--                                 <li class="box-text" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/A-bg-white.png"></li> -->
<!--                                 <li class="stack" onclick="suggest_type_select(this);">STACK</li> -->
<!--                                 <li class="table" onclick="suggest_type_select(this);">Table</li> -->
<!--                                 <li class="VR" onclick="suggest_type_select(this);">VR</li> -->
<!--                                 <li class="example" onclick="suggest_type_select(this);">환자사례</li> -->
                                <li class="desc">
                                    ** 왼쪽 버튼을 클릭하면 제시 자료를 추가할 수 있습니다.
                                </li>
                            </ul>
                            <ul class="content-list">
                                <!-- 컨텐츠 추가영역 -->
                            </ul>
                        </div>
                        <h3>답가지 및 정답 등록 (필수)</h3>
                        <div class="content answer-branch">
                            <ul class="icon-list">
                                <li class="text" onclick="brunch_suggest_type_select(this);"><img src="${IMG}/icons/question_registration/A.png"></li>
                                <li class="image"  onclick="brunch_suggest_type_select(this);"><img src="${IMG}/icons/question_registration/image.png"></li>
                                <li class="video"  onclick="brunch_suggest_type_select(this);"><img src="${IMG}/icons/question_registration/video.png"></li>
                                <li class="audio"  onclick="brunch_suggest_type_select(this);"><img src="${IMG}/icons/question_registration/audio.png"></li>
                                <li class="desc">
                                    ** 왼쪽 버튼을 클릭하면 유형을 변경할 수 있습니다.
                                </li>
                            </ul>
                            <ul class="content-list answer-froala-wrap">
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            1
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_1" class="checkbox" id="answer_1" value="y">
                                        <label for="answer_1"></label>
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            2
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_2"  class="checkbox" id="answer_2" value="y">
                                        <label for="answer_2"></label>
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            3
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_3" class="checkbox" id="answer_3" value="y">
                                        <label for="answer_3"></label>
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            4
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_4" class="checkbox" id="answer_4" value="y">
                                        <label for="answer_4"></label>
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                                <li class="flex">
                                    <div class="num">
                                        <div>
                                            5
                                        </div>
                                    </div>
                                    <div class="checkbox_wrap">
                                        <input type="checkbox" name="answer_5" class="checkbox" id="answer_5" value="y">
                                        <label for="answer_5"></label>
                                    </div>
                                    <div class="froala">
                                        <div class="answer_froala"></div>
                                    </div>
                                    <div class="delete">
                                        <button class="btn-gray" onclick="delete_type(event);">
                                            <i class="close"></i>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                            <div class="btn-add-answer"><button class="btn-white shadow" onclick="answer_branch_add();">+ 답가지 추가</button></div>
                        </div>
                        <h3>오답 노트 (선택)</h3>
                        <div class="content">
                            <ul class="icon-list">
                                <li class="text" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/A.png"></li>
                                <li class="image" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/image.png"></li>
                                <li class="video" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/video.png"></li>
                                <li class="audio" onclick="suggest_type_select(this);"><img src="${IMG}/icons/question_registration/audio.png"></li>
                                <li class="desc">
                                    ** 왼쪽 버튼을 클릭하면 오답 노트를 추가할 수 있습니다.
                                </li>
                            </ul>

                            <ul class="content-list incorrect_note_wrap">
                                <!--컨텐츠내용추가-->
                            </ul>
                        </div>

                        <div class="btn-wrap flex">
                            <button class="btn-white prev shadow" onclick="location.href='../../../question_registration/each/question_card/Individual_question_card.jsp';">
                                <i class="prev-gray"></i>이전단계
                            </button>
                            <div class="flex">
                                <button class="btn-red next shadow" onclick="location.href='../../../question_registration/each/preview/preview.jsp';">저장하고 다음으로<i class="next-white"></i></button>
                            </div>
                        </div>
                    </article>

                    <aside>
<!--                         <button class="btn-red btn-temp shadow" onclick="tmp_preview();"><i class="check-white"></i>임시저장 후 미리보기</button> -->
						<button class="btn-red btn-temp shadow" onclick=""><i class="check-white"></i>임시저장 후 미리보기</button>

                        <div class="essential">
                            <strong>선택한 필수정보 <button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li class="test_type">[시험 유형]<span>지필고사</span></li>
                                <li class="question_type">[문제 유형] <span>객관식(단일정답형)</span></li>
<!--                                 <li class="public_scope">[문항공개범위] <span>내가 속한 대학 내 전체 공유</span></li> -->
<!--                                 <li class="edit_rights">[편집권한] <span>타 출제자(일반교수) 편집 미허용(사용만가능)</span></li> -->
                                <li class="external_sharing">[외부공유] <span>해당사항 없음</span></li>
								<li class="multimedia">[멀티미디어 자료 등록] <span>해당사항 없음</span></li>
<!--                                 <li class="price">[마켓 공유 시 가격] <span>해당사항 없음</span></li> -->
                            </ul>
                        </div>

                        <div class="selected">
                            <strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
                            <ul class="selected-list">
                                <li># [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가<i class="close"></i></li>
                                <li># [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈<i class="close"></i></li>
								<li># [성취수준] 수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.<i class="close"></i></li>
								<li># [지식수준] 해석(판단)<i class="close"></i></li>
								<li># [예상난이도] 어려움(4)<i class="close"></i></li>
								<li># [문항의 적절성] 필수적인(essential)<i class="close"></i></li>
								<li># [주제어(자유 키워드)] 평가(임시)<i class="close"></i></li>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
            <div id="preview"></div>
        </section>

    </body>
</html>

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<link rel="stylesheet" href=" ${CSS}/question_registration/question_registration.css">
	<link rel="stylesheet" href=" ${CSS}/question_registration/register_information.css">
	<link href="${CSS}/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="${JS}/vue/mathlive/dist/mathlive.core.css">
	<link rel="stylesheet" href="${JS}/vue/mathlive/dist/mathlive.css">
	<link href="${CSS}/select2.min.css" rel="stylesheet" />
	<style>
		.sectionnone {
			display: none;
		}
		.froala-wrap{
			height: auto;
		}
		.sectiondisplay {
			display: block;
		}
		.option {
			margin-bottom: 20px;
		}
		.option .order {
			padding: 15px;
			border: 1px solid #eee;
		}

		.option .order label {
			padding: 8px;
			font-size: 17px;
		}

		.qbank-input {
			width: 85%;
			text-align: center;
			border-top: 0px;
			border-left: 0px;
			border-right: 0px;
			border-bottom: 1px solid #dee2ef;
		}
		.titBlack{
			color: black;
		}
		.titBlack:before {
			content: '';
			display: inline-block;
			width: 2px;
			height: 13px;
			background-color: black;
			margin-right: 10px;
		}
		.ckeditor .cke_1 .cke_inner >>> .cke_4_contents{
			height: 465px;
		}

		.froala {
			width: 100%;
		}

		.ck-rounded-corners .ck.ck-editor__main > .ck-editor__editable, .ck.ck-editor__main > .ck-editor__editable.ck-rounded-corners {
			min-height: 180px;
		}
		#se-pre-con {
			position: fixed;
			-webkit-transition: opacity 5s ease-in-out;
			-moz-transition: opacity 5s ease-in-out;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
		}
	</style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="questioncard" class="sectionnone">
	<div class="wrap bg-white pd-50 radius-5 shadow-wrap">
		<div class="breadcrumb">
			<span><a onclick="location.href='${HOME}/main';">
				<spring:message code="all.home"/>
			</a></span>
			<span><a href="../../question_registration/each/essential.jsp">
				<spring:message code="question.register.name"/></a></span>
		</div>
		<div class="content-wrap flex">
			<link rel="stylesheet" href="${CSS}/question_registration/each/question_card.css">
			<link rel="stylesheet" href="${CSS}/question_registration/each/making.css">

			<article style="padding-top: 0">
				<div class="table-wrap">
					<ul class="ul-pad">
						<li>
							<h2 style="margin: 0px">
								<spring:message code="question.register.name"/>
							</h2>
						</li>
					</ul>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<colgroup>
							<col width="5%">
							<col width="9%">
							<col width="9%">
							<col width="5%">
							<col width="4%">
							<col width="5%">
						</colgroup>
						<tbody>
						<tr class="border-red-top-2 border-gray-bottom-2">
							<td class="tit"><spring:message code="question.register.qType"/><span class="red">*
							</span></td>
							<td colspan="5">
								<button class="btn-check" :class="{ 'on': question.questionType == 1 }"
										v-on:click="typeChange(1)">
									<spring:message code="question.register.single"/>
								</button>
								<button class="btn-check" :class="{ 'on': question.questionType == 3 }"
										v-on:click="typeChange(3)">
									<spring:message code="question.register.multiple"/>
								</button>
								<button class="btn-check" :class="{ 'on': question.questionType == 2 }"
										v-on:click="typeChange(2)">
									<spring:message code="question.register.shortAnswer"/>
								</button>
								<button class="btn-check" :class="{ 'on': question.questionType == 4 }"
										v-on:click="typeChange(4)">
									<spring:message code="question.register.drawing"/>
								</button>
								<button class="btn-check" :class="{ 'on': question.questionType == 5 }"
										v-on:click="typeChange(5)">
									<spring:message code="question.register.image"/>
								</button>
							</td>
							<%--							<td>--%>
							<%--								<spring:message code="question.register.realNumber"/>--%>
							<%--							</td>--%>
							<%--							<td>--%>
							<%--								<input type="text" class="qbank-input" v-model="question.controlNo"--%>
							<%--									   placeholder="Control no">--%>
							<%--							</td>--%>
						</tr>
						<tr>
							<td class="tit"><spring:message code="question.register.mainCate"/><span class="red">*
							</span>
							</td>
							<td colspan="3">
								<select class="small-width" v-model="question.examCategory.main"
										@change="categoryChange('exam', 'main')">
									<option value="0" disabled><spring:message code="question.register.main"/></option>
									<template v-for="(item, index) in examCategory.main">
										<option :value="item.id" :key="index">{{item.name}}</option>
									</template>
								</select>
								<select class="small-width" v-model="question.examCategory.middle"
										@change="categoryChange('exam', 'middle')">
									<option value="0"
											disabled><spring:message code="question.register.middle"/></option>
									<template v-for="(item, index) in examCategory.middle">
										<option :value="item.id" :key="index">{{item.name}}</option>
									</template>
								</select>
								<select class="small-width" v-model="question.examCategory.low"
										@change="categoryChange('exam', 'low')">
									<option value="0" disabled><spring:message code="question.register.low"/></option>
									<template v-for="(item, index) in examCategory.low">
										<option :value="item.id" :key="index">{{item.name}}</option>
									</template>
								</select>
							</td>
							<td class="tit"><spring:message code="question.register.score"/><span class="red">*
							</span></td>
							<td>
								<input name="question_score" v-model="question.point" class="qbank-input" size="30"
									   type="number">
							</td>
						</tr>
						<%--						<tr>--%>
						<%--							<td class="tit"><spring:message code="question.register.subCate"/>--%>
						<%--							</td>--%>
						<%--							<td colspan="3">--%>
						<%--								<select class="small-width" v-model="question.subjectCategory.main"--%>
						<%--										@change="categoryChange('subject', 'main')">--%>
						<%--									<option value="0" disabled><spring:message code="question.register.main"/></option>--%>
						<%--									<template v-for="(item, index) in subjectCategory.main">--%>
						<%--										<option :value="item.id" :key="index">{{item.name}}</option>--%>
						<%--									</template>--%>
						<%--								</select>--%>
						<%--								<select class="small-width" v-model="question.subjectCategory.middle"--%>
						<%--										@change="categoryChange('subject', 'middle')">--%>
						<%--									<option value="0"--%>
						<%--											disabled><spring:message code="question.register.middle"/></option>--%>
						<%--									<template v-for="(item, index) in subjectCategory.middle">--%>
						<%--										<option :value="item.id" :key="index">{{item.name}}</option>--%>
						<%--									</template>--%>
						<%--								</select>--%>
						<%--								<select class="small-width" v-model="question.subjectCategory.low"--%>
						<%--										@change="categoryChange('subject', 'low')">--%>
						<%--									<option value="0" disabled><spring:message code="question.register.low"/></option>--%>
						<%--									<template v-for="(item, index) in subjectCategory.low">--%>
						<%--										<option :value="item.id" :key="index">{{item.name}}</option>--%>
						<%--									</template>--%>
						<%--								</select>--%>
						<%--							</td>--%>
						<%--							<td class="tit">--%>
						<%--								<spring:message code="question.register.cRate"/>--%>
						<%--							</td>--%>
						<%--							<td>--%>
						<%--								<input name="question_score" v-model="question.correctRate" class="qbank-input">--%>
						<%--								<span>%</span>--%>
						<%--							</td>--%>
						<%--						</tr>--%>
						<tr>
							<td class="tit"><spring:message code="question.register.qLevel"/><span class="red">*</span>
							</td>
							<td colspan="2">
								<button class="btn-check-small" :class="{ 'diff': question.questionLevel == 1 }"
										@click="diffChange('1') ">
									<spring:message code="question.register.vEasy"/>
								</button>
								<button class="btn-check-small" :class="{ 'diff': question.questionLevel == 2 }"
										@click="diffChange('2') ">
									<spring:message code="question.register.easy"/>
								</button>
								<button class="btn-check-small" :class="{ 'diff': question.questionLevel == 3 }"
										@click="diffChange('3') ">
									<spring:message code="question.register.normal"/>
								</button>
								<button class="btn-check-small" :class="{ 'diff':question.questionLevel == 4 }"
										@click="diffChange('4') ">
									<spring:message code="question.register.diff"/>
								</button>
								<button class="btn-check-small" :class="{ 'diff': question.questionLevel == 5 }"
										@click=diffChange('5') style="width: 86px;">
									<spring:message code="question.register.vDiff"/>
								</button>
							</td>
							<td class="tit"><spring:message code="question.register.knowledge"/></td>
							<td colspan="2">
								<button class="btn-check-auto" :class="{ 'diff': question.knowledgeLevel == 1 }"
										@click="knowledgeChange('1') ">
									<spring:message code="question.register.memoriz"/>
								</button>
								<button class="btn-check-auto" :class="{ 'diff': question.knowledgeLevel == 2 }"
										@click="knowledgeChange('2') ">
									<spring:message code="question.register.analyz"/>
								</button>
								<button class="btn-check-auto" :class="{ 'diff': question.knowledgeLevel == 3 }"
										@click="knowledgeChange('3') ">
									<spring:message code="question.register.solution"/>
								</button>
								<button class="btn-check-auto" :class="{ 'diff': question.knowledgeLevel == 4 }"
										@click="knowledgeChange('4') ">
									<spring:message code="question.register.na"/>
								</button>
							</td>
						</tr>

						<%--						<tr>--%>
						<%--							<td class="tit">--%>
						<%--								<spring:message code="question.register.keyword"/>--%>
						<%--							</td>--%>
						<%--							<td colspan="3">--%>
						<%--								<div class="flex" style="margin-top: 15px">--%>
						<%--									<input class="width88" v-model="question.keyword"--%>
						<%--										   placeholder="Write your keyword here."--%>
						<%--										   v-on:keyup.enter="inputKeyword()"--%>
						<%--										   style="border-top: 0px; border-left: 0px; border-right: 0px;--%>
						<%--                                    border-bottom: 1px solid #dee2ef;"/>--%>
						<%--									<button class="btn-search" @click="inputKeyword()"><spring:message--%>
						<%--											code="question.register.add"/></button>--%>
						<%--								</div>--%>

						<%--								<div style="display: inline-flex; margin: 10px; height: auto;">--%>
						<%--									<div v-for="(item, index) in question.keywords"--%>
						<%--										 v-show="question.keywords.length > 0" class="">--%>
						<%--										<span class="showKeyword">{{item}}--%>
						<%--											<a href="javascript:void(0)" class="removeKeyword"--%>
						<%--											   @click="removeKey(index)">X</a>--%>
						<%--										</span>--%>
						<%--									</div>--%>
						<%--								</div>--%>
						<%--							</td>--%>
						<%--							<td class="tit"><spring:message code="question.register.rela"/></td>--%>
						<%--							<td>--%>
						<%--								<div class="flex margin20">--%>
						<%--									<input id="essential" type="checkbox" class="checkbox"--%>
						<%--										   v-model="question.questionRevevance" value="1"/>--%>
						<%--									<label for="essential"><spring:message code="question.register.essential"/></label>--%>
						<%--									<input id="important" type="checkbox" class="checkbox"--%>
						<%--										   v-model="question.questionRevevance" value="2"/>--%>
						<%--									<label class="margin10" for="important"><spring:message--%>
						<%--											code="question.register.important"/></label>--%>
						<%--								</div>--%>
						<%--							</td>--%>
						<%--						</tr>--%>
						<%--						<tr>--%>
						<%--							<td class="tit"><spring:message code="question.register.topic"/>--%>
						<%--							</td>--%>
						<%--							<td colspan="5">--%>
						<%--								<input type="text" v-model="question.topic" placeholder="Please write your topic."--%>
						<%--									   class="qbank-input">--%>
						<%--							</td>--%>
						<%--						</tr>--%>
						<%--						<tr>--%>
						<%--							<td class="tit"><spring:message code="question.register.ref"/>--%>
						<%--							</td>--%>
						<%--							<td colspan="5">--%>
						<%--								<input type="text" v-model="question.reference"--%>
						<%--									   placeholder="Reference"--%>
						<%--									   class="qbank-input">--%>
						<%--							</td>--%>
						<%--						</tr>--%>
						</tbody>
					</table>
				</div>


				<%--				<div class="table-wrap">--%>
				<%--					<h3><spring:message code="qbank.cloudSetting"/></h3>--%>
				<%--					<table width="100%" border="0" cellspacing="0" cellpadding="0">--%>
				<%--						<colgroup>--%>
				<%--							<col width="4%">--%>
				<%--							<col width="8%">--%>
				<%--							<col width="6%">--%>
				<%--							<col width="10%">--%>
				<%--						</colgroup>--%>
				<%--						<tbody>--%>
				<%--						<tr class="border-red-top-2 border-gray-bottom-2">--%>
				<%--							<td class="tit">--%>
				<%--								<spring:message code="qbank.qType"/>--%>
				<%--							</td>--%>
				<%--							<td>--%>
				<%--								<div class="flex margin20">--%>
				<%--									<input name="cloudType" id="publicId" type="radio" class="radio"--%>
				<%--										   v-model="question.cloudQType" value="public"/>--%>
				<%--									<label for="publicId"><spring:message code="qbank.public"/></label>--%>
				<%--									<input name="cloudType" id="privateId" type="radio" class="radio"--%>
				<%--										   v-model="question.cloudQType" value="private"/>--%>
				<%--									<label class="margin10"--%>
				<%--										   for="privateId"><spring:message code="qbank.private"/></label>--%>
				<%--									<input name="cloudType" id="onlyMeId" type="radio" class="radio"--%>
				<%--										   v-model="question.cloudQType" value="onlyme"/>--%>
				<%--									<label class="margin10" for="onlyMeId"><spring:message code="qbank.onlyMe"/></label>--%>
				<%--								</div>--%>
				<%--							</td>--%>
				<%--							<td>--%>
				<%--								<spring:message code="qbank.permission"/>--%>
				<%--							</td>--%>
				<%--							<td>--%>
				<%--								<div class="flex margin20">--%>
				<%--									<input id="transId" type="checkbox" class="checkbox"--%>
				<%--										   v-model="question.permissions" value="translate"/>--%>
				<%--									<label for="transId"><spring:message code="qbank.trans"/></label>--%>
				<%--									--%>
				<%--									<input id="sellId" type="checkbox" class="checkbox"--%>
				<%--										   v-model="question.permissions" value="tosell"/>--%>
				<%--									<label class="margin10" for="sellId"><spring:message code="qbank.sell"/></label>--%>
				<%--									--%>
				<%--									<input id="editId" type="checkbox" class="checkbox"--%>
				<%--										   v-model="question.permissions" value="edit"/>--%>
				<%--									<label class="margin10"--%>
				<%--										   for="editId"><spring:message code="qbank.permission"/></label>--%>
				<%--								</div>--%>
				<%--							</td>--%>
				<%--						</tr>--%>
				<%--						<tr>--%>
				<%--							<td>--%>
				<%--								<spring:message code="qbank.qPrice"/>--%>
				<%--							</td>--%>
				<%--							<td style="border-right: 1px solid #dee2e6;">--%>
				<%--								<div class="flex margin20">--%>
				<%--									<input type="radio" class="radio" name="priceId" id="freeId"--%>
				<%--										   v-model="question.questionPrice" value="free"/>--%>
				<%--									<label for="freeId"> <spring:message code="qbank.free"/></label>--%>
				<%--									--%>
				<%--									<input type="radio" class="radio" name="priceId" id="paidId"--%>
				<%--										   v-model="question.questionPrice" value="true"/>--%>
				<%--									<label class="margin10" for="paidId"> <spring:message code="qbank.paid"/> </label>--%>
				<%--								</div>--%>
				<%--							</td>--%>
				<%--							<td v-show="question.questionPrice == 'true'">--%>
				<%--								<spring:message code="qbank.qPricePoint"/>--%>
				<%--							</td>--%>
				<%--							<td v-show="question.questionPrice == 'true'">--%>
				<%--								<div class="flex margin20">--%>
				<%--									<spring:message code="qbank.writePrice" var="questionPrice"/>--%>
				<%--									<input type="number" class="qbank-input"--%>
				<%--										   v-model="question.qInputPrice" placeholder="${questionPrice}"/>--%>
				<%--								</div>--%>
				<%--							</td>--%>
				<%--						</tr>--%>
				<%--						</tbody>--%>
				<%--					</table>--%>
				<%--				</div>--%>

				<h2 v-if="question.questionType == 1"><spring:message code="question.register.single"/></h2>
				<h2 v-if="question.questionType == 3"><spring:message code="question.register.multiple"/></h2>
				<h2 v-if="question.questionType == 2"><spring:message code="question.register.shortAnswer"/></h2>
				<h2 v-if="question.questionType == 4"><spring:message code="question.register.drawing"/></h2>
				<h2 v-if="question.questionType == 5"><spring:message code="question.register.image"/></h2>

				<h3 class="titBlack"><spring:message code="question.register.headerText"/></h3>
				<ul class="icon-list" style="margin-top: 10px; margin-bottom: 20px;">
					<li>
						<img src="${IMG}/icons/question_registration/A.png"  @click="headerClick()" >
					</li>
					<li class="desc">
						** <spring:message code="question.register.leftButtonText"/>
					</li>
				</ul>
				<div class="content" v-show="headerShow == true">
					<div class="flex align-items-center head-title-type">
						<span><spring:message code="question.register.headerText"/></span>
					</div>
					<div class="froala-wrap head-title-edit">
						<vue-ckeditor
								:config="config"
								@blur="onBlur($event)"
								@focus="onFocus($event)"
								@contentDom="onContentDom($event)"
								@dialogDefinition="onDialogDefinition($event)"
								@fileUploadRequest="onFileUploadRequest($event)"
								@fileUploadResponse="onFileUploadResponse($event)"
								tag-name="textarea"
								v-model="question.headerText"/>{{question.headerText}}

					</div>
				</div>
				<h3><spring:message code="question.register.question"/></h3>
				<div class="content froala-wrap">
					<vue-ckeditor
							:config="config"
							@blur="onBlur($event)"
							@focus="onFocus($event)"
							@contentDom="onContentDom($event)"
							@dialogDefinition="onDialogDefinition($event)"
							@fileUploadRequest="onFileUploadRequest($event)"
							@fileUploadResponse="onFileUploadResponse($event)"
							tag-name="textarea"
							v-model="question.questionData"/>{{question.questionData}}

				</div>

				<h3 class="titBlack"><spring:message code="question.register.pData"/></h3>
				<div class="content">
					<ul class="icon-list">
						<li class="text" v-on:click="presentDataAdd('text')"><img
								src="${IMG}/icons/question_registration/A.png"></li>
						<li class="text" v-on:click="presentDataAdd('math')"><img
								src="${IMG}/icons/math.png"></li>
						<li class="image" v-on:click="presentDataAdd('image')"><img
								src="${IMG}/icons/question_registration/image.png"></li>
						<li class="video" v-on:click="presentDataAdd('video')"><img
								src="${IMG}/icons/question_registration/video.png"></li>
						<li class="audio" v-on:click="presentDataAdd('audio')"><img
								src="${IMG}/icons/question_registration/audio.png"></li>
						<li class="desc">
							**<spring:message code="question.register.leftButtonText"/>.
						</li>
					</ul>
					<ul class="content-list">
						<template v-for="(item, index) in question.presentDatas">
							<%--math--%>
							<li v-if="item.type == 'math'" class="flex math" :key="index">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="froala">
									<mathlive-mathfield
											:id="'mf'+index"
											:ref="'mathfield'+index"
											:config="{smartMode:true, virtualKeyboardMode:'onfocus'}"
											@focus="ping"
											:on-keystroke="displayKeystroke"
											v-model="item.text"
											style="border: 1px solid #ccc;padding: 10px;font-size: 17px;">
										{{item.text}}
									</mathlive-mathfield>
								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removePresentData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>
							<%--text--%>
							<li v-if="item.type == 'text'" class="flex text" :key="index">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="froala">
									<vue-ckeditor
											:config="config"
											@blur="onBlur($event)"
											@focus="onFocus($event)"
											@contentDom="onContentDom($event)"
											@dialogDefinition="onDialogDefinition($event)"
											@fileUploadRequest="onFileUploadRequest($event)"
											@fileUploadResponse="onFileUploadResponse($event)"
											v-model="item.text"
									/>{{item.text}}

								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removePresentData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>
							<%--image--%>
							<li v-if="item.type == 'image'" class="flex suggest-image">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="type" style="overflow: hidden">
                                      <span v-if="item.url">
                                       <img class="data_img" :src="item.url"/>
                                    </span>
									<span v-else>
                                        <img v-if="item.fileName!= ''"
											 class="data_img"
											 :src="'https://ubt.mn/uploadingDir/question/'+item.fileName">
                                         <img v-else class="data_img" src="${IMG}/icons/question_registration/image.png" class="auto">
                                    </span>

								</div>
								<div class="file">
									<input v-model="item.imgDesc" type="hidden">
									<p><spring:message code="question.register.fileUpload"/></p>
									<button class="btn-white shadow" onclick="file_load(event);"><i
											class="media"></i><spring:message code="question.register.computer"/>
									</button>
									<%--<button class="btn-white shadow"--%>
									<%--onclick="popup2('./form/multimedia_data.jsp','multimedia_data','800','');">--%>
									<%--<i class="media"></i>멀티미디어자료 검색--%>
									<%--</button>--%>
									<input type="file" name="file" v-on:change="previewFiles($event, item)">
									<div class="source">{{item.fileName}}</div>
								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removePresentData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>
							<%--video--%>
							<li v-if="item.type == 'video'" class="flex suggest-video">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="type" style="overflow: hidden">
                                      <span v-if="item.url">
                                          <video controls><source :src="item.url"></video>
                                    </span>
									<span v-else>
                                        <video v-if="item.fileName!= ''"
											   class="data_img"
										>
                                            <source :src="'https://ubt.mn/uploadingDir/question/'+item.fileName"
											>
                                        </video>
                                         <img v-else class="data_img" src="${IMG}/icons/question_registration/play.png"
											  class="auto">
                                    </span>
								</div>

								<div class="file">
									<input v-model="item.imgDesc" name="keyword" type="hidden">
									<p><spring:message code="question.register.fileUpload"/></p>
									<button class="btn-white shadow" onclick="file_load(event);"><i
											class="media"></i><spring:message code="question.register.computer"/>
									</button>
									<%--<button class="btn-white shadow"--%>
									<%--onclick="popup2('./form/multimedia_data.jsp','multimedia_data','800','');">--%>
									<%--<i class="media"></i>멀티미디어자료 검색--%>
									<%--</button>--%>
									<input type="file" name="file" v-on:change="previewFiles($event, item)">
									<div class="source">{{item.fileName}}</div>
								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removePresentData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>
							<%--audio--%>
							<li v-if="item.type == 'audio'" class="flex suggest-audio">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="type" style="overflow: hidden">
                                      <span v-if="item.url">
                                          <audio controls><source :src="item.url"></audio>
                                    </span>
									<span v-else>
                                        <audio v-if="item.fileName!= ''"
											   class="data_img">
                                            <source :src="'https://ubt.mn/uploadingDir/question/'+item.fileName">
                                        </audio>
                                         <img v-else class="data_img"
											  src="${IMG}/icons/question_registration/type_audio.png"
											  class="auto">
                                    </span>
								</div>
								<div class="file">
									<p><spring:message code="question.register.fileUpload"/></p>
									<button class="btn-white shadow" onclick="file_load(event);"><i
											class="media"></i><spring:message code="question.register.computer"/>
									</button>
									<%--<button class="btn-white shadow"--%>
									<%--onclick="popup2('./form/multimedia_data.jsp','multimedia_data','800','');">--%>
									<%--<i class="media"></i>멀티미디어자료 검색--%>
									<%--</button>--%>
									<input type="file" name="file" v-on:change="previewFiles($event, item)">
									<div class="source">{{item.fileName}}</div>
								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removePresentData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>
						</template>
					</ul>
				</div>

				<div v-if="question.questionType == 4">
					<h3><spring:message code="question.register.drawData" /> </h3>
					<div class="content">
						<ul class="content-list">
							<template v-for="(item, index) in question.answerCanvas">
								<%--image--%>
								<li class="flex suggest-image" :key="index">
									<p class="uk-margin-remove" style="width: 110px"
									   v-if="index == 0"><b><spring:message code="question.register.trueData"/></b></p>
									<p class="uk-margin-remove" style="width: 110px" v-else><b>
										<spring:message code="question.register.drawData"/>
									</b></p>

									<div class="type" style="overflow: hidden">
                                      <span v-if="item.url">
                                       <img class="data_img" :src="item.url"/>
                                    </span>
										<span v-else>
                                        <img v-if="item.fileName!= ''"
											 class="data_img"
											 :src="'https://ubt.mn/uploadingDir/answer/'+item.fileName">
                                         <img v-else class="data_img" src="${IMG}/icons/question_registration/image.png" class="auto">
                                    </span>

									</div>
									<div class="file">
										<input v-model="item.imgDesc" type="hidden">
										<p><spring:message code="question.register.fileUpload"/></p>
										<button class="btn-white shadow" onclick="file_load(event)"><i
												class="media"></i><spring:message code="question.register.computer"/>
										</button>
										<input type="file" name="file" v-on:change="previewFiles($event, item)">
										<div class="source">{{item.fileName}}</div>
									</div>

								</li>
							</template>
						</ul>
					</div>
				</div>
				<div v-if="question.questionType == 5">
					<%--					<h3><spring:message code="question.register.image" /> </h3>--%>
					<div class="content">
						<h4><spring:message code="student.upload.picture"/></h4>
					</div>
				</div>

				<div v-else-if="question.questionType == 2">
					<h3><spring:message code="question.register.shortAnswer"/></h3>
					<ul class="icon-list">
						<li class="text" v-on:click="shortFieldChange('text')">
							<img src="${IMG}/icons/question_registration/A.png">
						</li>
						<li class="text" v-on:click="shortFieldChange('math')">
							<img src="${IMG}/icons/math.png"></li>
						<li class="desc">
							** <spring:message code="question.register.leftButtonText"/>
						</li>
					</ul>

					<div class="content">
						<template v-for="(item, index) in question.shortAnswers">
							<div class="input-wrap flex" :key="index">
								<spring:message code="all.trueData" var="trueData"/>
								<input v-if="item.fieldType == 'text'" v-model="item.trueData" type="text"
									   class="input" placeholder="${trueData}">
								<mathlive-mathfield
										v-if="item.fieldType == 'math'"
										:id="'mf'+index"
										ref="mathfield"
										:config="{smartMode:true, virtualKeyboardMode:'onfocus'}"
										@focus="ping"
										:on-keystroke="displayKeystroke"
										v-model="item.trueData"
										style="border-bottom: 1px solid rgb(204, 204, 204);padding: 10px;font-size: 17px;flex: 1;margin-right: 30px;">
									{{item.trueData}}
								</mathlive-mathfield>
								<%--								<spring:message code="all.hintText" var="hintText"/>--%>
								<%--								<input v-model="item.hint" type="text" class="input" placeholder="${hintText}">--%>
								<button v-if="index == 0" class="btn-white shadow btn-add-answer"
										v-on:click="addShortAnswer">
									<spring:message code="question.register.add"/>
								</button>
								<button v-else class="btn-white shadow btn-delete-answer"
										v-on:click="closeShortAnswer(index)">
									<spring:message code="question.register.delete"/>
								</button>
							</div>
						</template>
						<!-- <div class="btn-delete-answer"><button class="btn-white shadow">답안 삭제</button></div> -->
					</div>
				</div>
				<div v-else>
					<h3 class="titBlack"><spring:message code="question.register.answerSelectType"/></h3>
					<div class="content answer-branch">
						<div class="option">
							<div class="order">
								<label><input class="radio" v-model="answerOptionType" type="radio"
											  value="number"/>
									1,
									2, 3,
									4&nbsp;&nbsp;&nbsp;</label>
								<label> <input class="radio" v-model="answerOptionType" type="radio"
											   value="eletter"/>
									A, B, C, D&nbsp;&nbsp;&nbsp;</label>
								<label><input class="radio" v-model="answerOptionType" type="radio"
											  value="kletter"/>
									А, Б, В, Г</label>
							</div>
						</div>
						<h3><spring:message code="question.register.answer" /></h3>
						<ul class="icon-list">
							<li class="text" v-on:click="answerTypeChange('text')"><img
									src="${IMG}/icons/question_registration/A.png"></li>
							<li class="text" v-on:click="answerTypeChange('math')"><img
									src="${IMG}/icons/math.png"></li>
							<li class="image" v-on:click="answerTypeChange('image')"><img
									src="${IMG}/icons/question_registration/image.png"></li>
							<li class="video" v-on:click="answerTypeChange('video')"><img
									src="${IMG}/icons/question_registration/video.png"></li>
							<li class="audio" v-on:click="answerTypeChange('audio')"><img
									src="${IMG}/icons/question_registration/audio.png"></li>
							<li class="desc">
								** <spring:message code="question.register.leftButtonText"/>.
							</li>
						</ul>
						<ul class="content-list" :class="{ 'answer-froala-wrap': answerType=='text'}">
							<template v-for="(item, index) in question.answers">
								<%--text--%>
								<li v-if="answerType == 'text'" class="flex text" :key="index">
									<div class="num">
										<div>
											<template v-if="answerOptionType == 'number'">
												{{index+1}}
											</template>
											<template v-if="answerOptionType == 'eletter'">
												{{options.eletter[index]}}
											</template>
											<template v-if="answerOptionType == 'kletter'">
												{{options.kletter[index]}}
											</template>
										</div>
									</div>
									<div class="checkbox_wrap">
										<input v-on:click="checkAnswer(item)" :checked="item.correctAnswer == true"
											   type="checkbox" :id="'answer'+index" class="checkbox">
										<label :for="'answer'+index"></label>
									</div>
									<div class="froala">
										<vue-ckeditor
												:config="config"
												@blur="onBlur($event)"
												@focus="onFocus($event)"
												@contentDom="onContentDom($event)"
												@dialogDefinition="onDialogDefinition($event)"
												@fileUploadRequest="onFileUploadRequest($event)"
												@fileUploadResponse="onFileUploadResponse($event)"
												v-model="item.text" />
										<%--                                        :id="item.optionNumber"--%>
									</div>
									<div class="delete">
										<button class="btn-gray" v-on:click="closeAnswer(index)">
											<i class="close"></i>
										</button>
									</div>
								</li>

								<%--math--%>
								<li v-if="answerType == 'math'" class="flex math" :key="index">
									<div class="num">
										<div>
											<template v-if="answerOptionType == 'number'">
												{{index+1}}
											</template>
											<template v-if="answerOptionType == 'eletter'">
												{{options.eletter[index]}}
											</template>
											<template v-if="answerOptionType == 'kletter'">
												{{options.kletter[index]}}
											</template>
										</div>
									</div>
									<div class="checkbox_wrap">
										<input v-on:click="checkAnswer(item)" :checked="item.correctAnswer == true"
											   type="checkbox" :id="'answer'+index" class="checkbox">
										<label :for="'answer'+index"></label>
									</div>
									<div class="froala">
										<mathlive-mathfield
												:id="'mf'+index"
												ref="mathfield"
												:config="{smartMode:true, virtualKeyboardMode:'onfocus'}"
												@focus="ping"
												:on-keystroke="displayKeystroke"
												v-model="item.text"
												style="border: 1px solid #ccc;padding: 10px;font-size: 17px;">
											{{item.text}}
										</mathlive-mathfield>
									</div>
									<div class="delete">
										<button class="btn-gray" v-on:click="closeAnswer(index)">
											<i class="close"></i>
										</button>
									</div>
								</li>

								<%--image--%>
								<li v-if="answerType == 'image'" class="flex suggest-image">
									<div class="num">
										<div>
											<template v-if="answerOptionType == 'number'">
												{{index+1}}
											</template>
											<template v-if="answerOptionType == 'eletter'">
												{{options.eletter[index]}}
											</template>
											<template v-if="answerOptionType == 'kletter'">
												{{options.kletter[index]}}
											</template>
										</div>
									</div>
									<div class="checkbox_wrap">
										<input v-on:click="checkAnswer(item)" :checked="item.correctAnswer == true"
											   type="checkbox" :id="'answer'+index" class="checkbox">
										<label :for="'answer'+index"></label>
									</div>
									<div class="type" style="overflow: hidden">
                                        <span v-if="item.url">
                                           <img class="data_img" :src="item.url"/>
                                        </span>
										<span v-else>
                                        <img v-if="item.fileName!= ''"
											 class="data_img"
											 :src="'https://ubt.mn/uploadingDir/answer/'+item.fileName">
                                         <img v-else class="data_img" src="${IMG}/icons/question_registration/image.png" class="auto">
                                    </span>

									</div>
									<div class="file">
										<input v-model="item.imgDesc" type="hidden">
										<p><spring:message code="question.register.fileUpload"/></p>
										<button class="btn-white shadow" onclick="file_load(event);"><i
												class="media"></i><spring:message code="question.register.computer"/>
										</button>
										<%--<button class="btn-white shadow"--%>
										<%--onclick="popup2('./form/multimedia_data.jsp','multimedia_data','800','');">--%>
										<%--<i class="media"></i>멀티미디어자료 검색--%>
										<%--</button>--%>
										<input type="file" name="file" v-on:change="previewFiles($event, item)">
										<div class="source">{{item.fileName}}</div>
									</div>
									<div class="delete">
										<button class="btn-gray" v-on:click="closeAnswer(index)">
											<i class="close"></i>
										</button>
									</div>
								</li>
								<%--video--%>
								<li v-if="answerType == 'video'" class="flex suggest-video">
									<div class="num">
										<div>
											<template v-if="answerOptionType == 'number'">
												{{index+1}}
											</template>
											<template v-if="answerOptionType == 'eletter'">
												{{options.eletter[index]}}
											</template>
											<template v-if="answerOptionType == 'kletter'">
												{{options.kletter[index]}}
											</template>
										</div>
									</div>
									<div class="checkbox_wrap">
										<input v-on:click="checkAnswer(item)" :checked="item.correctAnswer == true"
											   type="checkbox" :id="'answer'+index" class="checkbox">
										<label :for="'answer'+index"></label>
									</div>
									<div class="type" style="overflow: hidden">
                                      <span v-if="item.url">
                                          <video controls><source :src="item.url"></video>
                                    </span>
										<span v-else>
                                        <video v-if="item.fileName!= ''"
											   class="data_img"
										>
                                            <source :src="'https://ubt.mn/uploadingDir/answer/'+item.fileName"
											>
                                        </video>
                                         <img v-else class="data_img" src="${IMG}/icons/question_registration/play.png"
											  class="auto">
                                    </span>
									</div>
									<div class="file">
										<input v-model="item.imgDesc" name="keyword" type="hidden">
										<p><spring:message code="question.register.fileUpload"/></p>
										<button class="btn-white shadow" onclick="file_load(event);"><i
												class="media"></i><spring:message code="question.register.computer"/>
										</button>
										<%--<button class="btn-white shadow"--%>
										<%--onclick="popup2('./form/multimedia_data.jsp','multimedia_data','800','');">--%>
										<%--<i class="media"></i>멀티미디어자료 검색--%>
										<%--</button>--%>
										<input type="file" name="file" v-on:change="previewFiles($event, item)">
										<div class="source">{{item.fileName}}</div>
									</div>
									<div class="delete">
										<button class="btn-gray" v-on:click="closeAnswer(index)">
											<i class="close"></i>
										</button>
									</div>
								</li>
								<%--audio--%>
								<li v-if="answerType == 'audio'" class="flex suggest-audio">
									<div class="num">
										<div>
											<template v-if="answerOptionType == 'number'">
												{{index+1}}
											</template>
											<template v-if="answerOptionType == 'eletter'">
												{{options.eletter[index]}}
											</template>
											<template v-if="answerOptionType == 'kletter'">
												{{options.kletter[index]}}
											</template>
										</div>
									</div>
									<div class="checkbox_wrap">
										<input v-on:click="checkAnswer(item)" :checked="item.correctAnswer == true"
											   type="checkbox" :id="'answer'+index" class="checkbox">
										<label :for="'answer'+index"></label>
									</div>
									<div class="type" style="overflow: hidden">
                                      <span v-if="item.url">
                                          <audio controls><source :src="item.url"></audio>
                                    </span>
										<span v-else>
                                        <audio v-if="item.fileName!= ''"
											   class="data_img"
										>
                                            <source :src="'https://ubt.mn/uploadingDir/answer/'+item.fileName"
											>
                                        </audio>
                                         <img v-else class="data_img"
											  src="${IMG}/icons/question_registration/type_audio.png"
											  class="auto">
                                    </span>
									</div>
									<div class="file">
										<input v-model="item.imgDesc" name="keyword" type="hidden">
										<p><spring:message code="question.register.fileUpload"/></p>
										<button class="btn-white shadow" onclick="file_load(event);"><i
												class="media"></i><spring:message code="question.register.computer"/>
										</button>
										<%--<button class="btn-white shadow"--%>
										<%--onclick="popup2('./form/multimedia_data.jsp','multimedia_data','800','');">--%>
										<%--<i class="media"></i>멀티미디어자료 검색--%>
										<%--</button>--%>
										<input type="file" name="file" v-on:change="previewFiles($event, item)">
										<div class="source">{{item.fileName}}</div>
									</div>
									<div class="delete">
										<button class="btn-gray" v-on:click="closeAnswer(index)">
											<i class="close"></i>
										</button>
									</div>
								</li>
							</template>
						</ul>
						<div class="btn-add-answer">
							<button v-on:click="addAnswer" class="btn-white shadow">+ <spring:message
									code="question.register.add"/></button>
						</div>
					</div>
				</div>
				<h3><spring:message code="question.register.wAnswer"/></h3>
				<div class="content">
					<ul class="icon-list">
						<li class="text" v-on:click="wrongDataAdd('text')"><img
								src="${IMG}/icons/question_registration/A.png"></li>
						<li class="text" v-on:click="wrongDataAdd('math')"><img
								src="${IMG}/icons/math.png"></li>
						<li class="image" v-on:click="wrongDataAdd('image')"><img
								src="${IMG}/icons/question_registration/image.png"></li>
						<li class="video" v-on:click="wrongDataAdd('video')"><img
								src="${IMG}/icons/question_registration/video.png"></li>
						<li class="audio" v-on:click="wrongDataAdd('audio')"><img
								src="${IMG}/icons/question_registration/audio.png"></li>
						<li class="desc">
							** <spring:message code="question.register.leftButtonText"/>
						</li>
					</ul>
					<ul class="content-list">
						<template v-for="(item, index) in question.wrongNotes">
							<%--text--%>
							<li v-if="item.type == 'text'" class="flex text" :key="index">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="froala">
									<vue-ckeditor
											:config="config"
											@blur="onBlur($event)"
											@focus="onFocus($event)"
											@contentDom="onContentDom($event)"
											@dialogDefinition="onDialogDefinition($event)"
											@fileUploadRequest="onFileUploadRequest($event)"
											@fileUploadResponse="onFileUploadResponse($event)" v-model="item.text"
									/>{{item.text}}
								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removeWrongData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>


							<%--math--%>
							<li v-if="item.type == 'math'" class="flex math" :key="index">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="froala">
									<mathlive-mathfield
											:id="'mf'+index"
											ref="mathfield"
											:config="{smartMode:true, virtualKeyboardMode:'onfocus'}"
											@focus="ping"
											:on-keystroke="displayKeystroke"
											v-model="item.text"
											style="border: 1px solid #ccc;padding: 10px;font-size: 17px;">
										{{item.text}}
									</mathlive-mathfield>
								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removeWrongData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>

							<%--image--%>
							<li v-if="item.type == 'image'" class="flex suggest-image">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="type" style="overflow: hidden">
                                      <span v-if="item.url">
                                       <img class="data_img" :src="item.url"/>
                                    </span>
									<span v-else>
                                        <img v-if="item.fileName!= ''"
											 class="data_img"
											 :src="'https://ubt.mn/uploadingDir/wrong/'+item.fileName"
										>
                                         <img v-else class="data_img" src="${IMG}/icons/question_registration/image.png" class="auto">
                                    </span>

								</div>
								<div class="file">
									<input v-model="item.imgDesc" type="hidden">
									<p><spring:message code="question.register.fileUpload"/></p>
									<button class="btn-white shadow" onclick="file_load(event);"><i
											class="media"></i><spring:message code="question.register.computer"/>
									</button>
									<%--<button class="btn-white shadow"--%>
									<%--onclick="popup2('./form/multimedia_data.jsp','multimedia_data','800','');">--%>
									<%--<i class="media"></i>멀티미디어자료 검색--%>
									<%--</button>--%>
									<input type="file" name="file" v-on:change="previewFiles($event, item)">
									<div class="source">{{item.fileName}}</div>
								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removeWrongData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>
							<%--video--%>
							<li v-if="item.type == 'video'" class="flex suggest-video">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="type" style="overflow: hidden">
                                      <span v-if="item.url">
                                          <video controls><source :src="item.url"></video>
                                    </span>
									<span v-else>
                                        <video v-if="item.fileName!= ''"
											   class="data_img"
										>
                                            <source :src="'https://ubt.mn/uploadingDir/wrong/'+item.fileName"
											>
                                        </video>
                                         <img v-else class="data_img" src="${IMG}/icons/question_registration/play.png"
											  class="auto">
                                    </span>
								</div>

								<div class="file">
									<input v-model="item.imgDesc" name="keyword" type="hidden">
									<p><spring:message code="question.register.fileUpload"/></p>
									<button class="btn-white shadow" onclick="file_load(event);"><i
											class="media"></i><spring:message code="question.register.computer"/>
									</button>
									<%--<button class="btn-white shadow"--%>
									<%--onclick="popup2('./form/multimedia_data.jsp','multimedia_data','800','');">--%>
									<%--<i class="media"></i>멀티미디어자료 검색--%>
									<%--</button>--%>
									<input type="file" name="file" v-on:change="previewFiles($event, item)">
									<div class="source">{{item.fileName}}</div>
								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removeWrongData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>
							<%--audio--%>
							<li v-if="item.type == 'audio'" class="flex suggest-audio">
								<div class="num">
									<div>{{index+1}}</div>
									<div class="count">
										<button onclick="up_list(this);"><i class="up-pointing"></i></button>
										<button onclick="down_list(this);"><i class="down-pointing"></i></button>
									</div>
								</div>
								<div class="type" style="overflow: hidden">
                                      <span v-if="item.url">
                                          <audio controls><source :src="item.url"></audio>
                                    </span>
									<span v-else>
                                        <audio v-if="item.fileName!= ''"
											   class="data_img"
										>
                                            <source :src="'https://ubt.mn/uploadingDir/wrong/'+item.fileName"
											>
                                        </audio>
                                         <img v-else class="data_img"
											  src="${IMG}/icons/question_registration/type_audio.png"
											  class="auto">
                                    </span>
								</div>
								<div class="file">
									<p><spring:message code="question.register.fileUpload"/></p>
									<button class="btn-white shadow" onclick="file_load(event);"><i
											class="media"></i><spring:message code="question.register.computer"/>
									</button>
									<%--<button class="btn-white shadow"--%>
									<%--onclick="popup2('./form/multimedia_data.jsp','multimedia_data','800','');">--%>
									<%--<i class="media"></i>멀티미디어자료 검색--%>
									<%--</button>--%>
									<input type="file" name="file" v-on:change="previewFiles($event, item)">
									<div class="source">{{item.fileName}}</div>
								</div>
								<div class="delete">
									<button class="btn-gray" v-on:click="removeWrongData(index)">
										<i class="close"></i>
									</button>
								</div>
							</li>
						</template>
					</ul>
				</div>

				<div class="btn-wrap" style="float: right;">
					<button class="btn-blue shadow" v-if="actionType == 'update'" @click="createQuestion()">
						<spring:message code="question.register.modify"/></button>
					<button class="btn-blue shadow" v-else @click="createQuestion()">
						<spring:message code="question.register.register"/></button>
				</div>
			</article>
		</div>
		<div id="examListModal" class="modalDialog">
			<div class="modal-mask">
				<div class="modal-wrapper">
					<div class="modal-container">
						<a href="#close" title="modelclose" class="modelclose">X</a>
						<div class="modal-header">
							<p><spring:message code="question.resultRemove" /></p>
						</div>
						<div class="modal-body">
							<div>
								<p>** <spring:message code="question.belongs" /> </p>
							</div>
							<div class="modal-table">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									   style="table-layout: auto;">
									<colgroup>
										<col width="4%">
										<col width="15%">
										<col width="30%">
										<col width="15%">
									</colgroup>
									<thead>
									<tr class="title_table border-red-top-2">
										<th>
											<span><spring:message code="all.number" /></span>
										</th>
										<th>
											<span><spring:message code="question.examId" /> </span>
										</th>
										<th>
											<span><spring:message code="examreg.examName" /></span>
										</th>
										<%--										<th>--%>
										<%--											<span>Variant</span>--%>
										<%--										</th>--%>
									</tr>
									</thead>
									<tbody>
									<tr v-for="(item,index) in seted" :key="index">
										<td>
											{{index+1}}
										</td>
										<td>
											{{item.examId}}
										</td>
										<td>
											{{item.examName}}
										</td>
										<%--										<td>--%>
										<%--											{{item.variant}}--%>
										<%--										</td>--%>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="modal-footer flex">
							<a href="#close" class="modal-default-button" type="button">Close</a>
							<button @click="resultClear()" class="modal-default-button margin-left"
									style="color: #fff; background: #fb5f33"
									type="button">
								<spring:message code="question.register.delete" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script src="${JS}/vue/vue-router.js"></script>
<script src="https://cdn.ckeditor.com/4.10.0/full-all/ckeditor.js"></script>
<script src="${JS}/making.js"></script>
<script src="${JS}/vue/ckeditor.js"></script>
<script src="${JS}/select2.min.js"></script>
<script>
	function file_load(e) {
		var btn = e.target;
		var is_button = $(btn).prop("tagName").toLowerCase() == "button";

		if (is_button) {
			$(btn).parent().children('input[type="file"]').click();
		} else {
			$(btn).parent().next().click();
		}

	}
</script>

<script type="module">
	"use strict"
	import MathLive from '${JS}/vue/mathlive/dist/src/mathlive.js'
	import Mathfield from '${JS}/vue/mathlive/dist/vue-mathlive.js'

	Vue.use(Mathfield, MathLive)
	var router = new VueRouter({
		mode: 'history',
		routes: []
	})
	var app = new Vue({
		router,
		// el: '#questioncard',
		components: {VueCkeditor},
		data() {
			return {
				formula: 'g(x)',
				keystroke: '',
				config: {
					toolbar: [
						['Undo', 'Redo', '-', 'Styles', 'Format', 'Bold', 'Italic', 'Underline', 'Strike', "-", "Subscript", "Superscript", "RemoveFormat", "Outdent", "Indent", 'NumberedList', 'BulletedList', 'BlockQuote', 'Link', 'Unlink', '-', 'Source']
					],
					// extraPlugins:'ckeditor_wiris',
					// allowedContent: true,
					height: 170
				},
				actionType: 'insert',
				examCategory: {
					main: [],
					middle: [],
					low: []
				},
				subjectCategory: {
					main: [],
					middle: [],
					low: []
				},
				answerType: 'text',
				answerOptionType: 'number',
				options: {
					kletter: ["가", "나", "다", "라", "마", "바", "사", "아", "자", "차",
						"카", "타", "파", "하"],
					eletter: ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
						"L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W",
						"X", "Y", "Z"]
				},
				question: {
					questionType: '1',
					correctRate: '100',
					controlNo: '',
					examCategory: {
						main: '0',
						middle: '0',
						low: '0'
					},
					bulletedListType: [
						{
							first: 'A',
							second: '',
							third: '',
							fourth: '',
							fifth: ''
						}
					],
					point: 1,
					subjectCategory: {
						main: '0',
						middle: '0',
						low: '0'
					},
					questionLevel: 3,
					estimatedCorrectRate: '',
					knowledgeLevel: 4,
					questionRevevance: [],
					topic: '',
					keywords: [],
					reference: '',
					headerText: '',
					questionData: '',
					presentDatas: [],
					wrongNotes: [],
					shortAnswers: [
						{
							hint: '...',
							trueData: '',
							fieldType: 'text'
						},
					],
					answerCanvas: [
						{
							file: null,
							text: '',
							imgDesc: '',
							correntAnswer: false,
							fileName: '',
						},
						{
							file: null,
							text: '',
							imgDesc: '',
							correntAnswer: false,
							fileName: '',
						}
					],
					answers: [
						{
							fileName: '',
							file: null,
							imgDesc: '',
							text: '',
							correctAnswer: false,
							optionNumber: '1'
						},
						{
							fileName: '',
							imgDesc: '',
							file: null,
							text: '',
							correctAnswer: false,
							optionNumber: '2'
						},
						{
							fileName: '',
							imgDesc: '',
							file: null,
							text: '',
							correctAnswer: false,
							optionNumber: '3'
						},
						{
							fileName: '',
							imgDesc: '',
							file: null,
							text: '',
							correctAnswer: false,
							optionNumber: '4'
						},
						{
							fileName: '',
							imgDesc: '',
							file: null,
							text: '',
							correctAnswer: false,
							optionNumber: '5'
						},

					],
					cloudQType: 'public',
					questionPrice: 'true',
					qInputPrice: 0
				},
				seted: [],
				examListModal: false,
				logindd: '',
				headerShow: false
			}
		},

		watch: {},

		computed: {},

		filters: {},

		mounted: function () {
			// let parameters = this.$route.query
			//  console.log(parameters)
			var element = document.getElementById('questioncard');
			element.classList.remove("sectionnone");
			element.classList.add("sectiondisplay");

			if (typeof this.$route.query.id != 'undefined' && this.$route.query.loginId != 'undefined') {
				console.log()
				let id = this.$route.query.id
				let logindd = this.$route.query.loginId

				<%--if ( logindd != '${sessionScope.S_LOGINID}')--%>
				<%--{--%>
				<%--	alert("<spring:message code="permisstionQuestionDenied"/>")--%>
				<%--	window.location.href = "${HOME}/error";--%>
				<%--}--%>
				// else{
				this.getQuestionDetail(id)
				this.actionType = 'update'
				// }
			} else {
				this.actionType = 'insert'
			}
			this.loadCates("main", 1, 0)
			this.loadCates("subject", 1, 0)
		},
		methods: {
			onBlur(evt) {
			},
			onFocus(evt) {
			},
			onContentDom(evt) {
			},
			onDialogDefinition(evt) {
			},
			onFileUploadRequest(evt) {
			},
			onFileUploadResponse(evt) {
			},
			headerClick(){
				this.headerShow=!this.headerShow

				console.log("this.headerShow" , this.headerShow)
			},
			inputKeyword() {
				this.question.keywords.push(this.question.keyword)
				this.question.keyword = ''
			},
			removeKey(indexs) {
				this.question.keywords.splice(indexs, 1)
			},
			shortFieldChange(type) {
				for (let i = 0; i < this.question.shortAnswers.length; i++) {
					let answer = this.question.shortAnswers[i];
					answer.fieldType = type;
				}
			},
			diffChange(type) {
				this.question.questionLevel = type
			},
			knowledgeChange(know) {
				this.question.knowledgeLevel = know
			},
			typeChange(type) {

				this.question.questionType = type

				if (this.question.questionType == '1') {
					for (let i = 0; i < this.question.answers.length; i++) {
						let answer = this.question.answers[i];
						if (answer.correctAnswer == true)
							answer.correctAnswer = false
					}
				}
			},
			getQuestionDetail(qId) {
				try{
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}

					axios.get('${BASEURL}/questions/' + qId, {
						params: {},
						headers: headers
					}).then((response) => {
						if (response.data.status == 200) {
							let questionDetail = response.data.result.questionDetail
							this.question.examCategory.main = questionDetail.cate1_code
							if (questionDetail.cate1_code != 0)
								this.categoryChange('exam', 'main')
							this.question.examCategory.middle = questionDetail.cate2_code
							if (questionDetail.cate2_code != 0)
								this.categoryChange('exam', 'middle')
							this.question.examCategory.low = questionDetail.cate3_code
							this.question.correctRate = questionDetail.correct_rate
							this.question.questionLevel = questionDetail.difficulty_level
							this.question.cloudQType = questionDetail.exportType
							this.question.questionPrice = questionDetail.free + ''
							this.question.qInputPrice = questionDetail.price
							this.question.headerText = questionDetail.headerText
							this.question.id = questionDetail.id
							this.question.keywords = []

							if (questionDetail.keywords.length > 0) {
								if (questionDetail.keywords.includes(","))
									this.question.keywords = questionDetail.keywords.split(',')
								else
									this.question.keywords[0] = questionDetail.keywords
							}

							this.question.knowledgeLevel = questionDetail.question_level
							this.question.point = questionDetail.point
							if (questionDetail.qRelevance.length > 0) {
								if (questionDetail.qRelevance.includes(","))
									this.question.questionRevevance = questionDetail.qRelevance.split(',')
								else
									this.question.questionRevevance[0] = questionDetail.qRelevance
							}

							this.question.questionData = questionDetail.question
							this.question.questionType = questionDetail.question_type
							this.question.reference = questionDetail.reference

							this.question.subjectCategory.main = questionDetail.scate1_code
							if (questionDetail.scate1_code != 0)
								this.categoryChange('subject', 'main')
							this.question.subjectCategory.middle = questionDetail.scate2_code
							if (questionDetail.scate2_code != 0)
								this.categoryChange('subject', 'middle')
							this.question.subjectCategory.low = questionDetail.scate3_code
							this.question.topic = questionDetail.topic
							this.question.controlNo = questionDetail.controlNo
							if (questionDetail.question_type != '4' && questionDetail.question_type != '2') {
								if (questionDetail.question_answers[0].optionNumber == 'A') {
									// alert("A")
									this.answerOptionType = 'eletter'
								} else if (questionDetail.question_answers[0].optionNumber == 'А') {
									this.answerOptionType = 'kletter'
								} else if (questionDetail.question_answers[0].optionNumber == '1') {
									this.answerOptionType = 'number'
								}
							} else {
								this.answerOptionType = 'number'
							}
							this.answerType = questionDetail.answerType


							this.question.answers = []
							for (let a = 0; a < questionDetail.question_answers.length; a++) {
								let item = {
									fileName: '',
									file: null,
									imgDesc: '',
									text: questionDetail.question_answers[a].answer,
									correctAnswer: false,
									optionNumber: questionDetail.question_answers[a].optionNumber
								}
								if (questionDetail.question_answers[a].media != null) {
									item.fileName = questionDetail.question_answers[a].media.filename
									item.imgDesc = questionDetail.question_answers[a].media.keyword
								}

								item.correctAnswer = questionDetail.question_answers[a].answerFlag == 'true' ? true : false
								this.question.answers.push(item)

							}

							if (questionDetail.question_type === '4' && questionDetail.question_answers.length > 0) {
								this.question.answerCanvas = []
								let item1 = {
									file: null,
									text: '',
									imgDesc: '',
									correntAnswer: true,
									fileName: questionDetail.question_answers[0].trueData,
								}
								let item2 = {
									file: null,
									text: '',
									imgDesc: '',
									correntAnswer: true,
									fileName: questionDetail.question_answers[0].drawingData,
								}

								this.question.answerCanvas.push(item1)
								this.question.answerCanvas.push(item2)


								this.question.answers = []
								for (let i = 0; i < 5; i++) {
									let item = {
										fileName: '',
										file: null,
										imgDesc: '',
										text: '',
										correctAnswer: false,
										optionNumber: 1
									}

									this.question.answers.push(item)
								}
							}
							if (questionDetail.question_type === '2' && questionDetail.question_answers.length > 0) {
								this.question.shortAnswers = []
								for (let i = 0; i < questionDetail.question_answers.length; i++) {
									let item = {
										trueData: questionDetail.question_answers[i].trueData,
										hint: questionDetail.question_answers[i].answer,
										fieldType: questionDetail.question_answers[i].fieldType
									}
									this.question.shortAnswers.push(item)
								}

								this.question.answers = []
								for (let i = 0; i < 5; i++) {
									let item = {
										fileName: '',
										file: null,
										imgDesc: '',
										text: '',
										correctAnswer: false,
										optionNumber: 1
									}
									this.question.answers.push(item)
								}
							}
							this.question.presentDatas = []

							for (let p = 0; p < questionDetail.question_datas.length; p++) {
								let type = questionDetail.question_datas[p].dataType;
								let filename = ""
								let imgDesc = ""
								let file = null
								if (questionDetail.question_datas[p].media != null) {
									type = questionDetail.question_datas[p].media.mediatype
									filename = questionDetail.question_datas[p].media.filename
									imgDesc = questionDetail.question_datas[p].media.keyword
									file = ""
								}
								let item = {
									fileName: filename,
									type: type,
									file: file,
									fileChanged: false,
									text: questionDetail.question_datas[p].data_text,
									imgDesc: imgDesc
								}

								this.question.presentDatas.push(item)
							}

							this.question.wrongNotes = []
							for (let p = 0; p < questionDetail.question_wrongs.length; p++) {
								let type = questionDetail.question_wrongs[p].dataType;
								let filename = "";
								let imgDesc = "";
								let file = null;
								if (questionDetail.question_wrongs[p].media != null) {
									type = questionDetail.question_wrongs[p].media.mediatype
									filename = questionDetail.question_wrongs[p].media.filename
									imgDesc = questionDetail.question_wrongs[p].media.keyword
									file = ""
								}
								let item = {
									fileName: filename,
									type: type,
									file: file,
									fileChanged: false,
									text: questionDetail.question_wrongs[p].data_text,
									imgDesc: imgDesc
								}

								this.question.wrongNotes.push(item)
							}
						} else {
							alert("ERROR: " + response.data.message)
						}
					}, (error) => {
						console.log(error);
					});
				}
				catch(error){
					alert(error)
				}

			},
			clearData() {
				this.question.questionRevevance = []
				this.question.topic = ""
				this.question.keywords = []
				this.question.reference = ""
				this.question.headerText = ""
				this.question.questionData = ""
				this.question.controlNo = ""
				this.question.presentDatas = []
				this.question.wrongNotes = []

				for (let a = 0; a < this.question.answers.length; a++) {
					this.question.answers[a].correctAnswer = false;
					this.question.answers[a].text = ""
					this.answerType = "text"
					this.question.answers[a].fileName = ""
					this.question.answers[a].file = null
					this.question.answers[a].imgDesc = ""
				}


			},
			addAnswer() {
				let item = {
					fileName: '',
					file: null,
					imgDesc: '',
					text: '',
					correctAnswer: false,
					optionNumber: '1'
				}
				this.question.answers.push(item)
			},
			addShortAnswer() {
				let type = 'text'
				if (this.question.shortAnswers.length > 0) {
					if (this.question.shortAnswers[0].fieldType != 'text') {
						type = 'math';
					}
				}
				let item = {
					trueData: '',
					hint: '...',
					fieldType: type
				}
				this.question.shortAnswers.push(item)
			},
			closeAnswer(index) {
				if (this.question.answers.length < 3) {
					alert("At least two views")
					return;
				}
				this.question.answers.splice(index, 1)
			},
			closeShortAnswer(index) {
				// if (this.question.shortAnswers.length < 2) {
				//     alert("At least one views")
				//     return;
				// }
				this.question.shortAnswers.splice(index, 1)
			},
			checkAnswer(item) {
				if (this.question.questionType == '1') {
					for (let i = 0; i < this.question.answers.length; i++) {
						let answer = this.question.answers[i];
						if (answer.correctAnswer == true)
							answer.correctAnswer = false
					}

					item.correctAnswer = !item.correctAnswer
				}
				if (this.question.questionType == '3') {
					item.correctAnswer = !item.correctAnswer
				}
			},
			removePresentData(index) {
				this.question.presentDatas.splice(index, 1)
			},
			removeWrongData(index) {
				this.question.wrongNotes.splice(index, 1)
			},
			categoryChange(type, catelevel) {

				if (type == 'exam') {
					type = 'main'
				} else {
					type = 'subject'
				}

				if (catelevel == 'main') {
					let parentId = 0
					if (type == 'main')
						parentId = this.question.examCategory.main
					else
						parentId = this.question.subjectCategory.main

					this.loadCates(type, 2, parentId)
				}
				if (catelevel == 'middle') {
					let parentId = 0
					if (type == 'main')
						parentId = this.question.examCategory.middle
					else
						parentId = this.question.subjectCategory.middle

					this.loadCates(type, 3, parentId)
				}

			},
			loading(cmd) {
				var l = document.getElementById("se-pre-con");
				l.style.display = cmd;
			},
			async loadCates(type, cateStep, parentId) {
				try {
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}
					var _this = this
					_this.loading('block')

					axios.get('${BASEURL}/category/liststep/qreg', {
						params: {
							type: type,
							cateStep: cateStep,
							parentId: parentId
						},
						headers: headers
					}).then((response) => {
						_this.loading('none')
						if (response.data.status === 200) {
							if (type == "main") {
								if (cateStep == 1) {
									_this.examCategory.main = response.data.result.cates
								}
								if (cateStep == 2) {
									_this.examCategory.middle = response.data.result.cates
								}
								if (cateStep == 3) {
									_this.examCategory.low = response.data.result.cates
								}
							} else {
								//subject category
								if (cateStep == 1) {
									_this.subjectCategory.main = response.data.result.cates
								}
								if (cateStep == 2) {
									_this.subjectCategory.middle = response.data.result.cates
								}
								if (cateStep == 3) {
									_this.subjectCategory.low = response.data.result.cates
								}
							}
						} else {
							alert("ERROR: " + response.data.message)
						}
					}, (error) => {
						console.log(error);
					});


				} catch (error) {
					console.log(error)
				}
			},

			presentDataAdd(type) {
				let item = {
					fileName: "",
					type: type,
					file: null,
					text: "",
					imgDesc: ""
				}
				this.question.presentDatas.push(item)
			},
			wrongDataAdd(type) {
				let item = {
					fileName: "",
					type: type,
					file: null,
					text: "",
					imgDesc: ""
				}
				this.question.wrongNotes.push(item)
			},
			answerTypeChange(type) {
				this.answerType = type;
			},
			scrollToTop() {
				window.scrollTo(0, 0)
			},

			async createQuestionExcel() {
				try {
					let formData = new FormData()
					if (this.excel.fileName == '') {
						alert("choose excel questions")
						return;
					}

					if (this.question.examCategory.main == '0') {
						alert("<spring:message code="selectMainCate"/>")
						return;
					}
					if (this.question.examCategory.middle == '0') {
						alert("<spring:message code="selectMidCate"/>")
						return;
					}
					if (this.question.examCategory.low == '0') {
						alert("<spring:message code="selectLowCate"/>")
						return;
					}
					formData.append('efile', this.excel.excelFile)
					formData.append('topcate', this.question.examCategory.main)
					formData.append('midcate', this.question.examCategory.middle)
					formData.append('lowcate', this.question.examCategory.low)
					const response = await QuestionService.createQuestionExcel(formData)

					if (response.data.status === 200) {
						alert("Success")
						this.excel.excelFile = null
						this.excel.fileName = ""
					} else {
						alert(response.data.message)
					}
				} catch (e) {
					console.log(e)
					alert(e.response.data.message)
				}
			},
			async createQuestion() {
				try {
					let formData = new FormData()
					let attachedPFile = true;
					let attachedPText = true;
					for (let i = 0; i < this.question.presentDatas.length; i++) {
						if (this.question.presentDatas[i].type != 'text' && this.question.presentDatas[i].type != 'math') {

							if (this.question.presentDatas[i].fileName == "") {
								attachedPFile = false;
								break;
							} else {
								console.log(this.question.presentDatas[i].file)
								formData.append('presentDataFiles', this.question.presentDatas[i].file)
							}
						} else {
							if (this.question.presentDatas[i].text == '') {
								attachedPText = false;
								break;
							}
						}
					}
					if (!attachedPText) {
						alert("<spring:message code="selectPresentDataText" />")
						return;
					}
					if (!attachedPFile) {
						alert("<spring:message code="selectPresentDataFiles" />")
						return;
					}

					if (this.question.questionType == 2) {
						let attachedFileC = true;
						for (let i = 0; i < this.question.shortAnswers.length; i++) {

							if (this.question.shortAnswers[i].trueData == '' || this.question.shortAnswers[i].hint == '') {
								attachedFileC = false;
								break;
							}
						}
						if (!attachedFileC) {
							alert("please enter short answer data")
							return;
						}
					}

					if (this.question.questionType == 4 || this.question.questionType == 2) {
						this.question.answers = []
						this.answerType = 'text'
						if (this.question.questionType == 4) {
							let attachedFileC = true;
							for (let i = 0; i < this.question.answerCanvas.length; i++) {

								if (this.question.answerCanvas[i].fileName == '') {
									attachedFileC = false;
									break;
								} else {
									// console.log(this.question.answers[i].file)
									formData.append('answerFiles', this.question.answerCanvas[i].file)
								}
							}
							if (!attachedFileC) {
								alert("<spring:message code="essentail.chooseFiles"/>")
								return;
							}
						}
					}

					if (this.question.questionType == 5){
						this.question.answers = []
						this.answerType = 'image'
					}

					if (this.answerType != 'text' && this.answerType != 'math') {
						let attachedFile = true;
						for (let i = 0; i < this.question.answers.length; i++) {

							if (this.question.answers[i].fileName == '') {
								attachedFile = false;
								break;
							} else {
								// console.log(this.question.answers[i].file)
								formData.append('answerFiles', this.question.answers[i].file)
							}

						}
						if (!attachedFile) {
							alert("<spring:message code="essentail.chooseFiles"/>")
							return;
						}
					}

					let attachedWFile = true;
					let attachedWText = true;
					for (let i = 0; i < this.question.wrongNotes.length; i++) {
						if (this.question.wrongNotes[i].type != 'text' && this.question.wrongNotes[i].type != 'math') {

							if (this.question.wrongNotes[i].fileName == "") {
								attachedWFile = false;
								break;
							} else {
								console.log(this.question.wrongNotes[i].file)
								formData.append('wrongDataFiles', this.question.wrongNotes[i].file)
							}
						} else {
							if (this.question.wrongNotes[i].text == '') {
								attachedWText = false;
								break;
							}
						}
					}
					if (!attachedWText) {
						alert("<spring:message code="selectPresentDataText" />")
						return;
					}
					if (!attachedWFile) {
						alert("<spring:message code="selectPresentDataFiles"/>")
						return;
					}

					if (this.question.questionType == 1 || this.question.questionType == 3) {
						let rightAnswerFlag = false;

						for (let i = 0; i < this.question.answers.length; i++) {
							if (this.question.answers[i].correctAnswer == true) {
								rightAnswerFlag = true;
								break;
							}
						}
						if (!rightAnswerFlag) {
							alert("<spring:message code="essential.chooseAnswer" />")
							return;
						}
					}

					if (this.question.examCategory.main == 0) {
						alert("<spring:message code="essential.cateChoose"/>")
						this.scrollToTop()
						return;
					}
					if (this.question.examCategory.middle == 0) {
						alert("<spring:message code="essentail.midChoose"/>")
						this.scrollToTop()
						return;
					}
					<%--if (this.question.examCategory.low == 0) {--%>
					<%--	alert("<spring:message code="essentail.lowChoose"/>")--%>
					<%--	this.scrollToTop()--%>
					<%--	return;--%>
					<%--}--%>

					if (this.question.questionData.length == 0) {
						alert("<spring:message code="essentail.questionChoose"/>")
						this.scrollToTop()
						return;
					}

					for (let i = 0; i < this.question.answers.length; i++) {

						if (this.answerOptionType == 'number') {
							this.question.answers[i].optionNumber = i + 1
						}
						if (this.answerOptionType == 'eletter') {
							this.question.answers[i].optionNumber = this.options.eletter[i]
						}
						if (this.answerOptionType == 'kletter') {
							this.question.answers[i].optionNumber = this.options.kletter[i]
						}
					}

					this.question.actionType = this.actionType
					this.question.answerType = this.answerType

					formData.append('datas', JSON.stringify(this.question))


					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}

					<%--$.blockUI({message: '<span> <spring:message code="all.pleaseWait" /> ...</span>'});--%>
					var _this = this
					_this.loading('block')

					axios.post('${BASEURL}/questions/create', formData, {headers: headers}).then((response) => {
						if (response.data.status == 200) {
							_this.loading('none')
							if (response.data.success == true) {
								if (_this.actionType == 'insert') {
									_this.clearData()
									_this.scrollToTop()
									alert("<spring:message code="essentail.success"/>")
									window.location.href = ""
								} else {
									if (response.data.result.seted.length > 0) {
										_this.seted = response.data.result.seted
										window.location.href = "#examListModal"
									} else {
										alert("<spring:message code="essentail.success"/>")
									}
								}
							}
						} else {
							_this.loading('none')
							alert("<spring:message code="error.error" />!")
						}
					}, (error) => {
						_this.loading('none')
						alert("<spring:message code="error.error" />!")
						console.log(error);
					});
				} catch (error) {
					alert("<spring:message code="error.error" />!")
					console.log(error)
				}
			},
			async resultClear(){
				try {
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}
					var _this = this
					_this.loading('block')
					axios.post('${BASEURL}/kexam/clean/result', {datas: this.seted},
							{headers: headers}).then((response)=> {
						if (response.data.status === 200) {
							if (response.data.success == true)
							{
								window.location.href = "#close";
								alert(response.data.message);
								_this.loading('none')
							}
						} else {
							_this.loading('none')
							alert("ERROR: " + response.data.message)
						}
					}, (error) => {
						_this.loading('none')
						console.log(error);
					});
				} catch (error) {
					console.log(error);
				}
			},
			previewFiles(event, item) {

				// console.log("files size")
				// console.log(event.target.files[0].size)
				var FileSize = event.target.files[0].size / 1024 / 1024;
				if (FileSize > 30)
				{
					alert("File max size must be 30mb.")
					return
				}

				item.file = event.target.files[0]
				item.fileName = event.target.files[0].name
				item.url = URL.createObjectURL(item.file);

				if (item.file){
					if ( /\.(jpe?g|png|gif|mp3|mp4)$/i.test( item.fileName) ) {
					}
					else{
						alert("Please check file format.!");
						item.fileName = ''
						item.url = ''
						return
					}
				}

				// event.target.files[0]
			},
			canvasFiles(event, item) {
				item.file = event.target.files[0]
				item.fileName = event.target.files[0].name
				// event.target.files[0]
				if (FileSize > 5)
				{
					alert("File max size must be 5MB.")
					return
				}
				if (item.file){
					if ( /\.(jpe?g|png|gif)$/i.test( item.fileName) ) {
					}
					else{
						alert("Please check file format.!");
						item.fileName = ''
						return
					}
				}
			},
			previewFileExcel(event) {

				this.excel.excelFile = event.target.files[0]
				this.excel.fileName = event.target.files[0].name

			},
			sayIt: function (event) {
				this.$refs['mathfield'].$el.mathfield.$perform([
					'speak',
					'all',
				]);
			},
			setIt: function (event) {
				this.formula = 'x=-b\\pm \\frac {\\sqrt{b^2-4ac}}{2a}';
			},
			ping: function () {
				console.log('ping');
			},
			displayKeystroke: function (keystroke, _ev) {
				this.keystroke = keystroke;
				return true;
			},
			asSpokenText: function () {
				return (
						(this.$refs['mathfield'] &&
								this.$refs['mathfield'].$el.mathfield &&
								this.$refs['mathfield'].$el.mathfield.$text(
										'spoken'
								)) ||
						''
				);
			},

		},
	})
	app.$mount('#questioncard')
</script>
</body>

</html>

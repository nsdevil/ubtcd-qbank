<%@ page contentType = "text/html;charset=utf-8" %>
<% String page_name = "curriculum"; %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href=" ${CSS}/common/header.css">
        <link rel="stylesheet" href=" ${CSS}/common/footer.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card/test_subject1.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card/curriculum.css">
    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href="">문항등록</a></span>
                    <span><a href="">개별등록</a></span>
                </div>

                <nav>
                    <ul>
                        <li>필수정보</li>
                        <li class="on">문항카드</li>
                        <li>문항제작</li>
                        <li>미리보기</li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                <div class="content-wrap flex">
                    <%@ include file="../../../common/question_registration/each/question_card/header.jsp" %>
                
                    <article>
                        <h3>교육과정</h3>
                        <div class="search">
                            <input type="text" name="search" class="search-input" placeholder="1click search" onkeyup="search_keyword(this);">
                           
                            <div class="search-sub">
                                <ul>
                                    <li onclick="curriculum_select(this);">
                                        <span class="course">수와 연산</span>
                                        <span class="unit">수의 연산</span>
                                        <span class="interruption">곱셈</span>
                                        <span class="subsection">세 자리 수의 덧셈과 뺄셈</span>
                                    </li>
                                    <li onclick="curriculum_select(this);">
                                        <span class="course">수와 연산</span>
                                        <span class="unit">수의 연산</span>
                                        <span class="interruption">곱셈</span>
                                        <span class="subsection">자연수의 곱셈과 나눗셈</span>
                                    </li>
                                    <li onclick="curriculum_select(this);">
                                        <span class="course">도형</span>
                                        <span class="unit">평면 도형</span>
                                        <span class="interruption">평면도형과 그 구성 요소</span>
                                        <span class="subsection">원의 구성 요소</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                            

                        <div class="search-list">
                            <ul>
                                <li>
                                    <select class="wd-100" name="course">
                                        <option value="" disabled selected>교육과정명</option>
                                        <option value="수와 연산">수와 연산</option>
										<option value="도형">도형</option>
										<option value="측정">측정</option>
										<option value="규칙성">규칙성</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="unit">
                                        <option value="" disabled selected>단원명</option>
                                        <option value="수의 체계">수의 체계</option>
										<option value="수의 연산">수의 연산</option>
										<option value="평면 도형">평면 도형</option>
										<option value="입체 도형">입체 도형</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="interruption">
                                        <option value="" disabled selected>중단원명</option>
                                        <option value="곱셈">곱셈</option>
										<option value="평면도형과 그 구성 요소">평면도형과 그 구성 요소</option>
										<option value="여러 가지 삼각형">여러 가지 삼각형</option>

                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="subsection">
                                        <option value="" disabled selected>소단원명</option>
                                        <option value="세 자리 수의 덧셈과 뺄셈">세 자리 수의 덧셈과 뺄셈</option>
										<option value="자연수의 곱셈과 나눗셈">자연수의 곱셈과 나눗셈</option>
										<option value="원의 구성 요소">원의 구성 요소</option>
									</select>
                                </li>
                            </ul>
                        </div>
                        <button class="btn-white plus shadow" onclick="curriculum_add();">+ 추가하기</button>

                        <form name="" method="" action=""></form>

                        <div class="btn-wrap flex">
                            <button class="btn-white prev shadow" onclick="go_prev_card('curriculum')"><i class="prev-gray"></i>이전단계</button> 
                            <div class="flex">
                                <button class="btn-gray delete shadow" onclick="card_cancel()">삭제</button>
                                <button class="btn-red next shadow" onclick="go_next_card('curriculum')">저장하고 다음으로<i class="next-white"></i></button> 
                            </div>
                        </div>
                    </article>

                    <aside>
                        <div class="essential">
                            <strong>선택한 필수정보 <button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li class="test_type">[시험 유형] 의학교육 <span>지필고사</span></li>
                                <li class="question_type">[문제 유형] <span>객관식(단일정답형)</span></li>
<!--                                 <li class="public_scope">[문항공개범위] <span>내가 속한 대학 내 전체 공유</span></li> -->
<!--                                 <li class="edit_rights">[편집권한] <span>타 출제자(일반교수) 편집 미허용(사용만가능)</span></li> -->
                                <li class="external_sharing">[외부공유] <span>해당사항 없음</span></li>
								<li class="multimedia">[멀티미디어 자료 등록] <span>해당사항 없음</span></li>
<!--                                 <li class="price">[마켓 공유 시 가격] <span>해당사항 없음</span></li> -->
                            </ul>
                        </div>

                        <div class="selected">
                            <strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
                            <ul class="selected-list">
                                <li># [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가<i class="close"></i></li>
<!--                                 <li># [교육과정] 과정명 > 대단원명 > 중단원명 > 소단원명<i class="close"></i></li> -->
                            </ul>
                        </div>
                    </aside>
                </div>
      
            </div>
        </section>

    </body>
</html>
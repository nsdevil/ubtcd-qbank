<%@ page contentType = "text/html;charset=utf-8" %>
<% String page_name = "expected_difficulty"; %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href=" ${CSS}/common/header.css">
        <link rel="stylesheet" href=" ${CSS}/common/footer.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card/knowledge_level.css">
    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                        <span><a href="../../../main">Home</a></span>
                        <span><a href="">문항등록</a></span>
                        <span><a href="../../../question_registration/each/essential">개별등록</a></span>
                </div>

                <nav>
                    <ul>
                        <li>필수정보</li>
                        <li class="on">문항카드</li>
                        <li>문항제작</li>
                        <li>미리보기</li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                <div class="content-wrap flex">
                    <%@ include file="../../../common/question_registration/each/question_card/header.jsp" %>

                    <article>
                        <h3>예상 난이도</h3>
                        <div class="level-stick flex flex-wrap" id="level-bar">
                            <div>
                                <span class="one">
                                    <span onclick="circle_left(this);"></span>
                                </span>
                            </div>
                            <div>
                                <span class="two">
                                    <span onclick="circle_left(this);"></span>
                                </span>
                            </div>
                            <div>
                                <span class="three">
                                    <span onclick="circle_left(this);"></span>
                                </span>
                            </div>
                            <div>
                                <span class="four">
                                    <span onclick="circle_left(this);"></span>
                                    <span onclick="circle_left(this,1);"></span>
                                </span>
                            </div>
                            <div class="circle" draggable="true" id="circle"></div>
                        </div>
                        <div class="level-stick-label flex flex-wrap">
                            <div class="one"><span onclick="circle_left(this);">매우 쉬움(1)</span></div>
                            <div class="two"><span onclick="circle_left(this);">쉬움(2)</span></div>
                            <div class="three"><span onclick="circle_left(this);">보통(3)</span></div>
                            <div class="four"><span onclick="circle_left(this);">어려움(4)</span></div>
                            <div class="five"><span onclick="circle_left(this);">매우 어려움(5)</span></div>
                        </div>

                        <button class="btn-white plus shadow" onclick="expected_difficulty_add()">+ 추가하기</button>
                        
                        <form name="" method="" action="">
                            <input type="hidden" name="expected_difficulty">
                        </form>
                       
                        <div class="btn-wrap flex">
                            <button class="btn-white prev shadow" onclick="go_prev_card('expected_difficulty')"><i class="prev-gray"></i>이전단계</button> 
                            <div class="flex">
                                <button class="btn-gray delete shadow" onclick="card_cancel()">삭제</button>
                                <button class="btn-red next shadow" onclick="go_next_card('expected_difficulty')">저장하고 다음으로<i class="next-white"></i></button> 
                            </div>
                        </div>
                    </article>

                    

                    <aside>
                        <div class="essential">
                            <strong>선택한 필수정보 <button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li class="test_type">[시험 유형] 의학교육 <span>지필고사</span></li>
                                <li class="question_type">[문제 유형] <span>객관식(단일정답형)</span></li>
<!--                                 <li class="public_scope">[문항공개범위] <span>내가 속한 대학 내 전체 공유</span></li> -->
<!--                                 <li class="edit_rights">[편집권한] <span>타 출제자(일반교수) 편집 미허용(사용만가능)</span></li> -->
                                <li class="external_sharing">[외부공유] <span>해당사항 없음</span></li>
								<li class="multimedia">[멀티미디어 자료 등록] <span>해당사항 없음</span></li>
<!--                                 <li class="price">[마켓 공유 시 가격] <span>해당사항 없음</span></li> -->
                            </ul>
                        </div>

                        <div class="selected">
                            <strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
                            <ul class="selected-list">
                                <li># [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가<i class="close"></i></li>
                                <li># [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈<i class="close"></i></li>
								<li># [성취수준] 수와 연산 > 나눗셈 > [4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.<i class="close"></i></li>
								<li># [지식수준] 해석(판단)<i class="close"></i></li>
                            </ul>
                        </div>
            
                    </aside>
                </div>
            </div>
            <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
            <script src="${JS}/expected_difficulty.js"></script>
        </section>

    </body>
</html>
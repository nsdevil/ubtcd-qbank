<%@ page contentType = "text/html;charset=utf-8" %>
<% String page_name = "learning_result"; %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href=" ${CSS}/common/header.css">
        <link rel="stylesheet" href=" ${CSS}/common/footer.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card/test_subject1.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card/learning_result.css">
    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href="">문항등록</a></span>
                    <span><a href="">개별등록</a></span>
                </div> 
                
                <nav>
                    <ul>
                        <li>필수정보</li>
                        <li class="on">문항카드</li>
                        <li>문항제작</li>
                        <li>미리보기</li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                <div class="content-wrap flex">
                <%@ include file="../../../common/question_registration/each/question_card/header.jsp" %>

                    <article>
                        <h3>성취기준</h3>
                        <div class="search">
                            <input type="text" name="search" class="search-input" placeholder="1click search" onkeyup="search_keyword(this);">
                            <div class="search-sub">
                                <ul>
                                    <li onclick="learining_result_select(this);">
                                        <span class="clinical_expression">수와 연산</span>
                                        <span class="learning_outcomes">나눗셈</span>
                                        <span class="learning_objective">[4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.</span>
                                    </li>
                                    <li onclick="learining_result_select(this);">
                                        <span class="clinical_expression">수와 연산</span>
                                        <span class="learning_outcomes">나눗셈</span>
                                        <span class="learning_objective">[4수01-08]나누는 수가 한 자리 수인 나눗셈의 계산 원리를 이해하고 그 계산을 할 수 있으며, 나눗셈에서 몫과 나머지의 의미를 안다.</span>
                                    </li>
                                    <li onclick="learining_result_select(this);">
                                        <span class="clinical_expression">수와 연산</span>
                                        <span class="learning_outcomes">나눗셈</span>
                                        <span class="learning_objective">[4수01-09]나누는 수가 두 자리 수인 나눗셈의 계산 원리를 이해하고 그 계산을 할 수 있다.</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="search-list">
                            <ul>
                                <li>
                                    <select class="wd-100" name="clinical_expression">
                                        <option value="" disabled selected>선택</option>
                                        <option value="수와 연산">수와 연산</option>
										<option value="도형">도형</option>
										<option value="측정">측정</option>
										<option value="규칙성">규칙성</option>
										<option value="자료와 가능성">자료와 가능성</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="learning_outcomes">
                                        <option value="" disabled selected>선택</option>
                                        <option value="다섯 자리 이상의 수">다섯 자리 이상의 수</option>
										<option value="세 자리 수의 덧셈과 뺄셈">세 자리 수의 덧셈과 뺄셈</option>
										<option value="곱셈">곱셈</option>
										<option value="나눗셈">나눗셈</option>
										<option value="분수">분수</option>
										<option value="소수">소수</option>
										<option value="분수와 소수의 덧셈과 뺄셈">분수와 소수의 덧셈과 뺄셈</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="learning_objective">
                                        <option value="" disabled selected>선택</option>
                                        <option value="[4수01-01]10000 이상의 큰 수에 대한 자릿값과 위치적 기수법을 이해하고, 수를 읽고 쓸 수 있다.">[4수01-01]10000 이상의 큰 수에 대한 자릿값과 위치적 기수법을 이해하고, 수를 읽고 쓸 수 있다.</option>
										<option value="[4수01-02]다섯 자리 이상의 수의 범위에서 수의 계열을 이해하고 수의 크기를 비교할 수 있다.">[4수01-02]다섯 자리 이상의 수의 범위에서 수의 계열을 이해하고 수의 크기를 비교할 수 있다.</option>
										<option value="[4수01-03]세 자리 수의 덧셈과 뺄셈의 계산 원리를 이해하고 그 계산을 할 수 있다.">[4수01-03]세 자리 수의 덧셈과 뺄셈의 계산 원리를 이해하고 그 계산을 할 수 있다.</option>
										<option value="[4수01-04]세 자리 수의 덧셈과 뺄셈에서 계산 결과를 어림할 수 있다.">[4수01-04]세 자리 수의 덧셈과 뺄셈에서 계산 결과를 어림할 수 있다.</option>
										<option value="[4수01-05]곱하는 수가 한 자리 수 또는 두 자리 수인 곱셈의 계산 원리를 이해하고 그 계산을 할 수 있다.">[4수01-05]곱하는 수가 한 자리 수 또는 두 자리 수인 곱셈의 계산 원리를 이해하고 그 계산을 할 수 있다.</option>
										<option value="[4수01-06]곱하는 수가 한 자리 수 또는 두 자리 수인 곱셈에서 계산 결과를 어림할 수 있다.">[4수01-06]곱하는 수가 한 자리 수 또는 두 자리 수인 곱셈에서 계산 결과를 어림할 수 있다.</option>
										<option value="[4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.">[4수01-07]나눗셈이 이루어지는 실생활 상황을 통하여 나눗셈의 의미를 알고, 곱셈과 나눗셈의 관계를 이해한다.</option>
										<option value="[4수01-08]나누는 수가 한 자리 수인 나눗셈의 계산 원리를 이해하고 그 계산을 할 수 있으며, 나눗셈에서 몫과 나머지의 의미를 안다.">[4수01-08]나누는 수가 한 자리 수인 나눗셈의 계산 원리를 이해하고 그 계산을 할 수 있으며, 나눗셈에서 몫과 나머지의 의미를 안다.</option>
										<option value="[4수01-09]나누는 수가 두 자리 수인 나눗셈의 계산 원리를 이해하고 그 계산을 할 수 있다.">[4수01-09]나누는 수가 두 자리 수인 나눗셈의 계산 원리를 이해하고 그 계산을 할 수 있다.</option>
									</select>
                                </li>
                            </ul>
                        </div>
                        <button class="btn-white plus shadow" onclick="learining_result_add()">+ 추가하기</button>
                        
                        <form name="" method="" action=""></form>
                        
                        <div class="btn-wrap flex">
                            <button class="btn-white prev shadow" onclick="go_prev_card('learning_result')"><i class="prev-gray"></i>이전단계</button> 
                            <div class="flex">
                                <button class="btn-gray delete shadow" onclick="card_cancel()">삭제</button>
                                <button class="btn-red next shadow" onclick="go_next_card('learning_result')">저장하고 다음으로<i class="next-white"></i></button> 
                            </div>
                        </div>
                    </article>
                    
                    <aside>
                        <div class="essential">
                            <strong>선택한 필수정보 <button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li class="test_type">[시험 유형] 의학교육 <span>지필고사</span></li>
                                <li class="question_type">[문제 유형] <span>객관식(단일정답형)</span></li>
<!--                                 <li class="public_scope">[문항공개범위] <span>내가 속한 대학 내 전체 공유</span></li> -->
<!--                                 <li class="edit_rights">[편집권한] <span>타 출제자(일반교수) 편집 미허용(사용만가능)</span></li> -->
                                <li class="external_sharing">[외부공유] <span>해당사항 없음</span></li>
								<li class="multimedia">[멀티미디어 자료 등록] <span>해당사항 없음</span></li>
<!--                                 <li class="price">[마켓 공유 시 가격] <span>해당사항 없음</span></li> -->
                            </ul>
                        </div>

                        <div class="selected">
                            <strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
                            <ul class="selected-list">
                                <li># [시험과목] 2020년 > 우리초등학교 > 5학년 > 1학기 > 3반 > A조 (김누리, 하예은, 최소은) > 수시평가<i class="close"></i></li>
                                <li># [교육과정] 수와 연산 > 수의 연산 > 곱셈 > 자연수의 곱셈과 나눗셈<i class="close"></i></li>
                            </ul>
                        </div>
                    </aside>

                </div>
            </div>
        </section>

    </body>
</html>
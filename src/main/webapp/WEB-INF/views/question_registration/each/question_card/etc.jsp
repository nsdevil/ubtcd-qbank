<%@ page contentType = "text/html;charset=utf-8" %>
<% String page_name = "etc"; %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href=" ${CSS}/common/header.css">
        <link rel="stylesheet" href=" ${CSS}/common/footer.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card/test_subject1.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card/etc.css">

    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="">Home</a></span>
                    <span><a href="">문항등록</a></span>
                    <span><a href="">개별등록</a></span>
                </div> 
                
                <nav>
                    <ul>
                        <li>필수정보</li>
                        <li class="on">문항카드</li>
                        <li>문항제작</li>
                        <li>미리보기</li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                

                <div class="content-wrap flex">
                    <%@ include file="../../../common/question_registration/each/question_card/header.jsp" %>
                    
                    <article>                        
                        <h3>수술 및 기타 수기</h3>
                        <div class="search-type2">
							<div class="flex flex-space-between">
								<input type="text" name="etc" class="search-input mb-30" placeholder="Search"  onclick="popup('./form/etc_search.jsp','etc_search','1350','900');">
								<button   onclick="popup('./form/etc_search.jsp','etc_search','1350','900');"  class="bth-search">검색</button>
							</div>                           
                        </div>
                        <button class="btn-white plus shadow" onclick="etc_add()">+ 추가하기</button>



                        <form name="" method="" action=""></form>
                        
                        <div class="btn-wrap flex">
                            <button class="btn-white prev shadow" onclick="go_prev_card('etc')"><i class="prev-gray"></i>이전단계</button> 
                            <div class="flex">
                                <button class="btn-gray delete shadow" onclick="card_cancel()">삭제</button>
                                <button class="btn-red next shadow" onclick="go_next_card('etc')">저장하고 다음으로<i class="next-white"></i></button> 
                            </div>
                        </div>
                    </article> 

                    <aside>
                        <div class="essential">
                            <strong>선택한 필수정보 <button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li class="test_type">[시험 유형] 의학교육 <span>지필고사</span></li>
                                <li class="question_type">[문제 유형] <span>객관식(단일정답형)</span></li>
                                <li class="public_scope">[문항공개범위] <span>내가 속한 대학 내 전체 공유</span></li>
                                <li class="edit_rights">[편집권한] <span>타 출제자(일반교수) 편집 미허용(사용만가능)</span></li>
                                <li class="external_sharing">[외부공유] <span>해당사항 없음</span></li>
								<li class="multimedia">[멀티미디어 자료 등록] <span>해당사항 없음</span></li>
                                <li class="price">[마켓 공유 시 가격] <span>해당사항 없음</span></li>
                            </ul>
                        </div>

                        <div class="selected">
                            <strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
                            <ul class="selected-list">
                                <li># [시험과목] 연도 > 학급 > 학년 > 학기 > 대분류 > 중분류 > 소분류<i class="close"></i></li>
                                <li># [교육과정] 과정명 > 대단원명 > 중단원명 > 소단원명<i class="close"></i></li>
                                <li># [KAMC학습성과] 아기가 보채요 > 보채는 아기를 진료할 때 신속한 치료가 필요한 전신성 원인들을 찾아낼 수 있고 신경계 원인인 경우 그 원인을 구별할 수 있다. > 1. 늘어진 아이의 의식을 평가할 수 있다.<i class="close"></i></li>
                                <li># [KAMC학습성과] 아기가 보채요 > 보채는 아기를 진료할 때 신속한 치료가 필요한 전신성 원인들을 찾아낼 수 있고 신경계 원인인 경우 그 원인을 구별할 수 있다. > 5. 진단과 치료계획을 설명할 수 있다.<i class="close"></i></li>
                            </ul>
                        </div>
                    </aside>
                </div>
             </div>
        </section>

    </body>
</html>
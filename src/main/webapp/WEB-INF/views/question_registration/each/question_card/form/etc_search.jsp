<%@ page contentType = "text/html;charset=utf-8" %>

        <div class="wrap wd1350">
            <div class="btn-close"><button onclick="popupClose();"><i class="close-red"></i></button></div>
			<div class="default-list-pop">
				<h2>수술 및 기타수기</h2>
                <div class="search-wrap flex">
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2" name="type" data-placeholder="구분">
                                <option value=""></option>
                                <option value="all" selected>전체</option>
                                <option value="num">번호</option>
                                <option value="num">수술 및 기타 수기명(국문)</option>
                                <option value="num">수술 및 기타 수기명(영문)</option>
                                <option value="num">저자</option>
                                <option value="num">소속기관</option>
                                <option value="num">연도</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
                </div>
				<div class="table-wrap">
					<table>
						<colgroup>
							<col style="width : 95px;"/>
							<col style="width : 290px;" />
							<col />
							<col style="width : 90px;"/>
							<col style="width : 140px;"/>
							<col style="width : 80px;"/>
							<col style="width : 90px;"/>
						</colgroup>
						<thead class="border-red-top-2 border-gray-bottom-2">
							<tr>
								<th>번호</th>
								<th>수술 및 기타 수기명(국문)</th>
								<th>수술 및 기타 수기명(영문)</th>
								<th>저자</th>
								<th>소속기관</th>
								<th>연도</th>
								<th>선택</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>2</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>3</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>4</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>5</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>6</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>7</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>8</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>9</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
							<tr>
								<td>10</td>
								<td class="name_kr t-left">복강경검사의 기본 수기</td>
								<td class="name_en t-left">A study on the correlation between</td>
								<td class="writer">류호상 </td>
								<td class="affiliation">고려대 의학대학<br/>내과학교실</td>
								<td class="year">2019</td>
								<td><div class="btn-wrap"><button type="button" class="btn-red-line" onclick="etc_select(this);">선택</button></div></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="pagination">
					<button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
					<button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
					<ul class="paginate-list">
						<li class="page-num active"><a href="#">1</a></li>
						<li class="page-num"><a href="#">2</a></li>
						<li class="page-num"><a href="#">3</a></li>
						<li class="page-num"><a href="#">4</a></li>
						<li class="page-num"><a href="#">5</a></li>
						<li class="page-num"><a href="#">6</a></li>
						<li class="page-num"><a href="#">7</a></li>
						<li class="page-num"><a href="#">8</a></li>
						<li class="page-num"><a href="#">9</a></li>
						<li class="page-num"><a href="#">10</a></li>
					</ul>
					<button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
					<button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
				</div>
			</div>
        </div>

<%@ page contentType = "text/html;charset=utf-8" %>
<% String page_name = "test_subject1"; %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href=" ${CSS}/common/header.css">
        <link rel="stylesheet" href=" ${CSS}/common/footer.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/question_registration.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card.css">
        <link rel="stylesheet" href=" ${CSS}/question_registration/each/question_card/test_subject1.css">
    </head>
    <body>
        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                        <span><a href="../../../main.jsp">Home</a></span>
                        <span><a href="">문항등록</a></span>
                        <span><a href="../../../question_registration/each/essential.jsp">개별등록</a></span>
                </div>

                <nav>
                    <ul>
                        <li>필수정보</li>
                        <li class="on">문항카드</li>
                        <li>문항제작</li>
                        <li>미리보기</li>
                    </ul>
                    <div class="group_breadcrumb">
                        그룹 ID : g000001 > 문제 ID : k000001 > Sub ID : s0001 > Version > 1.0
                    </div>
                </nav>

                <div class="content-wrap flex">
                    <%@ include file="../../../common/question_registration/each/question_card/header.jsp" %>

                    <article>
                        <h3>시험과목</h3>
                        <div class="search">
                            <input type="text" name="search" class="search-input" placeholder="1click search" onkeyup="search_keyword(this);">
                            <div class="search-sub">
                                <ul>
                                    <li onclick="test_subject_select(this);">
                                        <span class="year">2020년</span>
                                        <span class="major">우리초등학교</span>
                                        <span class="grade">5학년</span>
                                        <span class="term">1학기</span>
                                        <span class="main">3반</span>
                                        <span class="middle">A조 (김누리, 하예은, 최소은)</span>
                                        <span class="subclass">수시평가</span>
                                    </li>
                                    <li onclick="test_subject_select(this);">
                                        <span class="year">2020년</span>
                                        <span class="major">목운초등학교</span>
                                        <span class="grade">4학년</span>
                                        <span class="term">1학기</span>
                                        <span class="main">2반</span>
                                        <span class="middle">B조 (이찬원, 정동원, 신인선)</span>
                                        <span class="subclass">수시평가</span>
                                    </li>
                                    <li onclick="test_subject_select(this);">
                                        <span class="year">2020년</span>
                                        <span class="major">신석초등학교</span>
                                        <span class="grade">6학년</span>
                                        <span class="term">2학기</span>
                                        <span class="main">1반</span>
                                        <span class="middle">C조 (조인성, 차인표, 이서진)</span>
                                        <span class="subclass">중간평가</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="search-list">
                            <ul>
                                <li>
                                    <select class="wd-100" name="year">
                                        <option value="" disabled selected>연도</option>
										<option value="2020년">2020년</option>
                                        <option value="2019년">2019년</option>
										<option value="2018년">2018년</option>
										<option value="2017년">2017년</option>
										<option value="2016년">2016년</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="major">
                                        <option value="">학교(학급)</option>
                                        <option value="우리초등학교">우리초등학교</option>
                                        <option value="영훈초등학교">영훈초등학교</option>
										<option value="염리초등학교">염리초등학교</option>
										<option value="신월초등학교">신월초등학교</option>
										<option value="신석초등학교">신석초등학교</option>
										<option value="목운초등학교">목운초등학교</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="grade">
                                        <option value="" disabled selected>학년</option>
                                        <option value="1학년">1학년</option>
                                        <option value="2학년">2학년</option>
										<option value="3학년">3학년</option>
										<option value="4학년">4학년</option>
										<option value="5학년">5학년</option>
										<option value="6학년">6학년</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="term">
                                        <option value="" disabled selected>학기</option>
                                        <option value="1학기">1학기</option>
                                        <option value="2학기">2학기</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="main">
                                        <option value="" disabled selected>대분류</option>
                                        <option value="1반">1반</option>
                                        <option value="2반">2반</option>
										<option value="3반">3반</option>
										<option value="4반">4반</option>
										<option value="5반">5반</option>
										<option value="6반">6반</option>
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="middle">
                                        <option value="" disabled selected>중분류</option>
										<option value="A조 (김누리, 하예은, 최소은)">A조 (김누리, 하예은, 최소은)</option>
										<option value="B조 (이찬원, 정동원, 신인선)">B조 (이찬원, 정동원, 신인선)</option>
                                        <option value="C조 (조인성, 차인표, 이서진)">C조 (조인성, 차인표, 이서진)</option>
<!--                                         <option value="기초치의학">기초치의학</option> -->
                                    </select>
                                </li>
                                <li>
                                    <select class="wd-100" name="subclass">
                                        <option value="" disabled selected>소분류</option>
                                        <option value="중간평가">중간평가</option>
                                        <option value="수시평가">수시평가</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                        <button class="btn-white plus shadow" onclick='test_subject_add();'>+ 추가하기</button>
                        
                        <form name="" method="" action=""></form>
                        
                        <div class="btn-wrap flex">
                            <button class="btn-white prev shadow" onclick="go_prev_card('test_subject1')"><i class="prev-gray"></i>이전단계</button> 
                            <div class="flex">
                                <button class="btn-gray delete shadow" onclick="card_cancel()">삭제</button>
                                <button class="btn-red next shadow" onclick="go_next_card('test_subject1')">
                                    저장하고 다음으로<i class="next-white"></i></button>
                            </div>
                        </div>
                    </article>

                    <aside>
                        <div class="essential">
                            <strong>선택한 필수정보 <button onclick="selected_essential_show(this);"><i class="plus-bg-gray"></i></button></strong>
                            <ul>
                                <li class="test_type">[시험 유형] 의학교육 <span>지필고사</span></li>
                                <li class="question_type">[문제 유형] <span>객관식(단일정답형)</span></li>
<!--                                 <li class="public_scope">[문항공개범위] <span>내가 속한 대학 내 전체 공유</span></li> -->
<!--                                 <li class="edit_rights">[편집권한] <span>타 출제자(일반교수) 편집 미허용(사용만가능)</span></li> -->
                                <li class="external_sharing">[외부공유] <span>해당사항 없음</span></li>
								<li class="multimedia">[멀티미디어 자료 등록] <span>해당사항 없음</span></li>
<!--                                 <li class="price">[마켓 공유 시 가격] <span>해당사항 없음</span></li> -->
                            </ul>
                        </div>

                        <div class="selected">
                            <strong>선택한 문항카드<button onclick="selected_question_card_show(this);"><i class="minus-bg-gray"></i></button></strong>
                            <ul class="selected-list">
<!--                                 <li>#[시험과목] 연도 > 학급 > 학년 > 학기 > 대분류 > 중분류 > 소분류<i class="close"></i></li> -->
                            </ul>
                        </div>
            
                    </aside>
                </div>

                

            </div>
        </section>

    </body>
</html>
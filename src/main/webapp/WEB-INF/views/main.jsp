<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <link rel="stylesheet" href="${CSS}/progress-circle.css">
    <link rel="stylesheet" href="${CSS}/main.css" type="text/css">

    <script src="${JS}/progresscircle.js"></script>
    <script src="${JS}/vue/axios.js"></script>
    <script src="${JS}/Chart.min.js"></script>
<%--    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>--%>
    <style>
        .check div {
            width: 100%;
        }
        .check .box > div > div:last-child {
            display: inline;
        }
        .check .box > div > div:first-child {
            margin-bottom: 18px;
        }
        .check .box .detail {
            text-align: center;
        }
        thead th {
            border-top: 2px solid #fb5f33;
        }
        th {
            font-size: 14px;
            font-weight: 700;
            color: #000;
        }
        td {
            border-left: 1px solid #dee2e6 !important;
            border-right: 1px solid #dee2e6;
            color: #000;
        }
        .newLi {
            justify-content: left;
            padding-left: 10px;
        }

        .margin-small {
            margin-left: 10px;
            margin-right: 5px;
        }
    </style>
</head>
<body>
<section id="main">
    <div class="wrap">
        <article class="check bg-white radius-5">
            <div class="box flex">
                <div class="detail" v-if="'${sessionScope.S_ROLE}' == 'ROLE_ADMIN'">
                    <div><spring:message code="main.schoolCount"/></div>
                    <div>{{schCount}}</div>
                </div>
                <div class="detail">
                    <div><spring:message code="main.classCount"/></div>
                    <div>{{gCount}}</div>
                </div>
                <div class="detail">
                    <div><spring:message code="main.teacherCount"/></div>
                    <div>{{tCount}}</div>
                </div>
                <div class="detail">
                    <div><spring:message code="main.studentCount"/></div>
                    <div>{{sCount}}</div>
                </div>
                <div class="detail">
                    <div><spring:message code="main.examCount"/></div>
                    <div>{{eCount}}</div>
                </div>
                <div class="detail">
                    <div><spring:message code="main.questionCount"/></div>
                    <div>{{qCount}}</div>
                </div>
                <div class="detail">
                    <div><spring:message code="main.firstSubmit"/></div>
                    <div>{{firstSubmit}}</div>
                </div>
                <div class="detail">
                    <div><spring:message code="main.retakeSubmit"/></div>
                    <div>{{secondSubmit}}</div>
                </div>
                <div class="cok" style="text-align: center;">
                    <div><spring:message code="question.register.name"/></div>
                    <div>
                        <a href="${HOME}/question_registration/each/essential">+</a>
                    </div>
                </div>
            </div>
        </article>

        <div class="flex flex-wrap">
            <article class="ing wd-100">
                <h2 class="flex">
                    <spring:message code="loginUserInfo"/>
                    <a href="" class="more"><span class="dot"></span><span class="dot"></span><span class="dot"></span></a>
                </h2>
                <div class="flex content">
                    <div class="bg-white radius-5 flex-1 complete">
                        <%--						<div class="top flex">--%>
                        <%--							<div class="clock"><img src="${IMG}/icons/main/clock.png" class="auto"></div>--%>
                        <ul class="list">
                            <li><span v-if="'${sessionScope.S_ROLE}' == 'ROLL_ADMIN'"><spring:message
                                    code="userRegisteredAll"/></span>
                                <span v-else><spring:message code="schoolRegisterAll"/></span>: <span id="totalUser"
                                                                                                      class="margin-small"></span>
                            </li>
                            <li><spring:message code="userLogin"/> : <span id="signUser" class="margin-small"></span>
                            </li>
                        </ul>
                        <%--						</div>--%>
                        <div class="middle flex flex-center">
                            <canvas id="doughnut-chart" width="400" height="250">
                                <p id="percent"></p>
                            </canvas>
                        </div>
                    </div>
                    <div class="bg-white radius-5 flex-1 complete">
                        <ul class="list">
                            <li><spring:message code="examTakeUsers"/></li>
                            <li class="newLi">1.<span id="firstWKS" class="margin-small"></span> - <span id="firstWKE"
                                                                                                         class="margin-small"> </span>
                                : <span id="firstWK" class="margin-small"></span></li>
                            <li class="newLi">2.<span id="secondWKS" class="margin-small"></span> - <span id="secondWKE"
                                                                                                          class="margin-small"> </span>
                                : <span id="secondWK" class="margin-small"></span></li>
                            <li class="newLi">3.<span id="thirdWKS" class="margin-small"></span> - <span id="thirdWKE"
                                                                                                         class="margin-small"> </span>
                                : <span id="thirdWK" class="margin-small"></span></li>
                        </ul>
                        <div class="graph">
                            <canvas id="bar-chart-grouped" width="400" height="250"></canvas>
                        </div>
                    </div>
                </div>
            </article>

        </div>
    </div>
    <div class="wrap bg-white pd-50 radius-5 shadow-wrap" style="margin-top: 30px;">
        <div class="breadcrumb">
            <div class="table-wrap">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <colgroup>
                        <col width="3%">
                        <col width="14%">
                        <col width="6%">
                        <col width="6%">
                    </colgroup>
                    <thead>
                    <tr>
                        <th><spring:message code="all.number"/></th>
                        <th><spring:message code="schoollist.schoolName"/></th>
                        <th><spring:message code="main.examCount"/></th>
                        <th><spring:message code="all.questionCount"/></th>
                    </tr>
                    </thead>
                    <tbody v-if="qeCount.length > 0 ">
                    <tr class="border-red-top-2 border-gray-bottom-2" v-for="(item,index) in qeCount">
                        <td v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_SCHCODE}' == item.sch_code">
                            {{index+1}}
                        </td>
                        <td class="tit"
                            v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_SCHCODE}' == item.sch_code">
                            {{item.sch_name}}
                        </td>
                        <td class="tit"
                            v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_SCHCODE}' == item.sch_code">
                            {{item.ecount}}
                        </td>
                        <td class="tit"
                            v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_SCHCODE}' == item.sch_code">
                            {{item.qcount}}
                        </td>
                    </tr>
                    </tbody>
                    <tbody v-else>
                    <tr>
                        <td colspan="4"><spring:message code="all.empty"/></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {

        if (document.getElementsByClassName('.circle-graph .circle')) {
            $('.circle-graph .circle').circlechart();
        }

        var firstweek = '';
        var fwstartdate = '';
        var fwenddate = '';

        var secondweek = '';
        var sdstartdate = '';
        var sdenddate = '';

        var thirdweek = '';
        var tdstartdate = '';
        var tdenddate = '';

        //	low level school here

        var language = "<%=pageContext.getResponse().getLocale()%>"
        var locale = ''
        if (language == 'kr') {
            locale = 'kr-KR'
        }
        if (language == 'mn') {
            locale = 'mn-MN'
        }
        if (language == 'en') {
            locale = 'en-EN'
        }
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
            'Accept-Language': locale
        }

        var _$ = $

        axios.get('${BASEURL}/dashboard/signincount', {
            params: {}, headers: headers
        }).then(function (response) {
            _$.unblockUI()
            if (response.data.status == 200) {
                if (response.data.success == true) {

                    var totaluser = response.data.result.totalUser;
                    var totalsign = response.data.result.totalSignIn;

                    this.score = (this.items / this.total) * 100;

                    $('.percent').text(totalsign + '/' + totaluser);
                    $('#dateNow').text(Date.now());

                    $('#signUser').text(totalsign);
                    $('#totalUser').text(totaluser);

                    new Chart(document.getElementById("doughnut-chart"), {
                        type: 'doughnut',
                        data: {
                            labels: ["Хандалт хийгээгүй", "Нэвтэрч орсон хүн"],
                            datasets: [
                                {
                                    label: "Нийт хэрэглэгч : ",
                                    backgroundColor: ["#AAAAAA", "#fb5f33"],
                                    data: [totaluser - totalsign, totalsign]
                                }
                            ]
                        },
                        options: {
                            title: {
                                display: true,
                                // text: ''
                            }
                        }
                    });
                } else {
                    alert(response.data.message)
                }
            } else {
                alert(response.data.message)
            }

        }, function (error) {
            setTimeout(_$.unblockUI, 500)
            if (error.response.status == 403) {
                window.location.href = "${HOME}"
            }
            if (error.response.status == 404) {
                window.location.href = "${BASEHOST}/qbank/error"
            }
        });

        //first week schedule
        axios.get('${BASEURL}/dashboard/weeksubmit', {
            params: {
                week: '2'
            }, headers: headers
        }).then(function (response) {
            _$.unblockUI()
            if (response.data.status == 200) {
                if (response.data.success == true) {

                    secondweek = response.data.result.resCount
                    sdstartdate = response.data.result.startdate
                    sdenddate = response.data.result.enddate

                    $('#secondWK').text(secondweek);
                    $('#secondWKS').text(sdstartdate);
                    $('#secondWKE').text(sdenddate);

                } else {
                    alert(response.data.message)
                }
            } else {
                alert(response.data.message)
            }

        }, function (error) {
            console.log(error)
        });

        axios.get('${BASEURL}/dashboard/weeksubmit', {
            params: {
                week: '3'
            }, headers: headers
        }).then(function (response) {
            _$.unblockUI()
            if (response.data.status == 200) {
                if (response.data.success == true) {

                    firstweek = response.data.result.resCount
                    fwstartdate = response.data.result.startdate
                    fwenddate = response.data.result.enddate

                    $('#firstWK').text(firstweek);
                    $('#firstWKS').text(fwstartdate);
                    $('#firstWKE').text(fwenddate);

                } else {
                    alert(response.data.message)
                }
            } else {
                alert(response.data.message)
            }

        }, function (error) {
            console.log(error)
        });

        axios.get('${BASEURL}/dashboard/weeksubmit', {
            params: {
                week: '1'
            }, headers: headers
        }).then(function (response) {
            _$.unblockUI()
            if (response.data.status == 200) {
                if (response.data.success == true) {

                    thirdweek = response.data.result.resCount
                    tdstartdate = response.data.result.startdate
                    tdenddate = response.data.result.enddate

                    $('#thirdWK').text(thirdweek);
                    $('#thirdWKS').text(tdstartdate);
                    $('#thirdWKE').text(tdenddate);

                    new Chart(document.getElementById("bar-chart-grouped"), {
                        type: 'bar',
                        data: {
                            labels: [fwstartdate + " - " + fwenddate, sdstartdate + " - " + sdenddate, tdstartdate + " - " + tdenddate],
                            datasets: [
                                // {
                                // 	label: "Africa",
                                // 	backgroundColor: "#3e95cd",
                                // 	data: [221,783,478]
                                // },
                                {
                                    label: "Нийт шалгалт өгсөн ",
                                    backgroundColor: "#fb5f33",
                                    data: [firstweek, secondweek, thirdweek,]
                                }
                            ]
                        },
                        options: {
                            title: {
                                display: true,
                                // text: 'Population growth (millions)'
                            }
                        }
                    });

                } else {
                    alert(response.data.message)
                }
            } else {
                alert(response.data.message)
            }

        }, function (error) {
            console.log(error)
        });

    });
</script>

<script>
    var app = new Vue({
        data() {
            return {
                schCount: '',
                qCount: '',
                sCount: '',
                tCount: '',
                gCount: '',
                eCount: '',
                uCount: '',
                firstSubmit: '',
                secondSubmit: '',
                weekSubmits: '',
                weekEndDate: '',
                weekStartDate: '',
                signinTotal: '',
                signinUser: '',
                qeCount: []
            }
        },
        watch: {},
        computed: {},
        mounted() {
        },
        created() {
            this.qcount()
            this.schcount()
            this.gcount()
            this.ecount()
            this.ucount()
            this.submitCount()
            this.weeksubmit()
            this.signincount()
            this.qandecount()
        },
        filters: {},
        methods: {
            qandecount: function () {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/dashboard/qandecount', {
                        params: {}, headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.qeCount = response.data.result.sdatas
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    alert(error)
                }
            },
            signincount: function () {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/dashboard/signincount', {
                        params: {}, headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.signinUser = response.data.result.totalUser
                            _this.signinTotal = response.data.result.totalSignIn
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    alert(error)
                }
            },
            weeksubmit: function () {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/dashboard/weeksubmit', {
                        params: {
                            week: '1'
                        }, headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.weekSubmits = response.data.result.qCount
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    alert(error)
                }
            },
            qcount: function () {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/dashboard/qcount', {
                        params: {}, headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.qCount = response.data.result.qCount
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    alert(error)
                }
            },
            schcount: function () {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/dashboard/schoolcount', {
                        params: {}, headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.schCount = response.data.result.schCount
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    alert(error)
                }
            },
            gcount: function () {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/dashboard/groupcount', {
                        params: {}, headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.gCount = response.data.result.groupCount
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    alert(error)
                }
            },
            ecount: function () {
                try {
                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/dashboard/examcount', {
                        params: {}, headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.eCount = response.data.result.examCount
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    alert(error)
                }
            },
            ucount: function () {
                try {

                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/dashboard/usercount', {
                        params: {}, headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.sCount = response.data.result.stCount
                            _this.tCount = response.data.result.teachCount
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    alert(error)
                }
            },
            submitCount: function () {
                try {

                    var language = "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''

                    if (language == 'kr') {
                        locale = 'kr-KR'
                    }
                    if (language == 'mn') {
                        locale = 'mn-MN'
                    }
                    if (language == 'en') {
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language': locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/dashboard/submitcount', {
                        params: {}, headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.firstSubmit = response.data.result.firstSubmit
                            _this.secondSubmit = response.data.result.lastSubmit
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    alert(error)
                }
            },
        },
    })
    app.$mount('#main')
</script>
</body>
</html>

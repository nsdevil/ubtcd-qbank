    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

        <!DOCTYPE html>
        <html>
        <head>
        <meta charset="UTF-8">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta property="og:image" content="">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="628">
        <meta property="og:type" content="website">
        <meta name="theme-color" content="#">
        <meta name="msapplication-navbutton-color" content="rgb(251, 95, 51)">
        <meta name="apple-mobile-wep-app-status-bar-style" content="rgb(251, 95, 51)">

        <title><sitemesh:write property='title'/></title>

        <link rel="stylesheet" href="${CSS}/lib/select2/select2.min.css">
        <link rel="stylesheet" href="${CSS}/question_card/popup.css">
        <link rel="stylesheet" href="${CSS}/style.css">
        <link rel="icon" href="${IMG}/icon.jpg">
        <link rel="short icon" href="${IMG}/icon.jpg">
        <script src="${JS}/jquery3.3.1.js"></script>
        <script src="${JS}/script.js"></script>

        <%--    <script src="${JS}/lib/select2/select2.min.js"></script>    --%>

        <link rel="stylesheet" href="${CSS}/common/header.css">
        <link rel="stylesheet" href="${CSS}/common/footer.css">

        <script src="${JS}/vue/vue.js"></script>
        <script src="${JS}/vue/axios.js"></script>
        <script src="${JS}/blockUI.js" type="text/javascript"></script>
        <script src="${JS}/polyfill.min.js"></script>

        <script>
        <%----%>
        <%--function loadBody() {--%>
        <%--var sess = '${sessionScope.loginId}';--%>
        <%----%>
        <%--if (sess == "") {--%>
        <%--window.location.href = '${HOME}';--%>
        <%--}--%>
        <%--}--%>
        $('input').attr('autocomplete', 'off');
        </script>

        <sitemesh:write property='head'/>
        </head>

        <body>
        <header id="login">
        <div class="wrap top" id="">
        <div class="logo">
        <a href=" ${HOME}/main">
        <img src="${IMG}/header/nslogo.png" class="auto">
        </a>
        </div>
<%--            exam.edu.mn todo check --%>
<%--            <div class="logo">--%>
<%--            <a href=" ${HOME}/main">--%>
<%--            <div class="flex" style="align-items: center; justify-content: center;">--%>
<%--            <div class="logo"><img src="${IMG}/header/esis1.png" class="auto"></div>--%>
<%--            <div class="logo" style="margin-left: 50px;"><img src="${IMG}/header/edu1.png" class="auto"></div>--%>
<%--            </div>--%>
<%--            </a>--%>
<%--            </div>--%>
        <div class="member-info">
        <div class="image">
        <img src="${IMG}/header/profile.png" class="auto">
        </div>
        <div class="name">${sessionScope.S_NAME} (${sessionScope.S_LOGINID})</div>
        <%--		<div class="logintime">--%>
        <%--		<span><img src="${IMG}/icons/time.png" class="auto"></span>--%>
        <%--		<span>09:00</span>--%>
        <%--		<span>연장하기</span>--%>
        <%--		</div>--%>
        <%--		<div class="question"><a href=""><img src="${IMG}/icons/question.png" class="auto"></a></div>--%>
        <div class="logout">
        <a href="${HOME}/logout">
        <img src="${IMG}/icons/logout.png" class="auto">
        </a>
        </div>
        </div>
        </div>
        <nav>
        <div class="wrap flex align-items-center">
        <div class="menu flex">
        <a href="">
        <span></span>
        <span></span>
        <span></span>
        </a>
        </div>
        <ul class="flex menu-list">
        <!-- <li> -->
        <!-- <a href="" onclick="sub_show(this,event);">MY</a> -->
        <!-- <div class="flex align-items-center submenu"> -->
        <!-- <ul class="wrap flex"> -->
        <%--	<!--                             <li><a href=" ${HOME}/notice/list">공지사항</a></li> -->--%>
        <%--	<!--                             <li><a href=" ${HOME}/qna/list">1:1문의</a></li> -->--%>
        <%--	<!--                             <li><a href=" ${HOME}/my/show">내 정보 관리</a></li> -->--%>
        <%--	<!--                             <li><a href=" ${HOME}/my/pc_certification">PC인증 관리</a></li> -->--%>
        <!-- </ul> -->
        <!-- </div> -->
        <!-- </li> -->

            <% if (request.getSession().getAttribute("S_ROLE").toString().length() > 0) {
            if (request.getSession().getAttribute("S_ROLE").toString().contains("ROLE_ADMIN"))
            { %>
        <%--            <li>--%>
        <%--            <a href="" onclick="sub_show(this,event);">--%>
        <%--            <spring:message code="all.user.register"/>--%>
        <%--            <div class="flex align-items-center submenu">--%>
        <%--            <ul class="wrap flex">--%>
        <%--            <li><a href="${HOME}/my/teacher">--%>
        <%--            <spring:message code="all.teacher"/></a></li>--%>
        <%--            <li><a href=" ${HOME}/my/student">--%>
        <%--            <spring:message code="all.student"/></a></li>--%>
        <%--            </ul>--%>
        <%--            </div>--%>
        <%--            </a>--%>
        <%--            </li>--%>
        <li><a href="${HOME}/kexam/schoollist"><spring:message code="all.school.register"/></a></li>
            <% }
        } %>

            <% if (request.getSession().getAttribute("S_ROLE").toString().length() > 0) {
            if (request.getSession().getAttribute("S_ROLE").toString().contains("ROLE_ADMIN")
                || request.getSession().getAttribute("S_ROLE").toString().contains("ROLE_PROFESSOR"))
            { %>
        <li><a href="${HOME}/question_card/institutional/default_list1"><spring:message code="all.mainCate"/></a></li>
        <%--        <li>--%>
        <%--        <a href="" onclick="sub_show(this,event);"><spring:message code="header.qCard"/></a>--%>
        <%--        <div class="flex align-items-center submenu">--%>
        <%--        <ul class="wrap flex">--%>
        <%--        <li><a href="${HOME}/question_card/institutional/default_list1"><spring:message--%>
        <%--            code="all.mainCate"/></a></li>--%>
        <%--        <li><a href=" ${HOME}/question_card/institutional/default_list2"><spring:message--%>
        <%--            code="all.subjectcategory"/></a></li>--%>
        <%--        </ul>--%>
        <%--        </div>--%>
        <%--        </li>--%>

        <li>
        <a href="" onclick="sub_show(this,event);"><spring:message code="all.question"/></a>
        <div class="flex align-items-center submenu">
        <ul class="wrap flex">
        <li><a href=" ${HOME}/question_registration/each/essential"><spring:message code="header.qReg"/></a></li>
        <li><a href=" ${HOME}/question_card/questionbank"><spring:message code="header.qBank"/></a></li>
            <% if (request.getSession().getAttribute("S_ROLE").toString().length() > 0) {
            if (request.getSession().getAttribute("S_ROLE").toString().contains("ROLE_ADMIN"))
            { %>
        <li><a href=" ${HOME}/question_registration/each/form_regist"><spring:message code="header.qReg.form"/></a></li>
            <% }
        } %>
        <%--		<li><a href=" ${HOME}/question_registration/each/temporary"><spring:message code="header.qReg.temprory"/></a></li>--%>
        </ul>
        </div>
        </li>

        <!-- <li> -->
        <!-- <a href="" onclick="sub_show(this,event);">문항뱅크</a> -->
        <!-- <div class="flex align-items-center submenu"> -->
        <!-- <ul class="wrap flex"> -->
        <!-- <li><a href="">시험지</a></li> -->
        <!-- <li><a href="">교재</a></li> -->
        <!-- <li><a href="">문제</a></li> -->
        <!-- <li><a href="">학습자료</a></li> -->
        <!-- <li><a href="">소스</a></li> -->
        <!-- </ul> -->
        <!-- </div> -->
        <!-- </li> -->
        <%--		<li>--%>
        <%--		<a href="" onclick="sub_show(this,event);"><spring:message code="header.multimedia"/></a>--%>
        <%--		<div class="flex align-items-center submenu">--%>
        <%--		<ul class="wrap flex">--%>
        <%--		<li><a href=" ${HOME}/multimedia/multimedia_card_search"><spring:message code="header.dataSearch"/></a></li>--%>
        <%--		<li><a href=" ${HOME}/multimedia/essential"><spring:message code="header.dataReg"/></a></li>--%>
        <%--		--%>
        <%--		</ul>--%>
        <%--		</div>--%>
        <%--		</li>--%>
        <%--		<li>--%>
        <%--		<a href="" onclick="sub_show(this,event);"><spring:message code="header.check"/></a>--%>
        <!-- <div class="flex align-items-center submenu"> -->
        <!-- <ul class="wrap flex"> -->
        <!-- <li><a href="">시험지</a></li> -->
        <!-- <li><a href="">교재</a></li> -->
        <!-- <li><a href="">문제</a></li> -->
        <!-- <li><a href="">학습자료</a></li> -->
        <!-- <li><a href="">소스</a></li> -->
        <!-- </ul>-->
        <!-- </div> -->
        <%--		</li>--%>
        <!-- <li>
        <a href="" onclick="sub_show(this,event);">요청</a>
        <div class="flex align-items-center submenu">
        <ul class="wrap flex">
        <li><a href="">진행 중 내역</a></li>
        <li><a href="">완료 내역</a></li>
        </ul>
        </div>
        </li> -->
        <li>
        <a href="" onclick="sub_show(this,event);"><spring:message code="header.exam"/></a>
        <div class="flex align-items-center submenu">
        <ul class="wrap flex">
        <li><a href="${HOME}/kexam/examregister"><spring:message code="header.regExam"/></a></li>
        <li><a href="${HOME}/kexam/examlist"><spring:message code="header.examList"/></a></li>
        </ul>
        </div>
        </li>
            <% }
        } %>
            <% if (request.getSession().getAttribute("S_ROLE").toString().length() > 0) {
            if (request.getSession().getAttribute("S_ROLE").toString().contains("ROLE_MANAGER"))
            { %>
        <li><a href="${HOME}/list/teacherlist?schCode=${sessionScope.S_SCHCODE}"><spring:message code="all.teachers"/>
        </a></li>
        <li><a href="${HOME}/list/classlist?schCode=${sessionScope.S_SCHCODE}"><spring:message code="class"/></a></li>
            <% }
        } %>

            <% if (request.getSession().getAttribute("S_ROLE").toString().length() > 0) {
                if (request.getSession().getAttribute("S_ROLE").toString().contains("ROLE_PROFESSOR"))
                { %>
        <li><a href="${HOME}/list/teacherClasses"><spring:message code="class"/></a></li>
            <% }
                } %>
<%--            <% if (request.getSession().getAttribute("S_ROLE").toString().length() > 0) {--%>
<%--            if (request.getSession().getAttribute("S_ROLE").toString().contains("ROLE_ADMIN")--%>
<%--                || request.getSession().getAttribute("S_ROLE").toString().contains("ROLE_MANAGER"))--%>
<%--            { %>--%>
<%--        <li>--%>
<%--        <a href="" onclick="sub_show(this,event);"><spring:message code="survey.name"/></a>--%>
<%--        <div class="flex align-items-center submenu">--%>
<%--        <ul class="wrap flex">--%>
<%--        <li><a href="${HOME}/notice/survey_list"><spring:message code="survey.list"/></a></li>--%>
<%--        <li><a href="${HOME}/notice/survey_result"><spring:message code="survey.result"/></a></li>--%>
<%--        </ul>--%>
<%--        </div>--%>
<%--        </li>--%>

<%--            <% }--%>
<%--        } %>--%>
        </ul>
        </div>
        </nav>

        <div class="submenu-bg"></div>
        </header>

        <div class="container">
        </div>

        <sitemesh:write property='body'/>

        <footer>
        <div class="wrap flex">
        <div class="info">
        <%--		<h1>KERIS</h1>--%>
        <ul class="flex">
        <li>
        <span class="foot_label"><spring:message code="all.postCode"/></span>
        <span class="foot_info"><spring:message code="all.address"/></span></li>
                        <li><span class="foot_label">고객센터</span> <span class="foot_info">1544-0079 월-금 9:00 – 18:00 (공휴일 제외)</span></li>
<%--        <li><span class="foot_label"><spring:message code="all.contact"/></span>--%>
<%--        <span class="foot_info">+976 77040454</span></li>--%>
        </ul>
<%--            exam.edu.mn todo check --%>
        <p>COPYRIGHT&copy;Nsdevil consulting. ALL RIGHTS RESERVED.</p>
<%--            <p>COPYRIGHT&copy;БМТТ. ALL RIGHTS RESERVED.</p>--%>
        </div>
        <div class="logo">
        <img src="${IMG}/icons/footer/nslogo.png" class="auto">
        </div>
<%--            exam.edu.mn todo check --%>
<%--            <div class="flex" style="align-items: center;">--%>
<%--                    <div class="logo"><img src="${IMG}/header/esis1.png" class="auto"></div>--%>
<%--                    <div class="logo" style="margin-left: 50px;"><img src="${IMG}/header/edu1.png" class="auto"></div>--%>
<%--            </div>--%>
        </div>
        </footer>
        </body>


        <!-- s_2018-08-07 추가 : 시험 중단 알림 팝업 -->
        </html>

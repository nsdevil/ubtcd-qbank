<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
	<title>Exam register</title>
	<link href="${CSS}/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="${CSS}/question_registration/question_registration.css">
	<link rel="stylesheet" href="${CSS}/question_registration/each/question_card.css">
	<link rel="stylesheet" href="${CSS}/question_registration/each/making.css">
	<link rel="stylesheet" href="${CSS}/multimedia/multimedia.css">
	<link rel="stylesheet" href="${CSS}/question_card/institutional/default_list.css">
<%--	<script src="${JS}/vue/vuejs-datepicker.js"></script>--%>
	<script src="${JS}/questionbank/datepicker.min.js"></script>
	<script src="${JS}/making.js"></script>
	<style>
		#se-pre-con {
			position: fixed;
			-webkit-transition: opacity 5s ease-in-out;
			-moz-transition: opacity 5s ease-in-out;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
		}
	</style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="examregister">
	<div class="wrap bg-white pd-50 radius-5 shadow-wrap">
		<div class="breadcrumb">
			<span><a href=""><spring:message code="all.home"/></a></span>
			<span><a href=""></a><spring:message code="examreg.exam"/></span>
			<span><a href=""><spring:message code="examreg.register"/></a></span>
		</div>

		<nav class="mgt20">
			<ul>
				<li class="on"></li>
			</ul>
		</nav>
		<div class="content-wrap type2 flex">
			<article>
				<h3><spring:message code="examreg.examName"/></h3>
				<div class="content">
					<input type="text" class="input" placeholder="Please write exam name." v-model="exam.examName" name="exam.name">
				</div>
				<h3>
					<spring:message code="examreg.desc"/>
				</h3>
				<div class="content">
					<div class="flex" style="width: 100%">
						<input type="text" class="input" placeholder="Description."
							   v-model="exam.description" name="exam.desc">
					</div>
				</div>
				<h3>
					<spring:message code="examreg.examSubject"/>
				</h3>
				<div class="content">
					<div class="flex" style="width: 100%">
						<input type="text" class="input" placeholder="Please write exam subject."
							   v-model="exam.examSubject" name="exam.subj">
					</div>
				</div>
				<h3>
					<spring:message code="examreg.examOptions"/>
				</h3>
				<div class="content">
					<div>
						<input type="checkbox" class="checkbox" id="retake" v-model="exam.reTake">
						<label for="retake">
							<spring:message code="examreg.retake"/>
						</label>

						<input type="checkbox" class="checkbox" id="review" v-model="exam.review">
						<label for="review" style="margin-top: 10px;">
							<spring:message code="additional.review"/></label>

						<input type="checkbox" class="checkbox" id="shaffle" v-model="exam.shaffle">
						<label for="shaffle" style="margin-top: 10px;">
							<spring:message code="additional.shaffle"/></label>

						<div v-if="'${sessionScope.S_ROLE}' == 'ROLE_ADMIN'">
							<input type="checkbox" class="checkbox" id="record"  v-model="exam.recording">
							<label for="record" style="margin-top: 10px;">
								<spring:message code="examreg.record"/></label>

							<input type="checkbox" class="checkbox" id="ai"  v-model="exam.aiUse">
							<label for="ai" style="margin-top: 10px;">
								<spring:message code="examreg.ai"/></label>
						</div>
						<p class="" style="color: #fb5f33; margin-top: 10px;">
							<spring:message code="examreg.warning"/>
						</p>
					</div>
				</div>
<%--				<h3>--%>
<%--					<spring:message code="examreg.deviceSet"/>--%>
<%--				</h3>--%>
<%--				<div class="content">--%>
<%--					<div class="flex" style="width: 100%">--%>
<%--						<input name="devices" id="deviceMob" type="radio" checked--%>
<%--							   class="radio" :value="1" v-model="exam.device">--%>
<%--						<label for="deviceMob"><spring:message code="examreg.byWeb"/></label>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<h3>--%>
<%--					Exam life time--%>
<%--				</h3>--%>
<%--				<div class="content">--%>
<%--					<div class="flex" style="width: 100%">--%>
<%--						<vuejs-datepicker :value="exam.lifeStartDate" class="my-datepicker"  v-model="exam.lifeStartDate"></vuejs-datepicker>--%>
<%--						--%>
<%--						<vuejs-datepicker :value="exam.endDate" class="my-datepicker" v-model="exam.endDate"--%>
<%--										  style="margin-left: 60px;"--%>
<%--						></vuejs-datepicker>--%>
<%--					</div>--%>
<%--				</div>--%>
				<div class="flex" style="display: none;">
					<div style="width: 100%; ">
						<h3><spring:message code="examlist.examName"/></h3>
						<input type="text" class="input" placeholder="Exam time (min)" v-model="exam.examTime">
					</div>
					<div style="width: 100%;  margin-left: 60px" >
						<h3><spring:message code="additional.totalScore"/></h3>
						<input type="text" class="input" placeholder="Exam total score "
							   v-model="exam.examTotalScore">
					</div>
					<div style="width: 100%; margin-left: 60px" class="flex">
						<h3>Status</h3>
						<div class="radio-wrap">
							<input name="status" id="status1" type="radio"
								   class="radio" value="1" v-model="exam.examStatus">
							<label for="status1"><spring:message code="examreg.register"/></label>
						</div>
						<div class="radio-wrap">
							<input name="status" id="status2" type="radio"
								   class="radio" value="2" v-model="exam.examStatus">
							<label for="status2"><spring:message code="examreg.finish"/></label>
						</div>
					</div>
				</div>
				<div class="btn-wrap flex" style=" margin-top: 100px; ">
					<div class="mgl-auto">
						<button class="btn-red next shadow" v-on:click="createExam()">
							<spring:message code="examreg.register"/>
						<i class="next-white"></i></button>
					</div>
				</div>
			</article>
		</div>
	</div>
	<div id="preview"></div>
</section>
<script>

	var app = new Vue({
		el: '#examregister',
        components: {
            vuejsDatepicker
        },
		data: {
			exam:{
				examName: '',
				description: '',
				examTime: '',
				examStatus: '',
				examTotalScore: '',
				lifeStartDate:'',
                lifeEndDate: '',
                autoStart: '',
				startDate: '',
				realExam: '',
				examSubject: '',
                recording: false,
				aiUse: false,
                review: false,
				device: 1,
				reTake: false,
				shaffle: false
			},
		},
		computed:{

		},
		mounted(){

		},
		created(){
            console.log("working")
		},
		methods:{
			loading(cmd) {
				var l = document.getElementById("se-pre-con");
				l.style.display = cmd;
			},
            createExam(){

				var language =  "<%=pageContext.getResponse().getLocale()%>"
				var locale = ''
				if (language == 'kr')
				{
					locale = 'kr-KR'
				}
				if(language == 'mn')
				{
					locale = 'mn-MN'
				}
				if(language == 'en'){
					locale = 'en-EN'
				}
				const headers = {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
					'Accept-Language' : locale
				}
                if ( this.exam.examName == '') {
                    alert("pls write exam name")
                    return;
                }

		        let item = {
                    examName:  this.exam.examName,
                    description: this.exam.description,
                    examTime: this.examTime,
                    examStatus: this.exam.examStatus,
                    examTotalScore: this.exam.examTotalScore,
                    lifeStartDate: this.exam.lifeStartDate,
                    lifeEndDate: this.exam.lifeEndDate ,
                    autoStart: this.exam.autoStart,
                    startDate: this.exam.startDate,
                    examSubject: this.exam.examSubject,
                    ereview: this.exam.review,
					aiuse: this.exam.aiUse,
					recording: this.exam.recording,
					reTake: this.exam.reTake,
					qshuffle: this.exam.shaffle
				};
				var _this = this
				_this.loading('block')
		        axios.post('${BASEURL}/kexam/create',item,{
                    headers: headers
				}).then(function(response){
					if (response.data.status == 200)
					{
						if (response.data.success == true)
						{
							_this.loading('none')
							alert(response.data.message)
							window.location.href = "${HOME}/kexam/examlist"
						}
						else{
							_this.loading('none')
							alert(response.data.message)
						}
					}
					else{
						_this.loading('none')
						alert(response.data.message)
					}

				}, (error) =>
                {
					_this.loading('none')
                    console.log(error);
                    alert('Error: 244')
				});
			}
		}
	})

</script>
</body>
</html>

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
	<title>Exam list</title>
	<link rel="stylesheet" href="${CSS}/question_registration/question_registration.css">
	<link rel="stylesheet" href="${CSS}/question_registration/each/question_card.css">
	<link rel="stylesheet" href="${CSS}/question_registration/each/making.css">
	<link rel="stylesheet" href="${CSS}/question_registration/register_information.css">
	<link rel="stylesheet" href="${CSS}/question_card/institutional/default_list.css">

	<script src='${JS}/making.js'></script>
	<script src="${JS}/vue/vuejs-datepicker.js"></script>
	<%--	<script src="${JS}/questionbank/datepicker.min.js"></script>--%>
	<style>
		.survey-btn{
			align-items: center;
			background-color: #fff;
			justify-content: center;
			height: 35px;
			border: 1px solid #666;
			border-radius: 12px;
			color: #666;
		}
		.survey-btn:hover{
			color: #fff;
			background-color: #fb5f33;
			border: 1px solid #fb5f33;
		}
		.attach{
			color: #fff;
			background-color: #fb5f33;
			display: flex;
			align-items: center;
			justify-content: center;
			width: 110px;
			height: 26px;
			border: 0;
			border-radius: 2px;
		}
		.unattach{
			color: #fff;
			background-color: #a3a3a3;
			display: flex;
			align-items: center;
			justify-content: center;
			width: 110px;
			height: 26px;
			border: 0;
			border-radius: 2px;
		}
		.btn-line-red{
			padding: 2px;
			color: #fb5f33;
			font-size: 17px;
			border-bottom: 1px solid;
		}
		.btn-red-small{
			color: #fb5f33;
			border: 1px solid #fb5f33;
			padding: 5px;
		}
		#se-pre-con {
			position: fixed;
			-webkit-transition: opacity 5s ease-in-out;
			-moz-transition: opacity 5s ease-in-out;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
		}
	</style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="examlist">
	<div>
		<div class="wrap bg-white pd-50 radius-5 shadow-wrap">
			<div class="search-wrap ">
				<div class="flex">
					<span style=" text-align: center;  font-size: 15px; font-weight: 600; margin-right: 30px;
					padding-top: 9px"><spring:message code="examlist.examDate"/></span>
					<div class="content" style="padding-top: 4px;">
						<div class="flex">
							<vuejs-datepicker :value="exam.lifeStartDate" class="my-datepicker"
											  v-model="exam.lifeStartDate">
							</vuejs-datepicker>

							<vuejs-datepicker :value="exam.lifeEndDate" class="my-datepicker" v-model="exam.lifeEndDate"
											  style="margin-left: 60px;"
							></vuejs-datepicker>
						</div>
					</div>
				</div>
				<div class="search-form flex" style="float: left">
					<span style="text-align: center;  font-size: 15px; font-weight: 600; margin-right: 20px;
					 padding-top: 4px; "><spring:message code="examlist.examName"/></span>
					<div class="flex">
						<input type="text" placeholder="Search" v-model="exam.examName" name="exams">
						<button @click="searchFilter()"><img src="${IMG}/icons/search.png" class="auto"></button>
<%--						<button @click="defaultSearch()" style="margin-left: 10px;"><img--%>
<%--								src="${IMG}/icons/preview/setting.png" class="auto"></button>--%>
					</div>
				</div>
				<div class="search-btn-area" style="float: right">
					<button class="btn-red shadow" @click="popupClose()"><i
							class="plus"></i><spring:message code="examlist.examSet"/>
					</button>
				</div>
			</div>

			<div class="table-wrap">
				<table>
					<colgroup>
						<col style="width : 3%;">
						<col style="width : 23%;">
						<col style="width : 6%;">
						<col style="width : 7%;">
<%--						<col style="width : 6%;" v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_LOGINID}' == '3test_teacher1'">--%>
						<col style="width : 7%;">
						<col style="width : 6%;">
						<col style="width : 8%;">
						<col style="width : 9%;">
					</colgroup>
					<thead class="border-red-top-2 border-gray-bottom-2">
					<tr>
						<th>

						</th>
						<th>
							<spring:message code="examlist.examName"/>
<%--							<a href=""><i class="up"></i></a>--%>
<%--							<a href=""><i class="down"></i></a>--%>
						</th>
						<th>
							<spring:message code="examlist.class"/>
<%--							<a href=""><i class="up"></i></a>--%>
<%--							<a href=""><i class="down"></i></a>--%>
						</th>
						<th>
							<spring:message code="examlist.tQues"/>
<%--							<a href=""><i class="up"></i></a>--%>
<%--							<a href=""><i class="down"></i></a>--%>
						</th>
<%--						<th v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_LOGINID}' == '3test_teacher1'">--%>
<%--							<spring:message code="survey.name"/>--%>
<%--						</th>--%>
						<th>
							<spring:message code="examlist.pCount"/>
<%--							<a href=""><i class="up"></i></a>--%>
<%--							<a href=""><i class="down"></i></a>--%>
						</th>
						<th>
							<spring:message code="examlist.sCount"/>
						</th>
						<th>
							<spring:message code="examlist.createDate"/>
<%--							<a href=""><i class="up"></i></a>--%>
<%--							<a href=""><i class="down"></i></a>--%>
						</th>
						<th ><spring:message code="examlist.setting"/></th>
					</tr>
					</thead>
					<tbody v-if="examDetails.length > 0">
					<tr v-for="item in examDetails" :key="item.id">
						<td>
							<div v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_LOGINID}' == item.regUser">
								<input v-if="item.checked" style="width: 30px;
    height: 30px;
    -webkit-appearance: checkbox;" type="checkbox" v-model="item.checked" value="true"/>
								<input v-else="item.checked" style="width: 30px;
    height: 30px;
    -webkit-appearance: checkbox;" type="checkbox" v-model="item.checked" value="false"/>
							</div>
							<%--<button type="button" class="checkbutton" @click="checkChange(item)"--%>
							<%--:class="{active: item.checked}"></button>--%>
						</td>
						<td class="t-left">
							{{item.examName}}
						</td>
						<td>
							<a href="#" @click="classListSee(item.classes)" class="btn-line-red">{{item.classes.length}}</a>
						</td>
						<td>
							<a href="javascript:void(0)" class="btn-line-red" v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_LOGINID}' == item.regUser"
							   @click="newtab('teacher/exam_check', item.id)">
								{{item.questionCount}}
							</a>
							<span v-else>{{item.questionCount}}</span>
						</td>
<%--						<td v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_LOGINID}' == '3test_teacher1'">--%>
<%--							<button class="survey-btn" @click="surveyModal(item.id)"><spring:message code="survey.attach" /></button>--%>
<%--						</td>--%>
						<td>
							<span>{{item.totalTeach}}</span>
						</td>
						<td>
							<span>{{item.totalSt}}</span>
						</td>
						<td>
							{{item.updateAt.substring(0,10)}}
						</td>
						<td class="management" v-if="'${sessionScope.S_ROLE}'=='ROLE_ADMIN' || '${sessionScope.S_LOGINID}' == item.regUser">
							<button class="btn-edit" @click="modifyExam(item)" style="margin: 0px;">
								<img src="${IMG}/icons/question_card/update.png"
									 style="width: 27px;">
							</button>
							<button class="btn-edit" @click="questionAdd(item.id)" style="margin: 0px;">
								<img src="${IMG}/icons/question_card/card_list_add.png"
									 style="width: 27px;">
							</button>
							<button @click="deleteExamClick(item)">
								<img src="${IMG}/icons/question_card/delete.png" style="margin: 0px;">
							</button>
						</td>
						<td v-else>
							_
						</td>
					</tr>
					</tbody>
					<tbody v-else>
					<tr>
						<td colspan="8">
							<spring:message code="all.empty" />
						</td>
					</tr>
					</tbody>
				</table>
			</div>

			<div class="pagination">
				<button class="start" @click="firstQuestion()"><img src="${IMG}/icons/start.png" class="auto"></button>
				<button class="prev" @click="prevQuestion()"><img src="${IMG}/icons/prev.png" class="auto"></button>
				<ul class="paginate-list">
					<%--				<li class="page-num active" v-for="item in totalpages">--%>
					<%--					<a href="#">{{item}}</a>--%>
					<%--				</li>--%>
					<li class="page-num " v-for="(item,index) in pages " :key="index"
						:class="{'active': currentPage === item}"
						v-if="Math.abs(item - currentPage) < 3 || item == pages || item == 1">

						<a href="javascript:void(0);" @click="questionCurrent(item)" :class="{
						last: (item == pages && Math.abs(item - currentPage) > 3),
						first: (item == 1 && Math.abs(item - currentPage) > 3)}">{{item}}</a>
					</li>
				</ul>
				<button class="next" @click="nextQuestion()"><img src="${IMG}/icons/next.png" class="auto"></button>
				<button class="end" @click="lastQuestion()"><img src="${IMG}/icons/end.png" class="auto"></button>
			</div>
		</div>
		<div id="preview">
		</div>
		<%--class set modal--%>
		<div id="popup-modal" v-show="examModal"
			 style="position: absolute;
			 display: none;
			top: 321.5px;
			min-width: 500px;
			min-height: 400px;
			margin-left: 32%;top: 24%;">

			<div class="wrap subpopup wd500" style="height: auto;">
				<div class="btn-close">
					<button @click="editClose()"><i class="close-red"></i></button>
				</div>
				<form name="" method="" action="">
					<div class="filed-wrap" style="display: block;">
						<div class="title"> <spring:message code="examlist.modify"/>
						</div>
						<br>
						<div class="exambody">
							<label for="examname"><spring:message code="examlist.examName"/></label>
							<input class="modifyinput" type="text" id="examname" v-model="currentExam.examName" name="examName"/>
							<br>
							<br>
							<label for="examsubject"><spring:message code="examlist.subject"/></label>
							<input class="modifyinput" type="text" id="examsubject" v-model="currentExam.examSubject" name="examSubject"/>
							<br>
							<br>
							<label for="examdesc"><spring:message code="examlist.desc"/></label>
							<textarea cols="50" class="modifyinput" type="text" id="examdesc"
									  v-model="currentExam.examDesc"></textarea>

							<h3>
								<spring:message code="examlist.examOptions"/>
							</h3>
							<div class="content">
								<div>
									<input type="checkbox" class="checkbox" id="retake" v-model="currentExam.reTake">
									<label for="retake"><spring:message code="examreg.retake"/></label>

									<input type="checkbox" class="checkbox" id="review" v-model="currentExam.review">
									<label for="review" style="margin-top: 20px;"><spring:message code="examlist.review"/></label>

									<input type="checkbox" class="checkbox" id="shaffle" v-model="currentExam.shaffle">
									<label for="shaffle" style="margin-top: 10px;">
										<spring:message code="additional.shaffle"/></label>

									<div v-if="${sessionScope.S_ROLE == 'ROLE_ADMIN'}">
										<input type="checkbox" class="checkbox" id="record"  v-model="currentExam.recording">
										<label for="record"  style="margin-top: 20px;"><spring:message code="examlist.record"/></label>

										<input type="checkbox" class="checkbox" id="ai" v-model="currentExam.aiUse" >
										<label for="ai"   style="margin-top: 20px;"><spring:message code="examlist.ai"/></label>
									</div>

									<p class="" style="color: #fb5f33; margin-top: 10px;">
										<spring:message code="examlist.warning"/>
									</p>
								</div>
							</div>
							<h3><spring:message code="examlist.deviceSet"/>
							</h3>
							<div class="content">
								<div class="flex" style="width: 100%">
									<input name="devices" id="deviceMob" type="radio" checked
										   class="radio" value="1" v-model="currentExam.device">
									<label for="deviceMob"><spring:message code="examlist.byWeb"/></label>
								</div>
							</div>

						</div>
					</div>
					<br>
					<div class="btn-wrap">
						<button class="btn-gray shadow" type="button" @click="editClose()">
							<spring:message code="examlist.cancel"/></button>
						<button type="button" class="btn-red shadow" @click="saveExam()">
							<spring:message code="examlist.register"/></button>
					</div>
					<br>
					<br>
				</form>
			</div>
		</div>
		<%--class check modal--%>

		<div id="popup-modal" v-show="classModal"
			 style="position: absolute; ;
			display: none;
			min-width: 500px;
			min-height: 185px;
			margin-left: 32%;
			top: 10%;">

			<div class="wrap subpopup wd500" style="height: auto;">
				<div class="btn-close">
					<button @click="popupClose()"><i class="close-red"></i></button>
				</div>
				<form name="" method="" action="" style="overflow: auto; max-height: 600px;">
					<div class="filed-wrap" style="display: block;">
						<div class="title"><spring:message code="examlist.examSet"/>
						</div>
						<div class="table-wrap" style="margin-top: 30px;
    overflow: hidden;">
							<table>
								<tbody>
								<tr v-for="(item,index) in classes" :key="index">
									<td>
										<input v-if="item.checked" style="width: 30px;
    height: 30px;
    -webkit-appearance: checkbox;" type="checkbox" v-model="item.checked" value="true" checked/>
										<input v-else style="width: 30px;
    height: 30px;
    -webkit-appearance: checkbox;" type="checkbox" v-model="item.checked" value="false"/>
									</td>
									<td><img width="35" height="35" :src="item.photoThumbnailUrl"/></td>
									<td class="t-left">{{item.groupName}}</td>
									<td class="t-left">{{item.groupNo}}</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
					<br>
					<div class="btn-wrap">
						<button class="btn-gray shadow" type="button" @click="popupClose()"><spring:message
								code="examlist.cancel"/></button>
						<button type="button" class="btn-red shadow" @click="saveClass()"><spring:message
								code="examlist.register"/></button>
					</div>
					<br>
					<br>
				</form>
			</div>
		</div>
		<%--class check modal--%>

		<div v-show="deleteExamModal" class="modalDialog" :class="{'displaydiv': deleteExamModal}"
			 style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
			<div class="modal-mask">
				<div class="modal-wrapper">
					<div class="modal-container">
						<a href="javascript:void(0)" @click="deleteExamClick()" title="modelclose" class="modelclose">X</a>
						<div class="modal-header">
							<p style="font-weight: 800"><spring:message code="deleteExam"/></p>
						</div>
						<div class="modal-body">
							<article>
								<div class="detail" style=" padding: 16px;">
									<div class="" style="text-align: center">
										<h2><spring:message code="deleteExamDesc" /></h2>
									</div>
									<div class="clear flex" style="margin-left: 34%; margin-top: 20px;">
										<button type="button" class="btn-red shadow" @click="deleteExam()"
												style="height: 39px; margin-right: 10px;">
											<spring:message code="question.register.delete" /></button>
										<button type="button" @click="deleteExamClick()" class="btn-gray shadow"
												style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
											<spring:message code="all.close" /></button>
									</div>
								</div>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
<%--		survey to exam attach --%>
		<div v-show="surveyAttachModal" class="modalDialog" :class="{'displaydiv': surveyAttachModal}"
			 style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
			<div class="modal-mask">
				<div class="modal-wrapper">
					<div class="modal-container">
						<a href="javascript:void(0)" @click="surveyModal()" title="modelclose" class="modelclose">X</a>
						<div class="modal-header">
							<p style="font-weight: 800"><spring:message code="survey.register"/></p>
						</div>
						<div class="modal-body">
							<article>
								<div class="detail" style=" padding: 16px;">
									<div class="table-wrap" style="overflow: hidden;">
										<table>
											<colgroup>
												<col style="width : 4%;">
												<col style="width : 23%;">
												<col style="width : 8%;">
												<col style="width : 8%;">
											</colgroup>
											<thead class="border-red-top-2 border-gray-bottom-2">
											<tr>
												<td><spring:message code="all.number" /> </td>
												<td><spring:message code="survey.title" /> </td>
												<td><spring:message code="all.questionCount" /></td>
												<td><spring:message code="survey.attach" /></td>
											</tr>
											</thead>
											<tbody v-if="surveyList.length > 0">
											<tr v-for="(item,index) in surveyList" :key="index">
												<td>{{index+1}}</td>
												<td class="t-left">{{item.title}}</td>
												<td class="t-left">{{item.qCount}}</td>
												<td class="t-left">
													<button class="attach" v-if="item.attached == false" @click="attachSurvey(item.id)"><spring:message code="survey.attach" />
													</button>
													<button class="unattach" v-else @click="unattachSurvey(item.id)"><spring:message code="survey.unattach" />
													</button>
												</td>
											</tr>
											</tbody>
											<tbody v-else>
											<tr>
												<td colspan="4">
													<spring:message code="all.empty" />
												</td>
											</tr>
											</tbody>
										</table>
									</div>
									<div class="clear flex" style="margin-left: 45%; margin-top: 20px;">
<%--										<button type="button" class="btn-red shadow" @click="a()"--%>
<%--												style="height: 39px; margin-right: 10px;">--%>
<%--											<spring:message code="examlist.register" /></button>--%>
										<button type="button" @click="surveyModal()" class="btn-gray shadow"
												style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
											<spring:message code="all.close" /></button>
									</div>
								</div>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
<%--		survey add to exam end--%>

<%--		class list show modal--%>

		<div v-show="classListModel" class="modalDialog" :class="{'displaydiv': classListModel}"
			 style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
			<div class="modal-mask">
				<div class="modal-wrapper">
					<div class="modal-container">
						<a href="javascript:void(0)" @click="classListSeeClose()" title="modelclose" class="modelclose">X</a>
						<div class="modal-header">
							<p style="font-weight: 800"><spring:message code="class"/></p>
						</div>
						<div class="modal-body">
							<article>
								<div class="detail" style=" padding: 16px; max-height: 650px; overflow: auto;">
									<div class="table-wrap" style="overflow: hidden;">
										<table>
											<colgroup>
												<col style="width : 3%;">
												<col style="width : 10%;">
												<col style="width : 8%;">
												<col style="width : 5%;">
												<col style="width : 5%;">
											</colgroup>
											<thead class="border-red-top-2 border-gray-bottom-2">
											<tr>
												<td><spring:message code="all.number" /> </td>
												<td><spring:message code="schoollist.schoolName" /> </td>
												<td><spring:message code="classlist.schoolName" /></td>
												<td><spring:message code="main.teacherCount" /></td>
												<td><spring:message code="main.studentCount" /></td>
											</tr>
											</thead>
											<tbody v-if="classListDesc.length > 0">
												<tr v-for="(item,index) in classListDesc" :key="index">
													<td>{{index+1}}</td>
													<td class="t-left">{{item.schName}}</td>
													<td>{{item.groupName}}</td>
													<td>{{item.teacherCount}}</td>
													<td>{{item.studentCount}}</td>
												</tr>
											</tbody>
											<tbody v-else>
												<tr>
													<td colspan="5">
														<spring:message code="all.empty" />
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="clear flex" style="margin-left: 45%; margin-top: 20px;">
									<%--										<button type="button" class="btn-red shadow" @click="a()"--%>
									<%--												style="height: 39px; margin-right: 10px;">--%>
									<%--											<spring:message code="examlist.register" /></button>--%>
									<button type="button" @click="classListSeeClose()" class="btn-gray shadow"
											style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;"><spring:message code="all.close" /></button>
								</div>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
<%--		end class list show modal--%>
		<div v-show="editModal" id="popup-modal" class="modalDialog" :class="{'displaydiv': editModal}"
			 style="opacity: 1;pointer-events: painted; overflow: scroll;background: rgba(0, 0, 0, 0.8);display: none">
			<div class="modal-mask">
				<div class="modal-wrapper">
					<div class="modal-container">
						<a href="javascript:void(0)" @click="popupClose()" title="modelclose" class="modelclose">X</a>
						<div class="modal-header">
							<p><spring:message code="additional.classSet" /></p>
						</div>
						<div class="modal-body">
							<div class="modal-table" style="min-height: auto;">
								<p><span class="red">{{sendExamCount}}</span>
									<spring:message code="additional.examCount" /></p><br>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<colgroup>
										<col width="20%">
										<col width="30%">
										<col width="50%">
									</colgroup>
									<thead>
									<tr class="title_table border-red-top-2">
										<th>
											<span></span>
										</th>
										<th>
											<span><spring:message code="additional.content" /></span>
										</th>
										<th>
											<span><spring:message code="examlist.examName" /></span>
										</th>
									</tr>
									</thead>
									<tbody>
									<template v-for="(item,index) in examDetails">
										<tr v-if="item.checked">
											<td>{{item.id}}</td>
											<td>{{item.examSubject}}</td>
											<td>{{item.examName}}</td>
										</tr>
									</template>

									</tbody>
								</table>
							</div>
							<div class="modal-table" style="min-height: auto;">
								<div class="table-wrap">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<colgroup>
											<col width="8%">
											<col width="24%">
											<%--											<col width="24%">--%>
											<%--											<col width="24%">--%>
										</colgroup>
										<tbody>
										<tr class="title_table border-red-top-2">
											<td colspan="2" style="text-align: left">
												<span class="red" style="margin-left: 15px">
													<spring:message code="additional.chooseClassSet" />
											</span>
											</td>
										</tr>
<%--										<tr>--%>
<%--											<td>--%>
<%--												학교급:--%>
<%--											</td>--%>
<%--											<td>--%>
<%--												<select class="" style="width: 300px;" v-model="schlvlCodeClass">--%>
<%--													<option value="" selected="" disabled>학교급</option>--%>
<%--													<template v-for="(item, index) in schoolLevel">--%>
<%--														<option :key="index" :value="item.schlvlCode"--%>
<%--																v-if="item.schlvl != 'test'">--%>
<%--															{{item.schlvl}}--%>
<%--														</option>--%>
<%--													</template>--%>
<%--												</select>--%>
<%--											</td>--%>
<%--										</tr>--%>
										<tr>
											<td>
												<spring:message code="classlist.schoolName" />
											</td>
											<td><input name="classsearch" v-model="searchclass" value="" v-on:keyup.enter="loadClasses()"></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="modal-table" style="min-height: auto;padding: 0 20px;">
								<p style="float: left;padding: 10px 5px;color: rgb(251, 95, 51);border: 1px solid rgb(251, 95, 51);">
									<spring:message code="additional.listofClasses" /> (<span>{{checkedCount}}</span>)
								</p>
								<div style="float: right;"><spring:message code="additional.classcount" />: {{classes.length}}

									<button type="button" class="btn-red-small" style="width: 55px;height: 25px;"
											@click="search_class">
										<spring:message code="all.search" />
									</button>
								</div>
							</div>
							<div class="modal-table">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<colgroup>
										<col width="10%">
										<col width="30%">
										<col width="30%">
										<col width="30%">
									</colgroup>
									<thead>
									<tr class="title_table border-red-top-2">
										<th>
											<input type="checkbox" v-model="selectall" @change="checkallclass"
												   id="checkall"
												   style="width: 25px;height: 25px;
													-webkit-appearance:checkbox;"/>
										</th>
										<th>
											<span><spring:message code="schoollist.schoolName" /></span>
										</th>

										<th>
											<span><spring:message code="classlist.schoolName" /></span>
										</th>
										<th>
											<span><spring:message code="classlist.schoolID" /></span>
										</th>
									</tr>
									</thead>
									<tbody>
									<template v-if="classes.length > 0">
										<tr v-for="(item,index) in classes" :key="index">
											<td>
												<input @change="changeItem"
													   style="width: 25px;height: 25px;-webkit-appearance:checkbox;"
													   type="checkbox" v-model="item.checked"/>
											</td>
											<td class="t-left">{{item.sch_name}}</td>
											<%--<td><img width="35" height="35" :src="item.photoThumbnailUrl"/></td>--%>
											<td class="t-left">{{item.group_name}}</td>
											<td class="t-left">{{item.class_id}}</td>
										</tr>
									</template>
									<template v-else>
										<tr>
											<td colspan="10" style="height: 100px;text-align: center;">Empty.</td>
										</tr>
									</template>
									</tbody>
								</table>
							</div>
						</div>
						<div class="modal-footer flex btn-wrap">
							<button type="button" @click="popupClose()" class="btn-gray shadow"><spring:message code="all.close" /></button>
							<button type="button" class="btn-red shadow" @click="saveClass()"><spring:message code="examlist.register" /></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
    let app = new Vue({
        // el: '#examList',
        components: {
            vuejsDatepicker
        },
        data: {
			sendExamCount: 0,
			checkedCount: 0,
			selectall: false,
            classes: [],
            classModal: false,
			editModal: false,
			examModal: false,
			surveyAttachModal: false,
			classListModel: false,
			deleteExamModal: false,
			searchclass: '',
			classListDesc: [],
			// selectallexam: false,
            currentExam: {
                examName: "",
                examDesc: "",
                examSubject: "",
                recording: false,
                aiUse: false,
                review: false,
                device: 1,
				reTake: false,
				shaffle: false
            },
            exam: {
                id: '',
                examName: '',
                examType: '',
                lifeStartDate: '2020-09-10',
                lifeEndDate: '2020-12-31',
                examFee: '',
                recording: false,
                aiUse: false,
                review: false,
                device: 1,
				reTake: false,
				shaffle: false
            },
            currentPage: 1,
            itemsPerPage: 1,
            pages: 0,
            totalElement: 0,
            examDetails: [],
			localPath: '${BASEHOST}/exam',
            // localPath: 'http://localhost:8084/exam',
			surveyExamId: '',
			surveyList: [],
			deleteExamId: ''

        },
        computed: {
            paginate: function () {
                if (!this.examDetails || this.examDetails.length != this.examDetails.length) {
                    return
                }
                if (this.currentPage >= this.pages) {
                    this.currentPage = this.pages
                }
                var index = this.currentPage * this.itemsPerPage - this.itemsPerPage
                return this.examDetails.slice(index, index + this.itemsPerPage)
            }
        },
        mounted() {

        },
        created() {
            this.loaddata(this.currentPage)

        },
        methods: {
			loading(cmd) {
				var l = document.getElementById("se-pre-con");
				l.style.display = cmd;
			},
			unattachSurvey(surId){
				var language =  "<%=pageContext.getResponse().getLocale()%>"
				var locale = ''
				if (language == 'kr')
				{
					locale = 'kr-KR'
				}
				if(language == 'mn')
				{
					locale = 'mn-MN'
				}
				if(language == 'en'){
					locale = 'en-EN'
				}
				const headers = {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
					'Accept-Language' : locale
				}
				var _this = this
				_this.loading('block')
				axios.post('${BASEURL}/research/unattach/survey', {
					examId: this.surveyExamId,
					researchId: surId
				},{headers: headers}).then((response) => {
					if (response.data.status === 200) {
						_this.loading('none')
						alert(response.data.message);
						_this.surveylist()
					} else {
						_this.loading('none')
						alert("ERROR: " + response.data.message)
					}
				}, (error) => {
					_this.loading('none')
					console.log(error);
				});
			},
			attachSurvey(surId){
				var language =  "<%=pageContext.getResponse().getLocale()%>"
				var locale = ''
				if (language == 'kr')
				{
					locale = 'kr-KR'
				}
				if(language == 'mn')
				{
					locale = 'mn-MN'
				}
				if(language == 'en'){
					locale = 'en-EN'
				}

				const headers = {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
					'Accept-Language' : locale
				}
				var _this = this;
				_this.loading('block')
				axios.post('${BASEURL}/research/attach/survey', {
						examId: this.surveyExamId,
						researchId: surId
						},{headers: headers}).then((response) => {
					if (response.data.status === 200) {
						_this.loading('none')
						alert(response.data.message);
						_this.surveylist()
					} else {
						_this.loading('none')
						alert("ERROR: " + response.data.message)
					}
				}, (error) => {
					_this.loading('none')
					console.log(error);
				});
			},
			checkallclass() {
				for (let i = 0; i < this.classes.length; i++) {
					this.classes[i].checked = this.selectall;
				}
				this.changeItem();
			},
			search_class() {
				this.loadClasses();
				this.selectall = false;
			},
			// checkallexam() {
			// 	for (let i = 0; i < this.examDetails.length; i++) {
			// 		this.examDetails[i].checked = this.selectallexam;
			// 	}
			// },
            newtab(link, id) {

                <%--var loginId = '${sessionScope.loginId}';--%>
                <%--this.userId = loginId--%>

                window.open(this.localPath + "/" + link + "?examId=" + id,
                    '_blank',  'width=1240,height=650')

                // window.location.href= "http://localhost:8088/exam/"+ link +"?id=" + id,target ="_blank","width=1170,height=700"

                <%-- let route = this.$router.resolve({name: link, params: {examId: id}});--%>
                <%-- window.open(route.href, '_blank','width=1240,height=652');--%>

                <%--localStorage.setItem('questionId', JSON.stringify(id));--%>
                <%--const questionId = JSON.parse(localStorage.getItem('questionId'));--%>
                <%--window.location.href= "${HOME}/question_registration/each/preview/preview?id=" + id--%>
            },
            editClose() {
                this.examModal = false;
            },
            clearData(){
                this.currentExam.id = '';
                this.currentExam.examName = '';
                this.currentExam.examDesc = '';
                this.currentExam.examSubject = '';
                this.currentExam.recording = '';
                this.currentExam.aiUse = '';
                this.currentExam.review = '';
				this.currentExam.reTake = '';
				this.currentExam.shaffle = '';
            },
            modifyExam(item) {
                this.clearData()
                this.currentExam.id = item.id;
                this.currentExam.examName = item.examName;
                this.currentExam.examDesc = item.examDesc;
                this.currentExam.examSubject = item.examSubject;
                this.currentExam.recording = item.recording;
                this.currentExam.aiUse = item.aiduse;
                this.currentExam.review = item.review;
				this.currentExam.reTake = item.reTake;
				this.currentExam.shaffle = item.qshuffle;

                if (this.currentExam.aiUse == 'false')
				{
                    this.currentExam.aiUse = ''
                }
                if (this.currentExam.review == 'false')
                {
                    this.currentExam.review = ''
                }
                if (this.currentExam.recording == 'false')
                {
                    this.currentExam.recording = ''
                }
				if (this.currentExam.reTake == 'false')
				{
					this.currentExam.reTake = ''
				}
				if (this.currentExam.shaffle == 'false')
				{
					this.currentExam.shaffle = ''
				}

                this.examModal = true;
            },
            saveExam() {
				var language =  "<%=pageContext.getResponse().getLocale()%>"
				var locale = ''
				if (language == 'kr')
				{
					locale = 'kr-KR'
				}
				if(language == 'mn')
				{
					locale = 'mn-MN'
				}
				if(language == 'en'){
					locale = 'en-EN'
				}
				const headers = {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
					'Accept-Language' : locale
				}
                var _this = this;
                var params = {
                    id: this.currentExam.id,
                    examName: this.currentExam.examName,
                    examSubject: this.currentExam.examSubject,
                    description: this.currentExam.examDesc,
                    ereview: this.currentExam.review,
                    aiuse: this.currentExam.aiUse,
                    recording: this.currentExam.recording,
					reTake: this.currentExam.reTake,
					qshuffle: this.currentExam.shaffle
                }

				_this.loading('block')
                axios.post('${BASEURL}/kexam/update', params,
                    {headers: headers}).then((response) => {
                    if (response.data.status === 200) {
						_this.loading('none')
                        alert(response.data.message);
                        _this.examModal = false;
                        _this.clearData();
                        _this.loaddata(this.currentPage);
                    } else {
						_this.loading('none')
                        alert("ERROR: " + response.data.message)
                    }
                }, (error) => {
					_this.loading('none')
                    console.log(error);
                });
            },
			deleteExamClick(item){
				if (!this.deleteExamModal) {
					this.deleteExamId = item.id
					document.body.style.position = 'fixed';
				} else {
					document.body.style.position = 'relative';
				}
				this.deleteExamModal = !this.deleteExamModal;
			},
            deleteExam() {
				var language =  "<%=pageContext.getResponse().getLocale()%>"
				var locale = ''
				if (language == 'kr')
				{
					locale = 'kr-KR'
				}
				if(language == 'mn')
				{
					locale = 'mn-MN'
				}
				if(language == 'en'){
					locale = 'en-EN'
				}
				const headers = {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
					'Accept-Language' : locale
				}
				var _this = this;
				_this.loading('block')
                axios.post('${BASEURL}/kexam/delete', {examId: _this.deleteExamId},
                    {headers: headers}).then((response) => {
                    if (response.data.status === 200) {
						_this.loading('none')
						_this.deleteExamModal = false
                        alert(response.data.message);
						document.body.style.position = 'relative'
                        _this.loaddata(_this.currentPage);
                    } else {
						_this.loading('none')
                        alert("ERROR: " + response.data.message)
                    }
                }, (error) => {
					_this.loading('none')
                    console.log(error);
                });
            },
            checkChange(item) {
                item.checked = !item.checked;
            },
            saveClass() {
                let sendClasses = [];
                for (let i = 0; i < this.classes.length; i++) {
                    if (this.classes[i].checked) {
                        let item =
                            {
                                classId: this.classes[i].class_id,
                                className: this.classes[i].group_name,
                            }
                        sendClasses.push(item);
                    }
                }
                let exams = [];
                for (let i = 0; i < this.examDetails.length; i++) {
                    if (this.examDetails[i].checked) {
                        exams.push(this.examDetails[i].id);
                    }
                }
				var _this = this;
				_this.loading('block')
				var language =  "<%=pageContext.getResponse().getLocale()%>"
				var locale = ''
				if (language == 'kr')
				{
					locale = 'kr-KR'
				}
				if(language == 'mn')
				{
					locale = 'mn-MN'
				}
				if(language == 'en'){
					locale = 'en-EN'
				}
				const headers = {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
					'Accept-Language' : locale
				}

                axios.post('${BASEURL}/kexam/put/classes', {classes: sendClasses, exams: exams},
                    {headers: headers}).then((response) => {
                    if (response.data.status === 200) {
						_this.loading('none')
                        alert(response.data.message);
                        _this.popupClose();
                        _this.loaddata(_this.currentPage);
                    } else {
						_this.loading('none')
                        alert("ERROR: " + response.data.message)
                    }
                }, (error) => {
					_this.loading('none')
                    console.log(error);
                });

            },
			classListSeeClose(){
				if (!this.classListModel) {
					document.body.style.position = 'fixed';
				} else {
					document.body.style.position = 'relative';
				}
				this.classListModel = !this.classListModel;
			},
			classListSee(item){

				this.loading('block')
				this.classListDesc = item
				if (!this.classListModel) {
					document.body.style.position = 'fixed';
					this.loading('none')
				} else {
					document.body.style.position = 'relative';
					this.loading('none')
				}
				this.classListModel = !this.classListModel;
			},
			surveyModal(examId){
				this.loading('block')
				this.surveyExamId = ''
				if (!this.surveyAttachModal) {
					this.surveyExamId = examId
					this.surveylist()
					document.body.style.position = 'fixed';
					this.loading('none')
				} else {
					document.body.style.position = 'relative';
					this.loading('none')
				}
				this.surveyAttachModal = !this.surveyAttachModal;
			},
			surveylist(){
				try {
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}
					var _this = this;
					axios.get('${BASEURL}/research/list', {
						params: {
							examId: this.surveyExamId
						},headers: headers
					}).then(function (response) {
						if (response.status == 200) {
							_this.surveyList = response.data.content
						} else {
							alert(response.data.msg);
						}
					});
				} catch (error) {
					console.log(error)
				}
			},
			popupClose() {
				if (!this.editModal) {
					this.loadClasses();
					document.body.style.position = 'fixed';
				} else {
					document.body.style.position = 'relative';
				}
				this.editModal = !this.editModal;
			},
			loadClasses() {
				let isExamChecked = false;
				this.sendExamCount = 0;
				for (let i = 0; i < this.examDetails.length; i++) {
					if (this.examDetails[i].checked) {
						if (!isExamChecked) {
							isExamChecked = true;
						}
						this.sendExamCount++;
					}
				}
				if (!isExamChecked) {
					alert("Please check exam");
					this.loaddata(this.currentPage);
					this.editModal = false;
					return;
				}
				var _this = this;
				var language =  "<%=pageContext.getResponse().getLocale()%>"
				var locale = ''
				if (language == 'kr')
				{
					locale = 'kr-KR'
				}
				if(language == 'mn')
				{
					locale = 'mn-MN'
				}
				if(language == 'en'){
					locale = 'en-EN'
				}
				const headers = {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
					'Accept-Language' : locale
				}
				_this.loading('block')
				axios.get('${BASEURL}/kexam/useroradmin/classes', {
						params: {
							groupName: this.searchclass,
						},
						headers: headers
					}
				).then(function (response) {
					_this.loading('none')
					_this.classes = response.data;
					for (let i = 0; i < _this.classes.length; i++) {
						_this.classes[i].checked = false;
					}
					_this.changeItem();
				}, function (error) {
					console.log(error);
					_this.loading('none')
				});
			},
			changeItem() {
				let cnt = 0;
				for (let i = 0; i < this.classes.length; i++) {
					if (this.classes[i].checked) {
						cnt++;
					}
				}
				this.checkedCount = cnt;
				if (this.classes.length == this.checkedCount) {
					this.selectall = true;
				} else {
					this.selectall = false;
				}
			},
            <%--popupClose() {--%>
            <%--    if (!this.classModal) {--%>
            <%--        let isExamChecked = false;--%>
            <%--        for (let i = 0; i < this.examDetails.length; i++) {--%>
            <%--            if (this.examDetails[i].checked) {--%>
            <%--                isExamChecked = true;--%>
            <%--                break;--%>
            <%--            }--%>
            <%--        }--%>
            <%--        if (!isExamChecked) {--%>
            <%--            alert("Please check exam");--%>
            <%--            return;--%>
            <%--        }--%>
            <%--        var _this = this;--%>
            <%--        _this.loading('block')--%>
			<%--		var language =  "<%=pageContext.getResponse().getLocale()%>"--%>
			<%--		var locale = ''--%>
			<%--		if (language == 'kr')--%>
			<%--		{--%>
			<%--			locale = 'kr-KR'--%>
			<%--		}--%>
			<%--		if(language == 'mn')--%>
			<%--		{--%>
			<%--			locale = 'mn-MN'--%>
			<%--		}--%>
			<%--		if(language == 'en'){--%>
			<%--			locale = 'en-EN'--%>
			<%--		}--%>
			<%--		const headers = {--%>
			<%--			'Content-Type': 'application/json',--%>
			<%--			'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',--%>
			<%--			'Accept-Language' : locale--%>
			<%--		}--%>
            <%--        var _this = this;--%>
            <%--        axios.get('${BASEURL}/kexam/allclasses', {--%>
            <%--            params: {},--%>
            <%--            headers: headers--%>
            <%--        }).then(function (response) {--%>
            <%--            _this.loading('none')--%>
            <%--            console.log(response)--%>
            <%--            --%>
            <%--            if (response.status == 200) {--%>
            <%--                _this.classes = response.data;--%>
            <%--                if (_this.classes.length > 0) {--%>
            <%--                    for (let i = 0; i < _this.classes.length; i++) {--%>
            <%--                        _this.classes[i].checked = false;--%>
            <%--                    }--%>
            <%--                    _this.classModal = true;--%>
            <%--                } else {--%>
            <%--                    alert("class not found");--%>
            <%--                }--%>
            <%--            } else {--%>
            <%--                alert("error");--%>
            <%--            }--%>
            <%--        }, function (error) {--%>
            <%--            console.log(error);--%>
            <%--            _this.loading('none')--%>
            <%--        });--%>
            <%--    }--%>
            <%--    this.classModal = !this.classModal;--%>
            <%--},--%>
            loaddata(page) {
                try {
                    if (page > 0) {
                        page = page - 1
                    }
					var language =  "<%=pageContext.getResponse().getLocale()%>"
					var locale = ''
					if (language == 'kr')
					{
						locale = 'kr-KR'
					}
					if(language == 'mn')
					{
						locale = 'mn-MN'
					}
					if(language == 'en'){
						locale = 'en-EN'
					}
					const headers = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
						'Accept-Language' : locale
					}
                    let item = {
                        page: page,
                        examId: this.exam.examId,
                        examName: this.exam.examName,
                        lifeStartDate: this.exam.lifeStartDate,
                        lifeEndDate: this.exam.lifeEndDate,
                        examType: this.exam.examType,
                        examFee: this.exam.examFee
                    }
                    var _this = this;
					_this.loading('block')
                    axios.get('${BASEURL}/kexam/list/?size=12', {
                        params: item,
                        headers: headers
                    }).then(function (response) {
						_this.loading('none')
                        if (response.status == 200) {
                            _this.examDetails = response.data.content;
                            for (let i = 0; i < _this.examDetails.length; i++) {
                                _this.examDetails[i].checked = false;
                            }
                            _this.pages = response.data.totalPages;
                            _this.totalElement = response.data.totalElements;
                        } else {
                            alert("response status error")
                        }
                    }, function (error) {
						_this.loading('none')
						console.log(error)
                    });
                } catch (error) {
                    console.log(error)
                }
            },
            questionAdd(item) {
                window.location.href = '${HOME}/question_card/questionset?id=' + item
            },
            questionCurrent(item) {
                this.currentPage = item
                this.loaddata(this.currentPage)
            },
            nextQuestion() {
                if (this.pages > this.currentPage) {
                    this.currentPage = this.currentPage + 1
                    this.loaddata(this.currentPage)
                }
            },
            prevQuestion() {
                if (this.currentPage > 1) {
                    this.currentPage = this.currentPage - 1
                    this.loaddata(this.currentPage)
                }
            },
            lastQuestion() {
                this.currentPage = this.pages
                this.loaddata(this.currentPage)
            },
            firstQuestion() {
                this.currentPage = 1
                this.loaddata(this.currentPage)
            },
            async searchFilter() {
                this.loaddata(this.currentPage);
            },
            defaultSearch() {
                window.location.href = "${HOME}/question_card/questionbank" + this.examId
            }
        }
    });
    app.$mount('#examlist')
</script>
<style scoped>
	.my-datepicker input {
		padding: 9px 9px 9px 0;
		border-radius: 8px;
		text-align: center;
	}
	.checkbutton {
		background-color: #bdbdbd;
		border-radius: 2px;
		width: 19px;
		height: 19px;
		display: inline;
		flex: auto;
		margin: 5px;
	}

	.checkbutton.active {
		background-color: #ee753d;
	}

	.modifyinput {
		width: 100%;
		border: 1px solid #eee;
		padding: 5px;
	}
	.displaydiv {
		display: block !important;
	}
</style>
</body>
</html>

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
    <title>user list</title>
    <link rel="stylesheet" href="${CSS}/question_registration/each/making.css">
    <link rel="stylesheet" href="${CSS}/question_card/institutional/default_list.css">
    <link rel="stylesheet" href="${CSS}/question_registration/register_information.css">

    <script src='${JS}/making.js'></script>
    <script src="${JS}/vue/vuejs-datepicker.js"></script>
    <%--	<script src="${JS}/questionbank/datepicker.min.js"></script>--%>
    <style>
        .modifyinput {
            width: 100%;
            padding: 5px;
            height: 37px;
            font-size: 16px;
            margin-top: 32px;
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
            border-bottom: 1px solid #dee2e6;
            outline: none;
        }

        #popup-modal form .filed-wrap .title span {
            font-size: 18px;
            font-weight: bold;
            width: 130px;
        }
        .classInput{
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
            width: 92%;
            margin-left: 10px;
            margin-right: 20px;
            border-bottom: 1px solid #dee2e6;
        }
        .radioTit{
            margin-right: 30px;
        }
        .btn-white-search {
            background-color: #fff;
            color: #000000;
            align-items: center;
            justify-content: center;
            height: 35px;
            border: 1px solid #dee2e6;
            border-radius: 2px;
            width: 150px;
            margin-right: 20px;
        }
        .btn-red-search {
            color: #fff;
            background-color: #fb5f33;
            align-items: center;
            justify-content: center;
            height: 35px;
            border: 0;
            border-radius: 2px;
            width: 80px;
        }
        .totalQues{
            padding: 6px;
            margin-right: 20px;
            font-size: 17px;
        }
    </style>
</head>
<body>
<section id="examlist">
    <div>
        <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
            <div class="table-wrap">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <colgroup>
                        <col width="8%">
                        <col width="24%">
                        <col width="8%">
                        <col width="24%">
                    </colgroup>
                    <tbody>
                    <tr class="border-red-top-2 border-gray-bottom-2">
                        <td class="tit">
                            <spring:message code="schoollist.schoolName"/>
                        </td>
                        <td>
                            <input name="" v-model="search.schoolName" class="classInput">
                        </td>
                        <td class="tit">
                            <spring:message code="schoollist.schoolID"/>
                        </td>
                        <td>
                            <input name="" v-model="search.usersId" class="classInput">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="table-wrap search-div" style="display: flex; float: right; margin-top: 20px; margin-bottom: 20px;">
                <button class="btn-white-search" @click="defaultSearch()">
                    <spring:message code="schoollist.defaultSearch"/>
                </button>
                <button class="btn-red-search" @click="searchFilter()">
                    <spring:message code="schoollist.search"/>
                </button>
            </div>
            <div class="table-wrap flex" style=" margin-top: 60px; ">
                <%--                <button class="btn-red" @click="createModalShow()" style="width: 180px; height: 36px;">--%>
                <%--                    <i class="plus"></i>--%>
                <%--                    <spring:message code="schoollist.schoolAdd"/>--%>
                <%--                </button>--%>
                <button class="btn-red" @click="createModalShow()" style="width: 180px; height: 36px;">
                    <i class="plus"></i>
                    <spring:message code="schoollist.schoolAdd"/>
                </button>
            </div>

            <div class="table-wrap" style="margin-top: 10px">
                <table>
                    <colgroup>
                        <col style="width : 60px;">
                        <col style="width : 210px;">
                        <col style="width : 200px;">
                        <col style="width : 110px;">
                        <col style="width : 110px;">
                        <col style="width : 110px;">
                        <col style="width : 130px;">
                    </colgroup>
                    <thead class="border-red-top-2 border-gray-bottom-2">
                    <tr>
                        <th>
                            №
                        </th>
                        <th>
                            <spring:message code="schoollist.schoolName"/>
                        </th>
                        <th>
                            <spring:message code="schoollist.schoolID"/>
                        </th>
                        <th>
                            <spring:message code="schoollist.teacherCount"/>
                        </th>
                        <th>
                            <spring:message code="schoollist.studentCount"/>
                        </th>
                        <th>
                            <spring:message code="schoollist.classSee"/>
                        </th>
                        <th><spring:message code="schoollist.addClass"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item,index) in schoolList" :key="index">
                        <td>
                            {{index+1}}
                        </td>
                        <td>
                            {{item.schName}}
                        </td>
                        <td>
                            {{item.schCode}}
                        </td>
                        <td>
                            <span>_</span>
                        </td>
                        <td>
                            <span>_</span>
                        </td>
                        <td>
                            <button class="btn-red-search" @click="classListSee(item.schCode)">
                                Харах
                            </button>
                        </td>
                        <td class="management">
                            <button class="btn-red-search" @click="createClassModel(item.schCode, item.schName)" >
                                Бүртгэх
                            </button>
                            <button class="btn-edit" @click="updateSchlvl(item.schName, item.schCode, item.id)">
                                <img src="${IMG}/icons/question_card/update.png"
                                     style="width: 27px;">
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="pagination">
                <button class="start" @click="firstQuestion()"><img src="${IMG}/icons/start.png" class="auto"></button>
                <button class="prev" @click="prevQuestion()"><img src="${IMG}/icons/prev.png" class="auto"></button>
                <ul class="paginate-list">
                    <li class="page-num " v-for="(item,index) in pages " :key="index"
                        :class="{'active': currentPage === item}"
                        v-if="Math.abs(item - currentPage) < 3 || item == pages || item == 1">

                        <a href="javascript:void(0);" @click="questionCurrent(item)" :class="{
					last: (item == pages && Math.abs(item - currentPage) > 3),
					first: (item == 1 && Math.abs(item - currentPage) > 3)}">{{item}}</a>

                    </li>
                </ul>
                <button class="next" @click="nextQuestion()"><img src="${IMG}/icons/next.png" class="auto"></button>
                <button class="end" @click="lastQuestion()"><img src="${IMG}/icons/end.png" class="auto"></button>
            </div>
        </div>
        <div id="popup-modal" v-show="createSchool"
             style="position: absolute;
			display: none;
			min-width: 500px;
			min-height: 600px;
			margin-left: 32%;
			top: 6%;
			border-radius: 15px;">
            <div class="wrap subpopup wd500" style="height: auto; border-radius: 15px;">
                <div class="btn-close">
                    <button @click="createSchool = false"><i class="close-red"></i></button>
                </div>
                <form name="" method="" action="">
                    <div class="filed-wrap" style="display: block;">
                        <div class="title" style="text-align: center">
                            <span v-if="flag == false">
                                <spring:message code="schoollist.title"/>
                            </span>
                            <span v-else>
                                <spring:message code="schoollist.modify"/>
                            </span>
                        </div>
                        <br>
                        <div class="exambody">
                            <%--								<label for="examname"></label>--%>
                            <spring:message code="schoollist.schoolName" var="schoolName"/>
                            <input class="modifyinput" placeholder="${schoolName}" type="text"
                                   v-model="create.schoolName"/>
                            <br>
                            <spring:message code="schoollist.schoolID" var="schoolID"/>
                            <input class="modifyinput" placeholder="${schoolID}" type="text"
                                   v-model="create.schoolId" v-if="flag == false"/>
                            <input class="modifyinput" placeholder="${schoolID}" type="text"
                                   v-model="create.schoolId" v-else disabled/>
                            <br>
                        </div>
                    </div>
                    <br>
                    <div class="btn-wrap">
                        <button class="btn-gray shadow" type="button" @click="createSchool = false">
                            <spring:message code="schoollist.cancel"/>
                        </button>
                        <button v-if="flag == true" type="button" class="btn-red shadow" @click="modifySchool()">
                            <spring:message code="schoollist.modify"/>
                        </button>
                        <button v-else type="button" class="btn-red shadow" @click="createSch()">
                            <spring:message code="schoollist.save"/>
                        </button>
                    </div>
                    <br>
                    <br>
                </form>
            </div>
        </div>

        <div id="classmodal" class="modalDialog">
            <div class="modal-mask">
                <div class="modal-wrapper">
                    <div class="modal-container" style="width: 600px;">
                        <a href="#close" title="modelclose" class="modelclose">X</a>
                        <div class="modal-header">
                            <p> <spring:message code="classlist.schoolAdd" /></p>
                        </div>
                        <div class="modal-body" style="padding: 38px;">
                            <spring:message code="classlist.schoolName" var="classlist"/>
                            <input class="modifyinput" placeholder="${classlist}" type="text" style="margin-top: 0px;"
                                   v-model="create.groupName"/>
                            <br>
                            <spring:message code="classlist.groupNo" var="groupNo"/>
                            <input class="modifyinput" placeholder="${groupNo}" type="text"
                                   v-model="create.groupNo"/>
                            <br>
                            <spring:message code="schoollist.schoolName" var="schoolName"/>
                            <input class="modifyinput" placeholder="${schoolName}" type="text"
                                   disabled v-model="schoolName"/>
                        </div>
                        <div class="modal-footer flex">
                            <a href="#close" class="modal-default-button" type="button">
                                <spring:message code="schoollist.cancel"/>
                            </a>
                            <button class="modal-default-button margin-left" type="button" @click="saveClass()">
                                <spring:message code="schoollist.save"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="classSeeModal" class="modalDialog">
            <div class="modal-mask">
                <div class="modal-wrapper">
                    <div class="modal-container" style="width: 600px;">
                        <a href="#close" title="modelclose" class="modelclose">X</a>
                        <div class="modal-header">
                            <p> <spring:message code="classlist.schoolAdd" /></p>
                        </div>
                        <div class="modal-body" style="height: 530px; overflow: auto; ">
                            <%--                            <div class="flex">--%>
                            <%--                                <input class="" placeholder="" v-model="search.groupName"/>--%>
                            <%--                                <img src="${IMG}/icons/search.png" @click="classListSee"/>--%>
                            <%--                            </div>--%>
                            <div class="modal-table">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                       style="table-layout: auto;">
                                    <colgroup>
                                        <col width="4%">
                                        <col width="15%">
                                        <col width="15%">
                                    </colgroup>
                                    <thead>
                                    <tr class="title_table border-red-top-2">
                                        <th>
                                            <span>Дугаар</span>
                                        </th>
                                        <th>
                                            <span>Class name</span>
                                        </th>
                                        <th>
                                            <span>Class ID</span>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody v-if="classList.length > 0">
                                    <tr v-for="(item,index) in classList" :key="index"
                                    >
                                        <td>
                                            {{index+1}}
                                        </td>
                                        <td>
                                            {{item.group_name}}
                                        </td>
                                        <td>
                                            {{item.class_id}}
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody  v-else>
                                    <tr>
                                        <td colspan="3">
                                            There is no information
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer flex">
                            <a href="#close" class="modal-default-button" type="button">
                                <spring:message code="all.close"/>
                            </a>
                            <%--                            <button class="modal-default-button margin-left" type="button" @click="saveClass()">--%>
                            <%--                                <spring:message code="schoollist.save"/>--%>
                            <%--                            </button>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    let app = new Vue({
        components: {},
        data: {
            newUModal: false,
            createSchool: false,
            updateSchool: false,
            classModel: false,
            search: {
                usersId: '',
                schoolName: '',
                groupNo: '',
                groupName: ''
            },
            create: {
                schId: '',
                schoolName: '',
                schoolId: '',
                image: 'https://mongolia-today.mn/wp-content/uploads/2017/08/%D1%85%D1%83%D1%83%D0%BB%D1%8C-%D1%81%D0%B0%D1%85%D0%B8%D1%83%D0%BB%D0%B0%D1%85%D0%B8%D0%B9%D0%BD-%D1%81%D1%83%D1%80%D0%B3%D1%83%D1%83%D0%BB%D1%8C.jpg',
                groupNo: '',
                groupName: ''
            },
            update:{
                schoolName: '',
                schoolId:'',
                image:''
            },
            schoolList: [],
            classList: [],
            currentPage: 1,
            itemsPerPage: 1,
            schoolCode: '',
            schoolName: '',
            pages: 0,
            totalElement: 0,
            flag: false
        },
        computed: {
            paginate: function () {
                if (!this.schoolList || this.schoolList.length != this.schoolList.length) {
                    return
                }
                this.resultCount = this.schoolList.length
                if (this.currentPage >= this.pages) {
                    this.currentPage = this.pages
                }
                var index = this.currentPage * this.itemsPerPage - this.itemsPerPage
                return this.schoolList.slice(index, index + this.itemsPerPage)
            }
        },
        mounted() {

        },
        created() {
            this.loaddata(this.currentPage)
        },
        methods: {
            loaddata(page){
                try {
                    if (page > 0) {
                        page = page - 1
                    }

                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    let item = {
                        page: page,
                        schName: this.search.schoolName,
                        schCode: this.search.usersId,
                    }
                    var _this = this

                    axios.get('${BASEURL}/school/list',{
                        params: item,
                        headers: headers
                    }).then(function (response) {
                        console.log(response)
                        if (response.status == 200) {
                            console.log(response.data.content);
                            _this.schoolList = response.data.content
                            _this.pages = response.data.totalPages;
                            _this.totalElement = response.data.totalElements;
                        } else {
                            console.log(response.data.message);
                        }

                    },(error) => {
                        console.log(error);
                        alert('Error')
                    });

                } catch (error) {
                    console.log(error)
                }
            },
            createClassModel(schCode, schoolName){
                this.create.schName = ''
                this.create.schCode = ''
                this.create.groupName = ''
                this.create.groupNo = ''
                this.schoolCode = schCode
                this.schoolName = schoolName
                console.log("school name _ " + schoolName + " __ schCode __ "+ schCode)
                window.location.href = "#classmodal"
            },
            saveClass(){
                try {
                    if (this.create.groupName != '' && this.create.groupNo != '')
                    {
                        var language =  "<%=pageContext.getResponse().getLocale()%>"
                        var locale = ''
                        if (language == 'kr')
                        {
                            locale = 'kr-KR'
                        }
                        if(language == 'mn')
                        {
                            locale = 'mn-MN'
                        }
                        if(language == 'en'){
                            locale = 'en-EN'
                        }
                        const headers = {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                            'Accept-Language' : locale
                        }
                        var _this = this;

                        let item = {
                            groupNo: this.create.groupNo,
                            groupName: this.create.groupName,
                            schCode: this.schoolCode
                        };
                        axios.post('${BASEURL}/kexam/addnewclass', item, {
                            headers: headers
                        }).then(function (response) {
                            if (response.data.status == 200) {
                                alert(response.data.message);
                                window.location.href = "#close"
                                _this.loaddata(_this.currentPage);
                            } else {
                                alert(response.data.message);
                            }
                        }, (error) => {
                            console.log(error);
                            alert('Error')
                        });
                    }
                    else{
                        if (this.create.groupName == '')
                        {
                            alert("Please write your class name")
                        }
                        else{
                            alert("Please write your class ID.")
                        }
                    }

                } catch (error) {
                    console.log(error)
                }
            },
            createModalShow(){
                this.create.schoolId = ''
                this.create.schoolName = ''
                this.createSchool = true
            },
            updateSchlvl(name, id, schId){
                this.create.schId = schId
                this.create.schoolName = name
                this.create.schoolId = id
                this.createSchool = true
                this.flag = true
            },
            createSch(){
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this;

                    let item = {
                        schName: this.create.schoolName,
                        schCode: this.create.schoolId,
                        schImage: this.create.image
                    };
                    axios.post('${BASEURL}/school/create', item, {
                        headers: headers
                    }).then(function (response) {
                        if (response.data.status == 200) {
                            alert(response.data.message);
                            _this.createSchool = false;
                            _this.loaddata(_this.currentPage);
                        } else {
                            alert(response.data.message);
                        }

                    }, (error) => {
                        console.log(error);
                        alert('Error')
                    });

                } catch (error) {
                    console.log(error)
                }
            },
            classListSee(schoolCode){
                try {

                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }

                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    let item = {
                        groupName: this.search.groupName,
                        schCode: schoolCode,
                    }
                    var _this = this

                    axios.get('${BASEURL}/kexam/school/classes',{
                        params: item,
                        headers: headers
                    }).then(function (response) {
                        if (response.status == 200) {
                            _this.classList = response.data
                            window.location.href = "#classSeeModal"
                        } else {
                            console.log(response.data.message);
                        }
                    },(error) => {
                        console.log(error);
                        alert('Error')
                    });

                } catch (error) {
                    console.log(error)
                }
            },
            modifySchool(){
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    var _this = this;
                    let item = {
                        id: this.create.schId,
                        schName: this.create.schoolName,
                        schCode: this.create.schoolId,
                        schImage: this.create.image
                    };
                    axios.post('${BASEURL}/school/modify', item, {
                        headers: headers
                    }).then(function (response) {
                        if (response.data.status == 200) {
                            alert(response.data.message);
                            _this.createSchool = false;
                            _this.loaddata(_this.currentPage);
                        } else {
                            alert(response.data.message);
                        }

                    }, (error) => {
                        console.log(error);
                        alert('Error')
                    });

                } catch (error) {
                    console.log(error)
                }
            },
            nextQuestion() {
                if (this.pages > this.currentPage) {
                    this.currentPage = this.currentPage + 1
                    this.loaddata(this.currentPage)
                }
            }
            ,
            prevQuestion() {
                if (this.currentPage > 1) {
                    this.currentPage = this.currentPage - 1
                    this.loaddata(this.currentPage)
                }
            }
            ,
            lastQuestion() {
                this.currentPage = this.pages
                this.loaddata(this.currentPage)
            }
            ,
            firstQuestion() {
                this.currentPage = 1
                this.loaddata(this.currentPage)
            }
            ,
            async searchFilter() {
                this.currentPage = 1
                this.loaddata(this.currentPage);
            }
            ,
            defaultSearch() {
                window.location.reload()
            }
        }
    });
    app.$mount('#examlist')
</script>
<style scoped>
    .my-datepicker input {
        padding: 9px 9px 9px 0;
        border-radius: 8px;
        text-align: center;
    }

    .checkbutton {
        background-color: #bdbdbd;
        border-radius: 2px;
        width: 19px;
        height: 19px;
        display: inline;
        flex: auto;
        margin: 5px;
    }

    .checkbutton.active {
        background-color: #ee753d;
    }

    .modifyinput {
        width: 100%;
        border: 1px solid #eee;
        padding: 5px;
        outline: none;
    }
</style>
</body>
</html>

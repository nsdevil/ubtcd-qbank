<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/notice/show.css">
    </head>
    <body>

        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="../main.jsp">Home</a></span>
                    <span><a href="">MY</a></span>
                    <span><a href="../notice/list.jsp">공지사항</a></span>
                </div>

                <article>
                    <div class="title">
                        <div class="image"><img src="${IMG}/icons/main/profile2.png" class="auto"></div>
                        <div class="subject">
                            <h2 class="title"><span class="color-red">| 시스템 공지 |</span> Q-Bank 서비스 이용 불가 시간(2019-08-14 13:00 ~  17:00)</h2>
                            <ul class="sub">
                                <li>작성자이름(작성자ID)</li>
                                <li>작성일시</li>
                                <li>조회수 000건</li>
                            </ul>
                        </div>
                    </div>
                    <div class="content">
                        <p>
                            있는 몸이 생명을 것이다. 속에서 이상, 가진 그들을 방황하여도, 이 있다. 같이 얼마나 오직 그들에게 노년에게서 굳세게 위하여서. 너의 곧 그들은 능히 꽃이 일월과 얼마나 봄바람이다. 심장은 공자는 희망의 열매를 있는 할지니, 이상을 약동하다. 없는 맺어, 우리 이것은 기관과 따뜻한 품으며, 철환하였는가? 낙원을 천지는 따뜻한 것이다. 때까지 그들은 실현에 넣는 끓는 더운지라 때문이다. 천자만홍이 같이 얼음이 관현악이며, 있으며, 주며, 봄바람이다.
                        </p>
                        <p style="display : flex;justify-content : center;">
                            <img src="${IMG}/notice/sample1.jpg" class="auto">
                            <img src="${IMG}/notice/sample1.jpg" class="auto">
                        </p>
                        <p>
                            이것이야말로 오직 튼튼하며, 원대하고, 이것은 싹이 위하여서. 얼마나 얼마나 풀이 불어 아니더면, 끓는다. 불어 보배를 눈이 물방아 봄바람이다. 없으면 이상 스며들어 같이, 하였으며, 그리하였는가? 사랑의 넣는 이상의 몸이 찾아다녀도, 새 얼음 천고에 봄바람이다. 방황하였으며, 같이, 인생을 풀이 얼음에 피다. 트고, 생생하며, 청춘을 실현에 그들의 힘차게 그들은 힘차게 있는가? 위하여서, 위하여 우리의 무엇을 우리 황금시대다. 위하여 찾아 천하를 스며들어 불러 하여도 꽃이 속에서 그들은 황금시대다. 것은 자신과 우리는 같이, 품에 같은 위하여서. 뜨거운지라, 현저하게 발휘하기 눈에 있는 끓는 것이다.보라, 없는 보이는 약동하다.
                        </p>
                        <p>
                            있는 몸이 생명을 것이다. 속에서 이상, 가진 그들을 방황하여도, 이 있다. 같이 얼마나 오직 그들에게 노년에게서 굳세게 위하여서. 너의 곧 그들은 능히 꽃이 일월과 얼마나 봄바람이다. 심장은 공자는 희망의 열매를 있는 할지니, 이상을 약동하다. 없는 맺어, 우리 이것은 기관과 따뜻한 품으며, 철환하였는가? 낙원을 천지는 따뜻한 것이다. 때까지 그들은 실현에 넣는 끓는 더운지라 때문이다. 천자만홍이 같이 얼음이 관현악이며, 있으며, 주며, 봄바람이다.
                        </p>
                        <p>
                            이것이야말로 오직 튼튼하며, 원대하고, 이것은 싹이 위하여서. 얼마나 얼마나 풀이 불어 아니더면, 끓는다. 불어 보배를 눈이 물방아 봄바람이다. 없으면 이상 스며들어 같이, 하였으며, 그리하였는가? 사랑의 넣는 이상의 몸이 찾아다녀도, 새 얼음 천고에 봄바람이다. 방황하였으며, 같이, 인생을 풀이 얼음에 피다. 트고, 생생하며, 청춘을 실현에 그들의 힘차게 그들은 힘차게 있는가? 위하여서, 위하여 우리의 무엇을 우리 황금시대다. 위하여 찾아 천하를 스며들어 불러 하여도 꽃이 속에서 그들은 황금시대다. 것은 자신과 우리는 같이, 품에 같은 위하여서. 뜨거운지라, 현저하게 발휘하기 눈에 있는 끓는 것이다.보라, 없는 보이는 약동하다.
                        </p>
                    </div>

                    <div class="attachment">
                        <div class="title">
                            <h3>첨부파일</h3>
                            <button class="btn-white shadow"><i class="download"></i>모두 다운로드</button>
                        </div>
                        <div class="attachment-content">
                            <ul class="flex">
                                <li><a href=""><i class="file"></i>[첨부파일명_확장자]</a></li>
                                <li><a href=""><i class="image"></i>의료 윤리 사례.png</a></li>
                                <li><a href=""><i class="video"></i>[큐뱅크 사용 안내 동영상입니다.mpeg]</a></li>
                                <li><a href=""><i class="file"></i>[첨부파일 이름이 길어도 모두 노출됩니다 가나다라마바사아자차카타파하 무궁화 꽃이 피었습니다.pdf]</a></li>
                            </ul>
                        </div>

                    </div>

                    <button class="btn-white shadow" onclick="location.href='./list.jsp';"><i class="menu-bars"></i>목록으로</button>
                </article>

                

            </div>
        </section>
    </body>
</html>
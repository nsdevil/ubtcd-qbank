<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <title>Survey</title>
    <link rel="stylesheet" href="${CSS}/common/header.css">
    <link rel="stylesheet" href="${CSS}/common/footer.css">
    <link rel="stylesheet" href="${CSS}/notice/list.css">
    <link rel="stylesheet" href="${CSS}/my/show.css">
    <link rel="stylesheet" href="${CSS}/question_registration/register_information.css">

    <style>
        .displaydiv {
            display: block !important;
        }
        article .detail {
            width: 900px;
        }
        article ul li{
            width: 100%;
        }
        article .detail .desc {
            border: none;
        }
        .imgtag{
            width: 50px;
            margin-left: 12px;
        }
        .imgtag2{
            width: 40px;
            margin-left: 12px;
            height: 40px;
            margin-left: 12px;
        }
        .answerSpan{
            font-size: 20px;
            margin-top: 6px;
            border: 1px solid #000;
            border-radius: 50%;
            width: 30px;
            height: 30px;
            padding: 5px 3px 3px 8px;
            margin-right: 15px;
        }
        .survey-question{
            border-bottom: 1px solid !important;
            margin-bottom: 20px;
        }
        input{
            padding-left: 10px;
        }
        .btn-red-line{
            padding: 0px 5px;
            height: 40px;
        }
        textarea{
            padding-left: 10px;
        }
        .surveyTotal{
            margin-top: 10px;
            font-size: 16px;
            margin-right: 20px;
        }
        .element{
            font-weight: 900;
            font-size: 18px;
        }
        .btn-classes{
            background: #fb5f33;
            border: 1px solid #fb5f33;
            border-radius: 15px;
            color: #fff;
            text-transform: capitalize;
            padding: 10px;
            width: auto;
        }
        .attach{
            color: #fff;
            background-color: #fb5f33;
            align-items: center;
            justify-content: center;
            width: 110px;
            height: 26px;
            border: 0;
            border-radius: 2px;
        }
        .unattach{
            color: #fff;
            background-color: #a3a3a3;
            align-items: center;
            justify-content: center;
            width: 110px;
            height: 26px;
            border: 0;
            border-radius: 2px;
        }
        #se-pre-con {
            position: fixed;
            -webkit-transition: opacity 5s ease-in-out;
            -moz-transition: opacity 5s ease-in-out;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
        }
    </style>
</head>
<body>
<div id="se-pre-con" style="display: none"></div>
<section id="surveyList">
    <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
        <div class="breadcrumb">
            <span><a href="../main.jsp">Home</a></span>
            <span><a href="">Survey</a></span>
            <span><a href="../notice/list">list</a></span>
        </div>

        <h2><spring:message code="survey.name"/></h2>

        <div class="search-wrap flex">
            <div class="search-form">
                <form name="searchForm" method="get" action="">
                    <%--                            <select class="select2" name="type" data-placeholder="구분">--%>
                    <%--                                <option value=""></option>--%>
                    <%--                                <option value="all" selected>전체</option>--%>
                    <%--                                <option value="notice_type">공지구분</option>--%>
                    <%--                                <option value="title">제목</option>--%>
                    <%--                                <option value="date">게시일</option>--%>
                    <%--                            </select>--%>
                    <input type="text" name="keyword" placeholder="Search" name="searchsurvey" v-model="surveyName">
                    <button><img src="${IMG}/icons/search.png" class="auto"></button>
                </form>
            </div>
            <span class="surveyTotal"><spring:message code="survey.total"/>: <span class="element">{{totalElement}}</span> </span>
            <button class="btn-red shadow" @click="popupClose('create')">
                <i class="plus"></i><spring:message code="survey.register"/>
            </button>
        </div>
        <div class="table-wrap">
            <table>
                <colgroup>
                    <col style="width : 70px;">
                    <col style="width : 200px;">
                    <col>
                    <col style="width : 120px;">
                    <col style="width : 200px;">
                    <col style="width : 200px;">
                    <col style="">
                </colgroup>
                <thead class="border-red-top-2 border-gray-bottom-2">
                <tr>
                    <th>
                        <spring:message code="all.number"/>
                    </th>
                    <th>
                        <spring:message code="survey.title"/>
                    </th>
                    <th>
                        <spring:message code="survey.content"/>
                    </th>
                    <th>
                        <spring:message code="survey.regUser"/>
                    </th>
                    <th>
                        Choose class
                    </th>
                    <th>
                        <spring:message code="survey.totalQuestion"/>
                    </th>
                    <th>
                        Activate
                    </th>
                    <th>
                        <spring:message code="survey.details"/>
                    </th>
                </tr>
                </thead>
                <tbody v-if="surveyList.length > 0">
                <tr v-for="(item,index) in surveyList">
                    <td>{{index+1}}</td>
                    <td>{{item.title}}</td>
                    <td style="padding: 10px;">{{item.memo}}</td>
                    <td>{{item.regUser}}</td>
                    <td>
                        <button v-if="item.rFlag == 'student'" class="btn-classes" @click="popupClass(item.id, item.attachedClasses)">
                            Students
                        </button>
                        <span v-else-if="item.rFlag == 'teacher'">
                                       Teachers
                                    </span>
                        <button v-else class="btn-classes" @click="popupClass(item.id, item.attachedClasses)">
                            Parents
                        </button>
                    </td>
                    <td>{{item.qCount}}</td>
                    <td>
                        <button v-if="item.useFlag == 'Y'"  class="attach" @click="useFlag('N', item.id)">
                            Unactivate
                        </button>
                        <button  class="unattach"  v-else @click="useFlag('Y', item.id)">
                            Activate
                        </button>
                    </td>
                    <td>
                        <div v-if="item.regUser == '${sessionScope.S_LOGINID}' || '${sessionScope.S_ROLE}' == 'ROLE_ADMIN'">
                            <button class="btn-edit" @click="detailsSurvey(item.id)" style="margin: 0px;">
                                <img src="${IMG}/icons/question_card/update.png"
                                     style="width: 27px;">
                            </button>
                            <button @click="deleteSurveyModal(item)">
                                <img src="${IMG}/icons/question_card/delete.png"
                                     style="margin: 0px;">
                            </button>
                            <%--                                    <button class="btn-edit" @click="surveyDetail(item)"--%>
                            <%--                                            style="margin: 0px;">--%>
                            <%--                                        <img src="${IMG}/icons/menu-bars.png"--%>
                            <%--                                             style="width: 27px;">--%>
                            <%--                                    </button>--%>
                        </div>
                    </td>
                </tr>
                </tbody>
                <tbody v-else>
                <tr>
                    <td colspan="8">
                        <spring:message code="all.empty"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="pagination">
            <button class="start" @click="firstQuestion()"><img src="${IMG}/icons/start.png" class="auto"></button>
            <button class="prev" @click="prevQuestion()"><img src="${IMG}/icons/prev.png" class="auto"></button>
            <ul class="paginate-list">
                <li class="page-num " v-for="(item,index) in pages " :key="index"
                    :class="{'active': currentPage === item}"
                    v-if="Math.abs(item - currentPage) < 3 || item == pages || item == 1">
                    <a href="javascript:void(0);" @click="questionCurrent(item)" :class="{
                            last: (item == pages && Math.abs(item - currentPage) > 3),
                            first: (item == 1 && Math.abs(item - currentPage) > 3)}">{{item}}</a>
                </li>
            </ul>
            <button class="next" @click="nextQuestion()"><img src="${IMG}/icons/next.png" class="auto"></button>
            <button class="end" @click="lastQuestion()"><img src="${IMG}/icons/end.png" class="auto"></button>
        </div>
    </div>

    <div v-show="surveyModal" class="modalDialog" :class="{'displaydiv': surveyModal}"
         style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <a href="javascript:void(0)" @click="popupClose('close')" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p style="font-weight: 800" v-if="createSurvey==true"><spring:message code="survey.register"/></p>
                        <p style="font-weight: 800" v-else><spring:message code="update"/></p>
                    </div>
                    <div class="modal-body">
                        <article>
                            <div class="detail">
                                <ul>
                                    <li style="border-bottom: 0px">
                                        <div class="flex" style="margin-left: 20%">
                                            <button class="btn-red-line" :class="{ 'btn-red': rFlag == 'student' }"
                                                    @click="surveyFlag('student')" style="height: 25px; width: 120px;">
                                                Students</button>
                                            <button class="btn-red-line margin20" :class="{ 'btn-red': rFlag == 'teacher' }"
                                                    @click="surveyFlag('teacher')" style="height: 25px; width: 136px;">
                                                Teachers
                                            </button>
                                            <button class="btn-red-line margin20" :class="{ 'btn-red': rFlag == 'parents' }"
                                                    @click="surveyFlag('parents')" style="height: 25px; width: 120px;">
                                                Parents</button>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="title"><spring:message code="survey.title"/> **
                                            <spring:message code="survey.title" var="titles"/></div>
                                        <input type="text" class="desc" placeholder="${titles}" v-model="title"
                                               name="surveyTitle"/>
                                    </li>
                                    <li>
                                        <div class="title"><spring:message code="survey.content"/> **
                                            <spring:message code="survey.content" var="contents"/></div>
                                        <textarea type="text" class="desc" placeholder="${contents}" v-model="data"
                                                  style="outline: none; padding: 0px;">
                                                </textarea>
                                    </li>
                                    <li class="" style="border-bottom: none; display: -webkit-box; width: 100%; padding-bottom: 20px; overflow: auto">
                                        <template v-for="(item, index) in questions">
                                            <div  :key="index">
                                                <a class="btn-red-line" style="margin-right: 10px; height: 50px; width: 35px;"
                                                   :class="{ 'checked': index == activeOption }"
                                                   @click="selectType(index)">{{index+1}}</a>
                                            </div>
                                        </template>
                                        <div class="flex">
                                            <img src="${IMG}/icons/add.png" class="imgtag" @click="addType()" v-if="createSurvey== true"/>
                                            <img src="${IMG}/icons/add.png" class="imgtag" @click="addTypeUpdate()" v-else/>
                                            <img src="${IMG}/icons/minus.png"  class="imgtag" @click="removeType()"/>
                                        </div>
                                    </li>
                                    <li style="border-bottom: 0px">
                                        <div class="flex">
                                            <button class="btn-red-line" :class="{ 'btn-red': questions[activeOption].type == 0 }"
                                                    @click="typeChange(0)" style="height: 25px; width: 120px;">
                                                <spring:message code="sChoice"/></button>
                                            <button class="btn-red-line margin20" :class="{ 'btn-red': questions[activeOption].type == 2 }"
                                                    @click="typeChange(2)" style="height: 25px; width: 136px;">Олон сонголттой</button>
                                            <button class="btn-red-line margin20" :class="{ 'btn-red': questions[activeOption].type == 1 }"
                                                    @click="typeChange(1)" style="height: 25px; width: 120px;">
                                                <spring:message code="question.register.shortAnswer"/></button>
                                        </div>
                                    </li>
                                    <li v-if="questions[activeOption].type == 1">
                                        <span class="title"><spring:message code="qBank.question"/> **</span>
                                        <textarea
                                                class="desc"
                                                style="width: 100%; margin-top: 25px; outline: none; border: 1px solid #dee2e6;padding: 10px;"
                                                v-model="questions[activeOption].question"
                                                rows="4"
                                                placeholder="Please write your question here."
                                        ></textarea>
                                    </li>
                                    <li v-else style="border-bottom: 0px">
                                        <div class="title"><spring:message code="survey.question"/> **
                                            <spring:message code="survey.question" var="questions"/></div>
                                        <input type="text" class="desc survey-question" placeholder="${questions}" name="questionActive"
                                               v-model="questions[activeOption].question"/>
                                        <div v-for="(item,index) in questions[activeOption].answers" :key="index" class="flex" style="margin-bottom: 10px;">
                                            <span class="answerSpan">{{index+1}}</span>
                                            <input type="text" class="" value="" style="width: 80%; height: 40px; border: 1px solid #dee2e6;" placeholder="answer"
                                                   v-model="questions[activeOption].answers[index].answer"/>
                                            <div class="flex">
                                                <img src="${IMG}/icons/add.png" class="imgtag2" @click="addAnswer()" />
                                                <img src="${IMG}/icons/minus.png"  class="imgtag2" @click="removeAnswer(index)"/>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="clear flex" style="margin-left: 30%; margin-top: 20px;">
                                    <button type="button" class="btn-red shadow" @click="surveyRegister()" v-if="createSurvey==true"
                                            style="height: 39px; margin-right: 10px;">
                                        <spring:message code="examlist.register" /></button>
                                    <button type="button" class="btn-red shadow" @click="surveyUpdate()" v-else
                                            style="height: 39px; margin-right: 10px;">
                                        <spring:message code="update" /></button>
                                    <button type="button" @click="popupClose('close')" class="btn-gray shadow"
                                            style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                                        <spring:message code="all.close" /></button>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--            end question survey --%>
    <%--            remove modal --%>
    <div id="surveyDeleteModal" class="modalDialog">
        <div class="modal-mask">
            <div class="modal-wrapper"  style="top: 18%; right: -30%; position: absolute;">
                <div class="modal-container">
                    <a href="#close" title="modelclose" class="modelclose">X</a>
                    <div style="text-align: center; border-bottom: 2px solid #deefee;">
                        <p style="font-size: 18px; padding: 18px;"><spring:message code="survey.removeTitle" /></p>
                    </div>
                    <div class="modal-header" style="height: 200px; border-top: 2px;">
                        <img src="${IMG}/icons/preview/survey.png" style="width: 100px; padding-top: 17px;">
                        <p><spring:message code="survey.remove" /></p>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer flex">
                        <a href="#close" class="modal-default-button" type="button">Close</a>
                        <button @click="deleteSurvey()" class="modal-default-button margin-left"
                                style="color: #fff; background: #fb5f33"
                                type="button">
                            <spring:message code="question.register.delete" />
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--            end remove modal--%>
    <%--            attach student --%>
    <div v-show="classModal" class="modalDialog" :class="{'displaydiv': classModal}"
         style="opacity: 1;pointer-events: painted; overflow: scroll;
                    background: rgba(0, 0, 0, 0.8); display: none">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <a href="javascript:void(0)" @click="popupClass()" title="modelclose" class="modelclose">X</a>
                    <div class="modal-header">
                        <p style="font-weight: 800">Survey add to class</p>
                    </div>
                    <div class="modal-body">
                        <article>
                            <div class="detail" style=" padding: 16px;">
                                <div style="margin-bottom: 20px;">
                                    <table>
                                        <col style="width : 10%;">
                                        <col style="width : 10%;">
                                        <thead class="border-red-top-2 border-gray-bottom-2">
                                        <tr>
                                            <td>Search by class name</td>
                                            <td>
                                                <input type="text" class="input-form" style="border: 1px solid #dee2e6"
                                                       v-model="clasgroupNo" v-on:keyup.enter="schoolClassList()">
                                            </td>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="table-wrap" style="overflow-x: hidden; max-height: 500px;">
                                    <table>
                                        <colgroup>
                                            <col style="width : 4%;">
                                            <col style="width : 23%;">
                                            <col style="width : 8%;">
                                            <col style="width : 8%;">
                                        </colgroup>
                                        <thead class="border-red-top-2 border-gray-bottom-2">
                                        <tr>
                                            <td></td>
                                            <td>Class name</td>
                                            <td>Student count</td>
                                            <td>Choose</td>
                                        </tr>
                                        </thead>
                                        <tbody v-if="classlist.length > 0">
                                        <tr v-for="(item,index) in classlist" :key="index">
                                            <td>{{index+1}}</td>
                                            <td class="t-left">{{item.group_name}}</td>
                                            <td class="t-left">{{item.scount}}</td>
                                            <td class="t-left">
                                                <button class="attach"  v-if="item.checked == false"  @click="attachSurvey(item)">
                                                    <spring:message code="survey.attach" />
                                                </button>
                                                <button class="unattach" v-else @click="unattachSurvey(item)">
                                                    <spring:message code="survey.unattach" />
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody v-else>
                                        <tr>
                                            <td colspan="4">
                                                <spring:message code="all.empty" />
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="clear flex" style="margin-left: 45%; margin-top: 20px;">
                                    <%--										<button type="button" class="btn-red shadow" @click="a()"--%>
                                    <%--												style="height: 39px; margin-right: 10px;">--%>
                                    <%--											<spring:message code="examlist.register" /></button>--%>
                                    <button type="button" @click="popupClass()" class="btn-gray shadow"
                                            style="width: 145px; height: 39px; font-size: 16px; font-weight: 700; margin-right: 15px;">
                                        <spring:message code="all.close" /></button>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--            end attach student--%>
</section>
<script type="text/javascript">
    var app = new Vue({
        data: {
            surveyList: [],
            currentPage: 1,
            pages: 0,
            itemsPerPage: 1,
            totalElement: 0,
            surveyModal: false,
            classModal: false,
            actionType:'insert',
            title: "",
            data: "",
            rFlag: "student",
            questions: [
                {
                    id: '',
                    number:1,
                    question: "",
                    delete: false,
                    type: 0,
                    answers: [
                        {
                            answer: ""
                        }
                    ]
                }
            ],
            activeOption: 0,
            anslib: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            surList: [],
            resultAnswer: "",
            surDetails: [],
            selected: [],
            researchId: 0,
            openedModal:'',
            actionType: '',
            surveyName: '',
            updateId: '',
            deleteSurveyId: '',
            clasgroupNo: '',
            classlist: [],
            attachSurId: '',
            surIds: [],
            createSurvey: false
        },
        watch: {},
        computed: {
            paginate: function () {
                if (!this.surveyList || this.surveyList.length != this.surveyList.length) {
                    return
                }
                this.resultCount = this.surveyList.length
                if (this.currentPage >= this.pages) {
                    this.currentPage = this.pages
                }
                var index = this.currentPage * this.itemsPerPage - this.itemsPerPage
                return this.surveyList.slice(index, index + this.itemsPerPage)
            }
        },
        filters: {},
        mounted: function () {
        },
        created(){
            this.loaddata()
        },
        methods: {
            surveyFlag(item){
                this.rFlag = item
            },
            typeChange(type){
                this.questions[this.activeOption].type = type
            },
            deleteSurveyModal(item){
                this.deleteSurveyId = item.id
                window.location.href = "#surveyDeleteModal"
            },
            deleteSurvey(){
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this
                    let arrays = [this.deleteSurveyId];
                    axios.post('${BASEURL}/research/delete', arrays,
                        {headers:headers}).then(function (response) {
                        if (response.data.status == 200) {
                            if (response.data.success == true)
                            {
                                alert("<spring:message code="essentail.success" />");
                                window.location.href = "#close"
                                _this.loaddata()

                            }
                            else{
                                alert(response.data.message);
                            }
                        } else {
                            alert(response.data.message);
                        }
                    });
                } catch (error) {
                    console.log(error);
                    alert(error)
                }
            },
            useFlag(useflag, id){
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this

                    let item = {
                        useFlag: useflag,
                        researchId: id,
                    };

                    axios.post('${BASEURL}/research/useflag',item,
                        {headers:headers}).then(function (response) {
                        if (response.data.status == 200) {
                            if (response.data.success == true)
                            {
                                alert("<spring:message code="essentail.success" />");
                                _this.loaddata()

                            }
                            else{
                                alert(response.data.message);
                            }
                        } else {
                            alert(response.data.message);
                        }
                    });
                } catch (error) {
                    console.log(error);
                    alert(error)
                }
            },
            detailsSurvey(rId){
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    this.updateId = rId
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }

                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    var _this = this

                    axios.get('${BASEURL}/research/'+ rId,
                        {
                            params:{},
                            headers:headers
                        }).then(function (response) {
                        console.log("lol lol")
                        console.log(response)
                        if (response.data.status == 200) {
                            if (response.data.success == true)
                            {
                                let detail = response.data.result.researchDetail;
                                _this.title = response.data.result.researchDetail.title
                                _this.data = response.data.result.researchDetail.memo
                                _this.rFlag = response.data.result.researchDetail.rFlag
                                _this.createSurvey = false
                                _this.questions = []

                                for (let a = 0; a < detail.questions.length; a++) {
                                    let question = detail.questions[a];
                                    let quesItem = {
                                        number:a+1,
                                        delete:question.delete,
                                        id: question.id,
                                        question: question.question,
                                        type: question.type
                                    };
                                    var answers = question.answers.split("||");
                                    let anses = [];
                                    for (let i = 0; i < answers.length; i++) {
                                        let answer = answers[i];
                                        let ans = {
                                            answer: answer
                                        };
                                        anses.push(ans);
                                    }
                                    quesItem.answers = anses;
                                    _this.questions.push(quesItem);
                                }
                                _this.popupClose('update')
                            }
                            else{
                                alert(response.data.message);
                            }
                        } else {
                            alert(response.data.message);
                        }
                    });
                } catch (error) {
                    console.log(error);
                    alert(error)
                }
            },
            surveyRegister(){
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    var _this = this

                    var  schCode = ''

                    if('${sessionScope.S_ROLE}'=='ROLE_ADMIN')
                    {
                        schCode = 'admin'
                    }
                    else{
                        schCode = '${sessionScope.S_SCHCODE}'
                    }
                    let item = {
                        title: this.title,
                        data: this.data,
                        rFlag: this.rFlag,
                        schCode: schCode,
                        questions: this.questions
                    };
                    axios.post('${BASEURL}/research/create', item ,
                        {headers:headers}).then(function (response) {
                        if (response.data.status == 200) {
                            if (response.data.success == true)
                            {
                                alert(response.data.message);
                                window.location.reload()
                            }
                            else{
                                alert(response.data.message);
                            }
                        } else {
                            alert(response.data.message);
                        }
                    });
                } catch (error) {
                    console.log(error);
                    alert(error)
                }
            },
            surveyUpdate(){
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }

                    var _this = this

                    let item = {
                        id: this.updateId,
                        title: this.title,
                        data: this.data,
                        rFlag: this.rFlag,
                        schCode: '${sessionScope.S_SCHCODE}',
                        questions: this.questions
                    };
                    axios.post('${BASEURL}/research/modify', item,
                        {headers:headers}).then(function (response) {
                        if (response.data.status == 200) {
                            if (response.data.success == true)
                            {
                                alert(response.data.message);
                                // _this.popupClose('close')
                                // _this.loaddata()
                                window.location.reload()
                            }
                            else{
                                alert(response.data.message);
                            }
                        } else {
                            alert(response.data.message);
                        }
                    });
                } catch (error) {
                    console.log(error);
                    alert(error)
                }
            },
            addAnswer() {
                let ans = {
                    number: this.anslib[this.questions[this.activeOption].answers],
                    answers: []
                };
                if (this.questions[this.activeOption].answers.length > 9) {
                    alert("Maximum answer is 9.");
                } else {
                    this.questions[this.activeOption].answers.push(ans);
                }
            },
            removeAnswer(index) {
                if (this.questions[this.activeOption].answers.length < 2) {
                    alert("at least one answer left.");
                    return;
                }
                else{
                    this.questions[this.activeOption].answers.splice(index, 1);
                }
            },
            addTypeUpdate(){
                let item = {
                    id: 0,
                    number:this.questions[this.questions.length-1].number+1,
                    question: "",
                    type: 0,
                    delete:false,
                    answers: [
                        {
                            answer: ""
                        }
                    ]
                };
                this.questions.push(item);
                this.selectType(this.questions.length - 1);
            },
            addType() {
                let item = {
                    id: '',
                    number:this.questions[this.questions.length-1].number+1,
                    question: "",
                    type: 0,
                    delete:false,
                    answers: [
                        {
                            answer: ""
                        }
                    ]
                };
                // if (this.questions.length > 20) {
                //     alert("Хамгийн ихдээ 20 асуулт оруулах боломжтой. ");
                // } else {
                this.questions.push(item);
                this.selectType(this.questions.length - 1);
                // this.selectType(this.activeOption - 1);
                // }
            },
            removeType() {
                // if (this.actionType == "update" && this.questions[this.activeOption].id != 'undefined') {
                //     let newArr = {
                //         question: this.questions[this.activeOption].question,
                //         type: this.questions[this.activeOption].type,
                //         delete: true,
                //         answers: [
                //             {
                //                 answer: ""
                //             }
                //         ]
                //     }
                //     this.questions.push(newArr)
                // }
                this.questions.splice(this.activeOption, 1);
                this.selectType(this.questions.length - 1);
            },
            selectType(index) {
                this.activeOption = index;
            },
            popupClose(item) {
                if (item == 'create'){
                    this.createSurvey = true
                    this.data = ''
                    this.title = ''
                    this.rFlag = 'student'
                    this.questions =  [
                        {
                            id: '',
                            number:1,
                            question: "",
                            delete: false,
                            type: 0,
                            answers: [
                                {
                                    answer: ""
                                }
                            ]
                        }
                    ]
                }
                if (!this.surveyModal) {
                    document.body.style.position = 'fixed';
                } else {
                    document.body.style.position = 'relative';
                }
                this.surveyModal = !this.surveyModal;
            },
            popupClass(id, surId){
                if (!this.classModal) {
                    this.attachSurId = id
                    this.surIds = []
                    if (surId.length > 0){
                        for (let i = 0; i < surId.length; i++)
                        {
                            let items = surId[i]
                            this.surIds.push(items.id)
                        }
                    }
                    this.schoolClassList()
                    document.body.style.position = 'fixed';
                } else {
                    document.body.style.position = 'relative';
                }
                this.classModal = !this.classModal;
            },
            loading(cmd) {
                var l = document.getElementById("se-pre-con");
                l.style.display = cmd;
            },
            schoolClassList() {
                try {
                    var _this = this;
                    _this.loading('block')
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    axios.get('${BASEURL}/kexam/useroradmin/classes', {
                            params: {
                                groupName: this.clasgroupNo,
                            },
                            headers: headers
                        }
                    ).then(function (response) {
                        _this.loading('none')
                        for (let i = 0; i < response.data.length; i++) {
                            if(_this.surIds.includes(response.data[i].id)){
                                response.data[i].checked = true
                            }
                            else{
                                response.data[i].checked = false
                            }
                        }
                        _this.classlist = response.data
                    }, function (error) {
                        console.log(error);
                        _this.loading('none')
                    });

                } catch (error) {
                    console.log(error)
                    alert(error)
                }
            },
            async unattachSurvey(classId){
                var language =  "<%=pageContext.getResponse().getLocale()%>"
                var locale = ''
                if (language == 'kr')
                {
                    locale = 'kr-KR'
                }
                if(language == 'mn')
                {
                    locale = 'mn-MN'
                }
                if(language == 'en'){
                    locale = 'en-EN'
                }
                const headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                    'Accept-Language' : locale
                }
                var _this = this
                _this.loading('block')
                await axios.post('${BASEURL}/research/unattach/survey/class', {
                    classId: classId.id,
                    researchId: this.attachSurId
                },{headers: headers}).then((response) => {
                    if (response.data.status === 200) {
                        _this.loading('none')
                        classId.checked = false
                        alert(response.data.message);
                    } else {
                        _this.loading('none')
                        alert("ERROR: " + response.data.message)
                    }
                }, (error) => {
                    _this.loading('none')
                    console.log(error);
                });
            },
            async attachSurvey(classId){
                var language =  "<%=pageContext.getResponse().getLocale()%>"
                var locale = ''
                if (language == 'kr')
                {
                    locale = 'kr-KR'
                }
                if(language == 'mn')
                {
                    locale = 'mn-MN'
                }
                if(language == 'en'){
                    locale = 'en-EN'
                }

                const headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                    'Accept-Language' : locale
                }
                var _this = this
                _this.loading('block')
                await axios.post('${BASEURL}/research/attach/survey/class', {
                    classId: classId.id,
                    researchId: this.attachSurId
                },{headers: headers}).then((response) => {
                    if (response.data.status === 200) {
                        _this.loading('none')
                        classId.checked = true
                        alert(response.data.message);
                    } else {
                        _this.loading('none')
                        alert("ERROR: " + response.data.message)
                    }
                }, (error) => {
                    _this.loading('none')
                    console.log(error);
                });
            },
            loaddata: function () {
                try {
                    var language =  "<%=pageContext.getResponse().getLocale()%>"
                    var locale = ''
                    if (language == 'kr')
                    {
                        locale = 'kr-KR'
                    }
                    if(language == 'mn')
                    {
                        locale = 'mn-MN'
                    }
                    if(language == 'en'){
                        locale = 'en-EN'
                    }
                    const headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                        'Accept-Language' : locale
                    }
                    var _this = this
                    _this.loading('block')
                    axios.get('${BASEURL}/research/list', {
                        params: {},headers: headers
                    }).then(function (response) {
                        _this.loading('none')
                        if (response.status == 200) {
                            _this.surveyList = response.data.content
                            _this.pages = response.data.totalPages;
                            _this.totalElement = response.data.totalElements;
                        } else {
                            alert(response.data.msg);
                        }
                    });
                } catch (error) {
                    this.loading('none')
                    alert(error)
                }
            },
            questionCurrent(item) {
                this.currentPage = item
                this.loaddata(this.currentPage)
            },
            nextQuestion() {
                if (this.pages > this.currentPage) {
                    this.currentPage = this.currentPage + 1
                    this.loaddata(this.currentPage)
                }
            },
            prevQuestion() {
                if (this.currentPage > 1) {
                    this.currentPage = this.currentPage - 1
                    this.loaddata(this.currentPage)
                }
            },
            lastQuestion() {
                this.currentPage = this.pages
                this.loaddata(this.currentPage)
            },
            firstQuestion() {
                this.currentPage = 1
                this.loaddata(this.currentPage)
            },
        }
    });
    app.$mount('#surveyList')
</script>
</body>
</html>

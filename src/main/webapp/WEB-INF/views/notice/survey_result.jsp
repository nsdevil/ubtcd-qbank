<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <title>Survey</title>
        <link rel="stylesheet" href="${CSS}/common/header.css">
        <link rel="stylesheet" href="${CSS}/common/footer.css">
        <link rel="stylesheet" href="${CSS}/notice/list.css">
        <link rel="stylesheet" href="${CSS}/my/show.css">
        <link rel="stylesheet" href="${CSS}/question_registration/register_information.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
        <script src='https://unpkg.com/hchs-vue-charts@1.2.8'></script>
        <style>
            .displaydiv {
                display: block !important;
            }
            article .detail {
                width: 900px;
            }
            article ul li{
                width: 100%;
            }
            article .detail .desc {
                border: none;
            }
            input{
                padding-left: 10px;
            }
            .btn-red-line{
                padding: 0px 5px;
                height: 40px;
            }
            textarea{
                padding-left: 10px;
            }
            h4{
                font-size: 18px;
                margin-right: 10px;
                font-weight: 800;
            }
            .btn-red-line {
                color: #fb5f33;
                background-color: #fff;
                border: 1px solid #fb5f33;
                border-radius: 2px;
                height: 21px;
                display: inline;
            }
            .optionnum{
                padding: 5px 8px;
                border: 1px solid #dee2e6;
                border-radius: 50%;
                width: 30px;
                height: 30px;
            }
            .type-two{
                min-height: 100px;
                overflow: auto;
                max-height: 200px;
                font-size: 16px;
                border: 1px solid #dee2ef;
                padding: 10px;
                margin-top: 30px;
            }
            .chart-pie{
                margin:auto;
                max-width: 300px;
                max-height: 300px;
                width: auto;
                height: auto;
            }
            .chart-bar{
                margin:auto;
                max-width: 500px;
                max-height: 400px;
                min-height: 260px;
                height: auto;
            }
            .btn-gray{
                width: 145px;
                height: 39px;
                font-size: 16px;
                font-weight: 700;
                margin-right: 15px;
            }
            .answer-write{
                padding: 5px;
                border: 1px solid #dee;
                border-radius: 10px;
            }
            #se-pre-con {
                position: fixed;
                -webkit-transition: opacity 5s ease-in-out;
                -moz-transition: opacity 5s ease-in-out;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('${IMG}/Loading_SVG.svg') center no-repeat #ffffff91;
            }
        </style>
    </head>
    <body>
        <div id="se-pre-con" style="display: none"></div>
        <section id="surveyList">
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="../main.jsp"><spring:message code="all.home"/></a></span>
                    <span><a href=""><spring:message code="survey.name"/></a></span>
                    <span><a href="../notice/list"><spring:message code="survey.result"/></a></span>
                </div>

                <h2><spring:message code="survey.result"/></h2>

                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col style="width : 70px;">
                            <col style="width : 200px;">
                            <col>
                            <col style="width : 120px;">
                            <col style="width : 200px;">
                            <col style="width : 200px;">
                        </colgroup>
                        <thead class="border-red-top-2 border-gray-bottom-2">
                            <tr>
                                <th>
                                    <spring:message code="all.number"/>
                                </th>
                                <th>
                                    <spring:message code="survey.title"/>
                                </th>
                                <th>
                                    <spring:message code="survey.content"/>
                                </th>
                                <th>
                                    <spring:message code="survey.regUser"/>
                                </th>
                                <th>
                                    <spring:message code="survey.totalQuestion"/>
                                </th>
                                <th>
                                    <spring:message code="survey.result"/>
                                </th>
                            </tr>
                        </thead>
                        <tbody v-if="surveyList.length > 0">
                            <tr v-for="(item,index) in surveyList" :key="index">
                                <td>{{index+1}}</td>
                                <td>{{item.title}}</td>
                                <td>{{item.memo}}</td>
                                <td>{{item.regUser}}</td>
                                <td>{{item.qCount}}</td>
                                <td>
                                    <button class="btn-red-line" @click="resultSurvey(item.id)" style="margin: 0px;">
<%--                                        <img src="${IMG}/icons/preview/survey.png"--%>
<%--                                             style="width: 27px;">--%>
                                        Харах
                                    </button>
<%--                                    <button class="btn-edit"--%>
<%--                                            style="margin: 0px;">--%>
<%--                                        <img src="${IMG}/icons/menu-bars.png"--%>
<%--                                             style="width: 27px;">--%>
<%--                                    </button>--%>
                                </td>
                            </tr>
                        </tbody>
                        <tbody v-else>
                            <tr>
                                <td colspan="6">
                                    <spring:message code="all.empty"/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <button class="start" @click="firstQuestion()"><img src="${IMG}/icons/start.png" class="auto"></button>
                    <button class="prev" @click="prevQuestion()"><img src="${IMG}/icons/prev.png" class="auto"></button>
                    <ul class="paginate-list">
                        <li class="page-num " v-for="(item,index) in pages " :key="index"
                            :class="{'active': currentPage === item}"
                            v-if="Math.abs(item - currentPage) < 3 || item == pages || item == 1">
                            <a href="javascript:void(0);" @click="questionCurrent(item)" :class="{
                            last: (item == pages && Math.abs(item - currentPage) > 3),
                            first: (item == 1 && Math.abs(item - currentPage) > 3)}">{{item}}</a>
                        </li>
                    </ul>
                    <button class="next" @click="nextQuestion()"><img src="${IMG}/icons/next.png" class="auto"></button>
                    <button class="end" @click="lastQuestion()"><img src="${IMG}/icons/end.png" class="auto"></button>
                </div>
            </div>
<%--            survey update modal --%>
            <div v-show="surveyResultModal" class="modalDialog" :class="{'displaydiv': surveyResultModal}"
                 style="opacity: 1;pointer-events: painted; overflow: scroll;
         background: rgba(0, 0, 0, 0.8); display: none">
                <div class="modal-mask">
                    <div class="modal-wrapper">
                        <div class="modal-container">
                            <a href="javascript:void(0)" @click="popupResults()" title="modelclose" class="modelclose">X</a>
                            <div class="modal-header">
                                <p style="font-weight: 800">
                                    Survey Result
                                </p>
                            </div>
                            <div class="modal-body">
                                <article>
                                    <div class="detail">
                                        <div style="margin-bottom: 30px;">
                                            <h4>Survey name : {{isTitle}}</h4>
                                            <h4>Survey description : {{isContent}}</h4>
                                        </div>
                                        <div>
                                            <ul>
                                                <li v-for="(item,index) in isResult" style="border-bottom: 0px; !important;" :key="index">
                                                    <div v-if="item.type == 1">
                                                       <div class="flex">
                                                           <h4 class="optionnum">{{index+1}}</h4>
                                                           <h4 class="quest">{{item.question}} <span style="color: #fb5f33">(Requests)</span></h4>
                                                       </div>
                                                        <div class="type-two" v-if="item.short_answers.length > 0">
                                                            <div v-for="(ans, ansIndex) in item.short_answers" class="wrap-answer" :key="ansIndex">
                                                                <p class="answer-write" v-if="ans.answers != '[]'">{{ansIndex+1}}. {{ans.answers}} ~ ({{ans.examinee_name}})</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div v-else-if="item.type == 0" >
                                                        <div class="flex">
                                                            <h4 class="optionnum">{{index+1}}</h4>
                                                            <h4 class="quest">{{item.question}} <span style="color: #fb5f33">(One choose)</span></h4>
                                                        </div>
                                                        <div class="flex">
                                                            <div style="width: 60%">
                                                                <chartjs-bar :labels="item.labels" :data="item.dataset" :bind="true"  class="chart-bar"
                                                                             v-if="item.dataset.length > 0"></chartjs-bar>
                                                            </div>
                                                            <div style="width: 40%">
                                                                <table class="mgl15" style="width: 90%; margin: 40px;">
                                                                    <thead>
                                                                        <tr v-for="(one, oneindex) in item.answers" :key="oneindex">
                                                                            <td class="" style="border: 1px solid #dee2e6; margin-top: 10px;">
                                                                                <p>{{oneindex+1}}. {{one.answer}} <span style="color: #fb5f33">({{one.checkedCount}})</span> ({{((one.checkedCount*100)/item.totalChecked).toFixed(1)}}%)</p>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div v-else>
                                                        <div class="flex">
                                                            <h4 class="optionnum">{{index+1}}</h4>
                                                            <h4 class="quest">{{item.question}} <span style="color: #fb5f33">(Multiple choose)</span></h4>
                                                        </div>
                                                        <div class="flex">
                                                            <div style="width: 60%">
                                                                <chartjs-pie :labels="item.labels" :data="item.dataset" :bind="true" class="chart-pie"
                                                                             v-if="item.dataset.length > 0"></chartjs-pie>
                                                            </div>
                                                            <div style="width: 40%">
                                                                <table class="mgl15" style="width: 90%; margin: 40px;">
                                                                    <thead>
                                                                    <tr v-for="(one, oneindex) in item.answers" :key="oneindex">
                                                                        <td class="" style="border: 1px solid #dee2e6;">
                                                                            <p>{{oneindex+1}}. {{one.answer}} <span style="color: #fb5f33">({{one.checkedCount}})</span> ({{((one.checkedCount*100)/item.totalChecked).toFixed(1)}}%)</p></td>
                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clear flex" style="margin-left: 40%; margin-top: 20px;">
                                            <button type="button" @click="popupResults()" class="btn-gray shadow">
                                                <spring:message code="all.close" /></button>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script type="text/javascript">
            Vue.use(VueCharts);
            var app = new Vue({
                data: {
                    surveyList: [],
                    currentPage: 1,
                    pages: 0,
                    itemsPerPage: 1,
                    totalElement: 0,
                    surveyResultModal: false,
                    questions: [
                        {
                            id: '',
                            number:1,
                            question: "",
                            delete: false,
                            type: 0,
                            answers: [
                                {
                                    answer: ""
                                }
                            ]
                        }
                    ],
                    isTitle: '',
                    isContent: '',
                    isResult : [],
                    dataentry: null,
                    datalabel: null,
                    charts:[],
                    data: [],
                    labels: []
                },
                watch: {},
                computed: {
                    paginate: function () {
                        if (!this.surveyList || this.surveyList.length != this.surveyList.length) {
                            return
                        }
                        this.resultCount = this.surveyList.length
                        if (this.currentPage >= this.pages) {
                            this.currentPage = this.pages
                        }
                        var index = this.currentPage * this.itemsPerPage - this.itemsPerPage
                        return this.surveyList.slice(index, index + this.itemsPerPage)
                    }
                },
                filters: {},
                mounted: function () {
                },
                created(){
                    this.loaddata()
                },
                methods: {
                    // addData() {
                    //     this.dataset.push(this.dataentry);
                    //     this.labels.push(this.datalabel);
                    //     this.datalabel = '';
                    //     this.dataentry = '';
                    // },
                    resultSurvey(rId){
                        try {
                            var language =  "<%=pageContext.getResponse().getLocale()%>"
                            var locale = ''
                            if (language == 'kr')
                            {
                                locale = 'kr-KR'
                            }
                            if(language == 'mn')
                            {
                                locale = 'mn-MN'
                            }
                            if(language == 'en'){
                                locale = 'en-EN'
                            }
                            const headers = {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                                'Accept-Language' : locale
                            }
                            var _this = this
                            axios.post('${BASEURL}/research/result/detail',
                                {
                                    researchId: rId
                                },{headers:headers}).then(function (response) {

                                if (response.data.status == 200) {
                                    if (response.data.success == true)
                                    {
                                        _this.isResult = response.data.result.questions
                                        _this.isTitle = response.data.result.surveyTitle
                                        _this.isContent = response.data.result.surveyContent
                                        for (let i = 0; i < response.data.result.questions.length; i++)
                                        {
                                            let ques = response.data.result.questions[i]
                                            ques.labels = []
                                            ques.dataset = []
                                            ques.totalChecked = 0

                                            for(let j = 0; j < ques.answers.length; j++){

                                                let ans = ques.answers[j]

                                                ques.labels.push(ans.answer)
                                                if (ans.checkedCount > 0){
                                                    ques.dataset.push(ans.checkedCount)
                                                    ques.totalChecked = ques.totalChecked + ans.checkedCount
                                                }
                                            }
                                        }
                                        _this.popupResults()
                                    }
                                    else{
                                        alert(response.data.message);
                                    }
                                } else {
                                    alert(response.data.message);
                                }
                            });
                        } catch (error) {
                            console.log(error);
                            alert(error)
                        }
                    },
                    popupResults() {
                        if (!this.surveyResultModal) {
                            document.body.style.position = 'fixed';
                        } else {
                            document.body.style.position = 'relative';
                        }
                        this.surveyResultModal = !this.surveyResultModal;
                    },
                    loading(cmd) {
                        var l = document.getElementById("se-pre-con");
                        l.style.display = cmd;
                    },
                    loaddata: function () {
                        try {
                            var language =  "<%=pageContext.getResponse().getLocale()%>"
                            var locale = ''
                            if (language == 'kr')
                            {
                                locale = 'kr-KR'
                            }
                            if(language == 'mn')
                            {
                                locale = 'mn-MN'
                            }
                            if(language == 'en'){
                                locale = 'en-EN'
                            }
                            const headers = {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + '${sessionScope.S_TOKEN}',
                                'Accept-Language' : locale
                            }
                            var _this = this
                            _this.loading('block')
                            axios.get('${BASEURL}/research/list', {
                                params: {},headers: headers
                            }).then(function (response) {
                                _this.loading('none')
                                if (response.status == 200) {
                                    _this.surveyList = response.data.content
                                    _this.pages = response.data.totalPages;
                                    _this.totalElement = response.data.totalElements;
                                } else {
                                    alert(response.data.msg);
                                }
                            });
                        } catch (error) {
                            _this.loading('none')
                            alert(error)
                        }
                    },
                    questionCurrent(item) {
                        this.currentPage = item
                        this.loaddata(this.currentPage)
                    },
                    nextQuestion() {
                        if (this.pages > this.currentPage) {
                            this.currentPage = this.currentPage + 1
                            this.loaddata(this.currentPage)
                        }
                    },
                    prevQuestion() {
                        if (this.currentPage > 1) {
                            this.currentPage = this.currentPage - 1
                            this.loaddata(this.currentPage)
                        }
                    },
                    lastQuestion() {
                        this.currentPage = this.pages
                        this.loaddata(this.currentPage)
                    },
                    firstQuestion() {
                        this.currentPage = 1
                        this.loaddata(this.currentPage)
                    },
                }
            });
            app.$mount('#surveyList')
        </script>
    </body>
</html>

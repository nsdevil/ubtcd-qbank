<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link href="${CSS}/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../lib/froala_editor/froala_editor.pkgd.min.css">
        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/qna/write.css">
        <script src="../lib/froala_editor/froala_editor.pkgd.min.js"></script>
    </head>
    <body>

        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="../main.jsp">Home</a></span>
                    <span><a href="">MY</a></span>
                    <span><a href="../notice/list.jsp">공지사항</a></span>
                </div>

                <form name="writeForm" class="writeForm" method="get" action="">
                    <div class="title">
                        <select class="select2" name="type" data-placeholder="구분" required>
                            <option value=""></option>
                            <option value="일반공지">일반공지</option>
                            <option value="긴급공지">긴급공지</option>
                            <option value="시스템공지">시스템공지</option>
                        </select>
                        <input type="text" name="title" value="">
                    </div>
                    <div class="content">
                        <div id="froala"></div>

                        <div class="btn-wrap">
                            <button class="btn-white shadow" onclick="location.href='./list.jsp';"><i class="menu-bars"></i>목록으로</button>
                            <div class="flex">
                                <button class="btn-gray shadow" onclick="location.reload();">취소</button>
                                <button class="btn-red shadow"><i class="check-white"></i>등록</button>
                            </div>
                        </div>
                    </div>
                </form>



            </div>
        </section>
    </body>
</html>

<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/notice/list.css">
    </head>
    <body>

        <section class="search">
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="../main.jsp">Home</a></span>
                    <span><a href="">MY</a></span>
                    <span><a href="../notice/list.jsp">공지사항</a></span>
                </div>

                <h2>공지사항</h2>
                <div class="search-wrap flex">
                    <p class="search-result">"<span class="color-red bold">문제은행</span>" 검색결과 (총 <span class="color-red bold">000</span>건)</p>
                    <div class="search-form">
                        <form name="searchForm" method="get" action="">
                            <select class="select2" name="type" data-placeholder="구분">
                                <option value=""></option>
                                <option value="all" selected>전체</option>
                                <option value="notice_type">공지구분</option>
                                <option value="title">제목</option>
                                <option value="date">게시일</option>
                            </select>
                            <input type="text" name="keyword" placeholder="Search">
                            <button><img src="${IMG}/icons/search.png" class="auto"></button>
                        </form>
                    </div>
                </div>

                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col style="width : 70px;">
                            <col style="width : 160px;">
                            <col>
                            <col style="width : 120px;">
                            <col style="width : 110px;">
                            <col style="width : 200px;">
                        </colgroup>
                        <thead class="border-red-top-2 border-gray-bottom-2">
                            <tr>
                                <th>No<i class="up"></i><i class="down"></i></th>
                                <th>구분<i class="up"></i><i class="down"></i></th>
                                <th>제목<i class="up"></i><i class="down"></i></th>
                                <th>조회수<i class="up"></i><i class="down"></i></th>
                                <th>게시자<i class="up"></i><i class="down"></i></th>
                                <th>게시일<i class="up"></i><i class="down"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>10</td>
                                <td>시스템 공지</td>
                                <td>[ 서버 작업 ] <span>문제은행</span> Q-Bank 서비스 이용 불가(08/07 19:00~19:30 30분간)</td>
                                <td>157</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>일반공지</td>
                                <td>[ 안내 ] Q-Bank 신규 메뉴 사용 가이드 <span>문제은행</span></td>
                                <td>100</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>일반공지</td>
                                <td><span>문제은행</span> 자료</td>
                                <td>100</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>일반공지</td>
                                <td><span>문제은행</span> 자료</td>
                                <td>100</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>일반공지</td>
                                <td><span>문제은행</span> 자료</td>
                                <td>100</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>일반공지</td>
                                <td><span>문제은행</span> 자료</td>
                                <td>100</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>일반공지</td>
                                <td><span>문제은행</span> 자료</td>
                                <td>100</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>일반공지</td>
                                <td><span>문제은행</span> 자료</td>
                                <td>100</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>일반공지</td>
                                <td><span>문제은행</span> 자료</td>
                                <td>100</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>일반공지</td>
                                <td><span>문제은행</span> 자료</td>
                                <td>100</td>
                                <td>관리자</td>
                                <td>2018-10-29 00:00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <button class="start"><img src="${IMG}/icons/start.png" class="auto"></button>
                    <button class="prev"><img src="${IMG}/icons/prev.png" class="auto"></button>
                    <ul class="paginate-list">
                        <li class="page-num active"><a href="#">1</a></li>
                        <li class="page-num"><a href="#">2</a></li>
                        <li class="page-num"><a href="#">3</a></li>
                        <li class="page-num"><a href="#">4</a></li>
                        <li class="page-num"><a href="#">5</a></li>
                        <li class="page-num"><a href="#">6</a></li>
                        <li class="page-num"><a href="#">7</a></li>
                        <li class="page-num"><a href="#">8</a></li>
                        <li class="page-num"><a href="#">9</a></li>
                        <li class="page-num"><a href="#">10</a></li>
                    </ul>
                    <button class="next"><img src="${IMG}/icons/next.png" class="auto"></button>
                    <button class="end"><img src="${IMG}/icons/end.png" class="auto"></button>
                </div>

            </div>
        </section>
    </body>
</html>
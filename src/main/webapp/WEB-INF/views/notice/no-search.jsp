<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ko">
    <head>

        <link rel="stylesheet" href="   ${CSS}/common/header.css">
        <link rel="stylesheet" href="   ${CSS}/common/footer.css">
        <link rel="stylesheet" href="   ${CSS}/notice/no-search.css">
    </head>
    <body>

        <section>
            <div class="wrap bg-white pd-50 radius-5 shadow-wrap">
                <div class="breadcrumb">
                    <span><a href="../main.jsp">Home</a></span>
                    <span><a href="">MY</a></span>
                    <span><a href="../notice/list.jsp">공지사항</a></span>
                </div>

<%--                <h2>공지사항</h2>--%>
<%--                --%>
<%--                <div class="search-wrap flex">--%>
<%--                    <p class="search-result">"<span class="color-red bold">문제은행</span>" 검색결과 (총 <span class="color-red bold">000</span>건)</p>--%>
<%--                    <div class="search-form">--%>
<%--                        <form name="searchForm" method="get" action="">--%>
<%--                            <select class="select2" name="type" data-placeholder="구분">--%>
<%--                                <option value=""></option>--%>
<%--                                <option value="all" selected>전체</option>--%>
<%--                                <option value="notice_type">공지구분</option>--%>
<%--                                <option value="title">제목</option>--%>
<%--                                <option value="date">게시일</option>--%>
<%--                            </select>--%>
<%--                            <input type="text" name="keyword" placeholder="Search">--%>
<%--                            <button><img src="${IMG}/icons/search.png" class="auto"></button>--%>
<%--                        </form>--%>
<%--                    </div>--%>
<%--                </div>--%>

                <div class="table-wrap">
                    <div class="image"><img src="${IMG}/icons/robot.png" class="auto"></div>
                    <p class="no-search-result">검색결과가 없습니다.</p>
                    <p class="reserarch">다른 검색어로 다시 검색해보세요.</p>
                    <button class="btn-white shadow">공지사항으로</button>
                </div>

            </div>
        </section>
    </body>
</html>

<%@ page contentType = "text/html;charset=utf-8" %>
<header>
    <div class="wrap top">
        <div class="logo">
            <a href="${JSP}/main.jsp"><img src="${IMG}/header/nslogo.png" class="auto"></a>
        </div>
<%--    exam.edu.mn todo check --%>
<%--    <div class="flex" style="align-items: center;">--%>
<%--        <div class="logo"><img src="${IMG}/header/esis1.png" class="auto"></div>--%>
<%--        <div class="logo" style="margin-left: 50px;"><img src="${IMG}/header/edu1.png" class="auto"></div>--%>
<%--    </div>--%>
        <div class="member-info">
            <div class="image"><img src="${IMG}/header/profile.png" class="auto"></div>
            <div class="name">Teacher</div>
            <div class="question"><a href=""><img src="${IMG}/icons/question.png" class="auto"></a></div>
            <div class="logout"><a href="${HOME}/login.jsp"><img src="${IMG}/icons/logout.png" class="auto"></a></div>
        </div>
    </div>
<%--    <nav>--%>
<%--        <div class="wrap flex align-items-center">--%>
<%--            <div class="menu flex">--%>
<%--                <a href="">--%>
<%--                    <span></span>--%>
<%--                    <span></span>--%>
<%--                    <span></span>--%>
<%--                </a>--%>
<%--            </div>--%>
<%--            <ul class="flex menu-list">--%>
<%--<!--                 <li> -->--%>
<%--<!--                     <a href="" onclick="sub_show(this,event);">MY</a> -->--%>
<%--<!--                     <div class="flex align-items-center submenu"> -->--%>
<%--<!--                         <ul class="wrap flex"> -->--%>
<%--<!--                             <li><a href="${HOME}/notice/list.jsp">공지사항</a></li> -->--%>
<%--<!--                             <li><a href="${HOME}/qna/list.jsp">1:1문의</a></li> -->--%>
<%--<!--                             <li><a href="${HOME}/my/show.jsp">내 정보 관리</a></li> -->--%>
<%--<!--                             <li><a href="${HOME}/my/pc_certification.jsp">PC인증 관리</a></li> -->--%>
<%--<!--                         </ul> -->--%>
<%--<!--                     </div> -->--%>
<%--<!--                 </li> -->--%>
<%--                <li>--%>
<%--                    <a href="" onclick="sub_show(this,event);">문항카드</a>--%>
<%--                    <div class="flex align-items-center submenu">--%>
<%--                        <ul class="wrap flex">--%>
<%--                            <li>--%>
<%--                                <a href="javascript:void(0);" onclick="location='${HOME}/question_card_default_card_list'">기관별 문항카드</a>--%>
<%--                            </li>--%>
<%--                            <li>--%>
<%--                               <a href="question_card_default_card_list">기관별 문항카드</a>--%>
<%--                            </li>--%>
<%--                            <li>--%>
<%--                                <a href="${HOME}/question_card/personal/main_card_list.jsp">개인별 문항카드</a>--%>
<%--                            </li>--%>
<%--                        </ul>--%>
<%--                    </div>--%>
<%--                </li>--%>
<%--                <li>--%>
<%--                    <a href="" onclick="sub_show(this,event);">문항등록</a>--%>
<%--                    <div class="flex align-items-center submenu">--%>
<%--                        <ul class="wrap flex">--%>
<%--                            <li><a href="${HOME}/question_registration/each/essential.jsp">개별등록</a></li>--%>
<%--                            <!-- <li><a href="">일괄등록</a></li> -->--%>
<%--                            <li><a href="${HOME}/question_registration/each/form_regist.jsp">양식등록</a></li>--%>
<%--                            <li><a href="${HOME}/question_registration/each/temporary.jsp">임시저장 목록</a></li>--%>
<%--                        </ul>--%>
<%--                    </div>--%>
<%--                </li>--%>
<%--<!--                 <li> -->--%>
<%--<!--                     <a href="" onclick="sub_show(this,event);">문항뱅크</a> -->--%>
<%--<!--                     <div class="flex align-items-center submenu"> -->--%>
<%--<!--                         <ul class="wrap flex"> -->--%>
<%--<!--                             <li><a href="">시험지</a></li> -->--%>
<%--<!--                             <li><a href="">교재</a></li> -->--%>
<%--<!--                             <li><a href="">문제</a></li> -->--%>
<%--<!--                             <li><a href="">학습자료</a></li> -->--%>
<%--<!--                             <li><a href="">소스</a></li> -->--%>
<%--<!--                         </ul> -->--%>
<%--<!--                     </div> -->--%>
<%--<!--                 </li> -->--%>
<%--                <li>--%>
<%--                    <a href="" onclick="sub_show(this,event);">멀티미디어자료</a>--%>
<%--                    <div class="flex align-items-center submenu">--%>
<%--                        <ul class="wrap flex">--%>
<%--                            <li><a href="${HOME}/multimedia/multimedia_card_search.jsp">자료검색</a></li>--%>
<%--                            <li><a href="${HOME}/multimedia/essential.jsp">자료등록</a></li>--%>
<%--                        </ul>--%>
<%--                    </div>--%>
<%--                </li>--%>
<%--                <li><a href=" ${HOME}/question_card/questionbank">문제은행</a></li>--%>
<%--                <li>--%>
<%--                    <a href="" onclick="sub_show(this,event);">평가(시험지)</a>--%>
<%--<!--                     <div class="flex align-items-center submenu"> -->--%>
<%--<!--                         <ul class="wrap flex"> -->--%>
<%--<!--                             <li><a href="">시험지</a></li> -->--%>
<%--<!--                             <li><a href="">교재</a></li> -->--%>
<%--<!--                             <li><a href="">문제</a></li> -->--%>
<%--<!--                             <li><a href="">학습자료</a></li> -->--%>
<%--<!--                             <li><a href="">소스</a></li> -->--%>
<%--<!--                         </ul>-->--%>
<%--<!--                     </div> -->--%>
<%--                </li>--%>
<%--                <!-- <li>--%>
<%--                    <a href="" onclick="sub_show(this,event);">요청</a>--%>
<%--                    <div class="flex align-items-center submenu">--%>
<%--                        <ul class="wrap flex">--%>
<%--                            <li><a href="">진행 중 내역</a></li>--%>
<%--                            <li><a href="">완료 내역</a></li>--%>
<%--                        </ul>--%>
<%--                    </div>--%>
<%--                </li>--%>
<%--                <li>--%>
<%--                    <a href="" onclick="sub_show(this,event);">대시보드</a>--%>
<%--                </li>--%>
<%--                <li>--%>
<%--                    <a href="" onclick="sub_show(this,event);">환경설정</a>--%>
<%--                </li> -->--%>
<%--            </ul>--%>
<%--        </div>--%>
<%--    </nav>--%>
    <div class="submenu-bg"></div>
</header>

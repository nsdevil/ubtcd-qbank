<%--<%@ page contentType = "text/html;charset=utf-8" %>--%>
<title>Nsdevil</title>
<meta charset="UTF-8">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta property="og:image" content="">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="628">
<meta property="og:type" content="website">
<meta name="theme-color" content="#">
<meta name="msapplication-navbutton-color" content="rgb(251, 95, 51)">
<meta name="apple-mobile-wep-app-status-bar-style" content="rgb(251, 95, 51)">
<link rel="stylesheet" href="${CSS}/lib/select2/select2.min.css">
<link rel="stylesheet" href="${CSS}/question_card/popup.css">
<link rel="stylesheet" href="${CSS}/style.css">
<%--<link rel="stylesheet" href="${CSS}/common/font.css">--%>
<%-- exam.edu.mn todo check --%>
<link rel="icon" href="${IMG}/icon.png">
<script src="${JS}/jquery3.3.1.js"></script>
<%--<script src="${JS}/lib/select2/select2.min.js"></script>--%>
<script src="${JS}/script.js"></script>
<script src="${JS}/vue/vue.js"></script>
<script src="${JS}/vue/axios.js"></script>
<script src="${JS}/blockUI.js" type="text/javascript"></script>
<script src="${JS}/polyfill.min.js"></script>


<%--<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/fmt" %>--%>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<footer>
    <div class="wrap flex">
        <div class="info">
            <h1>Nsdevil consulting</h1>
            <ul class="flex">
                <li>
                    <span class="foot_label"><spring:message code="all.postCode"/></span>
                    <span class="foot_info"><spring:message code="all.address"/></span></li>
                <li><span class="foot_label">고객센터</span> <span class="foot_info">1544-0079 월-금 9:00 – 18:00 (공휴일 제외)</span></li>
<%--                <li><span class="foot_label"><spring:message code="all.contact"/></span>--%>
<%--                    <span class="foot_info">+976 77040454</span></li>--%>
            </ul>
    <%--            exam.edu.mn todo check --%>
            <p>COPYRIGHT&copy;Nsdevil consulting. ALL RIGHTS RESERVED.</p>
<%--            <p>COPYRIGHT&copy;БМТТ. ALL RIGHTS RESERVED.</p>--%>
        </div>
        <div class="logo">
            <img src="${IMG}/icons/footer/nslogo.png" class="auto">
        </div>
<%--        exam.edu.mn todo check --%>
<%--        <div class="flex" style="align-items: center;">--%>
<%--            <div class="logo"><img src="${IMG}/header/esis1.png" class="auto"></div>--%>
<%--            <div class="logo" style="margin-left: 50px;"><img src="${IMG}/header/edu1.png" class="auto"></div>--%>
<%--        </div>--%>
    </div>
</footer>

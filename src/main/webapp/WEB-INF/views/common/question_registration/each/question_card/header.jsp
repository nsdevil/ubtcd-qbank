<%@ page contentType = "text/html;charset=utf-8" %>
<div class="left">
    <p>문항 검색 및 분류가 편리하도록 필요한 정보를 선택하여 입력해주세요.</p>
    <ul>
        <li class="<%=(page_name=="test_subject1")?"on":""%>">
            <a href="${HOME}/question_registration/each/question_card/test_subject1.jsp"><span class="red">필수</span>시험과목</a>
        </li>
        <li class="<%=(page_name=="curriculum")?"on":""%>">
            <a href="${HOME}/question_registration/each/question_card/curriculum.jsp"><span class="gray">권장</span>교육과정</a>
        </li>
        <li class="<%=(page_name=="learning_result")?"on":""%>">
            <a href="${HOME}/question_registration/each/question_card/learning_result.jsp"><span class="red">필수</span>성취기준</a>
        </li>
        <li class="<%=(page_name=="knowledge_level")?"on":""%>">
            <a href="${HOME}/question_registration/each/question_card/knowledge_level.jsp"><span class="gray">권장</span>지식수준</a>
        </li>
        <li class="<%=(page_name=="expected_difficulty")?"on":""%>">
            <a href="${HOME}/question_registration/each/question_card/expected_difficulty.jsp"><span class="gray">권장</span>예상난이도</a>
        </li>
        <li class="<%=(page_name=="question_relevance")?"on":""%>">
            <a href="${HOME}/question_registration/each/question_card/question_relevance.jsp"><span class="gray">권장</span>문항의 적절성</a>
        </li>
<!--         <li class="<%=(page_name=="clinical_technique")?"on":""%>"> -->
<!--             <a href="${HOME}/question_registration/each/question_card/clinical_technique.jsp"><span class="gray">권장</span>임상술기</a> -->
<!--         </li> -->
<!--         <li class="<%=(page_name=="clinical_expression")?"on":""%>"> -->
<!--             <a href="${HOME}/question_registration/each/question_card/clinical_expression.jsp"><span class="gray">권장</span>임상표현명</a> -->
<!--         </li> -->
<!--         <li class="<%=(page_name=="diagnosis_name")?"on":""%>"> -->
<!--             <a href="${HOME}/question_registration/each/question_card/diagnosis_name.jsp"><span class="gray">권장</span>진단명</a> -->
<!--         </li> -->
<!--         <li class="<%=(page_name=="health_care_term")?"on":""%>"> -->
<!--             <a href="${HOME}/question_registration/each/question_card/health_care_term.jsp"><span class="gray">권장</span>보건용어</a> -->
<!--         </li> -->
<!--         <li class="<%=(page_name=="reference")?"on":""%>"> -->
<!--             <a href="${HOME}/question_registration/each/question_card/reference.jsp"><span class="gray">권장</span>참고문헌</a> -->
<!--         </li> -->
<!--         <li class="<%=(page_name=="etc")?"on":""%>"> -->
<!--             <a href="${HOME}/question_registration/each/question_card/etc.jsp"><span class="gray">권장</span>수술 및 기타 수기</a> -->
<!--         </li> -->
        <li class="<%=(page_name=="subject")?"on":""%>">
            <a href="${HOME}/question_registration/each/question_card/subject.jsp">
                <span class="gray">권장</span>주제어 (자유 키워드)</a>
        </li>
        <li class="<%=(page_name=="individual_question_card")?"on":""%>">
            <a href="${HOME}/question_registration/each/question_card/Individual_question_card.jsp">
                <span class="gray">권장</span>개인별 문항카드</a>
        </li>
    </ul>
    <button class="btn-white shadow" onclick="location='${HOME}/question_registration/each/making/multiple';">
        문항카드 등록 SKIP</button>
</div>